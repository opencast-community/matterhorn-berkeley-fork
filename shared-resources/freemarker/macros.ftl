<#--
Functions
-->
<#function hasGlobalErrors >
    <#return errors?? && errors.globalErrors?? && (errors.globalErrors?size > 0)/>
</#function>

<#function hasFieldErrors fieldName>
    <#return errors?? && errors.hasFieldErrors(fieldName)/>
</#function>

<#function isAdminUser>
    <#return user?? && user.roles?? && (user.roles?seq_contains("ROLE_ADMIN")) />
</#function>

<#--
Macros
-->
<#macro message key args...>${messageSource.getProperty(key, args)}</#macro>

<#macro showTimeOfDay timeOfDay><#compress>
    <#assign minutes><#if (timeOfDay.minutes < 10)>0</#if>${timeOfDay.minutes}</#assign>
    <#assign hours><#if (timeOfDay.hour = 0)>12<#else>${timeOfDay.hour}</#if></#assign>
    ${hours}:${minutes}${timeOfDay.dayPeriod?lower_case}
</#compress></#macro>


<#macro showGlobalErrors ><#compress>
    <#if hasGlobalErrors()>
        <#list errors.globalErrors as globalError>
            ${messageSource.getErrorMessage(globalError)}
            <#if globalError_has_next><br/></#if>
        </#list>
    </#if>
</#compress></#macro>

<#macro showFieldErrors fieldName >
    <#if hasFieldErrors(fieldName)>
        <#assign fieldErrors = errors.getFieldErrors(fieldName) />
        <#list fieldErrors as fieldError>
            <div class="warning">${messageSource.getErrorMessage(fieldError)}</div>
            <#if fieldError_has_next><br/></#if>
        </#list>
    </#if>
</#macro>
