#!/bin/bash
# ==================================================================================== #
#  DESCRIPTION: Initialize ALL databases on which Matterhorn depends
#
# AUTHOR:        John Crossman, Fernando Alvarez
#
# REQUIREMENTS: postgres
#               $MATTERHORN_HOME/docs/scripts/ddl/postgres84.sql
#
# LICENSE:      Copyright 2009, 2010 The Regents of the University of California
#               Licensed under the Educational Community License, Version 2.0
#               (the "License"); you may not use this file except in compliance
#               with the License. You may obtain a copy of the License at
#
#               http://www.osedu.org/licenses/ECL-2.0
#
#               Unless required by applicable law or agreed to in writing,
#               software distributed under the License is distributed on an "AS IS"
#               BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
#               or implied. See the License for the specific language governing
#               permissions and limitations under the License.
# ==================================================================================== #

${MATTERHORN_HOME}/bin/install-matterhorn-database.sh \
  || { echo "FAILED to install 'matterhorn' database" ; exit 1; }

${MATTERHORN_HOME}/bin/install-warehouse-database.sh \
  || { echo "FAILED to install 'webcast-warehouse' database" ; exit 1; }

exit $?
