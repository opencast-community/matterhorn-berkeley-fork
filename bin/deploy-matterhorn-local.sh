#!/bin/bash

##
# Deploy all-in-one Matterhorn to 'local' environment
##

mhruntimeConfig="/opt/matterhorn/.mhruntime.cf"

if [ ! -f  "${mhruntimeConfig}" ]; then
    echo; echo
    echo "[ERROR] Expected file not found: ${mhruntimeConfig}"
    echo; echo
    exit 1
fi

expectedEnvironment="matterhorn.environment=local"
lineCount=$(grep "${expectedEnvironment}" ${mhruntimeConfig} | wc -l)


if (( ${lineCount} != 1 ));  then
    echo; echo
    echo "[ERROR] File ${mhruntimeConfig} has unexpected or missing value."
    echo "              Expected: ${expectedEnvironment}"
    echo; echo
    exit 1
fi

# Clean
rm -Rf ${MATTERHORN_HOME}/work/felix-cache/*
rm -Rf ${MATTERHORN_HOME}/lib/ext/*
rm -Rf ${MATTERHORN_HOME}/lib/matterhorn/*

# Build all-in-one
cd "${MATTERHORN_HOME}"

mvn -Dcheckstyle.skip=true -Dmaven.test.skip=true -DskipTests clean install \
    -DdeployTo="${MATTERHORN_HOME}" || { echo 'Maven all-in-one build FAILED' ; exit 1; }

mvn -Dcheckstyle.skip=true -Dmaven.test.skip=true -DskipTests install \
    -Pdirectory-ldap,directory-cas,directory-openid \
    -DdeployTo="${MATTERHORN_HOME}" || { echo 'Maven LDAP/CAS build FAILED' ; exit 1; }

exit 0
