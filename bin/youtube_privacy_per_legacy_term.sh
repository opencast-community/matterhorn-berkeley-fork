#!/bin/bash

# Fail the entire script when one of the commands in it fails
set -e

echo_usage() {
  echo "SYNOPSIS"
  echo "     ${0} -t edoTermId -l limit [-r]"; echo
  echo "DESCRIPTION"
  echo "The following options are available. Read the complete guide at https://goo.gl/RvJ6s9"; echo
  echo "     -t      Term expressed in EDO format. For example, Spring 2014 is '2142'."; echo
  echo "     -l      Max number of YouTube videos to process"; echo
  echo "     -r      Retry YouTube videos which our Selenium script previously failed to update"; echo
  echo "     The environment variable FELIX_HOME must equal the base dir of your Matterhorn deployment."; echo
  echo "EXAMPLES"; echo
  echo "     # Process 30 YouTube videos from Fall 2015 (retryPastFailures is false, by default)"
  echo "          ${0} -t 2158 -l 30"; echo
  echo "     # In this case, retryPastFailures is true"; echo
  echo "          ${0} -t 2158 -l 30 -r"; echo
}

[[ "${FELIX_HOME}" ]] || { echo; echo "[ERROR] 'FELIX_HOME' is undefined"; echo_usage; exit 1; }

[[ $# -gt 1 ]] || { echo_usage; exit 1; }

retryPastFailures=false

while getopts "l:rt:" arg; do
  case ${arg} in
    l)
      limit="${OPTARG}"
      ;;
    r)
      retryPastFailures=true
      ;;
    t)
      edo_term_id="${OPTARG}"
      ;;
  esac
done

cd "${FELIX_HOME}"

for jarFile in $(ls ${FELIX_HOME}/lib/ext/*.jar); do cp="${cp}:${jarFile}"; done

for jarFile in $(ls ${FELIX_HOME}/lib/matterhorn/*.jar); do cp="${cp}:${jarFile}"; done

export CLASSPATH="${cp}"

java org.opencastproject.warehouse.selenium.YouTubePrivacyPerLegacyTerm "${edo_term_id}" "${limit}" "${retryPastFailures}"

echo; echo "Done"; echo

exit 0
