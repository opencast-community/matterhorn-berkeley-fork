#!/bin/bash

##
# Run by Bamboo in order to build artifacts per deployment needs.
#
# Run this script in Bamboo with the following command line args, in order:
#      ${bamboo.build.working.directory}
#      ${bamboo.repository.revision.number}
#      ${bamboo.buildNumber}
##

# Usage
if [ $# != 4 ]; then
    echo '
    Usage:
    '
    echo "        $0 matterhornHome paellaHome revisionNumber buildNumber"
    echo '
    This script will build Admin, Worker and Engage artifacts that are suited for deployment.
    '
    exit 0;
fi

# Args
matterhornHome=$1
paellaHome=$2
revision=$3
buildNumber=$4

echo "-----------------------------------"
echo "Build parameters:"
echo "    matterhornHome: ${matterhornHome}"
echo "    paellaHome: ${paellaHome}"
echo "    revision: ${revision}"
echo "    buildNumber: ${buildNumber}"
echo "-----------------------------------"

# Checkout Paella (checkout 'paellaengage' in parent directory)
# Export the variable so Maven can pick it up in pom.xml
export MATTERHORN_HOME=${matterhornHome}
export PAELLA_HOME=${paellaHome}

function cleanUpMatterhornLib() {
    # Remove jars, etc.
    cd ${matterhornHome}
    rm -Rf ${matterhornHome}/lib/matterhorn
}

# Create file with build info so Matterhorn can display on UI
${matterhornHome}/bin/record-build-information.sh "${matterhornHome}" "${revision}" "${buildNumber}"

# Clean
cleanUpMatterhornLib

# Admin
cd ${matterhornHome}
mvn clean test install \
    -Dcheckstyle.skip=true \
    -Dmaven.test.skip=true \
    -DskipTests=true \
    -Padmin,dist-stub,engage-stub,worker-stub,workspace,serviceregistry,directory-ldap,directory-cas,directory-openid,participation-service \
    -DdeployTo=${matterhornHome} || { echo 'Maven Admin build failed' ; cleanUpMatterhornLib; exit 1; }

# (1) Wipe away previous Zip archives and (2) create new artifacts
rm -f *SNAPSHOT.zip
zip -r gp-1.4.${buildNumber}-admin-SNAPSHOT.zip * -x gp-1.4.*.zip \*/target/\* modules\* src\* logs\* work\*

# Clean
cleanUpMatterhornLib

## Engage
#cd ${matterhornHome}
#mvn clean test install \
#    -Dcheckstyle.skip=true \
#    -Dmaven.test.skip=true \
#    -DskipTests=true \
#    -Pengage-standalone,paella-engage-ui-bamboo,dist-using-series-service-remote,serviceregistry,workspace,directory-ldap,directory-cas,directory-openid,participation-service \
#    -DdeployTo=${matterhornHome} || { echo 'Maven Engage build failed' ; cleanUpMatterhornLib; exit 1; }
#
## Create artifact
#zip -r gp-1.4.${buildNumber}-engage-SNAPSHOT.zip * -x gp-1.4.*.zip \*/target/\* modules\* src\* logs\* work\*
#
## Clean
#cleanUpMatterhornLib

# Worker
cd ${matterhornHome}
mvn clean test install \
    -Dcheckstyle.skip=true \
    -Dmaven.test.skip=true \
    -DskipTests=true \
    -Pworker-standalone,serviceregistry,workspace,dist-using-series-service-remote,directory-ldap,directory-cas,directory-openid,participation-service \
    -DdeployTo=${matterhornHome} || { echo 'Maven Worker build failed' ; cleanUpMatterhornLib; exit 1; }

# Create artifact
zip -r gp-1.4.${buildNumber}-worker-SNAPSHOT.zip * -x  gp-1.4.*.zip \*/target/\* modules\* src\* logs\* work\*

# Clean
cleanUpMatterhornLib

exit 0
