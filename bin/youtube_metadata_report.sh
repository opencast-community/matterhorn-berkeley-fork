#!/bin/bash

# Fail the entire script when one of the commands in it fails
set -e

echo_usage() {
  echo "SYNOPSIS"
  echo "     ${0} -t edoTermId"; echo
  echo "DESCRIPTION"
  echo "The following options are available. Read the complete guide at https://goo.gl/RvJ6s9"; echo
  echo "     -t      Term expressed in EDO format. For example, Spring 2014 is '2142'."; echo
  echo "     The environment variable FELIX_HOME must equal the base dir of your Matterhorn deployment."; echo
  echo "EXAMPLES"; echo
  echo "     # Generate spreadsheet with YouTube metadata per term"
  echo "          ${0} -t 2158"; echo
}

[[ "${FELIX_HOME}" ]] || { echo; echo "[ERROR] 'FELIX_HOME' is undefined"; echo_usage; exit 1; }

[[ $# -gt 1 ]] || { echo_usage; exit 1; }

retryPastFailures=false

while getopts "t:" arg; do
  case ${arg} in
    t)
      edo_term_id="${OPTARG}"
      ;;
  esac
done

cd "${FELIX_HOME}"

for jarFile in $(ls ${FELIX_HOME}/lib/ext/*.jar); do cp="${cp}:${jarFile}"; done

for jarFile in $(ls ${FELIX_HOME}/lib/matterhorn/*.jar); do cp="${cp}:${jarFile}"; done

export CLASSPATH="${cp}"

filePath="${FELIX_HOME}/youtube_metadata_report_${edo_term_id}.csv"

java org.opencastproject.warehouse.YouTubeMetadataReport "${edo_term_id}" "${filePath}"

echo

exit 0
