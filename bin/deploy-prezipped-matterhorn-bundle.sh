#!/bin/bash

##
# Deploy Matterhorn admin, engage or worker build from Bamboo. See 'usage' below
# for more information.
##

# Prep
configFile="/opt/matterhorn/.mhruntime.cf";
howToGetHelp="Run '$0 --help' for more information."
thereWasProblem="There was a problem executing ${0}"

zipFile=$1

echo_usage() {
  echo;
  echo "USAGE";
  echo "    ${0} [Path to ZIP file]"
  echo
}

if [ "$1"  == "--help" ] || [ "$1"  == "-h" ]  || [ "$#" -gt "1" ] ; then
    echo_usage;
    exit 0;
fi

MATTERHORN_PID=$(ps aux | awk '/felix.jar/ && !/awk/ {print $2}')

if [ ! -z "$MATTERHORN_PID" ]; then
    echo;
    echo "You must shutdown Matterhorn (currently running with PID '$MATTERHORN_PID') in order to deploy a new build."; echo
    exit 0;
fi

filename=$(basename ${zipFile})
filename="${filename%.*}"
deploymentDir="/opt/matterhorn/${filename}"

# Backup directory
dateString=$(date +"%Y-%m-%d")

# Create deployment directory
rm -Rf "${deploymentDir}"
mkdir -p "${deploymentDir}"

# Unzip
echo; echo "Quietly unzipping ${zipFile}..."
unzip -q -d "${deploymentDir}" ${zipFile}

# FELIX_HOME: the matterhornHome created by deployment
matterhornHome="/opt/matterhorn/trunk"
if [ -a "${matterhornHome}" ] ; then
    rm -f "${matterhornHome}"
fi

ln -s "${deploymentDir}" "${matterhornHome}"
echo; echo "Created matterhornHome such that:"
echo "    ${matterhornHome}  -->"
echo "        ${deploymentDir}"
echo; echo

if [ -f ${matterhornHome}/bin/prepare_runtime_properties.sh ]
then
    export FELIX_HOME="${matterhornHome}"
    ${matterhornHome}/bin/prepare_runtime_properties.sh || { echo 'Prepare of runtime properties failed' ; exit 1; }
elif [ -a "/opt/matterhorn/opencast-config.properties" ]; then

    echo; echo "Overwrite existing config.properties with env-specific /opt/matterhorn/opencast-config.properties"
    cp "/opt/matterhorn/opencast-config.properties" ${matterhornHome}/etc/config.properties
fi

echo; echo "Done"; echo
