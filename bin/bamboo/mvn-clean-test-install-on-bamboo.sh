#!/bin/bash

##
# Run by Bamboo in order to run integration tests.
##

# Usage
if [ $# != 1 ]
then
    scriptFilename="${0##*/}"
    echo; echo 'USAGE:'; echo
    echo "    ${scriptFilename} \${matterhornHome}"; echo
    exit 0;
fi

# Args
matterhornHome="$1"

# Export the variable so Maven can pick it up in pom.xml
export MATTERHORN_HOME=${matterhornHome}

cd ${matterhornHome}

echo "-----------------------------------"
echo "Build parameters:"
echo "    matterhornHome: ${matterhornHome}"
echo "-----------------------------------"

# Build
mvn clean test install || { echo 'Build failed' ; exit 1; }

exit 0
