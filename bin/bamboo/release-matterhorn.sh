#!/bin/bash

# Usage
if [ $# != 1 ]
then
    scriptFilename="${0##*/}"
    echo; echo 'USAGE:'; echo
    echo "    ${scriptFilename} \${version}"
    echo; echo 'This script will update version in pom.xml files.'; echo
    exit 0;
fi

# Verify no SNAPSHOT in version
pomChangeCount=$(git status | grep pom.xml);
if [[ ${#pomChangeCount} > 0 ]];
then
    echo;
    echo '[ERROR] You have uncommitted changes to one or more pom.xml files.'
    echo '        Commit or revert those changes then retry.'; echo
    exit 1;
fi

# Verify no SNAPSHOT in version
snapshotCount=$(echo $1 | grep -i snapshot);
if [[ ${#snapshotCount} > 0 ]];
then
    echo;
    echo '[ERROR] Do NOT include "SNAPSHOT" in your version.'
    echo '        This script will automatically append when appropriate'; echo
    exit 1;
fi

masterBranchMarker=$(grep \/master$ .git/HEAD | wc -l)
if (( ${masterBranchMarker} == 0 ))
then
    mvn release:update-versions -DdevelopmentVersion="$1"
else
    mvn versions:set -DnewVersion="$1"
fi

if [ $? != 0 ]
then
    echo; echo '[FAIL] Maven failed on release:update-versions'; echo
    exit 1;
fi

echo;
echo '[SUCCESS] Maven has updated version in all pom.xml files'; echo
echo '          Next, push changes to Git repository:'; echo; echo

exit 0
