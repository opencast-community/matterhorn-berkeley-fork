#!/bin/bash

##
# Bamboo build server will run this script in order to inject version and build
# information into the specified file(s).
##

# Usage
if [ $# != 4 ]
then
    scriptFilename="${0##*/}"
    echo; echo 'USAGE:'; echo
    echo "    ${scriptFilename} \${matterhornHome} \${bambooBuild} \${pomVersion} \${gitRevision}"
    echo; echo 'This script will write files under lib/build-information containing version info, etc.'; echo
    exit 0;
fi

matterhornHome=$1
bambooBuild=$2
pomVersion=$3
gitRevision=$4

if [ -z "$matterhornHome" ]; then
     echo "[ERROR] Missing script argument: matterhornHome"
     exit 1
fi

# Make dir
directoryPath=${matterhornHome}/lib/matterhorn/build-information
rm -Rf ${directoryPath}
mkdir -p ${directoryPath}

# Write files
echo "${bambooBuild}" > "${directoryPath}/bambooBuild"
echo "1.4.53-SNAPSHOT" > "${directoryPath}/pomVersion"
echo "${gitRevision}" > "${directoryPath}/gitRevision"

exit 0
