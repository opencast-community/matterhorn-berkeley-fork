#!/bin/bash

##
# Run by Bamboo in order to build artifacts per deployment needs. Instructions are below.
##

# Usage
if [ $# != 4 ]
then
  scriptFilename="${0##*/}"
  echo; echo 'USAGE:'; echo
  echo "  ${scriptFilename} \${matterhornHome} \${buildKey} \${buildNumber} \${gitRevision}"
  echo; echo 'This script will build Admin, Worker and Engage artifacts that are suited for deployment.'; echo
  exit 0;
fi

# Args
matterhornHome="$1"
buildKey="$2"
buildNumber="$3"
gitRevision="$4"
zipSuffix="${buildKey}-${buildNumber}"

# Export the variable so Maven can pick it up in pom.xml
export MATTERHORN_HOME=${matterhornHome}

cd ${matterhornHome}

# Grab pom versuion
pomVersion=$(mvn org.apache.maven.plugins:maven-help-plugin:2.1.1:evaluate -Dexpression=project.version  | grep ^1\.)
# masterBranchMarker=$(grep \/master$ .git/HEAD | wc -l)
# if (( ${masterBranchMarker} == 1 ))
# then
#   releaseVersion=$(echo "${pomVersion}" | sed 's/\-SNAPSHOT//')
#   echo; echo "Branch equals 'master' so run release script using ${releaseVersion}"
#   ${matterhornHome}/bin/bamboo/release-matterhorn.sh "${releaseVersion}"
# fi

echo "-----------------------------------"
echo "Build parameters:"
echo "  matterhornHome: ${matterhornHome}"
echo "  buildKey: ${buildKey}"
echo "  buildNumber: ${buildNumber}"
echo "  gitRevision: ${gitRevision}"
echo "  pomVersion: ${pomVersion}"
echo "-----------------------------------"

function resetMatterhornLib() {
  # Remove jars, etc.
  cd ${matterhornHome}
  rm -Rf ${matterhornHome}/lib
  rm -Rf ${matterhornHome}/target
}

function recordBuildInformation() {
  # Create file with build info so Matterhorn can display on UI
  ${matterhornHome}/bin/bamboo/record-build-information.sh "${matterhornHome}" "${buildKey}-${buildNumber}" "${pomVersion}" "${gitRevision}"
}

# Given Maven what it needs
export MAVEN_OPTS='-Xms256m -Xmx512m -XX:PermSize=64m -XX:MaxPermSize=128m'

# Clean
resetMatterhornLib
# rm -f *.zip

# Zip files will go in a dedicated directory
artifactsDir="artifacts"
# rm -Rf "${artifactsDir}"
mkdir -p "${artifactsDir}"

# Are we building a UC Berkeley fork?
lineCount=$(find modules -name *participation-service* | wc -l)

if (( ${lineCount} == 0 ))
then
  # All-in-one deployment
  cd ${matterhornHome}
  mvn clean install -Dcheckstyle.skip=true -Dmaven.test.skip=true -DskipTests=true -DdeployTo=${matterhornHome} \
   || { echo 'Admin build failed' ; resetMatterhornLib; exit 1; }
  # Create artifact
  zip -r admin-${zipSuffix}.zip * \*/target/\* modules\* src\* logs\* work\* operations\*
  exit 0
fi

adminProfiles="admin,dist-stub,admin-with-search-service,worker-stub,workspace,serviceregistry,directory-ldap,directory-cas,directory-openid,participation-service"
# engageProfiles="engage-standalone,dist,serviceregistry,workspace,directory-ldap,directory-cas,directory-openid,participation-service"
workerProfiles="worker-standalone,dist,serviceregistry,workspace,directory-ldap,directory-cas,directory-openid"

# Admin
mvn clean install -Dcheckstyle.skip=true -Dmaven.test.skip=true -DskipTests=true -P${adminProfiles} -DdeployTo=${matterhornHome} \
  || { echo 'Admin build failed' ; resetMatterhornLib; exit 1; }

# Create artifact
recordBuildInformation
zipFile="admin-${zipSuffix}.zip"
zip -r "${zipFile}" * -x \*/target/\* modules\* src\* logs\* work\* operations\* artifacts\*
mv ${zipFile} ${artifactsDir}/

# # Clean
# resetMatterhornLib

# # Engage
# cd ${matterhornHome}
# mvn clean install -Dcheckstyle.skip=true -Dmaven.test.skip=true -DskipTests=true -P${engageProfiles} -DdeployTo=${matterhornHome} \
#   || { echo 'Engage build failed' ; resetMatterhornLib; exit 1; }

# # Create artifact
# recordBuildInformation
# zipFile="engage-${zipSuffix}.zip"
# zip -r "${zipFile}" * -x \*/target/\* modules\* src\* logs\* work\* operations\* artifacts\*
# mv ${zipFile} ${artifactsDir}/

# Clean
resetMatterhornLib

# Worker
cd ${matterhornHome}
mvn clean install -Dcheckstyle.skip=true -Dmaven.test.skip=true -DskipTests=true -P${workerProfiles} -DdeployTo=${matterhornHome} \
  || { echo 'Worker build failed' ; resetMatterhornLib; exit 1; }

# Create artifact
recordBuildInformation
zipFile="worker-${zipSuffix}.zip"
zip -r "${zipFile}" * -x \*/target/\* modules\* src\* logs\* work\* operations\* artifacts\*
mv ${zipFile} ${artifactsDir}/

# Clean
resetMatterhornLib

echo "------------------------------"
echo "Directory: "; echo "$(pwd)/${artifactsDir}"; echo; echo
echo "Zip files created by this build: "; echo; echo $(ls ${artifactsDir}/*-${zipSuffix}.zip); echo
echo "------------------------------"

exit 0
