UPDATE
  recording
SET treated_by_selenium=(
  CASE
    WHEN year < 2016 OR (year = 2016 AND semester_code = 'B') THEN false
    ELSE true
  END)

