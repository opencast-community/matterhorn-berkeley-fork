DO $$
    BEGIN
        BEGIN
            ALTER TABLE recording ADD COLUMN "video_status_adjusted" BOOLEAN DEFAULT FALSE NOT NULL;
        EXCEPTION
            WHEN duplicate_column THEN RAISE NOTICE 'column video_status_adjusted already exists in recording';
        END;
    END;
$$