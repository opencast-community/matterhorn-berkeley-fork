--
-- Jacobs Hall 310 will have a capture agent as of Fall 2017
--

ALTER TYPE e_building_name ADD VALUE 'JacobsHall' AFTER 'Hertz';
