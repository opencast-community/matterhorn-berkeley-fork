\set ON_ERROR_ROLLBACK on

BEGIN;

ALTER TYPE e_building_name ADD VALUE 'Haviland' AFTER 'Haas';

END;
