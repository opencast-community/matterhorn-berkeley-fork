DO $$
    BEGIN
        BEGIN
            ALTER TABLE recording ADD COLUMN "treated_by_selenium" BOOLEAN DEFAULT FALSE NOT NULL;
        EXCEPTION
            WHEN duplicate_column THEN RAISE NOTICE 'column treated_by_selenium already exists in recording';
        END;
    END;
$$
