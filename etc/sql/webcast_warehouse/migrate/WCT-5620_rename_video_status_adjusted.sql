DO $$
    BEGIN
        ALTER TABLE recording RENAME COLUMN "video_status_adjusted" TO "include_in_cc_feed";
    END;
$$
