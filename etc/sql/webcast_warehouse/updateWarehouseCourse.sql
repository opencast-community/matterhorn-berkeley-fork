<#import "macros.ftl" as macros>

UPDATE
    course
SET
    catalog_id=<#if course.catalogId??>'<@macros.escapeSql course.catalogId />'<#else>null</#if>,
    title='<@macros.escapeSql title />',
    department_name=<#if department_name??>'<@macros.escapeSql department_name />'<#else>null</#if>,
    description=<#if description??>'<@macros.escapeSql description />'<#else>null</#if>,
    section=<#if course.section??>'<@macros.escapeSql course.section />'<#else>null</#if>,
    audience_type='${audience_type}',
    recording_type='${recording_type}',
    license_type='${license_type}',
    building_name=<#if course.room?? && course.room.building??>'<@macros.escapeSql course.room.building.name() />'<#else>null</#if>,
    room_number=<#if course.room?? && course.room.roomNumber??>'<@macros.escapeSql course.room.roomNumber />'<#else>null</#if>,
    instructor_names=
    <#if course.instructors?? && (course.instructors?size > 0)>
        ARRAY[
            <#list course.instructors as instructor>
               '<@macros.escapeSql instructor />'<#if instructor_has_next>,</#if>
            </#list>
        ]
    <#else>
        null
    </#if>,
    youtube_playlist_id=<#if course.youTubePlaylistId??>'<@macros.escapeSql course.youTubePlaylistId />'<#else>null</#if>,
    meeting_days=
    <#if course.meetingDays?? && (course.meetingDays?size > 0)>
        '{
        <#list course.meetingDays as meetingDay>
            "${meetingDay.name()?substring(0, 3)}"<#if meetingDay_has_next>,</#if>
        </#list>
        }'
    <#else>
        null
    </#if>,
    itunes_audio_collection_id=<#if course.ITunesAudioId??>'${course.ITunesAudioId?c}'<#else>null</#if>,
    itunes_video_collection_id=<#if course.ITunesVideoId??>'${course.ITunesVideoId?c}'<#else>null</#if>,
    rss_feed_audio=<#if course.audioFeedRSS??>'<@macros.escapeSql course.audioFeedRSS />'<#else>null</#if>,
    rss_feed_video=<#if course.videoFeedRSS??>'<@macros.escapeSql course.videoFeedRSS />'<#else>null</#if>,
    rss_feed_screen=<#if course.screenFeedRSS??>'<@macros.escapeSql course.screenFeedRSS />'<#else>null</#if>
WHERE
    year=${course.year?c} AND semester_code='${course.semester.termCode}' AND ccn=${course.ccn?c}
