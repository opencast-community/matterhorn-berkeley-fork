<#--
Macros shared by templates in this directory.
-->

<#macro escapeSql value><#if value??>${value?replace("'", "''")}</#if></#macro>
