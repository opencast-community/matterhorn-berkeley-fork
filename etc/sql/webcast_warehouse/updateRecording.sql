<#import "macros.ftl" as macros>

UPDATE
    recording
SET
    year=${course.year?c},
    semester_code='${course.semester.termCode}',
    ccn=${course.ccn?c},
    title='<@macros.escapeSql title />',
    description=<#if description??>'<@macros.escapeSql description />'<#else>null</#if>,
    youtube_video_id=<#if youtube_video_id??>'<@macros.escapeSql youtube_video_id />'<#else>null</#if>,
    recording_start_utc=<#if recording.recordingStart??>(TIMESTAMP WITH TIME ZONE '${recording.recordingStart?string("${dateParsePattern}")}')<#else>null</#if>,
    recording_end_utc=<#if recording.recordingEnd??>(TIMESTAMP WITH TIME ZONE '${recording.recordingEnd?string("${dateParsePattern}")}')<#else>null</#if>,
    trimmed_length=<#if trimmed_length??>(INTERVAL '${trimmed_length}' HOUR TO SECOND)<#else>null</#if>,
    matterhorn_episode_id=<#if matterhorn_episode_id??>'<@macros.escapeSql matterhorn_episode_id />'<#else>null</#if>
WHERE
    recording_id=${recording.recordingId?c}
