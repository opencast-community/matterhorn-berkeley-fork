\set ON_ERROR_ROLLBACK on

BEGIN;

CREATE TYPE e_semester_code AS ENUM ('B', 'C', 'D');
CREATE TYPE e_audience_type AS ENUM ('campus', 'course', 'public');
CREATE TYPE e_recording_type AS ENUM ('audio', 'screencast', 'video', 'video_and_screencast');
CREATE TYPE e_license_type AS ENUM ('all_rights_reserved', 'creative_commons');
CREATE TYPE e_building_name AS ENUM ('Barker', 'Barrows', 'Birge', 'Boalt', 'Cory', 'Davis', 'DonnerLab', 'Dwinelle',
  'Etcheverry', 'Evans', 'Giannini', 'GPB', 'GSPP', 'Haas', 'Haviland', 'HearstFieldAnnex', 'HearstMin', 'Hertz',
  'JacobsHall', 'Kroeber', 'Latimer', 'LeConte', 'Lewis', 'LiKaShing', 'McCone', 'McLaughlin', 'Moffitt', 'Morgan',
  'Mulford', 'NorthGate', 'OffCampus', 'PauleyBallroom', 'Pimentel', 'Soda', 'Stanley', 'Tan', 'Tolman', 'ValleyLSB',
  'ValleyLifeSciences', 'Wheeler', 'Wurster');
CREATE TYPE e_meeting_day AS ENUM ('Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat');

CREATE TABLE course (
  year SMALLINT NOT NULL,
  semester_code e_semester_code NOT NULL,
  ccn int NOT NULL,
  catalog_id VARCHAR(20),
  title TEXT NOT NULL,
  department_name TEXT,
  section CHAR(5),
  description TEXT,
  audience_type e_audience_type NOT NULL,
  recording_type e_recording_type NOT NULL,
  license_type e_license_type NOT NULL,
  building_name e_building_name,
  room_number VARCHAR(20),
  instructor_names TEXT[],
  youtube_playlist_id TEXT,
  meeting_days e_meeting_day[],
  itunes_audio_collection_id INT,
  itunes_video_collection_id INT,
  rss_feed_audio TEXT,
  rss_feed_video TEXT,
  rss_feed_screen TEXT,

  PRIMARY KEY (year, semester_code, ccn)
);

CREATE TABLE recording (
  recording_id SERIAL PRIMARY KEY,
  year SMALLINT NOT NULL,
  semester_code e_semester_code NOT NULL,
  ccn INT NOT NULL,
  title TEXT NOT NULL,
  description TEXT,
  youtube_video_id TEXT,
  include_in_cc_feed BOOLEAN DEFAULT FALSE NOT NULL,
  treated_by_selenium BOOLEAN DEFAULT FALSE NOT NULL,
  recording_start_utc TIMESTAMP WITH TIME ZONE,
  recording_end_utc TIMESTAMP WITH TIME ZONE,
  trimmed_length INTERVAL HOUR TO SECOND,
  matterhorn_episode_id VARCHAR(40),
  FOREIGN KEY (year, semester_code, ccn) REFERENCES course (year, semester_code, ccn) ON UPDATE cascade
);

ALTER TABLE course OWNER TO webcast_warehouse;
ALTER TABLE recording OWNER TO webcast_warehouse;

GRANT USAGE ON SCHEMA public TO webcast_wh_app;
GRANT USAGE ON SCHEMA public TO webcast_wh_ro;

GRANT SELECT, UPDATE, INSERT, DELETE ON ALL TABLES IN SCHEMA public TO webcast_wh_app;
GRANT SELECT ON ALL TABLES IN SCHEMA public TO webcast_wh_ro;
GRANT ALL ON ALL SEQUENCES IN SCHEMA public TO webcast_wh_app;
GRANT SELECT ON ALL SEQUENCES IN SCHEMA public to webcast_wh_ro;

END;
