<#import "macros.ftl" as macros>

INSERT INTO
  recording
  (
    year, semester_code, ccn, title, description, youtube_video_id, include_in_cc_feed, treated_by_selenium, recording_start_utc, recording_end_utc, trimmed_length, matterhorn_episode_id
  )
VALUES
  (
    ${course.year?c},
    '${course.semester.termCode}',
    ${course.ccn?c},
    '<@macros.escapeSql title />',
    <#if description??>'<@macros.escapeSql description />'<#else>null</#if>,
    <#if youtube_video_id??>'<@macros.escapeSql youtube_video_id />'<#else>null</#if>,
    false,
    false,
    <#if recording.recordingStart??>(TIMESTAMP WITH TIME ZONE '${recording.recordingStart?string("${dateParsePattern}")}')<#else>null</#if>,
    <#if recording.recordingEnd??>(TIMESTAMP WITH TIME ZONE '${recording.recordingEnd?string("${dateParsePattern}")}')<#else>null</#if>,
    <#if trimmed_length??>(INTERVAL '${trimmed_length}' HOUR TO SECOND)<#else>null</#if>,
    <#if matterhorn_episode_id??>'<@macros.escapeSql matterhorn_episode_id />'<#else>null</#if>
  )
