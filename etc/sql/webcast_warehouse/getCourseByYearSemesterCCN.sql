SELECT
  -- course
  c.year, c.semester_code, c.ccn, c.catalog_id, c.title, c.department_name, c.description, c.section,
  c.audience_type, c.recording_type, c.license_type, c.building_name, c.room_number, c.instructor_names,
  c.youtube_playlist_id, c.meeting_days, c.itunes_audio_collection_id, c.itunes_video_collection_id,
  c.rss_feed_audio, c.rss_feed_video, c.rss_feed_screen,
  -- recording
  r.recording_id, r.title as recording_title, r.description as recording_description, r.youtube_video_id, r.include_in_cc_feed,
  r.recording_start_utc,  r.recording_end_utc, r.trimmed_length, r.matterhorn_episode_id
FROM
  course as c
LEFT JOIN
  recording as r
  ON
    c.year = r.year
  AND
    c.semester_code = r.semester_code
  AND
    c.ccn = r.ccn
  <#if include_in_cc_feed??>
    AND
    r.include_in_cc_feed IS ${include_in_cc_feed?string}
  </#if>

<#if year?? || semester_code?? || ccn??>
  WHERE
    <#assign appendAND = false />
    <#if year??>
      c.year=${year?c}
      <#assign appendAND = true />
    </#if>
    <#if semester_code??>
      <#if appendAND>AND</#if>
      c.semester_code='${semester_code}'
      <#assign appendAND = true />
    </#if>
    <#if ccn??>
      <#if appendAND>AND</#if>
      c.ccn=${ccn?c}
      <#assign appendAND = true />
    </#if>
  ORDER BY
    recording_start_utc, recording_title
</#if>
