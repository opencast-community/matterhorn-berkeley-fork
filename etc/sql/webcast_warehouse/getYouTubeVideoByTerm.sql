SELECT
  youtube_video_id
FROM
  recording
WHERE
  year=${year?c}
  AND
  semester_code='${semester_code}'
  AND
  youtube_video_id IS NOT NULL

  <#if include_in_cc_feed??>
    AND
    include_in_cc_feed IS ${include_in_cc_feed?string}
  </#if>

  <#if treated_by_selenium??>
    AND
    treated_by_selenium IS ${treated_by_selenium?string}
  </#if>

ORDER BY recording_id DESC

  <#if limit??>
    LIMIT ${limit?c}
  </#if>
