<#import "macros.ftl" as macros>

UPDATE
  recording
SET
  include_in_cc_feed=${include_in_cc_feed?string}
WHERE
  year=${year?c}
  AND
  semester_code='${semester_code}'
  AND
  youtube_video_id='${youtube_video_id}'
