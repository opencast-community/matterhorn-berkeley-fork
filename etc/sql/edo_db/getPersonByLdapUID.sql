SELECT
  distinct "campus-uid" AS ldap_uid,
  "emailAddress" AS email_address,
  "givenName" AS first_name,
  "familyName" AS last_name
FROM SISEDO.ASSIGNEDINSTRUCTORV00_VW
WHERE
  "campus-uid" = '${ldapUID}'
  AND "emailAddress" IS NOT NULL
  AND "familyName" IS NOT NULL
