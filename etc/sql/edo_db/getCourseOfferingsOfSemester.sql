SELECT
  sec."id" AS course_cntl_num,
  mtg."term-id" AS term_id,
  mtg."session-id" AS session_id,
  mtg."location-descr" AS location,
  mtg."meetsDays" AS meeting_days,
  mtg."startTime" AS meeting_start_time,
  mtg."endTime" AS meeting_end_time,
  sec."printInScheduleOfClasses" AS print_cd,
  crs."subjectArea" AS dept_name,
  crs."academicDepartment-descr" as dept_description,
  crs."catalogNumber-formatted" AS catalog_id,
  sec."displayName" AS display_name,
  sec."component-code" AS instruction_format,
  sec."sectionNumber" AS section_num,
  TRIM(crs."title") AS course_title,
  COUNT(enr."STUDENT_ID") AS student_count
FROM SISEDO.MEETINGV00_VW mtg
JOIN SISEDO.CLASSSECTIONALLV01_MVW sec ON (
  mtg."cs-course-id" = sec."cs-course-id" AND
  mtg."term-id" = sec."term-id" AND
  mtg."session-id" = sec."session-id" AND
  mtg."offeringNumber" = sec."offeringNumber" AND
  mtg."sectionNumber" = sec."sectionNumber"
)
JOIN SISEDO.ENROLLMENT_UIDV00_VW enr ON (
  enr."TERM_ID" = sec."term-id" AND
  enr."SESSION_ID" = sec."session-id" AND
  enr."CLASS_SECTION_ID" = sec."id" AND
  sec."status-code" = 'A'
)
LEFT OUTER JOIN SISEDO.DISPLAYNAMEXLATV01_MVW xlat ON (
  xlat."classDisplayName" = sec."displayName")
LEFT OUTER JOIN SISEDO.API_COURSEV00_VW crs ON (
  xlat."courseDisplayName" = crs."displayName" AND
  crs."status-code" = 'ACTIVE')
WHERE
  sec."printInScheduleOfClasses" = 'Y'
  AND sec."term-id" = '${termId}'
  AND sec."primary" = 'true'
  AND mtg."location-code" IS NOT NULL
  AND mtg."startDate" != mtg."endDate"
GROUP BY
  sec."id",
  mtg."term-id",
  mtg."session-id",
  mtg."location-descr",
  mtg."meetsDays",
  mtg."startTime",
  mtg."endTime",
  sec."printInScheduleOfClasses",
  crs."subjectArea",
  crs."academicDepartment-descr",
  crs."catalogNumber-formatted",
  sec."component-code",
  sec."sectionNumber",
  sec."displayName",
  crs."title"
ORDER BY sec."id"
