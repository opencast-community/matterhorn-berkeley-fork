SELECT
  instr."term-id" AS term_id,
  sec."id" AS course_cntl_num,
  instr."campus-uid" AS ldap_uid,
  instr."emailAddress" AS email_address,
  instr."givenName" AS first_name,
  instr."familyName" AS last_name,
  crs."academicDepartment-descr" AS dept_description
FROM SISEDO.ASSIGNEDINSTRUCTORV00_VW instr
JOIN SISEDO.CLASSSECTIONALLV01_MVW sec ON (
  instr."term-id" = sec."term-id" AND
  instr."session-id" = sec."session-id" AND
  instr."cs-course-id" = sec."cs-course-id" AND
  instr."offeringNumber" = sec."offeringNumber" AND
  instr."number" = sec."sectionNumber"
)
LEFT OUTER JOIN SISEDO.DISPLAYNAMEXLATV01_MVW xlat ON (
  xlat."classDisplayName" = sec."displayName"
)
LEFT OUTER JOIN SISEDO.API_COURSEV00_VW crs ON (
  xlat."courseDisplayName" = crs."displayName" AND
  crs."status-code" = 'ACTIVE'
)
WHERE
  instr."term-id" = ${termId}
  AND sec."id" = ${ccn}
  AND instr."role-code" IN ('ICNT', 'PI', 'TNIC')
  AND instr."campus-uid" IS NOT NULL
  AND instr."emailAddress" IS NOT NULL
  AND instr."familyName" IS NOT NULL
