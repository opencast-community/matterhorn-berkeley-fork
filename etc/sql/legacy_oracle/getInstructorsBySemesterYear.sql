SELECT
  instr.term_yr,
  instr.term_cd,
  instr.course_cntl_num,
  p.ldap_uid,
  p.email_address,
  p.first_name,
  p.last_name,
  course.dept_description
FROM WEBCAST_course_instructor_vw instr
  INNER JOIN WEBCAST_person_info_vw p ON p.ldap_uid = instr.instructor_ldap_uid
  INNER JOIN WEBCAST_course_info_vw course ON course.course_cntl_num = instr.course_cntl_num
WHERE
  instr.term_yr=${termYr}
  AND instr.term_cd='${termCd}'
  AND instr.instructor_func IN (1, 3)
