/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.smil;

import org.opencastproject.util.MimeType;

import org.apache.commons.lang.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Rudimentary abstraction of a Synchronized Media interface Language (SMIL)
 * Clip element used for attaching trim point metadata to a media package.
 *
 */
public class SmilClipElement {
  
  private ClipTag clip; 
  private String src;
  private long clipBegin;
  private long clipEnd;
  
  public SmilClipElement(MimeType mt, String src) {
    this.clip = ClipTag.valueOf(mt.getType());
    this.src = src;
  }

  /**
   * @return the clipBegin
   */
  public long getClipBegin() {
    return clipBegin;
  }

  /**
   * @param clipBegin the clipBegin to set
   */
  public void setClipBegin(long clipBegin) {
    this.clipBegin = clipBegin;
  }

  /**
   * @return the clipEnd
   */
  public long getClipEnd() {
    return clipEnd;
  }

  /**
   * @param clipEnd the clipEnd to set
   */
  public void setClipEnd(long clipEnd) {
    this.clipEnd = clipEnd;
  }

  /**
   * @return the src
   */
  public String getSrc() {
    return src;
  }
  
  /**
   * The SmilClipElement as an element of a SMIL document
   * 
   * @return marshalled SmilClipElement
   */
  public String marshalSmilClipElement() {
    StringBuilder sb = new StringBuilder();
    sb.append("<" + getClip().getClipTagValue() + " src=\"");
    sb.append(getSrc());
    sb.append("\"");
    sb.append(" clipBegin=\"");
    sb.append(Long.toString(getClipBegin()));
    sb.append("ms\"");
    sb.append(" clipEnd=\"");
    sb.append(Long.toString(getClipEnd()));
    sb.append("ms\"");
    sb.append("/>");
    return sb.toString();
  }
  
  public static SmilClipElement unMarshalSmilClipElement(String str) {
    final String regex = "^<(\\w+) src=\"(.+)\" clipBegin=\"(.+)ms\" clipEnd=\"(.+)ms\"/>$";
    final Pattern pattern = Pattern.compile(regex);
    final Matcher m = pattern.matcher(str);
    if (m.find()) {
      final String clipTag = m.group(1);
      final String src = m.group(2);
      final String clipBegin = m.group(3);
      final String clipEnd = m.group(4);
      final MimeType mt = MimeType.mimeType(clipTag, StringUtils.EMPTY);
      SmilClipElement element = new SmilClipElement(mt, src);
      element.setClipBegin(Integer.parseInt(clipBegin));
      element.setClipEnd(Integer.parseInt(clipEnd));
      return element;
    } else {
      return null;
    }  
  }

  /**
   * @return the clip
   */
  public ClipTag getClip() {
    return clip;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((clip == null) ? 0 : clip.hashCode());
    result = prime * result + (int) (clipBegin ^ (clipBegin >>> 32));
    result = prime * result + (int) (clipEnd ^ (clipEnd >>> 32));
    result = prime * result + ((src == null) ? 0 : src.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (!(obj instanceof SmilClipElement)) {
      return false;
    }
    SmilClipElement other = (SmilClipElement) obj;
    if (clip != other.clip) {
      return false;
    }
    if (clipBegin != other.clipBegin) {
      return false;
    }
    if (clipEnd != other.clipEnd) {
      return false;
    }
    if (src == null) {
      if (other.src != null) {
        return false;
      }
    } else if (!src.equals(other.src)) {
      return false;
    }
    return true;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("SmilClipElement [clip=");
    builder.append(clip);
    builder.append(", src=");
    builder.append(src);
    builder.append(", clipBegin=");
    builder.append(clipBegin);
    builder.append(", clipEnd=");
    builder.append(clipEnd);
    builder.append("]");
    return builder.toString();
  }
}
