/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.notify;

/**
 * This interface might expand to include:
 * <ul>
 *   <li>notifyOperationsTeam</li>
 *   <li>notifySupportTeam</li>
 *   <li>notifyCustomer</li>
 * </ul>
 * @author John Crossman
 */
public interface Notifier {

  /**
   * Send email to the Matterhorn Engineering team.
   * @param subject never null
   * @param message never null
   * @param debugInfo zero or more objects that will be dumped to email using {@link #toString()}
   */
  SendType notifyEngineeringTeam(String subject, String message, Object... debugInfo);

  /**
   * Send email to the Matterhorn Engineering team.
   * @param exception message and stacktrace are put into an email
   * @param debugInfo append toString() of these objects so engineering team has debug info
   */
  SendType notifyEngineeringTeam(Throwable exception, Object... debugInfo);

  /**
   * Send email to the Matterhorn Engineering team.
   * @param subject email subject line
   * @param exception message and stacktrace are put into an email
   * @param debugInfo append toString() of these objects so engineering team has debug info
   */
  SendType notifyEngineeringTeam(String subject, Throwable exception, Object... debugInfo);

}
