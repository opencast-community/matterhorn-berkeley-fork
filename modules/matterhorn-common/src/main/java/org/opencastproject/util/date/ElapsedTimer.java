/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.util.date;

import org.apache.commons.lang.time.StopWatch;

/**
 * Millisecond-based start/stop timer.
 * 
 * @author Fernando Alvarez
 *
 */
public class ElapsedTimer {
  private boolean stopWatchRunning = false;  
  private final StopWatch stopWatch = new StopWatch();
  
  public long getElapsedTime() {
    final long elapsedTime;
    
    if (isStopWatchRunning()) {
      stopWatch.suspend();
      elapsedTime = stopWatch.getTime();
      stopWatch.resume();  
    } else {
      elapsedTime = 0;
    }
    return elapsedTime;
  }
  
  public void start() {
    if (!isStopWatchRunning()) {
      stopWatch.start();
      stopWatchRunning = true;
    }
  }
  
  public void stop() {
    if (isStopWatchRunning()) {
      stopWatch.reset();
      stopWatchRunning = false;
    }
  }
  
  public boolean isStopWatchRunning() {
    return stopWatchRunning;
  }

}
