/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.util.date;

import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.lang.time.FastDateFormat;
import org.opencastproject.util.StringUtil;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;

/**
 * @author John Crossman
 */
public final class DateUtil {

  private DateUtil() {
  }

  /**
   * @param militaryTime never null. For example, "0900" or "1530"
   * @return same values represented in a different way
   */
  public static TimeOfDay getTimeOfDay(final String militaryTime) {
    final int number = Integer.parseInt(militaryTime);
    final int hour = (number / 100) % 12;
    final int minutes = number % 100;
    final TimeOfDay.DayPeriod dayPeriod = (number >= 1200) ? TimeOfDay.DayPeriod.PM : TimeOfDay.DayPeriod.AM;
    return new TimeOfDay(hour, minutes, dayPeriod);
  }

  /**
   * @param time never null
   * @return For example, "0900" or "1530"
   */
  public static String getMilitaryTime(final TimeOfDay time) {
    if (time == null) {
      return null;
    }
    return TimeOfDay.DayPeriod.AM == time.getDayPeriod()
        ? StringUtil.asTwoDigitString(time.getHour()) + StringUtil.asTwoDigitString(time.getMinutes())
        : (time.getHour() + 12) + StringUtil.asTwoDigitString(time.getMinutes());
  }

  public static Date getTodayDate(final TimeOfDay timeOfDay) {
    final int hours = TimeOfDay.DayPeriod.AM.equals(timeOfDay.getDayPeriod()) ? timeOfDay.getHour() : 12 + timeOfDay.getHour();
    final int minutes = timeOfDay.getMinutes();
    Date adjusted = new Date();
    adjusted = DateUtils.setHours(adjusted, hours);
    adjusted = DateUtils.setMinutes(adjusted, minutes);
    adjusted = DateUtils.setSeconds(adjusted, 0);
    return DateUtils.setMilliseconds(adjusted, 0);
  }

  /**
   * @see org.apache.commons.lang.time.DateUtils#parseDate(String, String[])
   * @param dateAsString date to parse
   * @param dateFormats see {@link org.apache.commons.lang.time.FastDateFormat} for options
   * @return parsed date
   */
  public static Date parseDate(final String dateAsString, final FastDateFormat... dateFormats) {
    try {
      final ArrayList<String> parsePatterns = new ArrayList<String>(dateFormats.length);
      for (final FastDateFormat dateFormat : dateFormats) {
        parsePatterns.add(dateFormat.getPattern());
      }
      return DateUtils.parseDate(dateAsString, parsePatterns.toArray(new String[dateFormats.length]));
    } catch (final ParseException e) {
      throw new IllegalArgumentException(e);
    }
  }
}
