/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.util;

import java.util.Dictionary;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

/**
 * @author John Crossman
 */
public final class MapUtils {

  private MapUtils() {
  }

  /**
   * Put all properties to Map. Keys must be of type String.
   * @param map Never null.
   * @param properties Never null and ALL keys must be of type String.
   */
  public static Map<String, String> merge(final Map<String, String> map, final Properties properties) {
    final Map<String, String> merge = new HashMap<String, String>(map);
    final Set<Object> keys = properties.keySet();
    for (final Object key : keys) {
      if (key instanceof String) {
        final String keyForMap = (String) key;
        merge.put(keyForMap, properties.getProperty(keyForMap));
      } else {
        final String type = (key == null) ? "null" : key.getClass().getSimpleName();
        throw new IllegalArgumentException("Property keys not of type String are not allowed. Found type: " + type);
      }
    }
    return merge;
  }

  /**
   * @param map Never null.
   * @return Same elements in different object type.
   */
  public static Properties toProperties(final Map<? extends String, ? extends String> map) {
    final Properties properties = new Properties();
    for (final String key : map.keySet()) {
      properties.put(key, map.get(key));
    }
    return properties;
  }

  /**
   * @param dictionary Never null.
   * @return Same elements in different object type.
   */
  public static Properties toProperties(final Dictionary dictionary) {
    final Properties properties = new Properties();
    final Enumeration keys = dictionary.keys();
    while (keys.hasMoreElements()) {
      final Object key = keys.nextElement();
      properties.put(key, dictionary.get(key));
    }
    return properties;
  }

  public static <K, V> Dictionary<K, V> clone(final Dictionary<K, V> properties) {
    if (properties == null) {
      return null;
    }
    final Dictionary<K, V> clone = new Hashtable<K, V>();
    final Enumeration<K> keys = properties.keys();
    while (keys.hasMoreElements()) {
      final K key = keys.nextElement();
      clone.put(key, properties.get(key));
    }
    return clone;
  }

}
