/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.util;

import org.opencastproject.util.env.EnvironmentUtil;

/**
 * @author John Crossman
 */
public final class MatterhornConfigs {

  private MatterhornConfigs() {
  }

  /**
   * @return Verify ffmpeg exists in expected location.
   */
  public static String getFFMpegExecutable() {
    final String binary = EnvironmentUtil.getMatterhornConfigProperties().get("org.opencastproject.composer.ffmpegpath");
    Process p = null;
    try {
      final ProcessBuilder processBuilder = new ProcessBuilder(binary, "-version");
      processBuilder.redirectErrorStream(true);
      p = processBuilder.start();
      //
      final int waitFor = p.waitFor();
      final int exitValue = p.exitValue();
      if (waitFor != 0 || exitValue != 0) {
        throw new ConfigurationException(binary + " returned non-zero status code: " + exitValue);
      }
    } catch (final Throwable e) {
      throw new ConfigurationException(binary + "-related error", e);
    } finally {
      IoSupport.closeQuietly(p);
    }
    return binary;
  }

}
