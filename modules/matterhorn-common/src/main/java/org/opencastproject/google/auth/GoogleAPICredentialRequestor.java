/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.google.auth;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.auth.oauth2.StoredCredential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.DataStore;
import org.apache.commons.lang.StringUtils;
import org.json.simple.parser.ParseException;
import org.opencastproject.google.youtube.BerkeleyGoogleCredential;
import org.opencastproject.google.youtube.GoogleUtils;
import org.opencastproject.google.youtube.StagedBackOff;
import org.opencastproject.google.youtube.YouTubeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.MessageFormat;

/**
 * <code>GoogleAPICredentialRequestor</code> obtains credentials from Google 
 * and persists them in a data store on the local file system for later use 
 * when invoking the Google Data API V3 to upload a file to YouTube.
 * 
 * @author Fernando Alvarez
 */
public final class GoogleAPICredentialRequestor {

  private static OAuth2CredentialFactory credentialFactory = null;
  private static final Logger logger = LoggerFactory.getLogger(GoogleAPICredentialRequestor.class);

  /**
   * Private constructor.
   */
  private GoogleAPICredentialRequestor() {
  }

  /**
   * The authorization process is dependent on authorization context parameters
   * which is data that is required to execute the request and persist the 
   * result:
   * <ul>
   * <li>client secrets - a file containing the client id and password</li>
   * <li>credentialDatastore - name of the datastore which stores tokens</li>
   * <li>dataStoreDirectory - location of the datastore in the file system</li>
   * </ul>
   *
   * @param args
   *          override parameters
   */
  public static void main(final String[] args) throws IOException, ParseException {
    int size = (args == null) ? 0 : args.length;
    if (size != 3) {
      throw new IllegalArgumentException('\n' + "[ERROR] Wrong number of arguments: " + size + '\n');
    } else {
      registerOAuth2Credential(new File(args[0]), args[1], args[2]);
    }
  }

  /**
   * @param clientSecrets Null not allowed.
   * @param credentialDataStore Null not allowed.
   * @param dataStoreDirectory Null not allowed.
   * @throws IOException when file not available
   * @throws ParseException when file is not JSON
   */
  private static void registerOAuth2Credential(final File clientSecrets, final String credentialDataStore,
                                               final String dataStoreDirectory) throws IOException, ParseException {
    if (clientSecrets.exists()) {
      final ClientCredentials c = new ClientCredentials();
      c.setClientSecrets(clientSecrets);
      c.setCredentialDatastore(credentialDataStore);
      c.setDataStoreDirectory(dataStoreDirectory);
      final OAuth2CredentialFactory factory = (credentialFactory == null) ? new OAuth2CredentialFactoryImpl() : credentialFactory;
      final DataStore<StoredCredential> dataStore = factory.getDataStore(c.getCredentialDatastore(),
              c.getDataStoreDirectory());
      //
      final StagedBackOff backOff = YouTubeUtils.getStagedBackOff(GoogleUtils.getYouTubeSettings());
      // Reads the client id and secret
      GoogleClientSecrets gClientSecrets = GoogleClientSecrets.load(new JacksonFactory(), new FileReader(c.getClientSecrets()));
      // Obtain tokens from credential in data store, or obtains a new one from Google if one doesn't exist
      String clientId = StringUtils.trim(c.getClientId());
      String dataStoreSummary = dataStore.getClass().getSimpleName() + " id:" + dataStore.getId() + "; size:" + dataStore.size();
      if (dataStore.isEmpty()) {
        dataStoreSummary += "; isEmpty:true";
      } else {
        dataStoreSummary += "; containsKey(" + clientId + "):" + dataStore.containsKey(clientId);
        for (String key : dataStore.keySet()) {
          dataStoreSummary += "; get(" + key + "):" + dataStore.get(key);
        }
      }
      logger.info(dataStoreSummary);
      // Installed applications
      final GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(new NetHttpTransport(), new JacksonFactory(),
          gClientSecrets, c.getScopes()).setCredentialDataStore(dataStore).setApprovalPrompt("auto")
          .setAccessType("offline").build();
      final LocalServerReceiver localReceiver = new LocalServerReceiver();
      final Credential cred = new AuthorizationCodeInstalledApp(flow, localReceiver).authorize(clientId);
      final String accessToken = cred.getAccessToken();
      final String refreshToken = cred.getRefreshToken();
      localReceiver.stop();
      logger.debug(MessageFormat.format("Created new credential for client {0} in data store {1}", clientId, dataStore.getId()));

      final GoogleCredential.Builder builder = new GoogleCredential.Builder()
          .setClientSecrets(gClientSecrets).setJsonFactory(new JacksonFactory())
          .setTransport(new NetHttpTransport());
      final Credential credential = new BerkeleyGoogleCredential(builder, backOff);
      credential.setAccessToken(accessToken);
      credential.setRefreshToken(refreshToken);
      logger.debug(MessageFormat.format("Found credential {0} using {1}", credential.getRefreshToken(), c.toString()));
    } else {
      throw new IllegalArgumentException("The client-secrets file (YouTube OAuth) was not found: " + clientSecrets.getAbsolutePath());
    }
  }

  static void setCredentialFactory(final OAuth2CredentialFactory oAuth2CredentialFactory) {
    credentialFactory = oAuth2CredentialFactory;
  }
}
