/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.google.youtube;

import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.JsonParser;
import com.google.api.client.json.JsonToken;
import com.google.api.services.youtube.model.Video;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.joda.time.Period;
import org.joda.time.format.ISOPeriodFormat;
import org.joda.time.format.PeriodFormatter;
import org.opencastproject.security.api.RecordingAccessRights;
import org.opencastproject.util.env.EnvironmentUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author John Crossman
 */
public final class GoogleUtils {

  /** Videos with no associated series will be mad private in YouTube */
  public static final RecordingAccessRights ACCESS_RIGHTS_DEFAULT = RecordingAccessRights.studentsOnlyAccessRights;
  private static final Logger logger = LoggerFactory.getLogger(GoogleUtils.class);

  private GoogleUtils() {
  }

  /**
   * See https://en.wikipedia.org/wiki/ISO_8601#Durations
   * @param video null not allowed
   * @return number of seconds calculated from {@link com.google.api.services.youtube.model.VideoContentDetails#getDuration()}
   */
  public static Integer getDurationSeconds(final Video video) {
    final int seconds;
    if (video == null || video.getContentDetails() == null) {
      seconds = 0;
    } else {
      final PeriodFormatter formatter = ISOPeriodFormat.standard();
      final Period period = formatter.parsePeriod(video.getContentDetails().getDuration());
      seconds = period.toStandardSeconds().getSeconds();
    }
    return seconds;
  }

  public static Properties getYouTubeSettings() throws IOException {
    final File file = EnvironmentUtil.getFileUnderFelixHome(
            "etc/services/org.opencastproject.publication.youtube.YouTubeV3PublicationServiceImpl.properties");
    final Properties p = new Properties();
    FileInputStream inputStream = new FileInputStream(file);
    p.load(inputStream);
    IOUtils.closeQuietly(inputStream);
    return EnvironmentUtil.createEProperties(p, true);
  }

  /**
   * Match incoming arg with {@link org.opencastproject.security.api.RecordingAccessRights#name()}
   * @param propertyAccessRights null allowed
   * @return when incoming arg is null we return {@link org.opencastproject.security.api.RecordingAccessRights#studentsOnlyAccessRights}
   */
  public static RecordingAccessRights findByPropertyAccessRights(final String propertyAccessRights) {
    return findByPropertyAccessRights(propertyAccessRights, ACCESS_RIGHTS_DEFAULT);
  }

  /**
   * Match incoming arg with {@link org.opencastproject.security.api.RecordingAccessRights#name()}
   * @param propertyAccessRights null allowed
   * @param defaultValue return this value when property is invalid
   * @return when incoming arg is null we return {@link org.opencastproject.security.api.RecordingAccessRights#studentsOnlyAccessRights}
   */
  public static RecordingAccessRights findByPropertyAccessRights(final String propertyAccessRights,
          final RecordingAccessRights defaultValue) {
    RecordingAccessRights accessRights = defaultValue;
    for (final RecordingAccessRights next : RecordingAccessRights.values()) {
      if (StringUtils.equalsIgnoreCase(next.name(), StringUtils.trimToNull(propertyAccessRights))) {
        accessRights = next;
        break;
      }
    }
    return accessRights;
  }

  /**
   * @param jsonFactory never null
   * @param stream never null
   * @return code property in JSON
   * @throws IOException when bad things happen.
   */
  static Integer extractGoogleJsonCode(final JsonFactory jsonFactory, final InputStream stream) throws IOException {
    Integer jsonCode = null;
    JsonParser jsonParser = null;
    try {
      if (stream != null) {
        jsonParser = jsonFactory.createJsonParser(stream);
        jsonParser.skipToKey("code");
        if (jsonParser.getCurrentToken() != JsonToken.END_OBJECT) {
          jsonCode = jsonParser.parseAndClose(Integer.class);
        }
      }
    } catch (final IllegalArgumentException e) {
      logger.warn("Failed to extract 'code' from Google JSON response: " + e.getMessage());
    } finally {
      if (jsonParser != null) {
        jsonParser.close();
      }
    }
    return jsonCode;
  }

}
