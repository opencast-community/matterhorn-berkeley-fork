/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.google.auth;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.auth.oauth2.StoredCredential;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.BackOff;
import com.google.api.client.util.store.DataStore;
import com.google.api.client.util.store.DataStoreFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import org.apache.commons.lang.ClassUtils;
import org.apache.commons.lang.StringUtils;
import org.opencastproject.google.youtube.BerkeleyGoogleCredential;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.MessageFormat;

/**
 * @see com.google.api.client.googleapis.auth.oauth2.GoogleCredential
 * @author Fernando Alvarez
 * @author John Crossman
 */
public final class OAuth2CredentialFactoryImpl implements OAuth2CredentialFactory {

  private final Logger logger = LoggerFactory.getLogger(this.getClass());

  public OAuth2CredentialFactoryImpl() {
  }

  @Override
  public Credential getGoogleCredential(final ClientCredentials credentials, final BackOff backOff) throws IOException {
    DataStore<StoredCredential> dataStore = getDataStore(credentials.getCredentialDatastore(), credentials.getDataStoreDirectory());
    // Reads the client id and secret
    GoogleClientSecrets gClientSecrets = GoogleClientSecrets.load(new JacksonFactory(), new FileReader(credentials.getClientSecrets()));
    final GoogleCredential.Builder builder = new GoogleCredential.Builder()
        .setClientSecrets(gClientSecrets).setJsonFactory(new JacksonFactory())
        .setTransport(new NetHttpTransport());
    // Obtain tokens from credential in data store, or obtains a new one from Google if one doesn't exist
    String clientId = StringUtils.trim(credentials.getClientId());
    String dataStoreSummary = dataStore.getClass().getSimpleName() + " id:" + dataStore.getId() + "; size:" + dataStore.size();
    if (dataStore.isEmpty()) {
      dataStoreSummary += "; isEmpty:true";
    } else {
      dataStoreSummary += "; containsKey(" + clientId + "):" + dataStore.containsKey(clientId);
      for (String key : dataStore.keySet()) {
        dataStoreSummary += "; get(" + key + "):" + dataStore.get(key);
      }
    }
    logger.info(dataStoreSummary);
    final StoredCredential storedCredential = dataStore.get(clientId);
    if (storedCredential == null) {
      throw new IllegalStateException(ClassUtils.getShortClassName(dataStore.getClass()) + " was unable to load StoredCredential with clientId: " + clientId);
    }
    final Credential result = new BerkeleyGoogleCredential(builder, backOff);
    result.setAccessToken(storedCredential.getAccessToken());
    result.setRefreshToken(storedCredential.getRefreshToken());
    logger.info(MessageFormat.format("Found credential {0} using {1}", storedCredential.getRefreshToken(), credentials));
    return result;
  }

  @Override
  public DataStore<StoredCredential> getDataStore(final String id, final String dataStoreDirectory) throws IOException {
    DataStoreFactory factory = new FileDataStoreFactory(new File(dataStoreDirectory));
    DataStore<StoredCredential> dataStore = factory.getDataStore(id);
    logger.warn(ClassUtils.getShortClassName(factory.getClass()) + " loaded dataStore of type " + ClassUtils.getShortClassName(dataStore.getClass())
        + " where id = " + id + " and dataStoreDirectory = " + dataStoreDirectory);
    return dataStore;
  }

}
