/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.google.youtube;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.http.HttpBackOffIOExceptionHandler;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import org.json.simple.parser.ParseException;
import org.opencastproject.google.auth.ClientCredentials;
import org.opencastproject.google.auth.OAuth2CredentialFactory;
import org.opencastproject.google.auth.OAuth2CredentialFactoryImpl;
import org.opencastproject.key.ConfigUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

/**
 * @author John Crossman
 */
class GoogleHttpRequestInitializer implements HttpRequestInitializer {

  private static final Logger logger = LoggerFactory.getLogger(GoogleHttpRequestInitializer.class);

  // Note the following three params are purposely NOT configurable from the properties file in the belief that doing
  // so may inadvertently cause problems that would be worse than the problem being addressed.

  // Timeout in milliseconds to establish a connection; the default was 20 seconds, set to 0 for an infinite timeout
  private static final int CONNECT_TIMEOUT = 60000; // One minute

  // Timeout in milliseconds to read data from an established connection; the default was 20 seconds, set to 0 for an infinite timeout
  private static final int READ_TIMEOUT = 60000; // One minute

  // Number of retries that will be allowed to execute before the request will be terminated; the default was 10
  private static final int NUMBER_OF_RETRIES = 100;

  private final ClientCredentials clientCredentials;
  private final StagedBackOff stagedBackOff;

  GoogleHttpRequestInitializer(final Properties properties) throws IOException {
    clientCredentials = new ClientCredentials();
    final String dataStore = ConfigUtils.get(properties, YouTubeKey.credentialDatastore, true);
    logger.info("YouTube credential data-store: " + dataStore);
    clientCredentials.setCredentialDatastore(dataStore);
    final String path = ConfigUtils.get(properties, YouTubeKey.clientSecretsV3, true);
    try {
      logger.info("Loading client-credentials from file: " + path);
      clientCredentials.setClientSecrets(new File(path));
    } catch (final ParseException e) {
      throw new IOException(e);
    }
    String dataStoreDirectory = ConfigUtils.get(properties, YouTubeKey.dataStore, true);
    logger.info("YouTube credential data-store directory: " + dataStoreDirectory);
    clientCredentials.setDataStoreDirectory(dataStoreDirectory);
    stagedBackOff = YouTubeUtils.getStagedBackOff(properties);
  }

  @Override
  public void initialize(final HttpRequest request) throws IOException {
    logger.info("Initialize with stagedBackOff: " + stagedBackOff);
    // Custom handler on IOException
    stagedBackOff.reset();
    final OAuth2CredentialFactory factory = new OAuth2CredentialFactoryImpl();
    final Credential credential = factory.getGoogleCredential(clientCredentials, stagedBackOff);
    initializeRequest(request, credential, stagedBackOff);
  }

  ClientCredentials getClientCredentials() {
    return clientCredentials;
  }

  static void initializeRequest(final HttpRequest request, final Credential credential,
                                final StagedBackOff stagedBackOff) {
    logger.info("Initialize request with credential: " + credential);
    request.setInterceptor(credential);
    request.setUnsuccessfulResponseHandler(credential);
    request.setIOExceptionHandler(new HttpBackOffIOExceptionHandler(stagedBackOff));
    request.setConnectTimeout(CONNECT_TIMEOUT);
    request.setReadTimeout(READ_TIMEOUT);
    request.setNumberOfRetries(NUMBER_OF_RETRIES);
  }

}
