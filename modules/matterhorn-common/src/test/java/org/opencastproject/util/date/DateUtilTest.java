/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.util.date;

import org.apache.commons.lang.time.DateUtils;
import org.junit.Test;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author John Crossman
 */
public class DateUtilTest {

  @Test
  public void testGetTimeOfDay() {
    final TimeOfDay timeOfDayAM = DateUtil.getTimeOfDay("0900");
    assertEquals(9, timeOfDayAM.getHour());
    assertEquals(0, timeOfDayAM.getMinutes());
    assertEquals(TimeOfDay.DayPeriod.AM, timeOfDayAM.getDayPeriod());
    final TimeOfDay timeOfDayPM = DateUtil.getTimeOfDay("1530");
    assertEquals(3, timeOfDayPM.getHour());
    assertEquals(30, timeOfDayPM.getMinutes());
    assertEquals(TimeOfDay.DayPeriod.PM, timeOfDayPM.getDayPeriod());
  }

  @Test
  public void testGetMilitaryTime() {
    final TimeOfDay timeAM = new TimeOfDay(9, 59, TimeOfDay.DayPeriod.AM);
    final TimeOfDay timePM = new TimeOfDay(1, 5, TimeOfDay.DayPeriod.PM);
    assertEquals("0959", DateUtil.getMilitaryTime(timeAM));
    assertEquals("1305", DateUtil.getMilitaryTime(timePM));
  }

  @Test
  public void testGetTodayDateAM() {
    final Date now = new Date();
    final TimeOfDay timeOfDay = new TimeOfDay(8, 30, TimeOfDay.DayPeriod.AM);
    final Date todayDate = DateUtil.getTodayDate(timeOfDay);
    assertTrue(DateUtils.isSameDay(now, todayDate));
    assertEquivalent(timeOfDay, todayDate);
  }

  @Test
  public void testGetTodayDatePM() {
    final Date now = new Date();
    final TimeOfDay timeOfDay = new TimeOfDay(5, 0, TimeOfDay.DayPeriod.AM);
    final Date todayDate = DateUtil.getTodayDate(timeOfDay);
    assertTrue(DateUtils.isSameDay(now, todayDate));
    assertEquivalent(timeOfDay, todayDate);
  }

  private void assertEquivalent(final TimeOfDay timeOfDay, final Date todayDate) {
    final Calendar date = new GregorianCalendar();
    date.set(Calendar.HOUR_OF_DAY, timeOfDay.getHour());
    date.set(Calendar.MINUTE, timeOfDay.getMinutes());
    date.set(Calendar.SECOND, 0);
    date.set(Calendar.MILLISECOND, 0);
    //
    assertEquals(todayDate.getTime(), date.getTime().getTime());
  }

}
