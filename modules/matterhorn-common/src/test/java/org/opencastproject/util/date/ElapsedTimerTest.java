/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.util.date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;

/**
 * 
 * @author Fernando Alvarez
 *
 */
public class ElapsedTimerTest {
  private ElapsedTimer timer;

  @Before
  public void setUp() throws Exception {
    timer = new ElapsedTimer();
  }

  @After
  public void tearDown() throws Exception {
    timer.stop();
  }

  @Test
  public void testStartStopStart() {
    timer.start();
    timer.stop();
    assertThat(timer.isStopWatchRunning()).isFalse();
    timer.start();
    assertThat(timer.isStopWatchRunning()).isTrue();
  }
  
  @Test
  public void testElpasedTimeGreaterThanZero() throws InterruptedException {
    assertThat(timer.getElapsedTime()).isZero();
    
    timer.start();  
    
    //Pause for one second to ensure enough time has transpired before checking
    Thread.sleep(1000); 
    
    assertThat(timer.getElapsedTime()).isGreaterThan(0);
  }
}
