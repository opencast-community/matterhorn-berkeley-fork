/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.util.env;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.junit.Test;
import org.opencastproject.util.XProperties;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Dictionary;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author John Crossman
 */
public class PrepareMatterhornRuntimePropertiesTest {

  private static final String ADMIN_URL_KEY = "org.opencastproject.admin.ui.url";

  @Test
  public void testXProperties() {
    final Properties p = new XProperties();
    p.put("MATTERHORN_ENV", "dev");
    p.put("key1", "before-${MATTERHORN_ENV}");
    p.put("key2", "${MATTERHORN_ENV}-after");
    p.put("key3", "before-${MATTERHORN_ENV}-after");
    p.put("key4", " ${MATTERHORN_ENV} ");
    p.put("key5", "${MATTERHORN_ENV}");
    //
    assertEquals("before-dev", p.getProperty("key1"));
    assertEquals("dev-after", p.getProperty("key2"));
    assertEquals("before-dev-after", p.getProperty("key3"));
    assertEquals(" dev ", p.getProperty("key4"));
    assertEquals("dev", p.getProperty("key5"));
  }

  @Test
  public void testMainMethod() throws IOException {
    final Environment environment = EnvironmentUtil.getEnvironment();
    // Create source properties file. Append a custom property to test overrideProperties feature.
    final File baseDir = File.createTempFile("tmp", ".txt").getParentFile();
    final String felixHome = baseDir.getAbsolutePath();
    final File etcDir = new File(FilenameUtils.concat(felixHome, "etc"));
    final File servicesDir = new File(FilenameUtils.concat(etcDir.getAbsolutePath(), "services"));
    FileUtils.forceMkdir(etcDir);
    FileUtils.forceMkdir(servicesDir);
    createConfigProperties(etcDir);
    createSystemProperties(etcDir);
    // We expect env specific logging.properties file to be copied to expected location
    final File envSpecificFile = new File(servicesDir, "org.ops4j.pax.logging_" + environment + ".properties");
    final File loggingPropertiesFile = new File(servicesDir, "org.ops4j.pax.logging.properties");
    createLoggingProperties(loggingPropertiesFile, true);
    createLoggingProperties(envSpecificFile, false);
    //
    PrepareMatterhornRuntimeProperties.main(new String[]{felixHome});
    // Assert processed files exist in target location.
    assertTrue(new File(felixHome, "target/etc/config.properties").exists());
    assertTrue(new File(felixHome, "target/etc/system.properties").exists());
    //
    final Dictionary<String, String> configProperties = EnvironmentUtil.getMatterhornConfigProperties();
    final String expectedURL = Environment.local.equals(environment)
        ? "http://localhost:8080"
        : "http://video-" + environment + ".ets.berkeley.edu";
    assertEquals(expectedURL, configProperties.get(ADMIN_URL_KEY));
    //
    // The logging properties file will contain a property telling us if the env-appropriate file copy happened.
    final Properties loggingProperties = new Properties();
    loggingProperties.load(new FileInputStream(loggingPropertiesFile));
    // Expect one property because we did not "addExtraProperty".
    assertEquals(1, loggingProperties.size());
    assertEquals(envSpecificFile.getName(), loggingProperties.getProperty("filename"));
  }

  private void createLoggingProperties(final File loggingPropertiesFile, final boolean addExtraProperty) throws IOException {
    final List<String> configLines = new LinkedList<String>();
    configLines.add("filename=" + loggingPropertiesFile.getName());
    if (addExtraProperty) {
      configLines.add("key=value");
    }
    FileUtils.writeLines(loggingPropertiesFile, configLines);
  }

  private void createSystemProperties(final File etcDir) throws IOException {
    final List<String> configLines = new LinkedList<String>();
    configLines.add("felix.auto.deploy.dir=${felix.home}/lib/felix");
    final File file = new File(etcDir, "system.properties");
    FileUtils.writeLines(file, configLines);
    System.setProperty("felix.system.properties", file.getAbsolutePath());
  }

  private void createConfigProperties(final File etcDir) throws IOException {
    final File file = new File(etcDir, "config.properties");
    if (!file.exists()) {
      assertTrue(file.createNewFile());
    }
    // Create an empty file. We rely on /opt/matterhorn/.mhruntime.cf to load properties that we verify
    System.setProperty("felix.config.properties", file.getAbsolutePath());
  }

}
