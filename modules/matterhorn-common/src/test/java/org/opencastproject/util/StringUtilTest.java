/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.apache.commons.lang.StringUtils;
import org.opencastproject.util.env.Environment;

import org.junit.Test;

import java.util.Properties;

/**
 * @author John Crossman
 */
public class StringUtilTest {

  @Test
  public void testSubstringAfterLast() {
    assertEquals("World", StringUtils.substringAfterLast("   Hello World", " "));
    assertEquals("World", StringUtils.substringAfterLast("  World", " "));
  }

  @Test
  public void testToStringProperties() {
    final Properties p = getPropertiesContainingPassword();
    assertPropertiesPresent(StringUtil.toString(p), true);
  }

  @Test
  public void testToStringBean() {
    final Bean bean = new Bean(100, getPropertiesContainingPassword());
    final String toString = StringUtil.toStringObject(bean);
    assertPropertiesPresent(toString, false);
    assertContains(toString, "100");
  }

  @Test
  public void testToStringNull() {
    assertContains(StringUtil.toStringObject(null), "null");
  }

  @Test
  public void testToStringEnum() {
    assertContains(StringUtil.toStringObject(Environment.local), "local");
  }

  @Test
  public void testToStringInteger() {
    assertContains(StringUtil.toStringObject(3), "3");
  }

  @Test
  public void testToStringFloat() {
    assertContains(StringUtil.toStringObject(new Float("3.452")), "3.452");
  }

  @Test
  public void testToStringString() {
    final String content = "The rain in spain stays mainly in the plain.";
    assertContains(StringUtil.toStringObject(content), content);
  }

  private void assertContains(final String str, final String searchForThis) {
    assertTrue("[WRONG]:" + str, str.contains(searchForThis));
  }

  private Properties getPropertiesContainingPassword() {
    final Properties p = new Properties();
    p.put("key1", "value1");
    p.put("myPassword", "bazooka");
    return p;
  }

  private void assertPropertiesPresent(final String toString, final boolean expectCensoredPassword) {
    final String errorMessage = "[WRONG]:" + toString;
    assertTrue(errorMessage, toString.contains("key1"));
    assertTrue(errorMessage, toString.contains("value1"));
    assertTrue(errorMessage, toString.contains("myPassword"));
    final String password = "bazooka";
    final boolean expectTrue = expectCensoredPassword ? !toString.contains(password) : toString.contains(password);
    assertTrue(errorMessage, expectTrue);
  }

  private final class Bean {

    private final int number;
    private final Properties p;

    private Bean(final int number, final Properties p) {
      this.number = number;
      this.p = p;
    }

    private int getNumber() {
      return number;
    }

    private Properties getP() {
      return p;
    }
  }
}
