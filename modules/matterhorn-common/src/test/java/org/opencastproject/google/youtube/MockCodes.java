/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.google.youtube;

class MockCodes implements HasStatusCodes {

  private final Integer statusCode;
  private final Integer jsonCode;

  MockCodes(final Integer statusCode, final Integer jsonCode) {
    this.statusCode = statusCode;
    this.jsonCode = jsonCode;
  }

  @Override
  public Integer getJsonCode() {
    return jsonCode;
  }

  @Override
  public Integer getResponseStatus() {
    return statusCode;
  }

}
