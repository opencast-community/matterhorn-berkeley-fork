/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.workflow.handler;

import org.junit.Test;
import org.opencastproject.metadata.dublincore.DublinCore;
import org.opencastproject.metadata.dublincore.DublinCoreCatalog;
import org.opencastproject.metadata.dublincore.DublinCoreValue;
import org.opencastproject.publication.api.PublisherNamespace;

import java.util.LinkedList;
import java.util.List;

import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

/**
 * @author John Crossman
 */
public class YouTubeWorkspaceUtilsTest {

  @Test
  public void testExtractYouTubePlaylistIdNullCatalog() {
    assertNull(YouTubeWorkspaceUtils.extractYouTubePlaylistId(null));
  }

  @Test
  public void testExtractYouTubePlaylistIdUnpublished() {
    final DublinCoreCatalog catalog = createMock(DublinCoreCatalog.class);
    expect(catalog.get(DublinCore.PROPERTY_PUBLISHER)).andReturn(null);
    replay(catalog);
    assertNull(YouTubeWorkspaceUtils.extractYouTubePlaylistId(catalog));
  }

  @Test
  public void testExtractYouTubePlaylistId() {
    final DublinCoreCatalog catalog = createMock(DublinCoreCatalog.class);
    final List<DublinCoreValue> publishers = new LinkedList<DublinCoreValue>();
    final String playlistId = "PLlj55pSgE2t6xEIl__m6grJUrnxSVzssR";
    final DublinCoreValue dublinCore = new DublinCoreValue(PublisherNamespace.YOUTUBE.encodePlayListAsPublisherUrn(playlistId));
    publishers.add(dublinCore);
    expect(catalog.get(DublinCore.PROPERTY_PUBLISHER)).andReturn(publishers);
    replay(catalog);
    assertEquals(playlistId, YouTubeWorkspaceUtils.extractYouTubePlaylistId(catalog));
  }

  @Test
  public void testExtractNullYouTubePlaylistId() {
    assertExtractNullYouTubePlaylistId(" null ");
    assertExtractNullYouTubePlaylistId("NULL");
    try {
      assertExtractNullYouTubePlaylistId(null);
      fail("Expected " + IllegalStateException.class);
    } catch (final IllegalStateException e) {
      // All good
    }
  }

  private void assertExtractNullYouTubePlaylistId(final String nullValue) {
    final DublinCoreCatalog catalog = createMock(DublinCoreCatalog.class);
    final List<DublinCoreValue> publishers = new LinkedList<DublinCoreValue>();
    final DublinCoreValue dublinCore = new DublinCoreValue(PublisherNamespace.YOUTUBE.encodePlayListAsPublisherUrn(nullValue));
    publishers.add(dublinCore);
    expect(catalog.get(DublinCore.PROPERTY_PUBLISHER)).andReturn(publishers);
    replay(catalog);
    assertNull("Non-null return value when urn = " + nullValue, YouTubeWorkspaceUtils.extractYouTubePlaylistId(catalog));
  }

}
