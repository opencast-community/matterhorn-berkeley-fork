/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.workflow.handler;

import static org.mockito.Mockito.doReturn;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import org.opencastproject.job.api.JobContext;
import org.opencastproject.mediapackage.MediaPackage;
import org.opencastproject.metadata.dublincore.DublinCore;
import org.opencastproject.metadata.dublincore.DublinCoreCatalog;
import org.opencastproject.metadata.dublincore.DublinCoreCatalogImpl;
import org.opencastproject.workflow.api.WorkflowInstance;
import org.opencastproject.workflow.api.WorkflowOperationException;
import org.opencastproject.workflow.api.WorkflowOperationResult;

import junitparams.JUnitParamsRunner;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Ensure that <code>PublishDelayedWorkflowOperationHandler</code>'s <code>start</code> method processes publish delay
 * events correctly.
 * 
 * @author Fernando Alvarez
 * 
 */

@RunWith(JUnitParamsRunner.class)
public class PublishDelayedWorkflowOperationHandlerTest {
  private PublishDelayedWorkflowOperationHandler handler;
  private WorkflowInstance workflowInstance;

  @Before
  public void setUp() {
    handler = spy(new PublishDelayedWorkflowOperationHandler());
    workflowInstance = mock(WorkflowInstance.class);
    when(workflowInstance.getMediaPackage()).thenReturn(mock(MediaPackage.class));
  }

  @Test
  public void testStartWithFiveDayDelay() throws WorkflowOperationException {
    DublinCoreCatalog cat = createDublinCoreCatalog("5");
    doReturn(cat).when(handler).findDublinCoreCatalog(any(MediaPackage.class));
    WorkflowOperationResult result = handler.start(workflowInstance, mock(JobContext.class));
    assertThat(result.getAction()).isEqualTo(WorkflowOperationResult.Action.PAUSE);
  }

  @Test
  public void testStartWithNoDayDelay() throws WorkflowOperationException {
    DublinCoreCatalog cat = createDublinCoreCatalog("0");
    doReturn(cat).when(handler).findDublinCoreCatalog(any(MediaPackage.class));
    WorkflowOperationResult result = handler.start(workflowInstance, mock(JobContext.class));
    assertThat(result.getAction()).isEqualTo(WorkflowOperationResult.Action.CONTINUE);
  }
  
  @Test
  public void testStartWithNullDelay() throws WorkflowOperationException {
    DublinCoreCatalog cat = createDublinCoreCatalog(null);
    doReturn(cat).when(handler).findDublinCoreCatalog(any(MediaPackage.class));
    WorkflowOperationResult result = handler.start(workflowInstance, mock(JobContext.class));
    assertThat(result.getAction()).isEqualTo(WorkflowOperationResult.Action.CONTINUE);
  }

  private static DublinCoreCatalog createDublinCoreCatalog(String publishDelayDays) {
    final DublinCoreCatalogImpl d = DublinCoreCatalogImpl.newInstance();
    if (publishDelayDays != null) {
      int days = Integer.parseInt(publishDelayDays);
      Calendar publishDelayedDate = Calendar.getInstance();
      publishDelayedDate.setTime(new Date());
      publishDelayedDate.add(Calendar.DATE, days);
      SimpleDateFormat availableDF = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
      d.set(DublinCore.PROPERTY_AVAILABLE, "start=" + availableDF.format(publishDelayedDate.getTime()));
    }
    return d;
  }

}
