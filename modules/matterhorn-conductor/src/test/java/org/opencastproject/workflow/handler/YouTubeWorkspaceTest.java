/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.workflow.handler;

import org.junit.Test;
import org.opencastproject.google.youtube.GoogleUtils;
import org.opencastproject.google.youtube.HasYouTubePrivacyStatus;
import org.opencastproject.google.youtube.YouTubePrivacyStatus;
import org.opencastproject.mediapackage.MediaPackage;
import org.opencastproject.mediapackage.MediaPackageBuilder;
import org.opencastproject.mediapackage.MediaPackageBuilderImpl;
import org.opencastproject.mediapackage.MediaPackageException;
import org.opencastproject.metadata.dublincore.DublinCore;
import org.opencastproject.metadata.dublincore.DublinCoreCatalogImpl;
import org.opencastproject.security.api.RecordingAccessRights;

import java.io.InputStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;

/**
 * @author John Crossman
 */
public class YouTubeWorkspaceTest {

  private final YouTubeWorkspace youTubeWorkspace;

  public YouTubeWorkspaceTest() {
    youTubeWorkspace = new YouTubeWorkspace(null);
  }

  @Test
  public void testGetVideoId() throws MediaPackageException {
    final InputStream inputStream = this.getClass().getResourceAsStream("/media-package-statistics-20.xml");
    final MediaPackageBuilder builder = new MediaPackageBuilderImpl();
    final MediaPackage mediaPackage = builder.loadFromXml(inputStream);
    final String videoId = youTubeWorkspace.getVideoId(mediaPackage);
    assertNotNull(videoId);
    assertEquals("WJWG0jsSzYE", videoId);
  }

  @Test
  public void testGetNullVideoId() throws MediaPackageException {
    final InputStream inputStream = this.getClass().getResourceAsStream("/media-package-statistics-20-without-youtube.xml");
    final MediaPackageBuilder builder = new MediaPackageBuilderImpl();
    final MediaPackage mediaPackage = builder.loadFromXml(inputStream);
    final String videoId = youTubeWorkspace.getVideoId(mediaPackage);
    assertNull(videoId);
  }

  @Test
  public void testGetVideoIdWhenUnpublised() throws MediaPackageException {
    final InputStream inputStream = this.getClass().getResourceAsStream("/media-package-statistics-20-no-publications.xml");
    final MediaPackageBuilder builder = new MediaPackageBuilderImpl();
    final MediaPackage mediaPackage = builder.loadFromXml(inputStream);
    final String videoId = youTubeWorkspace.getVideoId(mediaPackage);
    assertNull(videoId);
  }

  @Test
  public void testGetPrivacyStatusPublicAccessRights() {
    final MyDublinCoreCatalogImpl dcSeries = new MyDublinCoreCatalogImpl();
    assertSame(GoogleUtils.ACCESS_RIGHTS_DEFAULT.getYouTubePrivacyStatus(), youTubeWorkspace.getPrivacyStatus(dcSeries));
    dcSeries.add(DublinCore.PROPERTY_ACCESS_RIGHTS, RecordingAccessRights.publicAccessRights.name());
    assertSame(YouTubePrivacyStatus.publicStatus, youTubeWorkspace.getPrivacyStatus(dcSeries));
  }

  @Test
  public void testGetPrivacyStatusStudentsOnlyAccessRights() {
    final MyDublinCoreCatalogImpl dcSeries = new MyDublinCoreCatalogImpl();
    dcSeries.add(DublinCore.PROPERTY_ACCESS_RIGHTS, RecordingAccessRights.studentsOnlyAccessRights.name());
    assertSame(YouTubePrivacyStatus.privateStatus, youTubeWorkspace.getPrivacyStatus(dcSeries));
  }

  @Test
  public void testGetPrivacyStatusTrimValue() {
    final MyDublinCoreCatalogImpl dcSeries = new MyDublinCoreCatalogImpl();
    dcSeries.add(DublinCore.PROPERTY_ACCESS_RIGHTS, " STUDENTSONLYACCESSRIGHTS  ");
    assertSame(YouTubePrivacyStatus.privateStatus, youTubeWorkspace.getPrivacyStatus(dcSeries));
  }

  @Test
  public void testGetPrivacyStatusDefault() {
    final HasYouTubePrivacyStatus defaultPrivacyStatus = GoogleUtils.ACCESS_RIGHTS_DEFAULT.getYouTubePrivacyStatus();
    assertSame(defaultPrivacyStatus, youTubeWorkspace.getPrivacyStatus(null));
    final MyDublinCoreCatalogImpl dcSeries = new MyDublinCoreCatalogImpl();
    assertSame(defaultPrivacyStatus, youTubeWorkspace.getPrivacyStatus(dcSeries));
    dcSeries.add(DublinCore.PROPERTY_ACCESS_RIGHTS, "foo");
    assertSame(defaultPrivacyStatus, youTubeWorkspace.getPrivacyStatus(dcSeries));
  }

  private class MyDublinCoreCatalogImpl extends DublinCoreCatalogImpl {
    MyDublinCoreCatalogImpl() {
    }
  }
}
