/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.schedulableservice.impl;

import static org.joda.time.DateTimeConstants.MILLIS_PER_MINUTE;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Ignore;
import org.opencastproject.schedulableservice.api.Periods;
import org.opencastproject.schedulableservice.api.SchedulableService;
import org.opencastproject.schedulableservice.api.SchedulableServiceService;
import org.opencastproject.security.api.SecurityService;

import org.joda.time.DateTime;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.osgi.framework.BundleContext;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.component.ComponentContext;
import org.quartz.SchedulerException;

import java.util.Properties;

/**
 * A suite of tests focusing on testing that a {@link #schedulableService} is called every {@link Periods}
 * by a {@link org.quartz.Scheduler}-based process.
 * <p>
 * Coverage: 
 * <p><ul>
 * <li>{@link SchedulableServiceServiceImpl#activate}
 * <li>{@link SchedulableServiceServiceImpl#scheduleServiceEvent}
 * <li>{@link SchedulableServiceServiceImpl#updatedConfiguration}
 * <ul><p>
 */
@Ignore
@RunWith(MockitoJUnitRunner.class)
public class SchedulableServiceServiceImplTest {
  private SchedulableServiceServiceImpl impl;
  private SchedulableServiceService schedulableServiceService;
  
  @Mock private Properties props;
  @Mock private SecurityService securityService;
  @Mock private ComponentContext componentContext;
  @Mock private BundleContext bundleContext;
  @Mock private SchedulableService schedulableService;
  
  /**
   * {@code schedulableService} will be called back immediately, then a minute later, for a total of two calls.
   */
  @Test
  public void schedulableServiceCalledTwice() throws InterruptedException {
    when(componentContext.getBundleContext()).thenReturn(bundleContext);
    when(bundleContext.getProperty(anyString())).thenReturn("systemUserName");
    System.out.println("Starting test at " + new DateTime().getHourOfDay() + ":" + new DateTime().getMinuteOfHour());
    schedulableServiceService.scheduleServiceEvent(schedulableService, "jobName", "userName", Periods.MINUTE);
    Thread.sleep(1 * MILLIS_PER_MINUTE);
    impl.shutdown();
    verify(schedulableService, times(2)).callBack();
  }
  
  /**
   * {@code ComponentContext} cannot be null.
   */
  @Test(expected = NullPointerException.class)
  public void cannotActivateWithNullComponentContext() throws SchedulerException {
    impl.activate(null);
  }

  /**
   * {@code Properties} can be empty but not null.
   */
  @Test(expected = ConfigurationException.class)
  public void updatedConfigurationWithInvalidDataThrowsConfigurationException() throws ConfigurationException {
    impl.updatedConfiguration(null);
  }
  
  @Before
  public void instantiateImplAndInjectDependencies() throws Exception {
    impl = new SchedulableServiceServiceImpl();
    impl.updatedConfiguration(props);
    impl.setSecurityService(securityService);
    impl.activate(componentContext);
    schedulableServiceService = impl;
  }

  @After
  public void tearDown() throws Exception {
    impl.shutdown();
  }
}
