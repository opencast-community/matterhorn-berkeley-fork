/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.publishdelayed.impl;

import org.opencastproject.job.api.EManagedService;
import org.opencastproject.mediapackage.Catalog;
import org.opencastproject.mediapackage.MediaPackage;
import org.opencastproject.mediapackage.MediaPackageElements;
import org.opencastproject.metadata.dublincore.DCMIPeriod;
import org.opencastproject.metadata.dublincore.DublinCore;
import org.opencastproject.metadata.dublincore.DublinCoreCatalog;
import org.opencastproject.metadata.dublincore.DublinCoreCatalogImpl;
import org.opencastproject.metadata.dublincore.EncodingSchemeUtils;
import org.opencastproject.publishdelayed.api.PublishDelayedService;
import org.opencastproject.schedulableservice.api.Periods;
import org.opencastproject.schedulableservice.api.SchedulableService;
import org.opencastproject.schedulableservice.api.SchedulableServiceService;
import org.opencastproject.workflow.api.WorkflowInstance;
import org.opencastproject.workflow.api.WorkflowOperationException;
import org.opencastproject.workflow.api.WorkflowQuery;
import org.opencastproject.workflow.api.WorkflowService;
import org.opencastproject.workflow.api.WorkflowSet;
import org.opencastproject.workspace.api.Workspace;

import org.apache.commons.io.IOUtils;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;

/**
 * Implements Publish Delayed service.
 * 
 * @author Fernando Alvarez
 * 
 */
public class PublishDelayedServiceImpl extends EManagedService implements PublishDelayedService, SchedulableService {

  protected synchronized void activate(ComponentContext cc) throws Exception {
    logger.info("Activating PublishDelayedServiceImpl");
  }

  /**
   * @see org.opencastproject.publishdelayed.api.PublishDelayedService#listPublishDelayedWorkflows()
   */
  public List<WorkflowInstance> listPublishDelayedWorkflows() {
    WorkflowQuery query = new WorkflowQuery();
    List<WorkflowInstance> listPublishDelayedWorkflows = new ArrayList<WorkflowInstance>();

    try {
      WorkflowSet workflowInstances = workflowService.getWorkflowInstances(query
              .withCurrentOperation(PUBLISH_DELAYED_OPERATION_ID).withCount(COUNT).withStartPage(0));

      listPublishDelayedWorkflows.addAll(Arrays.asList(workflowInstances.getItems()));
    } catch (Exception e) {
      e.printStackTrace();
      throw new IllegalStateException("Searching for publish delayed workflows throws " + e.getMessage());
    }
    return listPublishDelayedWorkflows;
  }

  /**
   * @see org.opencastproject.publishdelayed.api.PublishDelayedService#countPublishDelayedWorkflows()
   */
  public int countPublishDelayedWorkflows() {
    return listPublishDelayedWorkflows().size();
  }

  /**
   * @see org.opencastproject.publishdelayed.api.PublishDelayedService#resumePublishDelayedWorkflows(java.util.List,
   *      java.util.Date)
   */
  public int resumePublishDelayedWorkflows(List<WorkflowInstance> workFlows, Date date) {
    int tallyResumedFlows = 0;
    int tallyFailedResumedFlows = 0;
    final Calendar cutOff = Calendar.getInstance();
    final Date cutOffDate = (date == null) ? new Date() : date;
    cutOff.setTime(cutOffDate);

    for (WorkflowInstance wf : workFlows) {
      try {
        // this catalog contains the metadata we need
        final DublinCoreCatalog dc = findDublinCoreCatalog(wf.getMediaPackage());
        final DCMIPeriod availablePeriod = EncodingSchemeUtils.decodePeriod(dc.getFirst(DublinCore.PROPERTY_AVAILABLE));
        final Calendar publishDate = Calendar.getInstance();
        publishDate.setTime(availablePeriod.getStart());

        if (!publishDate.after(cutOff)) {
          logger.debug("Resuming {} for publishing", wf.getId());
          workflowService.resume(wf.getId());
          tallyResumedFlows++;
        }
      } catch (Throwable e) {
        // The fact that one of the workflows has a problem should not stop
        // the processing of the rest
        logger.debug("Resuming {} for publishing failed - {}, skipping", wf.getId(), e.getMessage());
        tallyFailedResumedFlows++;
      }
    }
    logger.debug("Completed evaluating " + workFlows.size() + " workflows, resuming " + tallyResumedFlows
            + " for publishing, with " + tallyFailedResumedFlows + " errors");
    return tallyResumedFlows;
  }

  @Override
  public void callBack() {
    logger.debug("Calling PublishDelayedService");
    resumePublishDelayedWorkflows(listPublishDelayedWorkflows(), null);

  }

  @Override
  public void updatedConfiguration(final Properties properties) throws ConfigurationException {
    logger.info("Updating configuration for PublishDelayedServiceImpl");
    // Gets the Period value from the PublishDelayedServiceImpl.properties file
    // Default to Daily processing if property not set or otherwise invalid
    String str = (String) properties.get(PROPKEY_PERIOD);
    schedulingPeriod = Periods.getPeriod(str);
    
    // Schedule this service to be invoked periodically; call placed here to 
    // ensure that we have the parms necessary to make the call before doing so
    schedulableServiceService.scheduleServiceEvent(this, "PublishDelayedService", "PublishDelayedService",
            schedulingPeriod);
  }

  private DublinCoreCatalog findDublinCoreCatalog(MediaPackage mediaPackage) throws WorkflowOperationException {
    DublinCoreCatalog dc = null;

    // There could be multiple catalogs, one per episode; we'll assume the last
    // (i.e. most recent episode) is the one we want
    for (Catalog catalog : mediaPackage.getCatalogs(MediaPackageElements.EPISODE)) {
      dc = parseDublinCoreCatalog(catalog, workspace);
    }
    if (dc == null) {
      throw new WorkflowOperationException("The media package does not contain a Dublin Core Catatlog");
    }
    return dc;
  }

  // TODO is there a better way to get the DublinCoreCatalog from a media package?
  private DublinCoreCatalog parseDublinCoreCatalog(Catalog catalog, Workspace workspace)
          throws WorkflowOperationException {
    InputStream is = null;
    try {
      File dcFile = workspace.get(catalog.getURI());
      is = new FileInputStream(dcFile);
      return new DublinCoreCatalogImpl(is);
    } catch (Exception e) {
      throw new WorkflowOperationException("Error reading Dublin Core Catatlog file");
    } finally {
      IOUtils.closeQuietly(is);
    }
  }

  // Inject reference to Workspace service
  private Workspace workspace;

  public void setWorkspace(Workspace workspace) {
    this.workspace = workspace;
  }

  // Inject reference to Workflow service
  private WorkflowService workflowService;

  public void setWorkflowService(WorkflowService workflowService) {
    this.workflowService = workflowService;
  }

  // Inject reference to SchedulableService service
  private SchedulableServiceService schedulableServiceService;

  public void setSchedulableServiceService(SchedulableServiceService schedulableServiceService) {
    this.schedulableServiceService = schedulableServiceService;
  }

  private static final Logger logger = LoggerFactory.getLogger(PublishDelayedServiceImpl.class);

  // How often this service should be called by a scheduled job
  private Periods schedulingPeriod;
  private static final String PROPKEY_PERIOD = "org.opencastproject.publishdelayed.impl.PublishDelayedServiceImpl.schedulingPeriod";

  // A Publish Delayed workflow is one whose current operation id is publish-delayed
  private static final String PUBLISH_DELAYED_OPERATION_ID = "publish-delayed";

  // Unless a count is provided in a workflow query, a count of 20 is assumed
  // by the WorkFlow Service, potentially causing this service to miss processing
  // some workflows
  private static final int COUNT = 999;
}
