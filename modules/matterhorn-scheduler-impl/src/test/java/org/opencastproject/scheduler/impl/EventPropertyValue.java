/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.scheduler.impl;

import org.opencastproject.mediapackage.EName;
import org.opencastproject.metadata.dublincore.DublinCore;
import org.opencastproject.metadata.dublincore.DublinCoreCatalog;
import org.opencastproject.metadata.dublincore.DublinCoreCatalogImpl;

import org.apache.commons.lang.StringUtils;

/**
 * A value bound to an {@code EventProperty}.
 * 
 * @author Fernando Alvarez
 *
 */
public final class EventPropertyValue {
  private final EventProperty eventProperty;
  private final String value;
  
  public enum EventProperty {
    DATE_RANGE(DublinCore.PROPERTY_TEMPORAL),
    TIMEZONE(DublinCoreCatalogImpl.PROPERTY_AGENT_TIMEZONE),
    EVENT_TITLE(DublinCore.PROPERTY_TITLE),
    AGENT(DublinCore.PROPERTY_SPATIAL),
    SERIES_ID(DublinCore.PROPERTY_IS_PART_OF),
    PUBLISH_DELAY(DublinCoreCatalogImpl.PROPERTY_AVAILABLE),
    RECURRENCE(DublinCoreCatalogImpl.PROPERTY_RECURRENCE);

    private final EName eName;

    private EventProperty(EName eName) {
      this.eName = eName;
    }

    private String getENameLocalName() {
      return StringUtils.upperCase(eName.getLocalName());
    }

    private EName geteName() {
      return eName;
    }
  }
  
/**
 * Returns the value of a property as read from a specific event.
 * 
 * @param eventProperty  enum which identifies the property
 * @param event  event that contains the value for the property
 * @return  value for a property
 */
  public static EventPropertyValue newInstance(EventProperty eventProperty, DublinCoreCatalog event) {
    final String value = event.getFirst(eventProperty.geteName());
    return newInstance(eventProperty, value);
  }
  
  /**
   * Returns the value provided bound to a event property.
   * 
   * @param eventProperty  enum which identifies the property
   * @param value  value
   * @return  value for a property
   */
  public static EventPropertyValue newInstance(EventProperty eventProperty, String value) {
    return new EventPropertyValue(eventProperty, value);
  }
  
  public String getDescription() {
    return String.format("%s: %s " , eventProperty.name(), eventProperty.getENameLocalName());
  }
  
  private EventPropertyValue(EventProperty eventProperty, String value) {
    this.eventProperty = eventProperty;
    this.value = value;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append(value);
    return builder.toString();
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((eventProperty == null) ? 0 : eventProperty.hashCode());
    result = prime * result + ((value == null) ? 0 : value.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (!(obj instanceof EventPropertyValue)) {
      return false;
    }
    EventPropertyValue other = (EventPropertyValue) obj;
    if (eventProperty != other.eventProperty) {
      return false;
    }
    if (value == null) {
      if (other.value != null) {
        return false;
      }
    } else if (!value.equals(other.value)) {
      return false;
    }
    return true;
  }
}
