/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.scheduler.impl;

import org.opencastproject.metadata.dublincore.DCMIPeriod;
import org.opencastproject.metadata.dublincore.DublinCore;
import org.opencastproject.metadata.dublincore.DublinCoreCatalog;
import org.opencastproject.metadata.dublincore.DublinCoreCatalogImpl;
import org.opencastproject.metadata.dublincore.EncodingSchemeUtils;
import org.opencastproject.scheduler.impl.EventPropertyValue.EventProperty;

import org.apache.commons.lang.StringUtils;
import org.fest.assertions.Assertions;
import org.fest.assertions.GenericAssert;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Interval;

/**
 * Assertions for {@code DublinCoreCatalog}-based events.
 * 
 * @author Fernando Alvarez
 *
 */
public final class EventAssert extends GenericAssert<EventAssert, DublinCoreCatalog> {

  protected EventAssert(DublinCoreCatalog actual) {
    super(EventAssert.class, actual);
  }
  
  public static EventAssert assertThat(DublinCoreCatalog actual) {
    return new EventAssert(actual);
  }
  
  /**
   * Two event's are equal if their properties are equal.
   *  
   * @param expected  {@code DublinCoreCatalog} for the expected event values
   * @return  this {@code EventAssert}
   */
  public EventAssert hasSamePropertyValuesAs(DublinCoreCatalog expected) {
    isNotNull();
    for (EventProperty eventProperty : EventProperty.values()) {
      EventPropertyValue expectedValue = EventPropertyValue.newInstance(eventProperty, expected);
      assertEqualsActual(eventProperty, expectedValue);
    }
    return this;
  }
  
  /**
   * Compares the value of an event's property to a value.
   * 
   * @param eventProperty
   * @param value  value to be compared to
   * @return  this {@code EventAssert}
   */
  public EventAssert hasSame(EventProperty eventProperty, String value) {
    isNotNull();
    EventPropertyValue expectedValue = EventPropertyValue.newInstance(eventProperty, value);
    assertEqualsActual(eventProperty, expectedValue);
    return this;
  }
  // The temporal assertion code has too much knowledge of DublinCoreCatalog, and so should not really be here.
  // However, there is no other more appropriate class in the test framework and I don't want to create a test utility class.
  public EventAssert occursWithinTemporalRangeOf(DublinCoreCatalog expected) {
    isNotNull();
    final String localName = StringUtils.upperCase(DublinCore.PROPERTY_TEMPORAL.getLocalName());
    
    if (!actual.hasValue(DublinCore.PROPERTY_TEMPORAL)) {
      fail(String.format("Actual %s does not contain valid temporal data", localName)); 
    }
    
    if (!expected.hasValue(DublinCore.PROPERTY_TEMPORAL)) {
      fail(String.format("Expected %s does not contain temporal data", localName));
    } 
    
    final Interval actualInterval = getTemporalInterval(actual);
    final Interval expectedInterval = getTemporalInterval(expected);
    org.assertj.jodatime.api.Assertions.assertThat(actualInterval.getStart()).isAfterOrEqualTo(expectedInterval.getStart());
    org.assertj.jodatime.api.Assertions.assertThat(actualInterval.getEnd()).isBeforeOrEqualTo(expectedInterval.getEnd());
    return this;
  }
  
  private Interval getTemporalInterval(DublinCoreCatalog catalog) {
    DateTimeZone tz = DateTimeZone.getDefault();
    final DCMIPeriod period = EncodingSchemeUtils.decodeMandatoryPeriod(catalog.getFirst(DublinCore.PROPERTY_TEMPORAL));

    if (catalog.hasValue(DublinCoreCatalogImpl.PROPERTY_AGENT_TIMEZONE)) {
      tz = DateTimeZone.forID(catalog.getFirst(DublinCoreCatalogImpl.PROPERTY_AGENT_TIMEZONE));
    }
    final DateTime startDate = new DateTime(period.getStart(), tz);
    final DateTime endDate = new DateTime(period.getEnd(), tz);
    return new Interval(startDate, endDate);
  }
  
  private void assertEqualsActual(EventProperty eventProperty, EventPropertyValue expectedValue) {
    EventPropertyValue actualValue = EventPropertyValue.newInstance(eventProperty, actual);
    final String message = String.format("%s expected: <%s>, actual: <%s>", expectedValue.getDescription(), expectedValue, actualValue);
    Assertions.assertThat(actualValue).overridingErrorMessage(message).isEqualTo(expectedValue);
  }
}
