/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.series.canvas;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.opencastproject.metadata.dublincore.DublinCoreCatalog;
import org.opencastproject.util.env.EnvironmentUtil;
import org.osgi.framework.BundleContext;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author John Crossman
 */
public final class LearningToolIntegration {

  /**
   * Canvas/LTI integration. These are the tokens we replace in {@link #CARTRIDGE_BASIC_LTI_XML}.
   */
  private enum CartridgeToken {
    series_title, series_description, playback_base_url, series_id, consumer_key, domain, course_navigation_label, ;
  }

  private static final String CARTRIDGE_BASIC_LTI_XML = "etc/canvas_integration/cartridge_basic_lti.xml";
  private final String label;
  private final String consumerKey;
  private final String sharedSecret;

  public static LearningToolIntegration newInstance(final BundleContext bundleContext) {
    final String keyPrefixLTI = "edu.berkeley.lti";
    final String labelLTI = StringUtils.trimToNull(bundleContext.getProperty(keyPrefixLTI + ".label"));
    final String consumerKeyLTI = StringUtils.trimToNull(bundleContext.getProperty(keyPrefixLTI + ".consumerKey"));
    final String sharedSecretLTI = StringUtils.trimToNull(bundleContext.getProperty(keyPrefixLTI + ".sharedSecret"));
    return new LearningToolIntegration(labelLTI, consumerKeyLTI, sharedSecretLTI);
  }

  private LearningToolIntegration(final String label, final String consumerKey, final String sharedSecret) {
    this.label = label;
    this.consumerKey = consumerKey;
    this.sharedSecret = sharedSecret;
  }

  public LTICartridge getLTICartridge(final DublinCoreCatalog series) {
    return new LTICartridge(series, label, consumerKey);
  }

  public String getSharedSecret() {
    return sharedSecret;
  }

  public String getLTICartridgeXMLAsString(final LTICartridge cartridge) throws IOException {
    final File file = new File(EnvironmentUtil.getMatterhornHome(), CARTRIDGE_BASIC_LTI_XML);
    final String cartridgeTemplate = FileUtils.readFileToString(file);
    return replaceAll(cartridgeTemplate, getLTICartridgeTokenMap(cartridge));
  }

  private String replaceAll(String template, final Map<CartridgeToken, String> map) {
    for (final CartridgeToken key : map.keySet()) {
      template = template.replaceAll("\\{" + key.name() + "\\}", map.get(key));
    }
    return template;
  }

  private Map<CartridgeToken, String> getLTICartridgeTokenMap(final LTICartridge cartridge) throws IOException {
    final Map<CartridgeToken, String> map = new HashMap<CartridgeToken, String>();
    put(map, CartridgeToken.series_id, cartridge.getSeriesId(), false);
    put(map, CartridgeToken.series_title, cartridge.getSeriesTitle(), false);
    put(map, CartridgeToken.series_description, cartridge.getSeriesDescription(), true);
    put(map, CartridgeToken.playback_base_url, cartridge.getPlaybackBaseURL(), false);
    put(map, CartridgeToken.consumer_key, cartridge.getConsumerKey(), false);
    put(map, CartridgeToken.domain, cartridge.getDomain(), false);
    put(map, CartridgeToken.course_navigation_label, cartridge.getCanvasNavigationLabel(), false);
    return map;
  }

  private void put(final Map<CartridgeToken, String> map, CartridgeToken token, final String value, final boolean tolerateNull) {
    if (value == null && !tolerateNull) {
      throw new IllegalArgumentException("Basic LTI integration does not allow NULL value for " + token.name());
    }
    map.put(token, StringUtils.trimToEmpty(value));
  }

}
