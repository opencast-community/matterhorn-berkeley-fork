/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.series.canvas;

import org.apache.commons.lang.StringUtils;
import org.json.simple.JSONObject;
import org.opencastproject.metadata.dublincore.DublinCore;
import org.opencastproject.metadata.dublincore.DublinCoreCatalog;
import org.opencastproject.util.env.EnvironmentUtil;

/**
 * Canvas/LTI integration.
 * @author John Crossman
 */
public class LTICartridge {

  private final String seriesTitle;
  private final String seriesDescription;
  private final String playbackBaseURL;
  private final String seriesId;
  private final String domain;
  private final String canvasNavigationLabel;
  private final String consumerKey;


  public LTICartridge(final DublinCoreCatalog series, final String canvasNavigationLabel, final String consumerKey) {
    this.canvasNavigationLabel = canvasNavigationLabel;
    this.consumerKey = consumerKey;
    seriesTitle = series.getFirst(DublinCore.PROPERTY_TITLE);
    seriesDescription = series.getFirst(DublinCore.PROPERTY_DESCRIPTION);
    final String baseURL = EnvironmentUtil.getEngageBaseURL(EnvironmentUtil.getEnvironment());
    // The playback URL will load within a secure page (eg, bCourses iframe) we it must be https
    playbackBaseURL = StringUtils.replace(baseURL, "http://", "https://");
    seriesId = series.getFirst(DublinCore.PROPERTY_IDENTIFIER);
    domain = EnvironmentUtil.getMatterhornDomain();
  }

  String getSeriesTitle() {
    return seriesTitle;
  }

  String getSeriesDescription() {
    return seriesDescription;
  }

  String getPlaybackBaseURL() {
    return playbackBaseURL;
  }

  String getSeriesId() {
    return seriesId;
  }

  String getConsumerKey() {
    return consumerKey;
  }

  String getDomain() {
    return domain;
  }

  String getCanvasNavigationLabel() {
    return canvasNavigationLabel;
  }

  public String toJson() {
    final JSONObject json = new JSONObject();
    json.put("seriesTitle", seriesTitle);
    json.put("seriesDescription", seriesDescription);
    json.put("playbackBaseURL", playbackBaseURL);
    json.put("seriesId", seriesId);
    json.put("domain", domain);
    json.put("canvasNavigationLabel", canvasNavigationLabel);
    json.put("consumerKey", consumerKey);
    return json.toString();
  }

}
