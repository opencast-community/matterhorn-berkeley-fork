/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */

package org.opencastproject.publication.api;

import org.apache.commons.lang.StringUtils;


/**
 * Domain for prefixes used to differentiate publishers.
 *
 */
public enum PublisherNamespace {

  YOUTUBE("urn:youtube:com:playlistId:"),
  ITUNES_AUDIO("urn:itunes:com:audio:collectionId:"),
  ITUNES_VIDEO("urn:itunes:com:video:collectionId:"); 

  private String urn;
  
  private PublisherNamespace(final String urn) {
    this.urn = urn;
  }
  
  public String encodePlayListAsPublisherUrn(final String playlistId) {
    if (playlistId == null) {
      throw new IllegalStateException("YouTube playlist object has null id.");
    }
    return urn + playlistId;
  }
  
  public String parsePlayListFromPublisherUrn(final String encodedPlayListId) {
    final String trimmed = StringUtils.trimToNull(encodedPlayListId);
    return trimmed == null ? null : StringUtils.remove(trimmed, urn);
  }

  public boolean isPublisher(final String encodedPlayListId) {
    return StringUtils.startsWith(encodedPlayListId, urn);
  }

}
