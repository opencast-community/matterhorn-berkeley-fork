/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.episode.endpoint;

import org.easymock.EasyMock;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.opencastproject.episode.EpisodeServiceTestEnv;
import org.opencastproject.episode.api.EpisodeService;
import org.opencastproject.episode.api.JaxbSearchResult;
import org.opencastproject.mediapackage.MediaPackage;
import org.opencastproject.security.api.SecurityService;
import org.opencastproject.workflow.api.WorkflowService;

import javax.ws.rs.core.Response;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.opencastproject.mediapackage.MediaPackageSupport.loadFromClassPath;

public class EpisodeServiceRestEndpointTest {

  private EpisodeServiceTestEnv env;
  private AbstractEpisodeServiceRestEndpoint rest;

  @SuppressWarnings("unchecked")
  @Before
  public void setUp() {
    env = new EpisodeServiceTestEnv();
    rest = new MyEpisodeServiceRestEndpoint(env);
  }

  @After
  public void tearDown() {
    env.tearDown();
  }

  @Test
  public void testFindEpisode() {
    final EpisodeService e = env.getService();
    final MediaPackage mpa = loadFromClassPath("/manifest-a.xml");
    final MediaPackage mpb = loadFromClassPath("/manifest-b.xml");
    final MediaPackage mpc = loadFromClassPath("/manifest-c.xml");
    env.setReadWritePermissions();
    e.add(mpa);
    e.add(mpb);
    e.add(mpc);

    // Search series-title, with logical OR
    assertEquals(findEpisodeByText("blargh", false).size(), 0);
    assertEquals(findEpisodeByText("british blargh", false).size(), 1);
    assertEquals(findEpisodeByText("british", false).size(), 1);
    assertEquals(findEpisodeByText("british literature", false).size(), 3);

    // Search series-title, with logical AND
    assertEquals(findEpisodeByText("blargh", true).size(), 0);
    assertEquals(findEpisodeByText("british blargh", true).size(), 0);
    assertEquals(findEpisodeByText("british", true).size(), 1);
    assertEquals(findEpisodeByText("british literature", true).size(), 1);
  }

  private JaxbSearchResult findEpisodeByText(String text, boolean useLogicalAnd) {
    Response response = rest.findEpisode(null, text, null, null, null, null, null, null, null, null, null, null, null, false, useLogicalAnd, null);
    assertTrue(response.getEntity() instanceof JaxbSearchResult);
    return (JaxbSearchResult) response.getEntity();
  }

  private static class MyEpisodeServiceRestEndpoint extends AbstractEpisodeServiceRestEndpoint {

    private EpisodeServiceTestEnv env;

    MyEpisodeServiceRestEndpoint(EpisodeServiceTestEnv env) {
      this.env = env;
    }

    @Override
    public EpisodeService getEpisodeService() {
      return env.getService();
    }

    @Override
    public WorkflowService getWorkflowService() {
      return EasyMock.createNiceMock(WorkflowService.class);
    }

    @Override
    public SecurityService getSecurityService() {
      return env.getSecurityService();
    }

    @Override
    public String getServerUrl() {
      return "localhost";
    }

    @Override
    public String getMountPoint() {
      return "test";
    }
  }
}
