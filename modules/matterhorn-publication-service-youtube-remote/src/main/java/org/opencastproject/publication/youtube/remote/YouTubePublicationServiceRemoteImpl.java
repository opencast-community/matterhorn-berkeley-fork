/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.publication.youtube.remote;

import org.opencastproject.job.api.Job;
import org.opencastproject.job.api.JobParser;
import org.opencastproject.mediapackage.MediaPackage;
import org.opencastproject.mediapackage.MediaPackageParser;
import org.opencastproject.mediapackage.Track;
import org.opencastproject.publication.api.PublicationException;
import org.opencastproject.publication.api.YouTubePublicationService;
import org.opencastproject.serviceregistry.api.RemoteBase;

import org.apache.http.HttpResponse;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * A remote youtube service invoker.
 */
public class YouTubePublicationServiceRemoteImpl extends RemoteBase implements YouTubePublicationService {

  public static final String KEY_MEDIAPACKAGE = "mediapackage";

  /** The logger */
  private static final Logger logger = LoggerFactory.getLogger(YouTubePublicationServiceRemoteImpl.class);

  public YouTubePublicationServiceRemoteImpl() {
    super(JOB_TYPE);
  }

  @Override
  public Job publish(final MediaPackage mediaPackage, final Track track) throws PublicationException {
    final String trackId = track.getIdentifier();
    final List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
    params.add(new BasicNameValuePair(KEY_MEDIAPACKAGE, MediaPackageParser.getAsXml(mediaPackage)));
    params.add(new BasicNameValuePair("elementId", trackId));
    HttpResponse response = null;
    try {
      logger.info("Publishing {} to YouTube", trackId);
      final HttpPost post = new HttpPost();
      post.setEntity(new UrlEncodedFormEntity(params));
      response = getResponse(post);
      if (response == null) {
        throw new PublicationException("Null response from remote service");
      }
      return JobParser.parseJob(response.getEntity().getContent());
    } catch (final Exception e) {
      throw new PublicationException("Unable to publish track " + trackId + " from mediapackage "
              + mediaPackage + " using a remote youtube publication service", e);
    } finally {
      closeConnection(response);
    }
  }

  @Override
  public Job retract(final MediaPackage mediaPackage) throws PublicationException {
    final List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
    params.add(new BasicNameValuePair(KEY_MEDIAPACKAGE, MediaPackageParser.getAsXml(mediaPackage)));
    HttpResponse response = null;
    try {
      logger.info("Retracting {} from YouTube", mediaPackage.getIdentifier().compact());
      final HttpPost post = new HttpPost("/retract");
      post.setEntity(new UrlEncodedFormEntity(params));
      response = getResponse(post);
      if (response == null) {
        throw new PublicationException("Null response from remote service");
      }
      return JobParser.parseJob(response.getEntity().getContent());
    } catch (final Exception e) {
      throw new PublicationException(e);
    } finally {
      closeConnection(response);
    }
  }

}
