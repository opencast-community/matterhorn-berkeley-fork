/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */

package org.opencastproject.schedulableservice.api;

/**
 * A simple, non-persistent approach to scheduling recurring events that trigger a service call.
 *
 */
public interface SchedulableServiceService {
  
  /**
   * Schedule a recurring call for a given {@link SchedulableService}, calling it once immediately and then every 
   * {@link Periods} value, unless the period is {@code DAY}, in which case it will not execute until the next  
   * midnight and thence once per day (at midnight) thereafter. Valid values for period:
   * <p><ul>
   * <li>{@code MINUTE}
   * <li>{@code HOUR}
   * <li>{@code DAY}
   * <ul><p>
   * 
   * @param service that will be called
   * @param jobName used for recurring to a specific event
   * @param userName typically a default admin account
   * @param period that determines how often the event will occur
   */
  void scheduleServiceEvent(SchedulableService service, String jobName, String userName, Periods period);

}
