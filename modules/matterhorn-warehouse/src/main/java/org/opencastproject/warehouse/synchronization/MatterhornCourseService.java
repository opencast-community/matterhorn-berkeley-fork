/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.warehouse.synchronization;

import org.opencastproject.participation.model.Booking;
import org.opencastproject.series.api.SeriesException;
import org.opencastproject.warehouse.WarehouseCourse;

import java.util.Set;
import java.util.regex.Pattern;

public interface MatterhornCourseService {
  
  /**
   * Returns the identifier for all Matterhorn series that match the pattern provided.
   * 
   * @param pattern {@code Pattern to be matched}
   * @return the {@literal PROPERTY_IDENTIFIER} value for all matching series
   * @throws SeriesException  if query could not be performed
   */
  Set<String> getMatchingSeriesIds(Pattern pattern) throws SeriesException;

  /**
   * Returns the {@code WarehouseCourse} that corresponds to the {@literal seriesId} provided.
   *  
   * @param seriesId  the series corresponding to the {@code WarehouseCourse} 
   * @return never null
   * @throws SeriesException  if query could not be performed
   */
  WarehouseCourse buildWarehouseCourse(String seriesId) throws SeriesException;

  /**
   * Returns one {@code Booking} containing the reference course and cross-listed courses.
   *
   * @param referenceCourseId  series or course offering identifier in yearSemesterCCNs format, e.g. {@literal 2014D12345}
   * @return  null if the series is not cross-listed
   * @throws org.opencastproject.participation.impl.persistence.CourseDatabaseException if there is a problem communicating with the underlying data store
   */
  Booking getCrossListedCourseSet(String referenceCourseId);

}
