/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.warehouse.selenium;

import org.opencastproject.notify.Notifier;
import org.opencastproject.participation.model.Term;
import org.opencastproject.warehouse.WarehouseService;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Se {

  private static final int DEFAULT_SECONDS_PAUSE_PRIOR_PAGE_LOAD = 1;

  private final Logger logger = LoggerFactory.getLogger(this.getClass());
  private final WebDriver driver;
  private final Config config;

  public static YouTubeSession youTubeSession(WarehouseService warehouseService, Notifier notifier, int limit, Config config) {
    return config.useFakeYouTube() ? new DummyPrivacyManager() : new YouTubeSessionImpl(warehouseService, notifier, limit, config);
  }

  /**
   * ESRs at https://www.mozilla.org/en-US/firefox/organizations/
   * See http://kb.mozillazine.org/Firefox_:_FAQs_:_About:config_Entries for ways to customize
   */
  Se(final String url, final Config config) {
    driver = SeUtils.perform(3, new WebDriverLoader(url, config));
    driver.manage().window().maximize();
    this.config = config;
  }

  boolean load(final String url) {
    Attemptable<Boolean> operation = new Attemptable<Boolean>() {
      private boolean success = false;
      @Override
      public Boolean perform() {
        pauseBeforePageLoad();
        driver.get(url);
        success = true;
        return true;
      }
      @Override
      public void close() {
        String message = success ? "Arrived at URL: " + url : "Failed to load URL: " + url;
        logger.warn(message);
      }
    };
    return SeUtils.perform(3, operation);
  }

  void acceptAlert() {
    try {
      WebDriverWait wait = new WebDriverWait(driver, DEFAULT_SECONDS_PAUSE_PRIOR_PAGE_LOAD);
      wait.until(ExpectedConditions.alertIsPresent());
      Alert alert = driver.switchTo().alert();
      alert.accept();
    } catch (Exception ignored) {
    }
  }

  private void pauseBeforePageLoad() {
    try {
      Thread.sleep(DEFAULT_SECONDS_PAUSE_PRIOR_PAGE_LOAD * 1000);
    } catch (InterruptedException e) {
      logger.warn(e.getClass().getSimpleName() + " during Selenium.wait(). Message: " + e.getMessage());
    }
  }

  /**
   * If Selenium times out then we assume element reference is invalid.
   *
   * @param by find criteria
   * @return true if element found
   */
  boolean isPresent(By by) {
    try {
      find(by);
      return true;
    } catch (TimeoutException e) {
      return false;
    }
  }

  private static class DummyPrivacyManager implements YouTubeSession {
    @Override
    public PrivacyAdjustmentSummary ucBerkeleyOnly(Term term, boolean retryPastFailures) {
      return new EmptyAdjustmentSummary(term);
    }
  }

  WebElement find(final By... chain) {
    WebElement element = null;
    for (By by : chain) {
      element = waitUntil(by, config.pageTimeoutSeconds());
    }
    return element;
  }

  /**
   * If the first id in the list is not found then try the second, etc. If find() fails on all ids then
   * the final TimeoutException will be thrown.
   *
   * @param byList List of potential markers on desired page element
   */
  void clickFirstMatch(By... byList) {
    for (int index = 0; index < byList.length; index++) {
      try {
        click(byList[index]);
        break;
      } catch (TimeoutException e) {
        if (index == byList.length - 1) {
          // If 'id' is last in the list of 'ids' then throw the exception
          throw e;
        }
      }
    }
  }

  void click(By by) {
    WebElement element = find(by);
    try {
      WebDriverWait wait = new WebDriverWait(driver, config.pageTimeoutSeconds());
      wait.until(ExpectedConditions.elementToBeClickable(element));
      element.click();
    } catch (WebDriverException e) {
      // If element we want to click is obstructed by another element then click
      // with JavaScript - a strategy in which element positions do not matter.
      executeScript("arguments[0].click();", element);
    }
  }

  void type(By by, String text) {
    WebElement field = find(by);
    field.click();
    field.clear();
    field.sendKeys(text);
  }

  private WebElement waitUntil(By by, long waitSeconds) {
    ExpectedCondition<WebElement> expectation = ExpectedConditions.presenceOfElementLocated(by);
    return new WebDriverWait(driver, waitSeconds).until(expectation);
  }

  /**
   * @param script The JavaScript to execute
   * @param args The arguments to the script. May be empty
   * @return One of Boolean, Long, String, List or WebElement. Or null.
   * @see JavascriptExecutor
   */
  private WebElement executeScript(String script, Object... args) {
    if (driver instanceof JavascriptExecutor) {
      JavascriptExecutor executor = (JavascriptExecutor) driver;
      return (WebElement) executor.executeScript(script, args);
    } else {
      throw new UnsupportedOperationException(driver.getClass().getSimpleName() + " is not of type JavascriptExecutor");
    }
  }

  void quit() {
    driver.quit();
  }

}
