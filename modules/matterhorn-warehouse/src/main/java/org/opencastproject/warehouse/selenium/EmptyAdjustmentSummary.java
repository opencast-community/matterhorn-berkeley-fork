package org.opencastproject.warehouse.selenium;

import org.opencastproject.participation.model.Term;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

class EmptyAdjustmentSummary implements PrivacyAdjustmentSummary {
  private final Term term;
  private final List<String> emptyList;

  EmptyAdjustmentSummary(Term term) {
    this.term = term;
    this.emptyList = Collections.unmodifiableList(new LinkedList<String>());
  }

  @Override
  public Term term() {
    return term;
  }
  @Override
  public List<String> successes() {
    return emptyList;
  }
  @Override
  public List<String> failures() {
    return emptyList;
  }
  @Override
  public List<String> notFound() {
    return emptyList;
  }
}
