/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.warehouse;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.opencastproject.participation.CourseUtils;
import org.opencastproject.participation.model.CourseKey;
import org.opencastproject.participation.model.HasCourseKey;
import org.opencastproject.participation.model.RecordingType;
import org.opencastproject.participation.model.Semester;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * @author John Crossman
 */
final class WarehouseUtils {

  private static final Logger logger = LoggerFactory.getLogger(WarehouseUtils.class);

  private WarehouseUtils() {
  }

  static AssociatedRecordingType getAssociatedRecordingType(final WarehouseCourse course) {
    final AssociatedRecordingType type;
    final RecordingType recordingType = course.getCapturePreferences() == null
            ? null
            : course.getCapturePreferences().getRecordingType();
    if (recordingType != null) {
      switch (recordingType) {
        case audioOnly:
          type = AssociatedRecordingType.audio;
          break;
        case screencast:
          type = AssociatedRecordingType.screencast;
          break;
        case videoOnly:
          type = AssociatedRecordingType.video;
          break;
        case videoAndScreencast:
          type = AssociatedRecordingType.video_and_screencast;
          break;
        default:
          throw new UnsupportedOperationException("Unknown recordingType: " + recordingType);
      }
    } else {
      type = (course.getYouTubePlaylistId() != null) ? AssociatedRecordingType.video : AssociatedRecordingType.audio;
    }
    return type;
  }

  static String formatTrimmedLength(final Integer milliseconds) {
    final Integer totalSeconds = milliseconds / 1000;
    final int hours = totalSeconds / 3600;
    final int minutes = (totalSeconds % 3600) / 60;
    final int seconds = totalSeconds % 60;
    return hours + ":" + twoDigit(minutes) + ":" + twoDigit(seconds);
  }

  private static String twoDigit(final int value) {
    final String prefix = (value < 10) ? "0" : StringUtils.EMPTY;
    return prefix + value;
  }

  static WarehouseBuilding getWarehouseBuilding(String building) {
    building = StringUtils.trimToNull(building);
    WarehouseBuilding match = null;
    if (building != null) {
      match = getWarehouseBuildingPerEdoDb(building);
      if (match == null) {
        final String normalized = StringUtils.remove(building, ' ');
        if (StringUtils.startsWithIgnoreCase(normalized, "wheeler")) {
          // Legacy Oracle has building "WHEELER AUD" which does not align a name in our enum.
          match = WarehouseBuilding.Wheeler;
        } else {
          for (final WarehouseBuilding next : WarehouseBuilding.values()) {
            if (StringUtils.equalsIgnoreCase(next.name(), normalized)) {
              match = next;
              break;
            }
          }
        }
      }
    }
    return match;
  }

  private static WarehouseBuilding getWarehouseBuildingPerEdoDb(final String building) {
    final WarehouseBuilding match;
    if (building != null) {
      if (building.startsWith("Haas Pavilion")) {
        match = WarehouseBuilding.Haas;
      } else if (building.startsWith("Hearst Mining")) {
        match = WarehouseBuilding.HearstMin;
      } else if (building.startsWith("Moffitt")) {
        match = WarehouseBuilding.Moffitt;
      } else if (building.startsWith("Valley Life Sciences")) {
        match = WarehouseBuilding.ValleyLSB;
      } else {
        match = null;
      }
    } else {
      match = null;
    }
    return match;
  }

  static Map<HasCourseKey, Set<CourseCatalogEntry>> loadHardCodedCrossListings(final File file) {
    final Map<HasCourseKey, Set<CourseCatalogEntry>> map = new TreeMap<HasCourseKey, Set<CourseCatalogEntry>>(new JSONFeedComparator());
    try {
      final String fileContents = file.exists() ? StringUtils.trimToNull(FileUtils.readFileToString(file)) : null;
      if (fileContents != null) {
        final JSONObject json = (JSONObject) new JSONParser().parse(fileContents);
        final JSONArray jsonArray = (JSONArray) json.get("crossListedCCNs");
        if (jsonArray == null || jsonArray.isEmpty()) {
          logger.info("Zero cross-listings found in " + file.getAbsolutePath());
        } else {
          for (final Object obj : jsonArray) {
            final JSONObject jsonObject = (JSONObject) obj;
            final CourseKey courseKey = CourseUtils.getCourseKey(get(jsonObject, "matterhornSeriesId"));
            final int crossListedCCN = NumberUtils.toInt(get(jsonObject, "crossListedCCN"), 0);
            if (courseKey == null || crossListedCCN < 1) {
              logger.warn("Hard-coded cross-listings property has an improper entry : " + jsonObject.toJSONString());
            } else {
              final CourseKey crossListing = new CourseKey(courseKey.getYear(), courseKey.getSemester(), crossListedCCN);
              final String deptName = StringUtils.trimToNull(get(jsonObject, "deptName"));
              final String catalogId = StringUtils.trimToNull(get(jsonObject, "catalogId"));
              final CourseCatalogEntry catalogEntry = (deptName == null || catalogId == null)
                      ? null
                      : new CourseCatalogEntryImpl(crossListing, deptName, catalogId);
              if (catalogEntry == null) {
                logger.warn("Hard-coded cross-listings property has an improper entry : " + jsonObject.toJSONString());
              } else {
                final Set<CourseCatalogEntry> set;
                if (map.containsKey(courseKey)) {
                  set = map.get(courseKey);
                } else {
                  set = new TreeSet<CourseCatalogEntry>(new JSONFeedComparator());
                  map.put(courseKey, set);
                }
                set.add(catalogEntry);
              }
            }
          }
        }
      }
    } catch (final Exception e) {
      logger.warn("Failure in parsing JSON file (" + file.getAbsolutePath() + ") with hard-coded cross-listings", e);
    }
    return map;
  }

  private static String get(final JSONObject json, final Object key) {
    final Object obj = json == null ? null : json.get(key);
    return (obj == null) ? StringUtils.EMPTY : StringUtils.trimToNull(obj.toString());
  }

  private static final class CourseCatalogEntryImpl implements CourseCatalogEntry {

    private final CourseKey courseKey;
    private final String departmentName;
    private final String catalogId;

    private CourseCatalogEntryImpl(final CourseKey courseKey, final String departmentName, final String catalogId) {
      this.courseKey = courseKey;
      this.departmentName = departmentName;
      this.catalogId = catalogId;
    }

    @Override
    public int getYear() {
      return courseKey.getYear();
    }

    @Override
    public Semester getSemester() {
      return courseKey.getSemester();
    }

    @Override
    public int getCcn() {
      return courseKey.getCcn();
    }

    public String getDepartmentName() {
      return departmentName;
    }

    public String getCatalogId() {
      return catalogId;
    }
  }

}
