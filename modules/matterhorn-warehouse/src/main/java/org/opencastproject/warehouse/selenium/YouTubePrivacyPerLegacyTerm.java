/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.warehouse.selenium;

import org.apache.commons.lang.StringUtils;
import org.opencastproject.participation.impl.VoidNotifier;
import org.opencastproject.participation.model.Term;
import org.opencastproject.warehouse.YouTubeScriptPerTerm;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Collection;
import java.util.Set;

public class YouTubePrivacyPerLegacyTerm extends YouTubeScriptPerTerm {

  private final Integer limit;
  private final boolean retryPastFailures;

  private YouTubePrivacyPerLegacyTerm(Term term, Integer limit, boolean retryPastFailures) throws IOException {
    super(term);
    this.limit = limit;
    this.retryPastFailures = retryPastFailures;
  }

  public static void main(String[] args) throws IOException {
    Integer edoTermId = requiredIntArg(args[0], "edoTermId");
    Integer limit = requiredIntArg(args[1], "limit");
    boolean retryPastFailures = args.length > 2 && StringUtils.equalsIgnoreCase(args[2], "true");
    Term term = Term.construct(edoTermId.toString(), null, null);
    if (term == null || term.getYear() == null || term.getSemester() == null) {
      throw new IllegalArgumentException("Invalid edoTermId: " + edoTermId);
    }
    new YouTubePrivacyPerLegacyTerm(term, limit, retryPastFailures).run();
  }

  private void run() {
    // We use Selenium to make YouTube videos "private, shared UC Berkeley only".
    Config config = Config.seleniumConfigLegacyMigration();
    YouTubeSession youTubeSession = Se.youTubeSession(warehouseService, new VoidNotifier(), limit, config);
    PrivacyAdjustmentSummary summary = youTubeSession.ucBerkeleyOnly(term, retryPastFailures);
    summarize(term, summary);
  }

  private void summarize(Term term, PrivacyAdjustmentSummary s) {
    // Log all YouTube video ids (of term) that will be ignored when retryPastFailures is FALSE
    StringBuilder b = new StringBuilder(hr).append("This run of the script might have touched only a subset of the ").append(toS(term)).append(" videos:");
    b.append(lb).append(" - ").append(videoCount(s)).append(" YouTube video(s) processed");
    b.append(lb).append("   -- ").append(s.successes().size()).append(" successes");
    if (s.successes().size() > 0) {
      b.append(": ").append(join(s.successes()));
    }
    b.append(lb).append("   -- ").append(s.failures().size()).append(" failures");
    if (s.failures().size() > 0) {
      b.append(": ").append(join(s.failures()));
    }
    b.append(lb).append("   -- ").append(s.notFound().size()).append(" not found");
    if (s.notFound().size() > 0) {
      b.append(": ").append(join(s.notFound()));
    }
    // List Selenium failures of term
    b.append(hr);
    b.append("Consider ALL ").append(toS(term)).append(" YouTube videos");
    for (boolean treatedBySelenium : new boolean[] {true, false}) {
      for (boolean includeInFeed : new boolean[] {true, false}) {
        final Set<String> ids = warehouseService.getBySeleniumStatus(term.getYear(), term.getSemester(), treatedBySelenium, includeInFeed);
        b.append(lb).append(" - ").append(ids.size()).append(" where treated_by_selenium IS ").append(treatedBySelenium);
        b.append(" AND include_in_cc_feed IS ").append(includeInFeed);
        if (ids.size() > 0) {
          b.append(":").append(lb).append("    ").append(join(ids));
        }
      }
    }
    b.append(hr);
    LoggerFactory.getLogger(this.getClass()).info(b.toString());
  }

  private int videoCount(PrivacyAdjustmentSummary s) {
    return s.successes().size() + s.failures().size() + s.notFound().size();
  }

  private String join(Collection<String> list) {
    return StringUtils.join(list, ", ");
  }
}
