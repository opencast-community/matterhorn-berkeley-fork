/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.warehouse;

import org.opencastproject.participation.impl.Column;

/**
 * @author John Crossman
 */
enum ColStr implements Column<String> {

  audience_type, description, room_number, section, semester_code, title, youtube_playlist_id, building_name, recording_type,
  license_type, catalog_id, department_name, youtube_video_id, trimmed_length, recording, course, recordingId,
  recording_title, recording_description, dateParsePattern, matterhorn_episode_id;

  @Override
  public Class<String> getType() {
    return String.class;
  }

}
