/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.warehouse;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * @author John Crossman
 */
public class WarehouseRoom {

  private final WarehouseBuilding building;
  private final String roomNumber;

  public WarehouseRoom(final WarehouseBuilding building, final String roomNumber) {
    this.building = building;
    this.roomNumber = roomNumber;
  }

  public WarehouseBuilding getBuilding() {
    return building;
  }

  public String getRoomNumber() {
    return roomNumber;
  }

  @Override public String toString() {
    return ToStringBuilder.reflectionToString(this);
  }

  @Override
  public boolean equals(final Object o) {
    return EqualsBuilder.reflectionEquals(this, o);
  }

  @Override
  public int hashCode() {
    int result = building != null ? building.hashCode() : 0;
    result = 31 * result + (roomNumber != null ? roomNumber.hashCode() : 0);
    return result;
  }
}
