/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.warehouse.synchronization;

import org.opencastproject.episode.api.EpisodeQuery;
import org.opencastproject.episode.api.EpisodeService;
import org.opencastproject.episode.api.SearchResult;
import org.opencastproject.episode.api.UriRewriter;
import org.opencastproject.episode.api.Version;
import org.opencastproject.mediapackage.MediaPackageElement;
import org.opencastproject.metadata.dublincore.DublinCoreCatalog;
import org.opencastproject.metadata.dublincore.DublinCoreCatalogList;
import org.opencastproject.participation.api.CourseManagementService;
import org.opencastproject.participation.impl.persistence.CourseCatalogService;
import org.opencastproject.participation.model.Booking;
import org.opencastproject.participation.model.CapturePreferences;
import org.opencastproject.participation.model.CourseOffering;
import org.opencastproject.participation.model.RecordingAvailability;
import org.opencastproject.security.api.UnauthorizedException;
import org.opencastproject.series.api.SeriesException;
import org.opencastproject.series.api.SeriesQuery;
import org.opencastproject.series.api.SeriesService;
import org.opencastproject.util.NotFoundException;
import org.opencastproject.warehouse.WarehouseCourse;
import org.opencastproject.workspace.api.Workspace;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import static org.opencastproject.util.MimeTypeUtil.suffix;
import static org.opencastproject.util.OsgiUtil.getComponentContextProperty;
import static org.opencastproject.util.OsgiUtil.getContextProperty;
import static org.opencastproject.util.UrlSupport.uri;
import static org.opencastproject.util.data.Option.option;

public class MatterhornCourseServiceImpl implements MatterhornCourseService {

  private final Logger logger = LoggerFactory.getLogger(this.getClass());
  private EpisodeService episodeService;
  private SeriesService seriesService;
  private CourseManagementService courseManagementService;
  private UriRewriter uriRewriter;
  private Workspace workspace;
  private CourseCatalogService courseCatalogService;

  @SuppressWarnings("unused")
  protected synchronized void activate(final ComponentContext cc) throws Exception {
    setUriRewriter(cc);
    logger.info("Activating MatterhornCourseServiceImpl");
  }

  @Override
  public Set<String> getMatchingSeriesIds(final Pattern pattern) throws SeriesException {
    Set<String> filteredSeriesIds = new HashSet<String>(); 
    final SeriesQuery query = new SeriesQuery();
    final int seriesCount = seriesService.getSeriesCount();
    query.setCount(seriesCount);
    // TODO: switch to native SOLR regex in 4.0 - if Matterhorn ever upgrades to it
    // The query for seriesId would be = query.setSeriesId("/" + pattern.pattern() + "/");
    try {
      final List<DublinCoreCatalog> catalogList = querySeriesCatalogs(query);
      for (DublinCoreCatalog catalog : catalogList) {
        String seriesId = catalog.getFirst(DublinCoreCatalog.PROPERTY_IDENTIFIER);
        if (pattern.matcher(seriesId).matches()) {
          filteredSeriesIds.add(seriesId);
        }
      }
    } catch (UnauthorizedException e) {
      throw new SeriesException(e);
    }
    return filteredSeriesIds;
  }

  @Override
  public WarehouseCourse buildWarehouseCourse(final String seriesId) throws SeriesException {
    try {
      final CourseOffering course = courseManagementService.getCourseOffering(seriesId);
      final WarehouseCourse result;
      if (course == null) {
        logger.error("Potentially a problem: Salesforce has nothing for " + seriesId);
        result = null;
      } else {
        final WarehouseCourseFactory warehouseCourseFactory = WarehouseCourseFactory.instanceOfWarehouseCourseFactory(
                courseCatalogService, courseManagementService);
        final DublinCoreCatalog catalog = seriesService.getSeries(seriesId);
        final CapturePreferences capturePreferences = course.getCapturePreferences();
        // When Matterhorn metadata is null or invalid we use Salesforce metadata
        if (course.getCapturePreferences() != null) {
          final RecordingAvailability defaultValue = course.getCapturePreferences().getRecordingAvailability();
          final RecordingAvailability availability = SynchronizerUtils.getRecordingAvailability(catalog, defaultValue);
          course.getCapturePreferences().setRecordingAvailability(availability);
        }
        result = warehouseCourseFactory.newInstance(catalog, capturePreferences,
                SynchronizerUtils.getAssociatedLicense(course), queryEpisode(seriesId));
      }
      return result;
    } catch (final UnauthorizedException e) {
      throw new SeriesException(e);
    } catch (final NotFoundException e) {
      throw new SeriesException("Series does not exist in Matterhorn", e);
    }
  }

  @Override
  public Booking getCrossListedCourseSet(final String seriesId) {
    return courseCatalogService.getCrossListedCourseSet(seriesId);
  }

  @SuppressWarnings("All")
  public void setSeriesService(final SeriesService seriesService) {
    this.seriesService = seriesService;
  }

  @SuppressWarnings("All")
  public void setEpisodeService(EpisodeService episodeService) {
    this.episodeService = episodeService;
  }

  @SuppressWarnings("All")
  public void setCourseManagementService(final CourseManagementService courseManagementService) {
    this.courseManagementService = courseManagementService;
  }

  @SuppressWarnings("All")
  public void setWorkspace(Workspace workspace) {
    this.workspace = workspace;
  }

  @SuppressWarnings("All")
  public void setCourseCatalogService(CourseCatalogService courseCatalogService) {
    this.courseCatalogService = courseCatalogService;
  }
  
  private List<DublinCoreCatalog> querySeriesCatalogs(SeriesQuery query) throws SeriesException, UnauthorizedException {
    final DublinCoreCatalogList dublinCoreCatalogList = seriesService.getSeries(query);
    return dublinCoreCatalogList.getCatalogList();
  }
  
  private SearchResult queryEpisode(String seriesId) {
    final EpisodeQuery episodeQuery = EpisodeQuery.systemQuery();
    episodeQuery.seriesId(seriesId).onlyLastVersion();
    return episodeService.find(episodeQuery, getUriRewriter());
  }
  
  private UriRewriter getUriRewriter() {
    return uriRewriter;
  }

  private void setUriRewriter(ComponentContext cc) {
    final String serverUrl = getContextProperty(cc, "org.opencastproject.server.url");
    final String mountPoint = getComponentContextProperty(cc, "opencast.service.path");
    uriRewriter = new UriRewriter() {
      @Override public URI apply(Version version, MediaPackageElement mpe) {
        final String archivePathPrefix = "/archive/mediapackage/";
        final String mimeType = option(mpe.getMimeType()).bind(suffix).getOrElse("unknown");
        return uri(serverUrl,
                   mountPoint,
                   archivePathPrefix,
                   mpe.getMediaPackage().getIdentifier(),
                   mpe.getIdentifier(),
                   version,
                   mpe.getElementType().toString().toLowerCase() + "." + mimeType);
      }
    };
  }

}
