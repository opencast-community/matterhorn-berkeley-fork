/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.warehouse.synchronization;

import org.apache.commons.lang.StringUtils;
import org.opencastproject.google.youtube.GoogleUtils;
import org.opencastproject.metadata.dublincore.DublinCore;
import org.opencastproject.metadata.dublincore.DublinCoreCatalog;
import org.opencastproject.participation.CourseUtils;
import org.opencastproject.participation.model.CapturePreferences;
import org.opencastproject.participation.model.CourseData;
import org.opencastproject.participation.model.CourseOffering;
import org.opencastproject.participation.model.DayOfWeek;
import org.opencastproject.participation.model.License;
import org.opencastproject.participation.model.RecordingAvailability;
import org.opencastproject.participation.model.Room;
import org.opencastproject.security.api.RecordingAccessRights;
import org.opencastproject.warehouse.AssociatedLicense;
import org.opencastproject.warehouse.Recording;
import org.opencastproject.warehouse.WarehouseBuilding;
import org.opencastproject.warehouse.WarehouseCourse;
import org.opencastproject.warehouse.WarehouseRoom;

import java.util.List;
import java.util.Set;

/**
 * @author John Crossman
 */
final class SynchronizerUtils {

  private SynchronizerUtils() {
  }

  /**
   * Returns a {@code WarehouseCourse}
   * @param course course data from Materialized View
   * @param recordings apply to cross-listed courses
   * @param meetingDays apply to cross-listed courses
   * @param warehouseRoom apply to cross-listed courses
   * @param youTubePlaylistId apply to cross-listed courses
   * @param capturePreferences apply to cross-listed courses
   * @param license apply to cross-listed courses
   * @return  may be empty if the referenceCourse was null, otherwise contains at least the referenceCourse
   */
  static WarehouseCourse getWarehouseCourse(final CourseData course, final Set<Recording> recordings,
                                            final List<DayOfWeek> meetingDays, final WarehouseRoom warehouseRoom,
                                            final String youTubePlaylistId, final CapturePreferences capturePreferences,
                                            final AssociatedLicense license) {
    final WarehouseCourse warehouseCourse = new WarehouseCourse(course.getCanonicalCourse(), course.getYear(),
            course.getSemester(), course.getCatalogId(), course.getDeptName(), course.getSection(), meetingDays,
            warehouseRoom, course.getSeriesDescription(), CourseUtils.getLecturers(course), youTubePlaylistId,
            capturePreferences, license);
    warehouseCourse.setRecordings(recordings);
    return warehouseCourse;
  }

  static WarehouseRoom getWarehouseRoom(final Room room) {
    final String normalized = StringUtils.trimToEmpty(room.getBuilding()).replaceAll("[^a-zA-Z0-9]", StringUtils.EMPTY).toLowerCase();
    WarehouseBuilding building = null;
    if (isValleyLifeSciences(room)) {
      building = WarehouseBuilding.ValleyLSB;
    } else if (isHearstMining(room)) {
      building = WarehouseBuilding.HearstMin;
    } else {
      for (WarehouseBuilding next : WarehouseBuilding.values()) {
        if (next.name().toLowerCase().equals(normalized)) {
          building = next;
          break;
        }
      }
    }
    return (building == null) ? null : new WarehouseRoom(building, room.getRoomNumber());
  }

  private static boolean isValleyLifeSciences(final Room room) {
    final boolean isVLSB;
    String b = StringUtils.lowerCase(StringUtils.trimToNull(room.getBuilding()));
    if (b == null) {
      isVLSB = false;
    } else if (b.contains("vlsb")) {
      isVLSB = true;
    } else if (b.contains("valley")) {
      isVLSB = b.contains("lsb") || b.contains("life");
    } else {
      isVLSB = false;
    }
    return isVLSB;
  }

  private static boolean isHearstMining(final Room room) {
    String b = StringUtils.lowerCase(StringUtils.trimToNull(room.getBuilding()));
    return b != null && (b.contains("hearst") && b.contains("min"));
  }

  /**
   * Translate license enum types.
   * @param course Null not allowed.
   * @return null if incoming license type is null.
   */
  static AssociatedLicense getAssociatedLicense(final CourseOffering course) {
    final CapturePreferences cp = course.getCapturePreferences();
    final RecordingAvailability availability = cp == null ? null : cp.getRecordingAvailability();
    final AssociatedLicense license;
    if (availability == null) {
      license = null;
    } else {
      switch (availability) {
        case publicNoRedistribute:
        case studentsOnly:
          license = AssociatedLicense.all_rights_reserved;
          break;
        case publicCreativeCommons:
          license = AssociatedLicense.creative_commons;
          break;
        default:
          throw new UnsupportedOperationException("No logic to handle availability type: " + availability);
      }
    }
    return license;
  }

  /**
   * @param series Null allowed
   * @param fallback Null allowed
   * @return Never null. Appropriate value per {@link org.opencastproject.metadata.dublincore.DublinCore#PROPERTY_ACCESS_RIGHTS}
   *     and {@link org.opencastproject.metadata.dublincore.DublinCore#PROPERTY_LICENSE}. Return fallback when
   *     deduction on series metadata fails.
   */
  static RecordingAvailability getRecordingAvailability(final DublinCoreCatalog series,
          final RecordingAvailability fallback) {
    final RecordingAvailability defaultValue = fallback == null ? RecordingAvailability.studentsOnly : fallback;
    final RecordingAvailability availability;
    final String accessRightsProperty = series == null ? null : series.getFirst(DublinCore.PROPERTY_ACCESS_RIGHTS);
    final RecordingAccessRights accessRights = GoogleUtils.findByPropertyAccessRights(accessRightsProperty, null);
    if (accessRights == null) {
      availability = defaultValue;
    } else if (accessRights.equals(RecordingAccessRights.studentsOnlyAccessRights)) {
      availability = RecordingAvailability.studentsOnly;
    } else {
      final String licenseDescription = series == null ? null : series.getFirst(DublinCore.PROPERTY_LICENSE);
      final License license = StringUtils.containsIgnoreCase(licenseDescription, "creative")
              ? License.creativeCommons
              : License.allRightsReserved;
      availability = license.equals(License.creativeCommons)
              ? RecordingAvailability.publicCreativeCommons
              : RecordingAvailability.publicNoRedistribute;
    }
    return availability;
  }

}
