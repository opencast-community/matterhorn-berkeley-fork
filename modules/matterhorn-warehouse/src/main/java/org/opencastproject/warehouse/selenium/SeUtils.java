/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.warehouse.selenium;

import org.openqa.selenium.WebDriverException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class SeUtils {


  private SeUtils() {
  }

  static <T> T perform(int attemptCount, Attemptable<T> operation) {
    final Logger logger = LoggerFactory.getLogger(SeUtils.class);
    final String operationName = operation.getClass().getSimpleName();
    Exception error = null;
    T result = null;
    for (int attempt = 1; attempt <= attemptCount; attempt++) {
      if (attempt > 1) {
        sleep(3);
      }
      try {
        result = operation.perform();
        logger.info("After " + attempt + " attempts we succeeded with " + operationName);
        break;
      } catch (Exception tolerateThis) {
        error = tolerateThis;
        logger.warn("Error occurred on attempt " + attempt + " with " + operationName, tolerateThis);
      }
    }
    operation.close();
    if (error != null) {
      throw new WebDriverException("After " + attemptCount + " attempts we failed to perform " + operationName, error);
    }
    return result;
  }

  static void sleep(int seconds) {
    try {
      Thread.sleep(seconds * 1000);
    } catch (InterruptedException e) {
      throw new RuntimeException(e);
    }
  }

}
