/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.warehouse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.opencastproject.participation.CourseUtils;
import org.opencastproject.participation.model.CanonicalCourse;
import org.opencastproject.participation.model.CapturePreferences;
import org.opencastproject.participation.model.DayOfWeek;
import org.opencastproject.participation.model.HasCourseKey;
import org.opencastproject.participation.model.Semester;

import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * @author John Crossman
 */
public class WarehouseCourse extends PublishedMedia implements CourseCatalogEntry {

  private final CanonicalCourse canonicalCourse;
  private final int year;
  private final Semester semester;
  private final String catalogId;
  private final List<DayOfWeek> meetingDays;
  private final WarehouseRoom room;
  private final AssociatedLicense license;
  private final String[] instructors;
  private final CapturePreferences capturePreferences;
  private final Set<Recording> recordings = new TreeSet<Recording>(new Recording.RecordingComparator());
  private String departmentName;
  private String section;
  private String description;

  public WarehouseCourse(final CanonicalCourse canonicalCourse, final int year, final Semester semester, final String catalogId,
          final String departmentName, final String section, final List<DayOfWeek> meetingDays, final WarehouseRoom room, final String description,
          final String[] instructors, final String youTubePlaylistId, final CapturePreferences capturePreferences,
          final AssociatedLicense license) {
    this.canonicalCourse = canonicalCourse;
    this.year = year;
    this.semester = semester;
    this.catalogId = catalogId;
    this.departmentName = departmentName;
    this.section = StringUtils.trimToNull(section);
    this.meetingDays = meetingDays;
    this.room = room;
    this.description = StringUtils.trimToNull(description);
    this.instructors = instructors;
    this.setYouTubePlaylistId(StringUtils.trimToNull(youTubePlaylistId));
    this.capturePreferences = capturePreferences;
    this.license = license;
  }

  @Override
  public int getCcn() {
    return getCanonicalCourse().getCcn();
  }

  public CanonicalCourse getCanonicalCourse() {
    return canonicalCourse;
  }

  public int getYear() {
    return year;
  }

  public Semester getSemester() {
    return semester;
  }

  public String getSection() {
    return section;
  }

  public void setSection(final String section) {
    this.section = section;
  }

  public String getCatalogId() {
    return catalogId;
  }

  public String getDepartmentName() {
    return departmentName;
  }

  public void setDepartmentName(final String departmentName) {
    this.departmentName = departmentName;
  }

  public List<DayOfWeek> getMeetingDays() {
    return meetingDays;
  }

  public WarehouseRoom getRoom() {
    return room;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(final String description) {
    this.description = description;
  }

  public CapturePreferences getCapturePreferences() {
    return capturePreferences;
  }

  @SuppressWarnings("unused")
  public String[] getInstructors() {
    return instructors;
  }

  public AssociatedLicense getLicense() {
    return license;
  }

  public Set<Recording> getRecordings() {
    return recordings;
  }

  public void setRecordings(final Set<Recording> recordings) {
    this.recordings.clear();
    if (recordings != null) {
      this.recordings.addAll(recordings);
    }
  }

  public void addRecording(final Recording recording) {
    recordings.add(recording);
  }

  @Override
  public String toString() {
    return new ToStringBuilder(year, ToStringStyle.SIMPLE_STYLE).append(year).append(semester).append(getCcn()).toString();
  }

  @SuppressWarnings("unused")
  public String toStringForDebug() {
    return ToStringBuilder.reflectionToString(this);
  }

  @Override
  public final boolean equals(final Object o) {
    return o instanceof HasCourseKey && CourseUtils.equals(this, o);
  }

  @Override
  public final int hashCode() {
    return CourseUtils.hashCode(this);
  }

}
