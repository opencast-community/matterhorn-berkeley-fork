/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.warehouse;

import org.apache.commons.lang.StringUtils;
import org.opencastproject.participation.impl.Column;
import org.opencastproject.participation.impl.persistence.AbstractResultSetRowConverter;
import org.opencastproject.participation.model.CanonicalCourse;
import org.opencastproject.participation.model.CapturePreferences;
import org.opencastproject.participation.model.DayOfWeek;
import org.opencastproject.participation.model.RecordingAvailability;
import org.opencastproject.participation.model.RecordingType;
import org.opencastproject.participation.model.Semester;

import java.sql.Array;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author John Crossman
 */
class ResultConverterForWarehouseImpl extends AbstractResultSetRowConverter<WarehouseCourse> implements ResultConverterForWarehouse {

  /**
   * TODO: The warehouse-db column type: INTERVAL '${trimmed_length}' HOUR TO SECOND
   */
  @SuppressWarnings("unused")
  private enum ColInterval implements Column<Object> {
    trimmed_length;

    @Override
    public Class<Object> getType() {
      return Object.class;
    }
  }

  private final Map<String, WarehouseCourse> results;

  ResultConverterForWarehouseImpl() {
    results = new HashMap<String, WarehouseCourse>();
  }

  @Override
  public WarehouseCourse convert(final ResultSet r) throws SQLException {
    final Integer year = get(r, ColInt.year);
    final Semester semester = getSemester(r);
    final Integer ccn = get(r, ColInt.ccn);
    if (year == null || semester == null || ccn == null) {
      throw new IllegalStateException("Year (" + year + "), semester (" + semester + ") or CCN (" + ccn + ") are null. That is illegal.");
    }
    final String key = year.toString() + semester.getTermCode() + ccn.toString();
    final WarehouseCourse w;
    if (results.containsKey(key)) {
      w = results.get(key);
    } else {
      final CanonicalCourse canonicalCourse = new CanonicalCourse(ccn, null, get(r, ColStr.title));
      final AssociatedLicense license = getLicense(r);
      w = new WarehouseCourse(canonicalCourse, year, semester, get(r, ColStr.catalog_id), get(r, ColStr.department_name),
              get(r, ColStr.section), getDaysOfWeek(r), getWarehouseRoom(r), get(r, ColStr.description),
              getInstructorNames(r), get(r, ColStr.youtube_playlist_id), getCapturePreferences(r, license), license);
      results.put(key, w);
    }
    final Integer recordingId = get(r, ColInt.recording_id);
    if (recordingId != null && recordingId > 0) {
      w.addRecording(new Recording(recordingId, get(r, ColStr.recording_title),
              get(r, ColStr.recording_description), get(r, ColStr.youtube_video_id), get(r, ColDate.recording_start_utc),
              get(r, ColDate.recording_end_utc), getTrimmedLength(r), get(r, ColStr.matterhorn_episode_id)));
    }
    return w;
  }

  public List<WarehouseCourse> getWarehouseList() {
    return new LinkedList<WarehouseCourse>(results.values());
  }

  /**
   * Package-local used by unit test.
   * @return never null
   */
  Map<String, WarehouseCourse> getResults() {
    return results;
  }

  @SuppressWarnings("unused")
  private Integer getTrimmedLength(final ResultSet r) throws SQLException {
    // TODO: warehouse-db column type is INTERVAL '${trimmed_length}' HOUR TO SECOND
    // final Object value = get(r, ColInterval.trimmed_length);
    return null;
  }

  private String[] getInstructorNames(final ResultSet r) throws SQLException {
    final Array array = get(r, ColArray.instructor_names);
    final String[] values = array == null ? null : (String[]) array.getArray();
    return values == null || (values.length == 0) ? null : values;
  }

  private WarehouseRoom getWarehouseRoom(final ResultSet r) throws SQLException {
    final String building = StringUtils.trimToNull(get(r, ColStr.building_name));
    final String roomNumber = StringUtils.trimToNull(get(r, ColStr.room_number));
    final WarehouseBuilding wb = WarehouseUtils.getWarehouseBuilding(building);
    return building == null && roomNumber == null ? null : new WarehouseRoom(wb, roomNumber);
  }

  private List<DayOfWeek> getDaysOfWeek(final ResultSet r) throws SQLException {
    final Array array = get(r, ColArray.meeting_days);
    final String[] values = array == null ? null : (String[]) array.getArray();
    final List<DayOfWeek> days;
    if (values == null || values.length == 0) {
      days = null;
    } else {
      days = new LinkedList<DayOfWeek>();
      for (final String value : values) {
        for (final DayOfWeek day : DayOfWeek.values()) {
          if (StringUtils.startsWithIgnoreCase(day.name(), value)) {
            days.add(day);
          }
        }
      }
    }
    return days;
  }

  private CapturePreferences getCapturePreferences(final ResultSet r, final AssociatedLicense license) throws SQLException {
    final RecordingType recordingType = getRecordingType(r);
    final RecordingAvailability availability = getRecordingAvailability(r, license);
    return availability == null && recordingType == null ? null : new CapturePreferences(recordingType, availability, 0);
  }

  private RecordingAvailability getRecordingAvailability(final ResultSet r, final AssociatedLicense license) throws SQLException {
    final String type = get(r, ColStr.audience_type);
    final RecordingAvailability availability;
    if (type == null) {
      availability = null;
    } else if ("public".equals(type)) {
      availability = AssociatedLicense.creative_commons.equals(license)
              ? RecordingAvailability.publicCreativeCommons
              : RecordingAvailability.publicNoRedistribute;
    } else {
      availability = RecordingAvailability.studentsOnly;
    }
    return availability;
  }

  private Semester getSemester(final ResultSet r) throws SQLException {
    final String code = get(r, ColStr.semester_code);
    for (final Semester semester : Semester.values()) {
      if (semester.getTermCode() == code.charAt(0)) {
        return semester;
      }
    }
    return null;
  }

  private AssociatedLicense getLicense(final ResultSet r) throws SQLException {
    final String type = get(r, ColStr.license_type);
    return AssociatedLicense.valueOf(type);
  }

  private RecordingType getRecordingType(final ResultSet r) throws SQLException {
    final String type = get(r, ColStr.recording_type);
    if (type == null) {
      return null;
    } else {
      return AssociatedRecordingType.valueOf(type).getAssociatedEnum();
    }
  }

}
