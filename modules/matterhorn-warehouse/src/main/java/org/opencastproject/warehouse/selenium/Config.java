/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.warehouse.selenium;

import org.apache.commons.lang.StringUtils;
import org.opencastproject.util.MapUtils;
import org.opencastproject.util.env.EnvironmentUtil;

import java.util.Properties;

public final class Config {

  private Properties config;
  private String propertyKeyPrefix;

  private Config(final String propertyKeyPrefix) {
    this.propertyKeyPrefix = propertyKeyPrefix;
  }

  public static Config seleniumConfig() {
    return new Config(null);
  }

  static Config seleniumConfigLegacyMigration() {
    return new Config("legacyMigration");
  }

  String get(final String key) {
    return get(key, true);
  }

  String get(final String key, final boolean requiredValue) {
    if (config == null) {
      config = MapUtils.toProperties(EnvironmentUtil.getMatterhornConfigProperties());
    }
    String actualKey = StringUtils.isEmpty(propertyKeyPrefix) ? key : propertyKeyPrefix + '.' + key;
    String value = config.getProperty(actualKey);
    if (requiredValue && StringUtils.isBlank(value)) {
      throw new IllegalStateException("Blank value for property: " + actualKey);
    }
    return value;
  }

  int pageTimeoutSeconds() {
    return Integer.parseInt(get("selenium.youTube.timeoutSeconds.page"));
  }

  long ajaxTimeoutSeconds() {
    return pageTimeoutSeconds();
  }

  boolean useFakeYouTube() {
    return Boolean.valueOf(get("selenium.youTube.fake"));
  }

}
