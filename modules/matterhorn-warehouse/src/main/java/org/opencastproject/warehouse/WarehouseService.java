/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.warehouse;

import org.opencastproject.participation.model.HasCourseKey;
import org.opencastproject.participation.model.Semester;
import org.opencastproject.participation.model.Term;

import java.util.List;
import java.util.Set;

/**
 * Publishes elements from MediaPackages to youtube.
 */
public interface WarehouseService {

  /**
   * @return all courses in db.
   * @param forFeed if false then return ALL courses and ignore include_in_cc_feed column.
   */
  List<WarehouseCourse> getAllCourses(boolean forFeed);

  /**
   * Add or update course in warehouse db.
   *
   * @param course null not allowed
   */
  void upsertCourse(WarehouseCourse course);

  /**
   * Select all courses from warehouse db.
   */
  List<WarehouseCourse> getCourses(Integer year, Semester semester, boolean adjusted);

  /**
   * Delete course from warehouse db.
   *
   * @param courseKey
   *          primary key of associated warehouse db table
   */
  void deleteCourse(HasCourseKey courseKey);

  /**
   * YouTube videos per term.
   *
   * @param year
   *          Never null
   * @param semester
   *          Never null
   */
  Set<String> getYouTubeVideoIds(Integer year, Semester semester);

  /**
   * YouTube videos that have not had privacy status vetted.
   *
   * @param year
   *          Never null
   * @param semester
   *          Never null
   * @param retryPastFailures
   *          When true we try again on previous failures.
   */
  Set<String> getUnadjustedYouTubeVideos(Integer year, Semester semester, int limit, boolean retryPastFailures);

  /**
   * Make note of Selenium's successful edit of YouTube video metadata.
   *
   * @param year
   *          Never null
   * @param semester
   *          Never null
   * @param videoId
   *          YouTube video id
   * @param includeInFeed
   *          If true, the recording will be included in feed consumed by downstream services
   */
  void updateIncludeInFeed(Integer year, Semester semester, String videoId, boolean includeInFeed);

  /**
   * Regardless of success or failure, record the attempt by our Selenium code to edit YouTube video metadata.
   *
   * @param year
   *          Never null
   * @param semester
   *          Never null
   * @param videoId
   *          YouTube video id
   */
  void youTubeTreatedBySelenium(Integer year, Semester semester, String videoId);

  /**
   * Get YouTube video ids on which our Selenium code tried to edit metadata.
   *
   * @param year
   *          Never null
   * @param semester
   *          Never null
   * @param treatedBySelenium
   *          Filter result-set per treated_by_selenium flag in db
   * @param includeInFeed
   *          Filter result-set per include_in_cc_feed flag in db
   */
  Set<String> getBySeleniumStatus(Integer year, Semester semester, boolean treatedBySelenium, boolean includeInFeed);

}
