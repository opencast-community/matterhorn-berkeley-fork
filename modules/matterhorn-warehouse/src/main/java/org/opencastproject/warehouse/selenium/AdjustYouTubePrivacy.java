/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.warehouse.selenium;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.opencastproject.notify.Notifier;
import org.opencastproject.participation.model.Term;
import org.opencastproject.warehouse.WarehouseService;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

class AdjustYouTubePrivacy implements Attemptable<PrivacyAdjustmentSummary> {

  private static final int INTOLERABLE_FAILURE_COUNT = 12;
  private static final int INTOLERABLE_NOT_FOUND_COUNT = 3;

  private static final String LOGIN_URL = "https://accounts.google.com/ServiceLogin?continue=https://www.youtube.com";
  private static final String BASE_YOUTUBE_URL = "https://www.youtube.com";

  private final Logger logger = LoggerFactory.getLogger(this.getClass());

  private final List<String> successes = new LinkedList<String>();
  private final List<String> failures = new LinkedList<String>();
  private final List<String> notFound = new LinkedList<String>();
  private final Term term;
  private final LinkedList<String> unadjusted;
  private final WarehouseService warehouseService;
  private final Notifier notifier;
  private final Config config;
  private Se se = null;

  AdjustYouTubePrivacy(Term term, Set<String> unadjusted, WarehouseService warehouseService, Notifier notifier, Config config) {
    this.term = term;
    this.unadjusted = new LinkedList<String>(unadjusted);
    this.warehouseService = warehouseService;
    this.notifier = notifier;
    this.config = config;
  }

  @Override
  public PrivacyAdjustmentSummary perform() {
    logger.info("YouTube privacy will be UC Berkeley only for: " + ArrayUtils.toString(unadjusted));
    if (se == null) {
      se = new Se(LOGIN_URL, config);
    }
    String logInElementId = "identifierId";
    if (se.isPresent(By.id(logInElementId))) {
      // New Google UI
      logIn(se, logInElementId, "identifierNext");
    } else {
      // Old Google UI
      logIn(se, "Email", "next");
    }

    // It's nice to know which version of YouTube UI we're up against
    boolean isNewYouTubeUI = se.isPresent(By.id("sections"));
    logger.info('\n' + '\n' + "YouTube UI type: " + (isNewYouTubeUI ? "NEW" : "OLD") + '\n' + '\n');

    while (unadjusted.size() > 0) {
      // Pop next id off the stack
      final String id = unadjusted.removeFirst();
      boolean failure = false;
      // When we're done, the includeInFeed value will be written to the database.
      boolean includeInFeed = false;
      logger.info("Start work on YouTube video " + id);
      try {
        // The video exists. Next, adjust privacy-status.
        if (shareUcBerkeleyOnly(se, id)) {
          logger.info("Successfully adjusted privacy-status of video " + id);
          successes.add(id);
          includeInFeed = true;
        } else {
          logger.error("Failed to adjust privacy-status of video " + id);
          failure = true;
        }
      } catch (TimeoutException e) {
        logger.error("Failed to load video " + id + " due to " + e.getClass().getSimpleName() + ": " + e.getMessage());
        notFound.add(id);
      } catch (WebDriverException e) {
        logger.error("Error on YouTube video " + id, e);
        failure = true;
      }
      if (failure) {
        failures.add(id);
      }
      warehouseService.updateIncludeInFeed(term.getYear(), term.getSemester(), id, includeInFeed);
      warehouseService.youTubeTreatedBySelenium(term.getYear(), term.getSemester(), id);
      logger.info("Done with YouTube video " + id);
    }
    return new PrivacyAdjustmentSummary() {
      @Override
      public Term term() {
        return term;
      }
      @Override
      public List<String> successes() {
        return successes;
      }
      @Override
      public List<String> failures() {
        return failures;
      }
      @Override
      public List<String> notFound() {
        return notFound;
      }
    };
  }

  @Override
  public void close() {
    if (se != null) {
      se.quit();
    }
    if (successes.isEmpty() && failures.isEmpty() && notFound.isEmpty()) {
      logger.info("No videos need a privacy-status adjustment.");
    } else {
      String taskName = this.getClass().getSimpleName();
      if (notFound.size() > 0) {
        String subject = taskName + ": " + notFound.size() + " YouTube videos not found";
        String message = "Not found: " + join(notFound);
        logger.error(subject + '\n' + message);
        if (notFound.size() >= INTOLERABLE_NOT_FOUND_COUNT) {
          // Raise the alarm! Ops might need to delete a row from db or retract recording.
          notifier.notifyEngineeringTeam(subject, message);
        }
      }
      // Next, report on the videos we did find.
      String subject = successes.size() + " success(es); " + failures.size() + " failure(s)";
      String message = "Success(es): " + join(successes) + '\n' + '\n' + "Failure(s): " + join(failures);
      if (failures.isEmpty()) {
        logger.info(subject + '\n' + message);
      } else {
        if (failures.size() < INTOLERABLE_FAILURE_COUNT) {
          logger.warn(subject + '\n' + message);
        } else {
          // Ops should take action if failure count is intolerable
          notifier.notifyEngineeringTeam(taskName + ": " + subject, message);
          logger.error(subject + '\n' + message);
        }
      }
    }
  }

  private void logIn(final Se se, final String emailElementId, final String nextButtonId) {
    String userName = config.get("selenium.youTube.userName");
    logger.info("Begin YouTube login. Find element where id='" + emailElementId + "' and enter: " + userName);
    By googleEmail = By.id(emailElementId);
    se.type(googleEmail, userName);
    se.click(By.id(nextButtonId));
    SeUtils.sleep(2);
    logger.info("Enter CAS credentials");
    se.type(By.id("username"), config.get("selenium.youTube.casUserName"));
    se.type(By.id("password"), config.get("selenium.youTube.casPassword"));
    SeUtils.sleep(2);
    se.click(By.className("button"));
    SeUtils.sleep(2);
    logger.info("YouTube login step is complete where userName: " + userName);
  }

  private boolean shareUcBerkeleyOnly(final Se se, final String videoId) {
    boolean success;
    try {
      logger.info("Begin shareUcBerkeleyOnly(" + videoId + ")");
      makeVideoPrivate(se, videoId);
      se.click(By.className("metadata-share-button"));
      WebElement container = se.find(
          By.xpath("//div[contains(@class, 'dasher-container')]"),
          By.xpath("//span[contains(@class, 'yt-uix-form-input-checkbox-container')]"));
      String containerClass = container.getAttribute("class");
      if (containerClass.contains("checked")) {
        se.click(By.className("sharing-dialog-cancel"));
        logger.info("Do nothing. Video " + videoId + " is already 'private' and shared with UC Berkeley");
      } else {
        String shareWithDasher = "share_with_dasher";
        logger.info("Video " + videoId + " requires action");
        se.click(By.name(shareWithDasher));
        se.clickFirstMatch(By.className("sharing-dialog-ok"), By.className("metadata-share-button"));
      }
      SeUtils.sleep(2);

      logger.info("Video " + videoId + " now has privacy setting 'private', shared with UC Berkeley only.");
      String saveChangesClassName = "save-changes-button";
      By saveChangesButton = By.className(saveChangesClassName);
      if (se.find(saveChangesButton).isEnabled()) {
        se.click(saveChangesButton);
        logger.info("Button with class " + saveChangesClassName + " found and clicked");
        SeUtils.perform(5, new Attemptable<String>() {
          @Override
          public String perform() {
            WebElement saveMessage = se.find(By.className("save-error-message"));
            String text = saveMessage == null ? null : saveMessage.getText();
            String expected = "saved";
            if (text == null || !text.contains(expected)) {
              throw new WebDriverException(expected + " is the expected message. But we found: " + text);
            } else {
              logger.info("Message after save: " + text + " [videoId: " + videoId + "]");
            }
            return text;
          }
          @Override
          public void close() {
          }
        });
      } else {
        logger.info("Button with class " + saveChangesClassName + " is NOT enabled");
      }
      // Let the dust settle before we leave the page
      SeUtils.sleep(1);

      // Verify video status by reloading the 'edit' page
      logger.info("We are done with videoId: " + videoId + ". Next, analyze the updated privacy settings.");

      // Find text that describes latest sharing preference
      toEditPage(se, videoId);
      String shareTextClass = "metadata-share-text";
      WebElement element = se.find(By.className(shareTextClass));

      String descriptionOfShare = element.getText();
      logger.info("Element with class " + shareTextClass + " has text: " + descriptionOfShare);

      // If status is "Shared with berkeley.edu" then we know it's private for non-Berkeley users
      success = StringUtils.contains(descriptionOfShare, "Shared with berkeley.edu");
      logger.info(success ? "Success with " + videoId : "No success with " + videoId);

    } catch (Throwable e) {
      logger.error("Failed to share video with UC Berkeley only: " + videoId, e);
      success = false;
    }
    return success;
  }

  private void makeVideoPrivate(Se se, String videoId) {
    logger.info("First, make video " + videoId + " private");
    final String privacySelectName = "privacy";
    toEditPage(se, videoId);
    Select select = findSelectByNameAttribute(se, privacySelectName);
    String expectedValue = "private";
    boolean isSelected = expectedValue.equals(value(selectedOption(select)));
    if (!isSelected) {
      select.selectByValue(expectedValue);
      if (!isSelectedByNameAttribute(se, privacySelectName, expectedValue)) {
        throw new IllegalStateException(expectedValue + " is not selected in menu with className: " + privacySelectName);
      }
    }
    logger.info("YouTube privacy set to " + expectedValue + " for videoId: " + videoId);
  }

  private void toEditPage(Se se, String videoId) {
    se.load(BASE_YOUTUBE_URL + "/edit?video_id=" + videoId);
    se.acceptAlert();
    findSelectByNameAttribute(se, "privacy");
    logger.info("Found page element to EDIT video " + videoId);
  }

  private boolean isSelectedByNameAttribute(Se se, String attributeName, String optionValue) {
    Select select = findSelectByNameAttribute(se, attributeName);
    boolean isSelected = optionValue.equals(value(selectedOption(select)));
    logger.info("Select element with name='" + attributeName + "' is " + (isSelected ? "" : "NOT") + " set to optionValue: " + optionValue);
    return isSelected;
  }

  private WebElement selectedOption(Select select) {
    int size = select.getAllSelectedOptions().size();
    if (size != 1) {
      throw new IllegalStateException("The YouTube video privacy select menu has unexpected number of selected options: " + size);
    }
    return select.getAllSelectedOptions().get(0);
  }

  private Select findSelectByNameAttribute(Se se, String name) {
    WebElement element = se.find(By.name(name));
    element.click();
    logger.info("Element with name=" + name + " was found and then clicked");
    return new Select(element);
  }

  private String value(WebElement element) {
    return element.getAttribute("value");
  }

  private String join(List<String> list) {
    return StringUtils.join(list, ", ");
  }

}
