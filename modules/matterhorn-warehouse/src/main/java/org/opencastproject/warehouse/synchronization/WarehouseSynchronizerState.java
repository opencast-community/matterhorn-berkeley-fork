/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.warehouse.synchronization;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.opencastproject.participation.model.Term;

/**
 * Abstraction for the current or last execution of the {@link WarehouseSynchronizerService} service.
 *
 */
class WarehouseSynchronizerState {

  public enum Status {
    COMPLETE, IN_PROGRESS
  }

  private final DateTime startTime;
  private DateTime endTime;
  private final Term term;
  private final int courseTally;
  private final int warehouseCourseInTally;

  WarehouseSynchronizerState(Term term, int courseTally, int warehouseCourseTally) {
    this.startTime = new DateTime();
    this.term = term;
    this.courseTally = courseTally;
    this.warehouseCourseInTally = warehouseCourseTally;
  }
  
  void hasFinished() {
    endTime = new DateTime();
  }
  
  Status getStatus() {
    if (endTime == null) {
      return Status.IN_PROGRESS;
    } else {
      return Status.COMPLETE;
    }
  }

  long getDuration() {
    if (endTime != null) {
      final Duration duration = new Duration(startTime, endTime);
      return duration.getStandardSeconds();
    } else {
      final Duration duration = new Duration(startTime, new DateTime());
      return duration.getStandardSeconds();
    }
  }

  int getCourseTally() {
    return courseTally;
  }

  int getWarehouseCourseInTally() {
    return warehouseCourseInTally;
  }

  Term getTerm() {
    return term;
  }

  @Override
  public String toString() {
    return new StringBuilder("WarehouseSynchronizerState [Status=")
            .append(getStatus())
            .append(", Duration=")
            .append(getDuration())
            .append(" seconds")
            .append(", StartTime=")
            .append(startTime)
            .append(", EndTime=")
            .append(endTime)
            .append(", Term=")
            .append(term)
            .append(", CourseTally=")
            .append(courseTally)
            .append("]").toString();
  }

}
