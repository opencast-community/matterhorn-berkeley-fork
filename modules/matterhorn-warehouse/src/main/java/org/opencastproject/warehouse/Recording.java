/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.warehouse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.math.NumberUtils;

import java.util.Comparator;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author John Crossman
 */
public class Recording {

  private Integer recordingId;
  private String title;
  private final String description;
  private String youTubeVideoId;
  private String episodeId;
  private final Date recordingStart;
  private final Date recordingEnd;
  private Integer trimmedLengthSeconds;

  public Recording(final Integer recordingId, final String title, final String description, final String youTubeVideoId,
          final Date recordingStart, final Date recordingEnd, final Integer trimmedLengthSeconds, final String episodeId) {
    this.recordingId = recordingId;
    this.title = title;
    this.description = description;
    this.youTubeVideoId = youTubeVideoId;
    this.recordingStart = recordingStart;
    this.recordingEnd = recordingEnd;
    this.trimmedLengthSeconds = trimmedLengthSeconds;
    this.episodeId = episodeId;
  }

  public Recording(final String title, final String description, final String youTubeVideoId, final Date recordingStart,
          final Date recordingEnd, final Integer trimmedLengthSeconds, final String episodeId) {
    this(null, title, description, youTubeVideoId, recordingStart, recordingEnd, trimmedLengthSeconds, episodeId);
  }

  public Integer getRecordingId() {
    return recordingId;
  }

  public void setRecordingId(final Integer recordingId) {
    this.recordingId = recordingId;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(final String title) {
    this.title = title;
  }

  public String getDescription() {
    return description;
  }

  public String getYouTubeVideoId() {
    return youTubeVideoId;
  }

  public void setYouTubeVideoId(final String youTubeVideoId) {
    this.youTubeVideoId = youTubeVideoId;
  }

  public String getEpisodeId() {
    return episodeId;
  }

  public Date getRecordingStart() {
    return recordingStart;
  }

  public Date getRecordingEnd() {
    return recordingEnd;
  }

  public Integer getTrimmedLengthSeconds() {
    return trimmedLengthSeconds;
  }

  public void setTrimmedLengthSeconds(final Integer trimmedLengthSeconds) {
    this.trimmedLengthSeconds = trimmedLengthSeconds;
  }

  @Override
  public boolean equals(final Object o) {
    final boolean equals;
    if (o instanceof Recording) {
      final Recording that = (Recording) o;
      equals = new EqualsBuilder().append(youTubeVideoId, that.youTubeVideoId)
              .append(recordingStart, that.recordingStart)
              .append(recordingEnd, that.recordingEnd).isEquals();
    } else {
      equals = false;
    }
    return equals;
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder().append(youTubeVideoId).append(recordingStart).append(recordingEnd).toHashCode();
  }

  @Override public String toString() {
    return ToStringBuilder.reflectionToString(this);
  }

  public static class RecordingComparator implements Comparator<Recording> {

    private final Pattern findFirstNumber = Pattern.compile("(^|[\\sa-zA-Z\\-])([0-9]+)($|[\\sa-zA-Z\\-,\\(])");

    @Override public int compare(final Recording r1, final Recording r2) {
      final int comparison;
      if (r1.recordingStart != null && r2.recordingStart != null) {
        comparison = new CompareToBuilder().append(r1.recordingStart, r2.recordingStart)
                .append(r1.getTitle(), r2.getTitle())
                .toComparison();
      } else {
        final String title1 = getTitleSubstringForCompare(r1);
        final String title2 = getTitleSubstringForCompare(r2);
        final Integer index1 = getFirstNumber(title1);
        final Integer index2 = getFirstNumber(title2);
        comparison = index1.compareTo(index2);
      }
      return comparison;
    }

    private Integer getFirstNumber(final String s) {
      final Matcher m = findFirstNumber.matcher(s);
      return m.find() ? NumberUtils.toInt(m.group(2), 0) : 0;
    }

    private String getTitleSubstringForCompare(final Recording recording) {
      if (recording.getTitle() == null) {
        return null;
      }
      final String title = recording.getTitle();
      final String substring;
      final int indexOf = title.indexOf("Lecture");
      if (indexOf == -1) {
        substring = title.contains("-") ? StringUtils.substringAfter(title, "-") : title;
      } else {
        substring = title.substring(indexOf);
      }
      final String result = substring.contains(":") ? StringUtils.substringBefore(substring, ":") : substring;
      return StringUtils.trimToEmpty(result);
    }
  }

}
