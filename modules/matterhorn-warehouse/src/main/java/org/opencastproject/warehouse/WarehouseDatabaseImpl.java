/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.warehouse;

import com.google.common.util.concurrent.RateLimiter;
import org.apache.commons.lang.StringUtils;
import org.opencastproject.participation.impl.Column;
import org.opencastproject.participation.impl.persistence.AbstractResultSetRowConverter;
import org.opencastproject.participation.impl.persistence.DatabaseCredentials;
import org.opencastproject.participation.impl.persistence.DatabaseQueryManager;
import org.opencastproject.participation.impl.persistence.DatabaseQueryManagerImpl;
import org.opencastproject.participation.impl.persistence.ProducesSQL;
import org.opencastproject.participation.impl.persistence.ResultSetRowConverter;
import org.opencastproject.participation.model.CapturePreferences;
import org.opencastproject.participation.model.HasCourseKey;
import org.opencastproject.participation.model.License;
import org.opencastproject.participation.model.RecordingAvailability;
import org.opencastproject.participation.model.Semester;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author John Crossman
 */
public class WarehouseDatabaseImpl implements WarehouseDatabase {

  /** Used by Freemarker to format date in SQL statement */
  private static final String DATE_FORMAT_SQL = "yyyy-MM-dd HH:mmz";

  private final DatabaseQueryManager queryManager;

  public WarehouseDatabaseImpl(final DatabaseQueryManager queryManager) {
    this.queryManager = queryManager;
  }

  public WarehouseDatabaseImpl(final DatabaseCredentials credentials, final ProducesSQL producesSQL) {
    final String jdbcDriver = "org.postgresql.Driver";
    final RateLimiter rateLimiter = RateLimiter.create(4.0);
    queryManager = DatabaseQueryManagerImpl.getInstance(jdbcDriver, credentials, producesSQL, rateLimiter);
  }

  @Override
  public void upsertCourse(final WarehouseCourse course) {
    final WarehouseCourse existing =  getCourse(course);
    final Map<String, Recording> incomingRecordings = getYouTubeVideoIdMap(course);
    if (existing == null) {
      // Insert
      queryManager.upsert(WarehouseQuery.insertWarehouseCourse, getMapForCourseUpsert(course));
      if (!incomingRecordings.isEmpty()) {
        for (final Recording recording : incomingRecordings.values()) {
          queryManager.upsert(WarehouseQuery.insertRecording, getMapForRecordingUpsert(course, recording));
        }
      }
    } else {
      // Update course
      queryManager.upsert(WarehouseQuery.updateWarehouseCourse, getMapForCourseUpsert(course));
      final Map<String, Recording> existingRecordings = getYouTubeVideoIdMap(existing);
      // Update recordings
      for (final String youTubeVideoId : incomingRecordings.keySet()) {
        final Recording existingRecording = existingRecordings.get(youTubeVideoId);
        final Recording incomingRecording = incomingRecordings.get(youTubeVideoId);
        if (existingRecording == null) {
          queryManager.upsert(WarehouseQuery.insertRecording, getMapForRecordingUpsert(course, incomingRecording));
        } else {
          incomingRecording.setRecordingId(existingRecording.getRecordingId());
          queryManager.upsert(WarehouseQuery.updateRecording, getMapForRecordingUpsert(course, incomingRecording));
        }
      }
      // Delete recordings
      for (final String youTubeVideoId : existingRecordings.keySet()) {
        final Recording existingRecord = existingRecordings.get(youTubeVideoId);
        final boolean deleteRecording = !incomingRecordings.containsKey(youTubeVideoId);
        if (deleteRecording) {
          final Map<Column, Object> map = new HashMap<Column, Object>();
          put(map, ColStr.recordingId, existingRecord.getRecordingId());
          queryManager.upsert(WarehouseQuery.deleteRecording, map);
        }
      }
    }
  }

  @Override
  public WarehouseCourse getCourse(final HasCourseKey courseKey) {
    final ResultConverterForWarehouse converter = new ResultConverterForWarehouseImpl();
    queryManager.getSet(converter, WarehouseQuery.getCourseByYearSemesterCCN, getCourseKeyMap(courseKey));
    final List<WarehouseCourse> list = converter.getWarehouseList();
    final int size = list.size();
    if (size > 1) {
      throw new IllegalStateException(size + " results for a single courseKey: " + courseKey);
    }
    return (size == 0) ? null : list.iterator().next();
  }

  @Override
  public List<WarehouseCourse> getCourses(final Integer year, final Semester semester, final boolean adjusted) {
    ResultConverterForWarehouse converter = new ResultConverterForWarehouseImpl();
    Map<Column, Object> map = prepareQueryArgs(year, semester);
    map.put(ColBoolean.include_in_cc_feed, adjusted);
    queryManager.getSet(converter, WarehouseQuery.getCourseByYearSemesterCCN, map);
    return converter.getWarehouseList();
  }

  @Override
  public List<WarehouseCourse> getAllCourses(final boolean forFeed) {
    final Map<Column, Object> args = new HashMap<Column, Object>();
    if (forFeed) {
      args.put(ColBoolean.include_in_cc_feed, true);
    }
    final ResultConverterForWarehouse converter = new ResultConverterForWarehouseImpl();
    queryManager.getSet(converter, WarehouseQuery.getCourseByYearSemesterCCN, args);
    return converter.getWarehouseList();
  }

  @Override
  public void deleteCourse(final HasCourseKey courseKey) {
    final Map<Column, Object> map = getCourseKeyMap(courseKey);
    queryManager.upsert(WarehouseQuery.deleteAllCourseRecordings, map);
    queryManager.upsert(WarehouseQuery.deleteCourse, map);
  }

  @Override
  public Set<String> getYouTubeVideoIds(Integer year, Semester semester) {
    Map<Column, Object> map = prepareQueryArgs(year, semester);
    return queryManager.getSet(getYouTubeVideoIdRowConverter(), WarehouseQuery.getYouTubeVideoByTerm, map);
  }

  @Override
  public Set<String> getUnadjustedYouTubeVideos(Integer year, Semester semester, int limit, boolean retryPastFailures) {
    Map<Column, Object> map = prepareQueryArgs(year, semester);
    if (!retryPastFailures) {
      map.put(ColBoolean.treated_by_selenium, Boolean.FALSE);
    }
    map.put(ColBoolean.include_in_cc_feed, Boolean.FALSE);
    map.put(ColInt.limit, limit);
    return queryManager.getSet(getYouTubeVideoIdRowConverter(), WarehouseQuery.getYouTubeVideoByTerm, map);
  }

  @Override
  public void updateIncludeInFeed(Integer year, Semester semester, String videoId, boolean includeInFeed) {
    final Map<Column, Object> map = prepareQueryArgs(year, semester);
    put(map, ColStr.youtube_video_id, videoId);
    put(map, ColBoolean.include_in_cc_feed, includeInFeed);
    queryManager.upsert(WarehouseQuery.updateIncludeInFeed, map);
  }

  @Override
  public void youTubeTreatedBySelenium(Integer year, Semester semester, String videoId) {
    final Map<Column, Object> map = prepareQueryArgs(year, semester);
    put(map, ColStr.youtube_video_id, videoId);
    queryManager.upsert(WarehouseQuery.youTubeTreatedBySelenium, map);
  }

  @Override
  public Set<String> getBySeleniumStatus(Integer year, Semester semester, boolean treatedBySelenium, boolean includeInFeed) {
    final Map<Column, Object> map = prepareQueryArgs(year, semester);
    put(map, ColBoolean.treated_by_selenium, treatedBySelenium);
    put(map, ColBoolean.include_in_cc_feed, includeInFeed);
    return queryManager.getSet(getYouTubeVideoIdRowConverter(), WarehouseQuery.getBySeleniumStatus, map);
  }

  private Map<String, Recording> getYouTubeVideoIdMap(final WarehouseCourse course) {
    final Map<String, Recording> map = new HashMap<String, Recording>();
    for (final Recording recording : course.getRecordings()) {
      final String youTubeVideoId = recording.getYouTubeVideoId();
      if (StringUtils.isNotBlank(youTubeVideoId)) {
        map.put(youTubeVideoId, recording);
      }
    }
    return map;
  }

  Map<Column, Object> getMapForCourseUpsert(final WarehouseCourse course) {
    final CapturePreferences cp = course.getCapturePreferences();
    final RecordingAvailability availability = cp == null || cp.getRecordingAvailability() == null
            ? RecordingAvailability.studentsOnly
            : cp.getRecordingAvailability();
    final String audienceType = availability.equals(RecordingAvailability.studentsOnly) ? "course" : "public";
    final AssociatedLicense license = License.allRightsReserved.equals(availability.getLicense())
            ? AssociatedLicense.all_rights_reserved
            : AssociatedLicense.creative_commons;
    final Map<Column, Object> map = new HashMap<Column, Object>();
    put(map, ColStr.course, course);
    put(map, ColStr.title, course.getCanonicalCourse().getName());
    put(map, ColStr.department_name, course.getDepartmentName());
    put(map, ColStr.description, course.getDescription());
    put(map, ColStr.audience_type, audienceType);
    put(map, ColStr.recording_type, WarehouseUtils.getAssociatedRecordingType(course));
    put(map, ColStr.license_type, license);
    return map;
  }

  private Map<Column, Object> getMapForRecordingUpsert(final WarehouseCourse course, final Recording recording) {
    final Map<Column, Object> map = new HashMap<Column, Object>();
    put(map, ColStr.dateParsePattern, DATE_FORMAT_SQL);
    put(map, ColStr.course, course);
    put(map, ColStr.recording, recording);
    put(map, ColStr.title, recording.getTitle());
    put(map, ColStr.description, recording.getDescription());
    put(map, ColStr.youtube_video_id, recording.getYouTubeVideoId());
    if (recording.getEpisodeId() != null) {
      put(map, ColStr.matterhorn_episode_id, recording.getEpisodeId());
    }
    if (recording.getTrimmedLengthSeconds() != null) {
      put(map, ColStr.trimmed_length, WarehouseUtils.formatTrimmedLength(recording.getTrimmedLengthSeconds()));
    }
    return map;
  }

  private Map<Column, Object> getCourseKeyMap(final HasCourseKey courseKey) {
    final Map<Column, Object> map = prepareQueryArgs(courseKey.getYear(), courseKey.getSemester());
    put(map, ColInt.ccn, courseKey.getCcn());
    return map;
  }

  private void put(final Map<Column, Object> map, final Column<?> column, final Object value) {
    map.put(column, value);
  }

  private Map<Column, Object> prepareQueryArgs(Integer year, Semester semester) {
    Map<Column, Object> map = new HashMap<Column, Object>();
    put(map, ColInt.year, year);
    put(map, ColStr.semester_code, semester == null ? null : semester.getTermCode());
    return map;
  }

  private ResultSetRowConverter<String> getYouTubeVideoIdRowConverter() {
    return new AbstractResultSetRowConverter<String>() {
      @Override
      public String convert(ResultSet r) throws SQLException {
        return get(r, ColStr.youtube_video_id);
      }
    };
  }
}
