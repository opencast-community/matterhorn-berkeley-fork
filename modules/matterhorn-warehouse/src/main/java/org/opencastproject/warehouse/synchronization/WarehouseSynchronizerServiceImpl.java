/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.warehouse.synchronization;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.opencastproject.job.api.EManagedService;
import org.opencastproject.notify.Notifier;
import org.opencastproject.participation.CourseUtils;
import org.opencastproject.participation.impl.persistence.DatabaseCredentials;
import org.opencastproject.participation.impl.persistence.ProduceSQLFreemarker;
import org.opencastproject.participation.impl.persistence.WebcastDatabase;
import org.opencastproject.participation.model.Booking;
import org.opencastproject.participation.model.CourseData;
import org.opencastproject.participation.model.Term;
import org.opencastproject.schedulableservice.api.Periods;
import org.opencastproject.schedulableservice.api.SchedulableServiceService;
import org.opencastproject.util.data.Collections;
import org.opencastproject.util.env.EProperties;
import org.opencastproject.util.env.EnvironmentUtil;
import org.opencastproject.warehouse.JSONFeedComparator;
import org.opencastproject.warehouse.WarehouseCourse;
import org.opencastproject.warehouse.WarehouseDatabase;
import org.opencastproject.warehouse.WarehouseDatabaseImpl;
import org.opencastproject.warehouse.WarehouseFeedGeneratorJSON;
import org.opencastproject.warehouse.WarehouseService;
import org.opencastproject.warehouse.WarehouseServiceImpl;
import org.opencastproject.warehouse.selenium.Config;
import org.opencastproject.warehouse.selenium.Se;
import org.opencastproject.warehouse.selenium.YouTubeSession;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Pattern;

import static org.opencastproject.warehouse.synchronization.WarehouseSynchronizerState.Status.IN_PROGRESS;

/**
 * Periodically refreshes the contents of the Webcast Warehouse database by extracting all valid series for a given term
 * from Matterhorn, combining series and recording data with data from Salesforce and Course Catalog Materialized View
 * to create {@code WarehouseCourse} objects to insert into the database via the Warehouse service; once refreshed, this
 * data is used to create a new Webcast JSON feed file.
 *
 */
public class WarehouseSynchronizerServiceImpl extends EManagedService implements WarehouseSynchronizerService {

  private static final int DEFAULT_THRESHOLD_WAREHOUSE_COURSE_DELETE = 0;
  private static final String WAREHOUSE_SYNC_OPERATION_ID = "warehouse-synchronizer-service";
  static final String KEY_PERIOD = "org.opencastproject.warehouse.synchronization.WarehouseSynchronizerServiceImpl.schedulingPeriod";
  static final String THRESHOLD_WAREHOUSE_COURSE_DELETE = "org.opencastproject.warehouse.synchronization.WarehouseSynchronizerServiceImpl.thresholdWarehouseCourseDelete";

  private final Logger logger = LoggerFactory.getLogger(this.getClass());
  private Term term;
  private int schedulingPeriod;
  private int countDownToSchedulingPeriod;
  private WarehouseSynchronizerState execution;
  private SchedulableServiceService schedulableServiceService;
  private MatterhornCourseService matterhornCourseService;
  private WarehouseService warehouseService;
  private Notifier notifier;
  private int thresholdWarehouseCourseDelete = DEFAULT_THRESHOLD_WAREHOUSE_COURSE_DELETE;

  /**
   * Periodically called by {@code SchedulableServiceService}, this method is the driver for this service.
   */
  @Override
  public void callBack() {
    if (isTimeToRun()) {
      run();
    }
  }

  /**
   * Package-local for sake of unit tests.
   */
  void run() {
    try {
      term = Term.getTermFromProperties();
      String regex = term.getYear().toString() + term.getSemester().getTermCode() + "[0-9]{1,5}";
      logger.info("Update the warehouse-db for series IDs matching " + regex);
      final Set<String> currentTermSeriesIds = matterhornCourseService.getMatchingSeriesIds(Pattern.compile(regex));
      if (currentTermSeriesIds.size() == 0) {
        logger.error("Potentially a problem: No Matterhorn courses found (regex: " + regex + ") for " + term.getSemester() + ' ' + term.getYear());
      } else {
        final List<WarehouseCourse> thisSemesterCourseList = warehouseService.getCourses(term.getYear(), term.getSemester(), true);
        logger.info("Warehouse database has " + thisSemesterCourseList.size() + " " + term + " course records");
        execution = new WarehouseSynchronizerState(term, currentTermSeriesIds.size(), thisSemesterCourseList.size());
        // Delete courses from warehouse
        final List<WarehouseCourse> deleteList = new LinkedList<WarehouseCourse>();
        for (final WarehouseCourse warehouseCourse : thisSemesterCourseList) {
          if (!currentTermSeriesIds.contains(CourseUtils.getSeriesId(warehouseCourse))) {
            deleteList.add(warehouseCourse);
          }
        }
        deleteWarehouseCourses(deleteList);
        //
        for (final String seriesId : currentTermSeriesIds) {
          final WarehouseCourse warehouseCourse = matterhornCourseService.buildWarehouseCourse(seriesId);
          if (warehouseCourse == null) {
            logger.warn("Failed to construct warehouseCourse instance where seriesId = " + seriesId);
          } else {
            warehouseService.upsertCourse(warehouseCourse);
          }
        }
        //
        // We use Selenium to make YouTube videos "private, shared UC Berkeley only".
        YouTubeSession youTubeSession = Se.youTubeSession(warehouseService, notifier, 30, Config.seleniumConfig());
        youTubeSession.ucBerkeleyOnly(term, true);
        //
        logger.info("Generate JSON feed which will be consumed by CalCentral / bCourses.");
        final Set<WarehouseCourse> jsonFeed = new TreeSet<WarehouseCourse>(new JSONFeedComparator());
        final List<WarehouseCourse> allCourses = warehouseService.getAllCourses(true);
        final int allCourseListSize = allCourses.size();
        logger.info("warehouse-db has " + allCourseListSize + " courses to put in JSON feed");
        jsonFeed.addAll(allCourses);
        for (final WarehouseCourse next : allCourses) {
          // Add cross-listed courses
          final Booking set = matterhornCourseService.getCrossListedCourseSet(CourseUtils.getSeriesId(next));
          if (set != null) {
            final Set<CourseData> courseList = set.getCrossListedCourses();
            if (CollectionUtils.isNotEmpty(courseList)) {
              for (final CourseData courseData : courseList) {
                final WarehouseCourse crossListing = SynchronizerUtils.getWarehouseCourse(courseData,
                    next.getRecordings(), next.getMeetingDays(), next.getRoom(), next.getYouTubePlaylistId(),
                    next.getCapturePreferences(), next.getLicense());
                logger.info(next + " is cross-listed as " + crossListing);
                jsonFeed.add(crossListing);
              }
            }
          }
        }
        generateJsonFeed(jsonFeed);
        logger.info("Updates to the warehouse database and JSON feed are done.");
        logger.info(execution.toString());
      }
    } catch (final Exception e) {
      logger.error(e.getMessage(), e);
      final IllegalStateException e2 = new IllegalStateException(e);
      notifier.notifyEngineeringTeam(e.getMessage(), e, term);
      throw e2;
    } finally {
      if (execution != null) {
        execution.hasFinished();
      }
    }
  }

  @Override
  public WarehouseSynchronizerState getWarehouseSynchronizerState() {
    return execution;
  }

  @Override
  public void updatedConfiguration(final Properties properties) throws ConfigurationException {
    try {
      if (properties == null) {
        throw new IllegalStateException("Missing valid Properties file");
      }
      scheduleService(properties);
      final String thresholdValue = properties.getProperty(THRESHOLD_WAREHOUSE_COURSE_DELETE);
      thresholdWarehouseCourseDelete = NumberUtils.toInt(thresholdValue, DEFAULT_THRESHOLD_WAREHOUSE_COURSE_DELETE);
      logger.info("Updated {} with properties containing {} keys", this.getClass().getSimpleName(), properties.size());
    } catch (final Throwable e) {
      logger.error("Unable to configure {} with properties", this.getClass().getSimpleName());
      notifier.notifyEngineeringTeam("Initialization failed", e);
      throw new ConfigurationException(this.getClass().getSimpleName(), "Unable to load configuration properties", e);
    }
  }

  Term getTerm() {
    return term;
  }

  @SuppressWarnings("all")
  public void setSchedulableServiceService(SchedulableServiceService schedulableServiceService) {
    this.schedulableServiceService = schedulableServiceService;
  }

  @SuppressWarnings("all")
  public void setMatterhornCourseService(MatterhornCourseService matterhornCourseService) {
    this.matterhornCourseService = matterhornCourseService;
  }

  @SuppressWarnings("all")
  public void setWarehouseService(WarehouseService warehouseService) {
    this.warehouseService = warehouseService;
  }

  @SuppressWarnings("all")
  public void setNotifier(final Notifier notifier) {
    this.notifier = notifier;
  }

  @SuppressWarnings("unused")
  protected synchronized void activate(final ComponentContext cc) throws Exception {
    setWarehouseService(new WarehouseServiceImpl(getWarehouseDatabase()));
  }

  public static WarehouseDatabase getWarehouseDatabase() throws IOException {
    String relativePath = "etc/services/org.opencastproject.warehouse.WarehouseServiceImpl.properties";
    final File propertiesFile = EnvironmentUtil.getFileUnderFelixHome(relativePath);
    final Properties p = new Properties();
    FileInputStream inputStream = null;
    try {
      inputStream = new FileInputStream(propertiesFile);
      p.load(inputStream);
      final EProperties eProperties = EnvironmentUtil.createEProperties(p, true);
      final DatabaseCredentials credentials = CourseUtils.getDatabaseCredentials("db.warehouse.", eProperties, WebcastDatabase.webcast_warehouse);
      //
      final File directory = EnvironmentUtil.getFileUnderFelixHome("etc/sql/webcast_warehouse");
      final ProduceSQLFreemarker produceSQL = new ProduceSQLFreemarker(directory, "sql");
      return new WarehouseDatabaseImpl(credentials, produceSQL);
    } finally {
      IOUtils.closeQuietly(inputStream);
    }
  }

  private void generateJsonFeed(final Set<WarehouseCourse> courses) {
    try {
      final WarehouseFeedGeneratorJSON generatorJSON = new WarehouseFeedGeneratorJSON();
      writeWebcastJSON(generatorJSON.toJson(courses));
      logger.info("JSON generated with " + courses.size() + " courses");
    } catch (IOException e) {
      final String message = "Unable to generate JSON from warehouse-db, plus cross-listed courses.";
      notifier.notifyEngineeringTeam(message, e);
      logger.debug(message, e);
    }
  }

  /**
   * Update webcast.json file with content provided. UTF-8 encoding.
   * @param json The content to be written to file.
   * @throws IOException when process is unable to write to the file.
   */
  private void writeWebcastJSON(final String json) throws IOException {
    final File file = new File("/var/www/html/warehouse/webcast.json");
    File dir = file.getParentFile();
    if (dir.mkdirs()) {
      logger.info("Directory created: " + dir.getAbsolutePath());
    }
    FileUtils.writeStringToFile(file, json, "UTF-8");
  }

  private boolean isTimeToRun() {
    // Because SchedulableServiceService handles only minute, hour, or day periods. Since our requirement is for
    // n minutes, we put in this hack pending SchedulableServiceService getting enhanced; very doable but no time today
    //TODO enhance SchedulableServiceService to handle n minute periods
    boolean timeToRun = false;
    if (schedulingPeriod > 0) {
      countDownToSchedulingPeriod--;
      if (countDownToSchedulingPeriod == 0) {
        timeToRun = true;
        countDownToSchedulingPeriod = schedulingPeriod;
      }
    }
    if (timeToRun && execution != null && IN_PROGRESS.equals(execution.getStatus())) {
      timeToRun = false;
      if (logger.isDebugEnabled()) {
        logger.warn("The previous sync job is still running so we'll skip the run scheduled for now. Configurations: schedulingPeriod = " +
            schedulingPeriod + "; execution.status = " + execution.getStatus() + "; countDownToSchedulingPeriod = " +
            countDownToSchedulingPeriod);
      }
    }
    if (timeToRun) {
      logger.debug("Time to run; schedulingPeriod:" + schedulingPeriod);
    }
    return timeToRun;
  }

  private void scheduleService(final Properties properties) {
    final String propertyPeriodStr = (String) properties.get(KEY_PERIOD);
    final String propertyPeriod = (StringUtils.isNotBlank(propertyPeriodStr)) ? propertyPeriodStr : "0";
    schedulingPeriod = Integer.parseInt(propertyPeriod);
    if (schedulingPeriod < 0) {
      String errorMessage = "Negative number is not a valid time period: " + schedulingPeriod;
      schedulingPeriod = 0;
      throw new IllegalArgumentException(errorMessage);
    }
    if (schedulingPeriod == 0) {
      final String message = this.getClass().getSimpleName() + " will never run, as specified in configuration file.";
      logger.warn(message);
      notifier.notifyEngineeringTeam(message, message, propertyPeriodStr);
    } else {
      schedulableServiceService.scheduleServiceEvent(this, this.getClass().getSimpleName(), WAREHOUSE_SYNC_OPERATION_ID, Periods.MINUTE);
      countDownToSchedulingPeriod = schedulingPeriod;
      logger.warn("This job will run every {} minutes", schedulingPeriod);
    }
  }

  private void deleteWarehouseCourses(final List<WarehouseCourse> deleteList) {
    if (deleteList.isEmpty()) {
      logger.info("No course deletions are necessary for warehouse database.");
    } else if (deleteList.size() > thresholdWarehouseCourseDelete) {
      final String subject = WarehouseSynchronizerService.class.getSimpleName() + " wants to delete "
              + deleteList.size() + " warehouse courses";
      final String message = "No deletions have been performed. Ops intervention is necessary.\n\nCourses intended for deletion: "
              + Collections.mkString(deleteList, ", ");
      notifier.notifyEngineeringTeam(subject, message);
      logger.error(subject + '\n' + message);
    } else {
      for (final WarehouseCourse deleteThis : deleteList) {
        warehouseService.deleteCourse(deleteThis);
        logger.warn("Course deleted from warehouse database: " + deleteThis);
      }
    }
  }

}
