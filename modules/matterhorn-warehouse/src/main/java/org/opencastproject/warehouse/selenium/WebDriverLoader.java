/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.warehouse.selenium;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.opencastproject.util.env.EnvironmentUtil;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.util.Date;
import java.util.concurrent.TimeUnit;

final class WebDriverLoader implements Attemptable<WebDriver> {

  private Config config;
  private final String url;
  private WebDriver driver;

  WebDriverLoader(String url, Config config) {
    this.url = url;
    this.config = config;
  }

  public WebDriver perform() {
    try {
      String driverType = config.get("selenium.driver");
      if (driverType.equalsIgnoreCase("Firefox")) {
        String logfile = System.getProperty("opencast.logdir") + '/' + "selenium-firefox-driver_" + DateFormatUtils.ISO_DATE_FORMAT.format(new Date()) + ".log";
        System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE, logfile);
        //
        String pathToBinary = StringUtils.trimToNull(config.get("selenium.firefox.pathToBinary", false));
        FirefoxBinary firefoxBinary = pathToBinary == null ? new FirefoxBinary() : new FirefoxBinary(new File(pathToBinary));
        firefoxBinary.addCommandLineOptions("--display=:99.0");
        //
        String version = config.get("selenium.firefox.version");
        DesiredCapabilities capabilities = new DesiredCapabilities(BrowserType.FIREFOX, version, Platform.LINUX);
        driver = new FirefoxDriver(firefoxBinary, loadFirefoxProfile(), capabilities);
      } else {
        throw new UnsupportedOperationException("Unrecognized Selenium driver type: " + driverType);
      }
      WebDriver.Options options = driver.manage();
      options.deleteAllCookies();
      options.timeouts().setScriptTimeout(config.ajaxTimeoutSeconds(), TimeUnit.SECONDS);
      options.timeouts().pageLoadTimeout(config.pageTimeoutSeconds(), TimeUnit.SECONDS);
      driver.get(url);
      return driver;
    } catch (RuntimeException e) {
      if (driver != null) {
        driver.quit();
      }
      throw e;
    }
  }

  /**
   * If config 'selenium.firefox.profileDirectory' is not blank then construct profile as described in
   * https://seleniumhq.github.io/selenium/docs/api/java/org/openqa/selenium/firefox/FirefoxProfile.html#FirefoxProfile-java.io.File-
   * Otherwise, use default profile preferences.
   *
   * The optional selenium.firefox.profileDirectory allows QA to mimic certain browser conditions (e.g., cookies)
   * when testing Selenium scripts.
   *
   * @return {@link FirefoxProfile} instance
   */
  private FirefoxProfile loadFirefoxProfile() {
    String profileDirectory = EnvironmentUtil.getRuntimePropertyOverrides().get("selenium.firefox.profileDirectory");
    final FirefoxProfile profile;
    if (StringUtils.isEmpty(profileDirectory)) {
      profile = new FirefoxProfile();
      profile.setPreference("browser.download.folderList", 2);
      profile.setPreference("browser.download.manager.showWhenStarting", false);
      profile.setPreference("browser.helperApps.neverAsk.openFile", "application/xml");
      profile.setPreference("browser.helperApps.alwaysAsk.force", false);
    } else {
      // Construct a firefox profile from an existing profile directory.
      profile = new FirefoxProfile(new File(profileDirectory));
    }
    return profile;
  }

  @Override
  public void close() {
  }
}