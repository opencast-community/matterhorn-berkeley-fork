/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.warehouse.synchronization;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.opencastproject.episode.api.EpisodeQuery;
import org.opencastproject.episode.api.EpisodeService;
import org.opencastproject.episode.api.SearchResult;
import org.opencastproject.episode.api.UriRewriter;
import org.opencastproject.metadata.dublincore.DublinCoreCatalog;
import org.opencastproject.metadata.dublincore.DublinCoreCatalogList;
import org.opencastproject.notify.Notifier;
import org.opencastproject.notify.SendType;
import org.opencastproject.participation.CourseUtils;
import org.opencastproject.participation.api.CourseManagementService;
import org.opencastproject.participation.impl.CourseManagementServiceImpl;
import org.opencastproject.participation.impl.persistence.CourseCatalogServiceImpl;
import org.opencastproject.participation.impl.persistence.DatabaseCredentials;
import org.opencastproject.participation.impl.persistence.DatabaseQueryManager;
import org.opencastproject.participation.impl.persistence.UCBerkeleyCourseDatabaseImpl;
import org.opencastproject.participation.model.CanonicalCourse;
import org.opencastproject.participation.model.CapturePreferences;
import org.opencastproject.participation.model.DayOfWeek;
import org.opencastproject.participation.model.Semester;
import org.opencastproject.participation.model.Term;
import org.opencastproject.salesforce.SalesforceConnectorServiceImpl;
import org.opencastproject.salesforce.SalesforceObjectTransformerImpl;
import org.opencastproject.schedulableservice.api.SchedulableServiceService;
import org.opencastproject.security.api.SecurityService;
import org.opencastproject.security.api.UnauthorizedException;
import org.opencastproject.series.api.SeriesException;
import org.opencastproject.series.api.SeriesQuery;
import org.opencastproject.series.api.SeriesService;
import org.opencastproject.util.NotFoundException;
import org.opencastproject.util.data.Collections;
import org.opencastproject.util.env.EProperties;
import org.opencastproject.util.env.EnvironmentUtil;
import org.opencastproject.warehouse.AssociatedLicense;
import org.opencastproject.warehouse.WarehouseBuilding;
import org.opencastproject.warehouse.WarehouseCourse;
import org.opencastproject.warehouse.WarehouseDatabase;
import org.opencastproject.warehouse.WarehouseDatabaseImpl;
import org.opencastproject.warehouse.WarehouseRoom;
import org.opencastproject.warehouse.WarehouseServiceImpl;
import org.opencastproject.workspace.api.Workspace;
import org.osgi.service.cm.ConfigurationException;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;
import java.util.regex.Pattern;

import static junit.framework.TestCase.assertNotNull;
import static org.easymock.EasyMock.anyObject;
import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.junit.Assert.assertEquals;

/**
 * @author John Crossman
 */
@Ignore
public class WarehouseSynchronizerServiceImplTest {

  private static final String INTRODUCTION_TO_ENVIRONMENTAL_STUDIES = "Introduction to Environmental Studies";
  private static WarehouseSynchronizerServiceImpl service;

  @Test
  public void testPattern() {
    Term term = Term.getTermFromProperties();
    String regex = term.getYear().toString() + term.getSemester().getTermCode() + "[0-9]{1,5}";
    System.out.println(regex);
    Pattern pattern = Pattern.compile(regex);
    System.out.println(term.getYear());
    System.out.println(term.getSemester().getTermCode());
    System.out.println(pattern);
  }

  @BeforeClass
  public static void beforeClass() throws Exception {
    // Mock
    final SecurityService securityServiceMock = createSecurityServiceMock();
    final DublinCoreCatalog catalog = createDublinCoreCatalogMock();
    final SeriesService seriesServiceMock = createSeriesServiceMock(catalog);
    final SearchResult searchResultMock = createMock(SearchResult.class);
    final EpisodeService episodeServiceMock = getEpisodeServiceMock(searchResultMock);
    final Workspace workspaceMock = getWorkspaceMock();
    final SchedulableServiceService schedulableServiceServiceMock = getSchedulableServiceServiceMock();
    final Notifier notifier = getLoggerNotifier();
    //
    service = new WarehouseSynchronizerServiceImpl();
    final MatterhornCourseServiceImpl mcs = new MatterhornCourseServiceImpl();
    final CourseCatalogServiceImpl ccs = new CourseCatalogServiceImpl();
    ccs.setCourseDatabase(new UCBerkeleyCourseDatabaseImpl(getTestCatalogCredentials(), notifier));
    ccs.setSecurityService(securityServiceMock);
    ccs.setSeriesService(seriesServiceMock);
    //
    mcs.setCourseCatalogService(ccs);
    mcs.setSeriesService(seriesServiceMock);
    mcs.setCourseManagementService(getCourseManagementService(notifier));
    mcs.setEpisodeService(episodeServiceMock);
    mcs.setWorkspace(workspaceMock);
    //
    service.setMatterhornCourseService(mcs);
    service.setNotifier(notifier);
    service.setSchedulableServiceService(schedulableServiceServiceMock);
    final Properties properties = new Properties();
    properties.put(WarehouseSynchronizerServiceImpl.KEY_PERIOD, "1");
    properties.put(WarehouseSynchronizerServiceImpl.THRESHOLD_WAREHOUSE_COURSE_DELETE, "2");
    service.updatedConfiguration(properties);
    //
    replay(catalog, securityServiceMock, seriesServiceMock, searchResultMock, episodeServiceMock, workspaceMock,
            schedulableServiceServiceMock);
  }

  @Test
  public void testRun() {
    final WarehouseDatabase warehouseDatabase = getWarehouseDatabase();
    warehouseDatabase.upsertCourse(createWarehouseCourse(2014, Semester.Fall, 28927));
    service.setWarehouseService(new WarehouseServiceImpl(warehouseDatabase));
    service.run();
  }

  @Test
  public void testSemester() {
    Term term = service.getTerm();
    assertNotNull(term);
    assertEquals(Semester.Fall, term.getSemester());
    assertEquals(2016, term.getYear().intValue());
  }

  // ---------------------------------------------------------------------

  private static WarehouseDatabase getWarehouseDatabase() {
    DatabaseQueryManager queryManager = createMock(DatabaseQueryManager.class);
    return new WarehouseDatabaseImpl(queryManager);
  }

  private static WarehouseCourse createWarehouseCourse(final int year, final Semester semester, final int ccn) {
    final WarehouseCourse warehouseCourse = new WarehouseCourse(new CanonicalCourse(ccn, "ESPM C12",
            INTRODUCTION_TO_ENVIRONMENTAL_STUDIES), year,
            semester, "C12", "ESPM", "001", Collections.list(DayOfWeek.Tuesday),
            new WarehouseRoom(WarehouseBuilding.Dwinelle, "117"), "SeriesDescription", null, "youTubePlaylistId",
            new CapturePreferences(), AssociatedLicense.creative_commons);
    return warehouseCourse;
  }

  private static DublinCoreCatalog createDublinCoreCatalogMock() {
    final DublinCoreCatalog mock = createMock(DublinCoreCatalog.class);
    expect(mock.getFirst(DublinCoreCatalog.PROPERTY_TITLE)).andReturn(INTRODUCTION_TO_ENVIRONMENTAL_STUDIES).anyTimes();
    expect(mock.getFirst(DublinCoreCatalog.PROPERTY_DESCRIPTION)).andReturn("SeriesDescription").anyTimes();
    return mock;
  }

  private static SchedulableServiceService getSchedulableServiceServiceMock() {
    return createMock(SchedulableServiceService.class);
  }

  private static CourseManagementService getCourseManagementService(final Notifier notifier) throws IOException, ConfigurationException {
    final CourseManagementServiceImpl courseManagementService = new CourseManagementServiceImpl();
    courseManagementService.setNotifier(notifier);
    courseManagementService.setSalesforceObjectTransformer(new SalesforceObjectTransformerImpl());
    final Properties properties = getSalesforceConnectionProperties();
    final SalesforceConnectorServiceImpl connectorService = new SalesforceConnectorServiceImpl();
    connectorService.setNotifier(notifier);
    connectorService.updated(properties);
    courseManagementService.setSalesforceConnectorService(connectorService);
    courseManagementService.updatedConfiguration(properties);
    return courseManagementService;
  }

  private static Workspace getWorkspaceMock() {
    return createMock(Workspace.class);
  }

  private static EpisodeService getEpisodeServiceMock(final SearchResult searchResult) {
    final EpisodeService mock = createMock(EpisodeService.class);
    expect(mock.find(anyObject(EpisodeQuery.class), anyObject(UriRewriter.class))).andReturn(searchResult).anyTimes();
    return mock;
  }

  private static SeriesService createSeriesServiceMock(final DublinCoreCatalog catalog)
          throws SeriesException, NotFoundException, UnauthorizedException {
    expect(catalog.getFirst(DublinCoreCatalog.PROPERTY_IDENTIFIER)).andReturn("2014D28927").anyTimes();
    //
    final List<DublinCoreCatalog> catalogList = Collections.list(catalog);
    final SeriesService mock = createMock(SeriesService.class);
    expect(mock.getSeriesCount()).andReturn(1).anyTimes();
    expect(mock.getSeries(anyObject(String.class))).andReturn(catalog).anyTimes();
    expect(mock.getSeries(anyObject(SeriesQuery.class))).andReturn(new DublinCoreCatalogList(catalogList, 1));
    return mock;
  }

  private static SecurityService createSecurityServiceMock() {
    return createMock(SecurityService.class);
  }

  private static DatabaseCredentials getTestCatalogCredentials() throws IOException {
    final File file = EnvironmentUtil.getFileUnderFelixHome(
            "etc/services/org.opencastproject.participation.impl.CourseManagementServiceImpl.properties");
    final Properties p = new Properties();
    p.load(new FileInputStream(file));
    final EProperties eProperties = EnvironmentUtil.createEProperties(p, true);
    return CourseUtils.getCatalogCredentials(eProperties);
  }

  private static Properties getSalesforceConnectionProperties() throws IOException {
    final File file = EnvironmentUtil.getFileUnderFelixHome(
            "etc/services/org.opencastproject.salesforce.SalesforceConnectorServiceImpl.properties");
    final Properties p = new Properties();
    p.load(new FileInputStream(file));
    return EnvironmentUtil.createEProperties(p, true);
  }

  private static Notifier getLoggerNotifier() {
    return new Notifier() {
      @Override
      public SendType notifyEngineeringTeam(final String subject, final String message, final Object... debugInfo) {
        log(null, subject, message, debugInfo);
        return SendType.messageLoggedNotSent;
      }
      @Override
      public SendType notifyEngineeringTeam(final Throwable exception, final Object... debugInfo) {
        log(exception, null, null, debugInfo);
        return null;
      }
      @Override
      public SendType notifyEngineeringTeam(final String subject, final Throwable exception, final Object... debugInfo) {
        log(exception, subject, null, debugInfo);
        return SendType.messageLoggedNotSent;
      }
      private void log(final Throwable exception, final String subject, final String message, final Object... debugInfo) {
        if (exception != null) {
          final Throwable rootCause = ExceptionUtils.getCause(exception);
          final Throwable cause = rootCause == null ? exception : rootCause;
          System.out.println(cause.getClass().getSimpleName() + " : " + cause.getMessage());
        }
        System.out.print(subject);
        System.out.println(message);
        for (final Object next : debugInfo) {
          if (next != null) {
            System.out.println(next.toString());
          }
        }
      }

    };
  }

}
