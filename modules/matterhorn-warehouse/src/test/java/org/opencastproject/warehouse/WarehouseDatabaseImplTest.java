/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.warehouse;

import org.apache.commons.lang.time.DateUtils;
import org.easymock.IArgumentMatcher;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.opencastproject.participation.CourseUtils;
import org.opencastproject.participation.impl.Column;
import org.opencastproject.participation.impl.persistence.DatabaseQueryManager;
import org.opencastproject.participation.model.CanonicalCourse;
import org.opencastproject.participation.model.CapturePreferences;
import org.opencastproject.participation.model.CourseKey;
import org.opencastproject.participation.model.DayOfWeek;
import org.opencastproject.participation.model.HasCourseKey;
import org.opencastproject.participation.model.RecordingAvailability;
import org.opencastproject.participation.model.RecordingType;
import org.opencastproject.participation.model.Semester;
import org.opencastproject.util.data.Collections;

import java.net.MalformedURLException;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.easymock.EasyMock.anyObject;
import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.eq;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.reportMatcher;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * @author John Crossman
 */
public class WarehouseDatabaseImplTest {

  private DatabaseQueryManager queryManager;
  private WarehouseDatabaseImpl warehouseDatabase;

  @Before
  public void before() {
    queryManager = createMock(DatabaseQueryManager.class);
    warehouseDatabase = new WarehouseDatabaseImpl(queryManager);
  }

  @Test
  public void testGetAllCourses() {
    // Make sure we don't cache results under the hood and return different results per query
    final WarehouseCourse course0 = getWarehouseCourse(0, 2);
    final WarehouseCourse course1 = getWarehouseCourse(1, 1);
    final Set<WarehouseCourse> set = getSet(course0, course1);
    ResultConverterForWarehouse rowConverter = warehouseListMatcher(set);
    expect(queryManager.getSet(rowConverter, eq(WarehouseQuery.getCourseByYearSemesterCCN), anyObject(Map.class))).andReturn(set).times(2);
    replay(queryManager);
    for (int index = 0; index < 2; index++) {
      final List<WarehouseCourse> allCourses = warehouseDatabase.getAllCourses(false);
      assertEquals(2, allCourses.size());
      assertTrue(allCourses.contains(course0));
      assertTrue(allCourses.contains(course1));
    }
  }

  @Test
  public void testGetCourse() {
    final WarehouseCourse course = getWarehouseCourse(0, 2);
    final Set<WarehouseCourse> set = getSet(course);
    expect(queryManager.getSet(warehouseListMatcher(set), eq(WarehouseQuery.getCourseByYearSemesterCCN),
            anyObject(Map.class))).andReturn(
            set).once();
    replay(queryManager);
    final WarehouseCourse result = warehouseDatabase.getCourse(course);
    assertNotNull(result);
    assertEquals(course, result);
  }

  @Test
  public void testGetCourseNull() {
    final Set<WarehouseCourse> set = new HashSet<WarehouseCourse>();
    expect(queryManager.getSet(warehouseListMatcher(set), eq(WarehouseQuery.getCourseByYearSemesterCCN), anyObject(Map.class))).andReturn(
            set).once();
    replay(queryManager);
    final HasCourseKey courseKey = getWarehouseCourse(0, 0);
    final WarehouseCourse result = warehouseDatabase.getCourse(courseKey);
    assertNull(result);
  }

  @Test
  public void testDeleteCourse() {
    queryManager.upsert(eq(WarehouseQuery.deleteCourse), anyObject(Map.class));
    expectLastCall().once();
    queryManager.upsert(eq(WarehouseQuery.deleteAllCourseRecordings), anyObject(Map.class));
    expectLastCall().once();
    replay(queryManager);
    warehouseDatabase.deleteCourse(new CourseKey(2014, Semester.Fall, 12345));
  }

  @Test
  public void testInsertCourse() throws MalformedURLException {
    final Set<WarehouseCourse> set = new HashSet<WarehouseCourse>();
    expect(queryManager.getSet(warehouseListMatcher(set), eq(WarehouseQuery.getCourseByYearSemesterCCN), anyObject(Map.class))).andReturn(set).once();
    queryManager.upsert(eq(WarehouseQuery.insertWarehouseCourse), anyObject(Map.class));
    expectLastCall().once();
    queryManager.upsert(eq(WarehouseQuery.insertRecording), anyObject(Map.class));
    expectLastCall().once();
    queryManager.upsert(eq(WarehouseQuery.insertRecording), anyObject(Map.class));
    expectLastCall().once();
    replay(queryManager);
    warehouseDatabase.upsertCourse(getWarehouseCourse(0, 2));
  }

  @Test
  @Ignore("TODO")
  public void testUpdateCourseWithRecordingsUpsert() throws MalformedURLException {
    final WarehouseCourse incomingCourse = getWarehouseCourse(0, 2);
    final List<Recording> recordingList = new LinkedList<Recording>(incomingCourse.getRecordings());
    final Recording deletedRecording = recordingList.get(0);
    final Recording updateRecording = recordingList.get(1);
    incomingCourse.getRecordings().remove(deletedRecording);
    final Recording insertRecording = newRecording(3);
    incomingCourse.getRecordings().add(insertRecording);
    //
    final WarehouseCourse existingCourse = getWarehouseCourse(0, 2);
    final Set<WarehouseCourse> existingSet = getSet(existingCourse);
    expect(queryManager.getSet(
            warehouseListMatcher(existingSet),
            eq(WarehouseQuery.getCourseByYearSemesterCCN),
            anyObject(Map.class))).andReturn(existingSet).once();
    //
    queryManager.upsert(eq(WarehouseQuery.updateWarehouseCourse), matcher(incomingCourse));
    expectLastCall().once();
    queryManager.upsert(eq(WarehouseQuery.updateRecording), matcher(updateRecording, true));
    expectLastCall().once();
    queryManager.upsert(eq(WarehouseQuery.insertRecording), matcher(insertRecording, false));
    expectLastCall().once();
    queryManager.upsert(eq(WarehouseQuery.deleteRecording), matchDelete(deletedRecording));
    expectLastCall().once();
    replay(queryManager);
    warehouseDatabase.upsertCourse(incomingCourse);
  }

  @Test
  public void testMapForUpsertNullPreferences() {
    testMapForUpsert(null, AssociatedLicense.all_rights_reserved, true);
    testMapForUpsert(new CapturePreferences(null, null, 0), AssociatedLicense.all_rights_reserved, true);
  }

  @Test
  public void testGetMapForUpsertAvailability() {
    testMapForUpsert(new CapturePreferences(null, RecordingAvailability.studentsOnly, 0),
            AssociatedLicense.all_rights_reserved, true);
    testMapForUpsert(new CapturePreferences(null, RecordingAvailability.publicNoRedistribute, 0),
            AssociatedLicense.all_rights_reserved, false);
    testMapForUpsert(new CapturePreferences(null, RecordingAvailability.publicCreativeCommons, 0),
            AssociatedLicense.creative_commons, false);
  }

  private void testMapForUpsert(final CapturePreferences preferences, final AssociatedLicense expectedLicense,
          final boolean expectCourseOnly) {
    final WarehouseCourse w = getWarehouseCourse(0, preferences, 0);
    final Map<Column, Object> map = warehouseDatabase.getMapForCourseUpsert(w);
    assertEquals(expectedLicense, map.get(ColStr.license_type));
    final String expected = expectCourseOnly ? "course" : "public";
    assertEquals(expected, map.get(ColStr.audience_type));
  }

  private ResultConverterForWarehouse warehouseListMatcher(final Set<WarehouseCourse> set) {
    final Matcher<ResultConverterForWarehouse> matcher = new Matcher<ResultConverterForWarehouse>() {
      @Override
      public boolean isMatch(final ResultConverterForWarehouse c) {
        for (final WarehouseCourse next : set) {
          final String key = next.getYear() + next.getSemester().getTermCode() + next.getCcn() + "";
          ((ResultConverterForWarehouseImpl) c).getResults().put(key, next);
        }
        return true;
      }
    };
    reportMatcher(matcher);
    return matcher.getIncoming();
  }

  private Map<Column, Object> matcher(final WarehouseCourse w) {
    reportMatcher(new Matcher<Map<Object, Object>>() {
      @Override
      public boolean isMatch(final Map<Object, Object> map) {
        return map != null && CourseUtils.equals(w, map.get("course"));
      }
    });
    return null;
  }

  private Map<Column, Object> matcher(final Recording r, final boolean isUpdate) {
    reportMatcher(new Matcher<Map<Column, Object>>() {
      @Override
      public boolean isMatch(final Map<Column, Object> map) {
        final Recording fromMap = (Recording) map.get(ColStr.recording);
        return isUpdate
                ? r.getRecordingId().equals(fromMap.getRecordingId())
                : r.getYouTubeVideoId().equals(fromMap.getYouTubeVideoId());
      }
    });
    return null;
  }

  private Map<Column, Object> matchDelete(final Recording r) {
    reportMatcher(new Matcher<Map<Column, Object>>() {
      @Override
      public boolean isMatch(final Map<Column, Object> map) {
        return r.getRecordingId().equals(map.get(ColStr.recordingId));
      }
    });
    return null;
  }

  private abstract class Matcher<T> implements IArgumentMatcher {
    private T incoming;

    @Override
    public boolean matches(final Object arg) {
      incoming = (T) arg;
      return isMatch(incoming);
    }

    public abstract boolean isMatch(final T arg);

    @Override
    public void appendTo(final StringBuffer buffer) {
    }

    T getIncoming() {
      return incoming;
    }
  }

  private WarehouseCourse getWarehouseCourse(final int index, final int recordingCount) {
    final CapturePreferences capturePreferences = new CapturePreferences(RecordingType.screencast,
            RecordingAvailability.publicCreativeCommons, 0);
    return getWarehouseCourse(index, capturePreferences, recordingCount);
  }

  private WarehouseCourse getWarehouseCourse(final int index, final CapturePreferences capturePreferences,
          final int recordingCount) {
    final WarehouseCourse w;
    final CanonicalCourse canonicalCourse = new CanonicalCourse(index * 999, "name", "title");
    final int year = 2014;
    final Semester fall = Semester.Fall;
    final String catalogId = "catalog_id_" + index;
    final String department = "department's can have apostrophes";
    final String section = "001";
    final String description = "We think of ours as \"the information age.\" of production, recording, and storage. In every instance, we'll be concerned with both what and when.";
    switch (index % 2) {
      case 0 :
        w = newWarehouseCourse(canonicalCourse, year, fall, catalogId, department, section,
                Collections.list(DayOfWeek.Monday, DayOfWeek.Wednesday, DayOfWeek.Friday),
                new WarehouseRoom(WarehouseBuilding.Dwinelle, "8"), description,
                new String[] {"Judy Bernly", "Violet Newstead", "Doralee Rhodes", "John O'Connor"},
                null, capturePreferences);
        break;
      case 1 :
        w = newWarehouseCourse(canonicalCourse, year, fall, catalogId, department, section,
                Collections.list(DayOfWeek.Tuesday, DayOfWeek.Thursday), new WarehouseRoom(WarehouseBuilding.Wheeler, "117"),
                description, new String[] {"Jack O'Connor"}, "youTubePlaylistId", capturePreferences);
        break;
      default:
        throw new UnsupportedOperationException("No such test data: " + index);
    }
    for (int recordingIndex = 1; recordingIndex <= recordingCount; recordingIndex++) {
      w.addRecording(newRecording(recordingIndex));
    }
    return w;
  }

  private Recording newRecording(final int index) {
    final Date now = new Date();
    return new Recording(index, "title-" + index, "description-" + index, "youTubeVideoId-" + index, now,
            DateUtils.addHours(now, 1), index * 100, null);
  }

  private WarehouseCourse newWarehouseCourse(final CanonicalCourse canonicalCourse, final int year, final Semester semester,
          final String catalogId, final String department, final String section, final List<DayOfWeek> daysOfWeek, final WarehouseRoom warehouseRoom,
          final String description, final String[] instructorNames, final String youTubePlaylistId,
          final CapturePreferences capturePreferences) {
    return new WarehouseCourse(canonicalCourse, year, semester, catalogId, department, section, daysOfWeek,
        warehouseRoom, description, instructorNames, youTubePlaylistId, capturePreferences,
        AssociatedLicense.creative_commons);
  }

  private <T> Set<T> getSet(final T... objArray) {
    return new HashSet<T>(Collections.list(objArray));
  }

}
