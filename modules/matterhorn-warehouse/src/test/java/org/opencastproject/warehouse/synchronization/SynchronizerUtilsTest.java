/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.warehouse.synchronization;

import org.junit.Test;
import org.opencastproject.metadata.dublincore.DublinCore;
import org.opencastproject.metadata.dublincore.DublinCoreCatalogImpl;
import org.opencastproject.participation.model.CapturePreferences;
import org.opencastproject.participation.model.CourseOffering;
import org.opencastproject.participation.model.License;
import org.opencastproject.participation.model.RecordingAvailability;
import org.opencastproject.participation.model.Room;
import org.opencastproject.participation.model.RoomCapability;
import org.opencastproject.warehouse.AssociatedLicense;
import org.opencastproject.warehouse.WarehouseBuilding;
import org.opencastproject.warehouse.WarehouseRoom;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;

/**
 * @author John Crossman
 */
public class SynchronizerUtilsTest {

  private final String creativeCommonsLicenseDescription = "Creative Commons 3.0: Attribution-NonCommercial-NoDerivs";

  @Test
  public void testGetAssociatedLicenseNull() {
    assertNull(SynchronizerUtils.getAssociatedLicense(new CourseOffering()));
  }

  @Test
  public void testGetWarehouseRoomVLSB() {
    String roomNumber = "2060";
    for (String variation : new String[] { "Valley Life Sciences", "vlsb", "Valley LSB" }) {
      WarehouseRoom w = SynchronizerUtils.getWarehouseRoom(new Room(null, variation, roomNumber, RoomCapability.screencast));
      assertNotNull(w);
      assertEquals(WarehouseBuilding.ValleyLSB, w.getBuilding());
      assertEquals(roomNumber, w.getRoomNumber());
    }
  }

  @Test
  public void testGetWarehouseRoomUnrecognized() {
    assertNull(SynchronizerUtils.getWarehouseRoom(new Room(null, null, null, null)));
    assertNull(SynchronizerUtils.getWarehouseRoom(new Room(null, "Whopper", "Meal", null)));
  }

  @Test
  public void testGetWarehouseRoomHearstMin() {
    for (String variation : new String[] { "Hearst Min", "Hearst Mining" }) {
      WarehouseRoom w = SynchronizerUtils.getWarehouseRoom(new Room(null, variation, "222", RoomCapability.screencast));
      assertNotNull(w);
      assertEquals(WarehouseBuilding.HearstMin, w.getBuilding());
    }
  }

  @Test
  public void testGetAssociatedLicense() {
    final CourseOffering course = new CourseOffering();
    final CapturePreferences preferences = new CapturePreferences();
    course.setCapturePreferences(preferences);
    //
    preferences.setRecordingAvailability(RecordingAvailability.publicCreativeCommons);
    assertEquals(AssociatedLicense.creative_commons, SynchronizerUtils.getAssociatedLicense(course));
    //
    preferences.setRecordingAvailability(RecordingAvailability.publicNoRedistribute);
    assertEquals(AssociatedLicense.all_rights_reserved, SynchronizerUtils.getAssociatedLicense(course));
    //
    preferences.setRecordingAvailability(RecordingAvailability.studentsOnly);
    assertEquals(AssociatedLicense.all_rights_reserved, SynchronizerUtils.getAssociatedLicense(course));
  }

  @Test
  public void testGetRecordingAvailabilityNull() {
    assertSame(RecordingAvailability.studentsOnly, SynchronizerUtils.getRecordingAvailability(null, null));
    assertSame(RecordingAvailability.publicCreativeCommons, SynchronizerUtils.getRecordingAvailability(null, RecordingAvailability.publicCreativeCommons));
    //
    final MyDublinCoreCatalogImpl series = new MyDublinCoreCatalogImpl();
    assertSame(RecordingAvailability.studentsOnly, SynchronizerUtils.getRecordingAvailability(series, null));
    //
    series.add(DublinCore.PROPERTY_ACCESS_RIGHTS, " corrupt data ");
    series.add(DublinCore.PROPERTY_LICENSE, creativeCommonsLicenseDescription);
    assertSame(RecordingAvailability.studentsOnly, SynchronizerUtils.getRecordingAvailability(series, null));
    assertSame(RecordingAvailability.publicCreativeCommons,
            SynchronizerUtils.getRecordingAvailability(series, RecordingAvailability.publicCreativeCommons));
  }

  @Test
  public void testGetRecordingAvailabilityStudentsOnly() {
    final MyDublinCoreCatalogImpl series = new MyDublinCoreCatalogImpl();
    series.add(DublinCore.PROPERTY_ACCESS_RIGHTS, "studentsOnlyAccessRights  ");
    // The following license value will be ignored and that is okay
    series.add(DublinCore.PROPERTY_LICENSE, creativeCommonsLicenseDescription);
    final RecordingAvailability availability = SynchronizerUtils.getRecordingAvailability(series, RecordingAvailability.publicCreativeCommons);
    assertSame(RecordingAvailability.studentsOnly, availability);
    assertSame(License.allRightsReserved, availability.getLicense());
  }

  @Test
  public void testGetRecordingAvailabilityCreativeCommons() {
    // Simulate a hand-edited license value
    final String[] equivalentLicenseDescriptions =
            new String[] { creativeCommonsLicenseDescription, "creative commons license" };
    for (final String creativeCommonsLicense : equivalentLicenseDescriptions) {
      final MyDublinCoreCatalogImpl series = new MyDublinCoreCatalogImpl();
      series.add(DublinCore.PROPERTY_ACCESS_RIGHTS, " publicAccessRights ");
      series.add(DublinCore.PROPERTY_LICENSE, creativeCommonsLicense);
      final RecordingAvailability availability = SynchronizerUtils.getRecordingAvailability(series, RecordingAvailability.studentsOnly);
      assertSame(RecordingAvailability.publicCreativeCommons, availability);
      assertSame(License.creativeCommons, availability.getLicense());
    }
  }

  @Test
  public void testGetRecordingAvailabilityAllRightsReserved() {
    // Simulate a hand-edited license value
    final String[] equivalentLicenseDescriptions =
            new String[] { " all rights were reserved yesterday ", "All rights reserved" };
    for (final String allRightsReserved : equivalentLicenseDescriptions) {
      final MyDublinCoreCatalogImpl series = new MyDublinCoreCatalogImpl();
      // Simulate a hand-edited license value
      series.add(DublinCore.PROPERTY_ACCESS_RIGHTS, " publicAccessRights ");
      series.add(DublinCore.PROPERTY_LICENSE, allRightsReserved);
      //
      final RecordingAvailability availability = SynchronizerUtils.getRecordingAvailability(series, RecordingAvailability.studentsOnly);
      assertSame(RecordingAvailability.publicNoRedistribute, availability);
      assertSame(License.allRightsReserved, availability.getLicense());
    }
  }

  private class MyDublinCoreCatalogImpl extends DublinCoreCatalogImpl {
    MyDublinCoreCatalogImpl() {
    }
  }
}
