/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.warehouse.synchronization;

import org.apache.commons.lang.StringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.opencastproject.notify.Notifier;
import org.opencastproject.schedulableservice.api.Periods;
import org.opencastproject.schedulableservice.api.SchedulableService;
import org.opencastproject.schedulableservice.api.SchedulableServiceService;
import org.opencastproject.warehouse.WarehouseService;
import org.osgi.service.cm.ConfigurationException;

import java.util.Properties;
import java.util.regex.Pattern;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * A suite of tests focusing on the configuration and scheduling of {@link WarehouseSynchronizerServiceImpl}.
 * 
 */

@RunWith(MockitoJUnitRunner.class)
public class WarehouseSynchronizerServiceImplConfigurationTest {

  private static final String KEY_PERIOD = "org.opencastproject.warehouse.synchronization.WarehouseSynchronizerServiceImpl.schedulingPeriod";
  private WarehouseSynchronizerServiceImpl warehouseSynchronizerServiceImpl;
  
  @Mock private SchedulableServiceService schedulableServiceService;
  @Mock private MatterhornCourseService matterhornCourseService;
  @Mock private WarehouseService warehouseService;
  @Mock private Notifier notifier;

  @Test
  public void serviceScheduledToRunEveryNthSimulatedMinutes() throws Exception {
    // SchedulableServiceService will call every minute, we want to run once per fifteen invocations
    final int nthMinute = 15;
    final int shouldRunTotalOfTimes = 4;
    final Properties properties = new Properties();
    properties.put(KEY_PERIOD, Integer.toString(nthMinute));
    warehouseSynchronizerServiceImpl.updatedConfiguration(properties);
    final SchedulableService schedulableService = warehouseSynchronizerServiceImpl;
    
    for (int i = 0; i < (shouldRunTotalOfTimes * nthMinute); i++) {
      schedulableService.callBack();
    }
    verify(matterhornCourseService, times(shouldRunTotalOfTimes)).getMatchingSeriesIds(any(Pattern.class));
  }

  @Test
  public void parsableIntegerSchedulesService() throws Exception {
    final Properties properties = new Properties();
    properties.put(KEY_PERIOD, "15");
    warehouseSynchronizerServiceImpl.updatedConfiguration(properties);
    verify(schedulableServiceService).scheduleServiceEvent(any(SchedulableService.class), anyString(), anyString(), any(Periods.class));
  }
  
  @Test
  public void periodEqualZeroMeansNeverRun() throws Exception {
    final Properties properties = new Properties();
    properties.put(KEY_PERIOD, "0");
    warehouseSynchronizerServiceImpl.updatedConfiguration(properties);
    verify(schedulableServiceService, never()).scheduleServiceEvent(any(SchedulableService.class), anyString(), anyString(), any(Periods.class));
  }
  
  @Test
  public void periodEqualBlankDefaultsToNeverRun() throws Exception {
    final Properties properties = new Properties();
    properties.put(KEY_PERIOD, StringUtils.EMPTY);
    warehouseSynchronizerServiceImpl.updatedConfiguration(properties);
    verify(schedulableServiceService, never()).scheduleServiceEvent(any(SchedulableService.class), anyString(), anyString(), any(Periods.class));
  }

  @Test(expected = ConfigurationException.class)
  public void cannotScheduleServiceToRunForNegativeMinutes() throws ConfigurationException {
    final Properties properties = new Properties();
    properties.put(KEY_PERIOD, "-1");
    warehouseSynchronizerServiceImpl.updatedConfiguration(properties);
  }
  
  @Test(expected = ConfigurationException.class)
  public void periodMustBeParsableInteger() throws ConfigurationException {
    final Properties properties = new Properties();
    properties.put(KEY_PERIOD, "f");
    warehouseSynchronizerServiceImpl.updatedConfiguration(properties);
  }
  
  @Test(expected = ConfigurationException.class)
  public void propertiesCannotBeNull() throws ConfigurationException {
    warehouseSynchronizerServiceImpl.updatedConfiguration(null);
  }
  
  @Before
  public void instantiateImplAndInjectServices() throws Exception {
    warehouseSynchronizerServiceImpl = new WarehouseSynchronizerServiceImpl();
    warehouseSynchronizerServiceImpl.setSchedulableServiceService(schedulableServiceService);
    warehouseSynchronizerServiceImpl.setMatterhornCourseService(matterhornCourseService);
    warehouseSynchronizerServiceImpl.setWarehouseService(warehouseService);
    warehouseSynchronizerServiceImpl.setNotifier(notifier);
  }

  @After
  public void tearDown() throws Exception {
  }

}
