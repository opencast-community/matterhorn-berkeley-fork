package org.opencastproject.warehouse.selenium;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ConfigTest {

  @Test
  public void testSeleniumConfig() {
    Config config = Config.seleniumConfig();
    assertTrue(config.pageTimeoutSeconds() > 0);
    assertEquals("default-cas-username", config.get("selenium.youTube.casUserName"));
  }

  @Test
  public void testSeleniumConfigLegacyMigration() {
    Config config = Config.seleniumConfigLegacyMigration();
    assertEquals("legacy-migration-cas-username", config.get("selenium.youTube.casUserName"));
  }
}
