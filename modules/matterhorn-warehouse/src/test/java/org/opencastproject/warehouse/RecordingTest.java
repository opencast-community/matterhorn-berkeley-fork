/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.warehouse;

import org.apache.commons.lang.time.DateUtils;
import org.junit.Test;

import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;
import java.util.TreeSet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;

/**
 * @author John Crossman
 */
public class RecordingTest {

  private final String titlePrefix = "Statistics 133 - Lecture ";

  @Test
  public void testRecordingComparator() {
    testRecordingComparator(null);
    testRecordingComparator(new Date());
  }

  private void testRecordingComparator(final Date today) {
    final LinkedList<Recording> list = new LinkedList<Recording>();
    for (int index = 0; index < 32; index++) {
      final String youTubeVideoId = (index % 2 == 0) ? null : "aAbBcCdD" + index;
      list.add(getRecording(today, index, youTubeVideoId));
    }
    java.util.Collections.shuffle(list);
    final Set<Recording> set = new TreeSet<Recording>(new Recording.RecordingComparator());
    for (final Recording next : list) {
      set.add(next);
    }
    int index = 0;
    for (final Recording next : set) {
      assertEquals(titlePrefix + index, next.getTitle());
      index++;
    }
    assertEquals(32, index);
  }

  private Recording getRecording(final Date today, final int index, final String youTubeVideoId) {
    final Date start = (today == null) ? null : DateUtils.addDays(today, index);
    final Date end = (start == null) ? null : DateUtils.addHours(start, 1);
    return new Recording(titlePrefix + index, null, youTubeVideoId, start, end, null, null);
  }

  @Test
  public void testSetContains() {
    final Date start = new Date();
    final Date end = DateUtils.addHours(start, 1);
    final Recording r1 = new Recording("Computer Science 70 - 2014-01-30", null, null, start, end, null, null);
    final Recording r2 = new Recording("Computer Science 70 - 2014-01-30", null, "-WsW8vVNuOw", start, end, null, null);
    assertNotEquals(r1, r2);
    assertNotEquals(r1.hashCode(), r2.hashCode());
    final Set<Recording> set = new HashSet<Recording>();
    set.add(r1);
    assertFalse(set.contains(r2));
  }

}
