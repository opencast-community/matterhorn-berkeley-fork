/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.warehouse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.junit.Test;
import org.opencastproject.participation.model.CourseKey;
import org.opencastproject.participation.model.HasCourseKey;
import org.opencastproject.participation.model.Semester;
import org.opencastproject.participation.model.Term;

import java.io.IOException;
import java.net.URL;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * @author John Crossman
 */
public class WarehouseFeedGeneratorJSONTest {

  @Test
  public void testIncludeZeroRecordingsWhen2015() throws IOException {
    final Semester fall = Semester.Fall;
    final HashSet<WarehouseCourse> courses = new HashSet<WarehouseCourse>();
    courses.add(createWarehouseCourse(new CourseKey(2014, fall, 20140), 0));
    courses.add(createWarehouseCourse(new CourseKey(2014, fall, 20141), 1));
    final WarehouseCourse hasNullYouTubeVideoId = createWarehouseCourse(new CourseKey(2014, fall, 20142), 0);
    final Date now = new Date();
    hasNullYouTubeVideoId.addRecording(UnitTestUtils.createRecording(2, now, DateUtils.addHours(now, 1), false));
    courses.add(hasNullYouTubeVideoId);
    courses.add(createWarehouseCourse(new CourseKey(2015, fall, 20150), 0));
    courses.add(createWarehouseCourse(new CourseKey(2015, fall, 20151), 1));
    courses.add(createWarehouseCourse(new CourseKey(2016, fall, 20160), 0));
    courses.add(createWarehouseCourse(new CourseKey(2016, fall, 20161), 1));
    //
    final String json = new WarehouseFeedGeneratorJSON().toJson(courses);
    final String propertyName = WarehouseFeedGeneratorJSON.ColJSON.recordingStartUTC.name();
    assertEquals(3, StringUtils.countMatches(json, propertyName));
    for (final int ccn : new int[] {20140, 20142, 20150}) {
      assertContains(false, json, ccn);
    }
    for (final int ccn : new int[] {20141, 20151, 20160, 20161}) {
      assertContains(true, json, ccn);
    }
  }

  @Test
  public void testExcludeFromJSON() {
    Term currentTerm = Term.construct(Semester.Fall, 2016);
    CourseKey current = new CourseKey(currentTerm.getYear(), currentTerm.getSemester(), 12345);
    WarehouseCourse past2015WithZero = createWarehouseCourse(new CourseKey(2015, Semester.Fall, 12345), 0);
    WarehouseCourse past2016WithZero = createWarehouseCourse(new CourseKey(2016, Semester.Spring, 12345), 0);
    WarehouseCourse pastWithSome = createWarehouseCourse(new CourseKey(2011, Semester.Fall, 444), 3);
    //
    WarehouseFeedGeneratorJSON g = new WarehouseFeedGeneratorJSON();
    assertTrue(g.excludeFromJSON(past2015WithZero, currentTerm));
    assertTrue(g.excludeFromJSON(past2016WithZero, currentTerm));
    assertFalse(g.excludeFromJSON(pastWithSome, currentTerm));
    assertFalse(g.excludeFromJSON(createWarehouseCourse(current, 0), currentTerm));
    assertFalse(g.excludeFromJSON(createWarehouseCourse(current, 3), currentTerm));
  }

  @Test
  public void testLectureShortNameWithManyDashes() {
    final WarehouseFeedGeneratorJSON generatorJSON = new WarehouseFeedGeneratorJSON();
    final String shortName = generatorJSON.getShortName("Chemistry 101 - 2014-07-11 - edited", new LinkedList<String>(), false);
    assertEquals("2014-07-11 - edited", shortName);
  }

  @Test
  public void testGetShortNameBefore2014() throws IOException {
    testGetShortName("warehouse-recording-titles-before-2014.txt", true);
  }

  @Test
  public void testGetShortName2014() throws IOException {
    testGetShortName("warehouse-recording-titles-of-2014.txt", false);
  }

  private void testGetShortName(final String fileName, final boolean isYearBefore2014) throws IOException {
    final WarehouseFeedGeneratorJSON generatorJSON = new WarehouseFeedGeneratorJSON();
    final URL resource = this.getClass().getClassLoader().getResource(fileName);
    assertNotNull(resource);
    final String content = IOUtils.toString(resource.openStream());
    final String[] titleArray = StringUtils.split(content, '\n');
    assertTrue(titleArray.length > 3000);
    final List<String> previousShortNames = new LinkedList<String>();
    String previousLeftover = null;
    for (final String title : titleArray) {
      final String shortName = generatorJSON.getShortName(title, previousShortNames, isYearBefore2014);
      if (previousShortNames.contains(shortName)) {
        fail("Duplicate: ".concat(shortName).concat("where title = ").concat(title));
      }
      assertTrue(StringUtils.isNotBlank(shortName));
      if (isYearBefore2014) {
        final String marker = "Lecture ";
        if (!shortName.startsWith("Make") && title.contains(marker)) {
          assertTrue("Wrong: " + shortName, shortName.startsWith(marker));
        }
      } else {
        assertTrue(shortName.startsWith("2014-"));
      }
      assertTrue("title = " + title + "; shortName=" +  shortName, title.endsWith(shortName) || shortName.endsWith("Part 2"));
      final String leftover = StringUtils.trimToEmpty(StringUtils.remove(title, shortName.replaceAll(", Part 2", "")));
      if (!leftover.equals(previousLeftover)) {
        previousLeftover = leftover;
        previousShortNames.clear();
      } else {
        previousShortNames.add(shortName);
      }
      assertFalse("Wrong: " + leftover, StringUtils.containsIgnoreCase(leftover, "lecture"));
    }
  }

  private void assertContains(final boolean expectContains, final String s, final int number) {
    final boolean contains = s.contains(number + "");
    if (expectContains) {
      assertTrue(s + " should contain: " + number, contains);
    } else {
      assertFalse(s + " should NOT contain: " + number, contains);
    }
  }

  private WarehouseCourse createWarehouseCourse(final HasCourseKey key, final int recordingsCount) {
    final WarehouseCourse c = UnitTestUtils.createWarehouseCourse(key.getYear(), key.getSemester(), key.getCcn());
    final Date now = new Date();
    for (int i = 0; i < recordingsCount; i++) {
      final Date recordingStart = DateUtils.addHours(now, i);
      final Date recordingEnd = DateUtils.addMinutes(recordingStart, 50);
      c.addRecording(UnitTestUtils.createRecording(i, recordingStart, recordingEnd, true));
    }
    return c;
  }

}
