/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.warehouse;


import java.sql.Array;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import static org.easymock.EasyMock.expect;

/**
 * @author John Crossman
 */
abstract class AbstractResultConverterTest {

  void expectReturn(final ResultSet r, final ColStr column) throws SQLException {
    expectReturn(r, column, column.name());
  }

  void expectReturn(final ResultSet r, final ColStr column, final String expectedValue) throws SQLException {
    expect(r.getString(column.name())).andReturn(expectedValue).once();
  }

  void expectReturn(final ResultSet r, final ColInt column, final Integer expectedValue) throws SQLException {
    expect(r.getInt(column.name())).andReturn(expectedValue).once();
  }

  void expectReturn(final ResultSet r, final ColArray column, final Array expectedValue) throws SQLException {
    expect(r.getArray(column.name())).andReturn(expectedValue).once();
  }

  void expectReturn(final ResultSet r, final ColDate column, final Timestamp expectedValue) throws SQLException {
    expect(r.getTimestamp(column.name())).andReturn(expectedValue).once();
  }
}
