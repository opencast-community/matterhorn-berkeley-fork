/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.kernel.mail;

import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.http.HttpHeaders;
import com.google.api.client.http.HttpResponseException;
import org.opencastproject.job.api.EManagedService;
import org.opencastproject.notify.Notifier;
import org.opencastproject.notify.SendType;
import org.opencastproject.util.StringUtil;
import org.opencastproject.util.env.Environment;
import org.opencastproject.util.env.EnvironmentUtil;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.osgi.service.cm.ConfigurationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;

import javax.mail.Address;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * OSGi service that allows to send e-mails using <code>javax.mail</code>.
 */
public class SmtpService extends EManagedService implements Notifier {

  /** The logging facility */
  private static final Logger logger = LoggerFactory.getLogger(SmtpService.class);

  /** Parameter prefix common to all "mail" properties */
  private static final String OPT_MAIL_PREFIX = "mail.";

  /** Parameter name for the transport protocol */
  private static final String OPT_MAIL_TRANSPORT = "mail.transport.protocol";

  /** Parameter suffix for the mail host */
  private static final String OPT_MAIL_HOST_SUFFIX = ".host";

  /** Parameter suffix for the mail port */
  private static final String OPT_MAIL_PORT_SUFFIX = ".port";

  /** Parameter suffix for the start tls status */
  private static final String OPT_MAIL_TLS_ENABLE_SUFFIX = ".starttls.enable";

  /** Parameter suffix for the authentication setting */
  private static final String OPT_MAIL_AUTH_SUFFIX = ".auth";

  /** Parameter name for the username */
  private static final String OPT_MAIL_USER = "mail.user";

  /** Parameter name for the password */
  private static final String OPT_MAIL_PASSWORD = "mail.password";

  /** Parameter name for the recipient */
  private static final String OPT_MAIL_FROM = "mail.from";

  /** Parameter name for the debugging setting */
  private static final String OPT_MAIL_DEBUG = "mail.debug";

  /** Parameter name for the test setting */
  private static final String OPT_MAIL_TEST = "mail.test";

  /** Default value for the transport protocol */
  private static final String DEFAULT_MAIL_TRANSPORT = "smtp";

  /** Default value for the mail port */
  private static final String DEFAULT_MAIL_PORT = "25";

  /** The mail properties */
  private Properties mailProperties = new Properties();

  private SendMode sendMode = getDefaultSendMode();

  /** The mail user */
  private String mailUser = null;

  /** The mail password */
  private String mailPassword = null;

  /** The default mail session */
  private Session defaultMailSession = null;

  /** The current mail transport protocol */
  private String mailTransport = null;

  /**
   * Callback from the OSGi <code>ConfigurationAdmin</code> on configuration changes.
   * 
   * @param properties
   *          the configuration properties
   * @throws ConfigurationException
   *           if configuration fails
   */
  @SuppressWarnings("rawtypes")
  @Override
  public void updatedConfiguration(final Properties properties) throws ConfigurationException {
    if (!sendMode.equals(SendMode.neverSend)) {
      configureSmtpService(properties);
    }
    if ((Environment.prod == EnvironmentUtil.getEnvironment()) || (sendMode == SendMode.neverSend)) {
      final String mailTest = StringUtils.trimToNull((String) properties.get(OPT_MAIL_TEST));
      final String subject = "Matterhorn started";
      if (mailTest != null && Boolean.parseBoolean(mailTest)) {
        final TeamAddress team = beSafe(TeamAddress.grizzlyPeakDevelopers, TeamAddress.grizzlyPeakInfo);
        notify(team, subject, '\n' + "Alert emails will be sent to " + team.getAddress());
      }
      final String separator = "--------------------------------------------------------";
      logger.warn(separator + '\n' + subject + " : " + NotifierUtils.getApplicationVersionInfo() + '\n' + separator);
    }
  }

  /**
   * @see #updatedConfiguration(java.util.Properties)
   * @param properties never null
   * @throws ConfigurationException when configuration fails
   */
  private void configureSmtpService(final Properties properties) throws ConfigurationException {
    // Read the mail server properties
    mailProperties.clear();

    // Mail transport protocol
    mailTransport = StringUtils.trimToNull((String) properties.get(OPT_MAIL_TRANSPORT));
    if (!("smtp".equals(mailTransport) || "smtps".equals(mailTransport))) {
      if (mailTransport != null)
        logger.warn("'{}' protocol not supported. Reverting to default: '{}'", mailTransport, DEFAULT_MAIL_TRANSPORT);
      mailTransport = DEFAULT_MAIL_TRANSPORT;
      logger.debug("Mail transport protocol defaults to '{}'", mailTransport);
    } else {
      logger.debug("Mail transport protocol is '{}'", mailTransport);
    }
    logger.info("Mail transport protocol is '{}'", mailTransport);
    mailProperties.put(OPT_MAIL_TRANSPORT, mailTransport);

    // The mail host is mandatory
    String propName = OPT_MAIL_PREFIX + mailTransport + OPT_MAIL_HOST_SUFFIX;
    final String mailHost = StringUtils.trimToNull((String) properties.get(propName));
    if (mailHost == null) {
      throw new ConfigurationException(propName, "is not set");
    }
    logger.debug("Mail host is {}", mailHost);
    mailProperties.put(propName, mailHost);

    // Mail port
    propName = OPT_MAIL_PREFIX + mailTransport + OPT_MAIL_PORT_SUFFIX;
    String mailPort = StringUtils.trimToNull((String) properties.get(propName));
    if (mailPort == null) {
      mailPort = DEFAULT_MAIL_PORT;
      logger.debug("Mail server port defaults to '{}'", mailPort);
    } else {
      logger.debug("Mail server port is '{}'", mailPort);
    }
    mailProperties.put(propName, mailPort);

    // TSL over SMTP support
    propName = OPT_MAIL_PREFIX + mailTransport + OPT_MAIL_TLS_ENABLE_SUFFIX;
    String smtpStartTLSStr = StringUtils.trimToNull((String) properties.get(propName));
    boolean smtpStartTLS = Boolean.parseBoolean(smtpStartTLSStr);
    if (smtpStartTLS) {
      mailProperties.put(propName, "true");
      logger.debug("TLS over SMTP is enabled");
    } else {
      logger.debug("TLS over SMTP is disabled");
    }

    // Mail user
    mailUser = StringUtils.trimToNull((String) properties.get(OPT_MAIL_USER));
    if (mailUser != null) {
      mailProperties.put(OPT_MAIL_USER, mailUser);
      logger.debug("Mail user is '{}'", mailUser);
    } else {
      logger.debug("Sending mails to {} without authentication", mailHost);
    }

    // Mail password
    mailPassword = StringUtils.trimToNull((String) properties.get(OPT_MAIL_PASSWORD));
    if (mailPassword != null) {
      mailProperties.put(OPT_MAIL_PASSWORD, mailPassword);
      logger.debug("Mail password set");
    }

    // Mail sender
    String mailFrom = StringUtils.trimToNull((String) properties.get(OPT_MAIL_FROM));
    if (mailFrom == null) {
      logger.debug("Mail sender defaults to {}", mailFrom);
    } else {
      logger.debug("Mail sender is '{}'", mailFrom);
    }
    mailProperties.put(OPT_MAIL_FROM, mailFrom);

    // Authentication
    propName = OPT_MAIL_PREFIX + mailTransport + OPT_MAIL_AUTH_SUFFIX;
    mailProperties.put(propName, Boolean.toString(mailUser != null));

    // Mail debugging
    String mailDebug = StringUtils.trimToNull((String) properties.get(OPT_MAIL_DEBUG));
    if (mailDebug != null) {
      boolean mailDebugEnabled = Boolean.parseBoolean(mailDebug);
      mailProperties.put(OPT_MAIL_DEBUG, Boolean.toString(mailDebugEnabled));
      logger.info("Mail debugging is {}", mailDebugEnabled ? "enabled" : "disabled");
    }

    defaultMailSession = null;
    logger.info("Mail service configured with {}", mailHost);

    Properties props = getSession().getProperties();
    for (String key : props.stringPropertyNames()) {
      logger.info("{}: {}", key, props.getProperty(key));
    }
  }

  @Override
  public SendType notifyEngineeringTeam(final Throwable e, Object... debugInfo) {
    return notifyEngineeringTeam(ExceptionUtils.getMessage(e), getFullStackTrace(e), debugInfo);
  }

  @Override
  public SendType notifyEngineeringTeam(final String subject, final Throwable e, Object... debugInfo) {
    return notifyEngineeringTeam(subject, getFullStackTrace(e), debugInfo);
  }

  @Override
  public SendType notifyEngineeringTeam(final String subject, final String message, final Object... debugInfo) {
    return notify(getErrorsEmailAddress(), subject, message, debugInfo);
  }

  private SendType notify(final TeamAddress teamAddress, final String subject, final String message, final Object... debugInfo) {
    MimeMessage email = null;
    try {
      email = createMessage();
      email.addFrom(new Address[]{ getRecipient(TeamAddress.grizzlyPeakErrors) });
      email.addRecipient(RecipientType.TO, getRecipient(teamAddress));
      email.setSubject(subject);
      email.setText(message + NotifierUtils.getDebugInfo(debugInfo));
      email.saveChanges();
      return send(email);
    } catch (final Exception e) {
      throw new org.opencastproject.util.ConfigurationException("Failed to construct and send MimeMessage: " + email, e);
    }
  }

  /**
   * @return never null
   */
  private TeamAddress getErrorsEmailAddress() {
    return beSafe(TeamAddress.grizzlyPeakDevelopers, TeamAddress.grizzlyPeakErrors);
  }

  private TeamAddress beSafe(final TeamAddress devAddress, TeamAddress productionAddress) {
    final Environment environment = EnvironmentUtil.getEnvironment();
    final boolean notifyDevelopersOnly = Environment.local.equals(environment) || Environment.dev.equals(environment)
        || Environment.ci.equals(environment);
    return notifyDevelopersOnly ? devAddress : productionAddress;
  }

  /**
   * Returns the default mail session that can be used to create a new message.
   * 
   * @return the default mail session
   */
  public Session getSession() {
    if (defaultMailSession == null) {
      defaultMailSession = Session.getInstance(mailProperties);
    }
    return defaultMailSession;
  }

  /**
   * Creates a new message.
   * 
   * @return the new message
   */
  public MimeMessage createMessage() {
    return new MimeMessage(getSession());
  }

  /**
   * Sends <code>message</code> using the configured transport.
   *
   * @param rawMessage
   *          the message
   * @throws MessagingException
   *           if sending the message failed
   */
  public SendType send(final MimeMessage rawMessage) throws MessagingException {
    NotifierUtils.setSubjectPerSendMode(rawMessage, sendMode);
    //
    final SendType sendType;
    switch (sendMode) {
      case neverSend:
        logEmail(rawMessage);
        sendType = SendType.messageLoggedNotSent;
        break;
      default:
        final boolean sendRawMessage = sendMode.equals(SendMode.productionMode)
            || NotifierUtils.isLimitedToGrizzlyPeakTeam(rawMessage.getAllRecipients());
        final MimeMessage messageToSend;
        if (sendRawMessage) {
          sendType = SendType.messageSent;
          messageToSend = rawMessage;
        } else {
          sendType = SendType.messageDiverted;
          logEmail(rawMessage);
          messageToSend = createMessage();
          final Address[] from = rawMessage.getFrom();
          final Address teamAddress = getRecipient(getErrorsEmailAddress());
          messageToSend.setFrom(ArrayUtils.isEmpty(from) ? teamAddress : from[0]);
          messageToSend.setRecipient(RecipientType.TO, teamAddress);
          messageToSend.setSubject(rawMessage.getSubject());
          final String br = "\n";
          final String text = "------ Intended email recipient(s) ------".concat(br)
              .concat(InternetAddress.toString(rawMessage.getAllRecipients())).concat(br)
              .concat("------ Original message ------").concat(br)
              .concat(getContent(rawMessage));
          messageToSend.setText(text);
        }
        // Send email
        final Transport t = getTransport();
        try {
          if (mailUser != null) {
            t.connect(mailUser, mailPassword);
          } else {
            t.connect();
          }
          t.sendMessage(messageToSend, messageToSend.getAllRecipients());
        } finally {
          t.close();
        }        
        break;
    }
    return sendType;
  }

  private String getFullStackTrace(final Throwable e) {
    final StringBuilder b = new StringBuilder(ExceptionUtils.getFullStackTrace(e));
    b.append('\n');
    if (e instanceof HttpResponseException) {
      b.append(getDetails((HttpResponseException) e)).append('\n');
    } else {
      final Throwable cause = e.getCause();
      if (cause instanceof HttpResponseException) {
        b.append(getDetails((HttpResponseException) cause)).append('\n');
      } else if (cause != null && cause.getCause() instanceof HttpResponseException) {
        b.append(getDetails((HttpResponseException) cause.getCause())).append('\n');
      }
    }
    return b.toString();
  }

  private String getDetails(final HttpResponseException e) {
    final StringBuilder b = new StringBuilder("Google returned status code: ").append(e.getStatusCode()).append(':')
            .append(e.getStatusMessage()).append('\n');
    if (e instanceof GoogleJsonResponseException) {
      final GoogleJsonResponseException ge = (GoogleJsonResponseException) e;
      try {
        final HttpHeaders headers = ge.getHeaders();
        if (headers != null) {
          b.append("Google JSON response headers:").append('\n').append(headers.toString());
        }
        if (ge.getDetails() != null) {
          b.append(ge.getDetails().toPrettyString()).append('\n');
        }
        if (StringUtils.isNotBlank(ge.getContent())) {
          b.append("Google response content:").append('\n').append(ge.getContent());
        }
      } catch (final Exception ignoreThis) {
        // We don't care about this error during JSON parsing.
      }
    }
    return b.toString();
  }

  /**
   * Dedicated method such that unit test can mock this method.
   * @return never null
   * @throws NoSuchProviderException thrown by {@link #getSession()}
   */
  protected Transport getTransport() throws NoSuchProviderException {
    return getSession().getTransport(mailTransport);
  }

  /**
   * Default {@link SendMode} is determined by value of {@link org.opencastproject.util.env.EnvironmentUtil#getEnvironment()}
   * so override is strongly discouraged. In fact, this method is package-local so we can write effective unit tests;
   * there is no other intention for this method.
   * @param sendMode never null
   */
  void overrideDefaultSendMode(final SendMode sendMode) {
    logger.warn("Overriding default SendMode with: " + sendMode.name());
    this.sendMode = sendMode;
  }

  /**
   * Write contents of the email to logs instead of sending the email via mail-server.
   * @param message never null
   * @throws MessagingException when address retrieval fails
   */
  private void logEmail(final MimeMessage message) throws MessagingException {
    final String sectionBreak = "------------------------------------------";
    final String br = "\n";
    final String messageToString = new StringBuilder("This application is running in the 'local' environment so the email will be written to the logs instead of being sent via SMTP server.")
        .append(br).append(br).append(sectionBreak).append(br)
        .append("From: ").append(InternetAddress.toString(message.getFrom())).append(br)
        .append("To: ").append(InternetAddress.toString(message.getRecipients(RecipientType.TO))).append(br)
        .append("Cc: ").append(InternetAddress.toString(message.getRecipients(RecipientType.CC))).append(br).append(br)
        .append("Subject: ").append(message.getSubject()).append(br)
        .append(sectionBreak).append(br)
        .append(getContent(message)).append(br)
        .append(sectionBreak).append(br).toString();
    logger.info(messageToString);
  }

  private String getContent(MimeMessage message) {
    try {
      return StringUtil.toStringObject(message.getContent());
    } catch (final Exception e) {
      return StringUtil.toStringException(e);
    }
  }

  private Address getRecipient(final TeamAddress teamAddress) {
    final String address = teamAddress.getAddress();
    try {
      return new InternetAddress(address, teamAddress.getRecipientName());
    } catch (final Exception e) {
      try {
        return new InternetAddress(address);
      } catch (final AddressException ae) {
        throw new org.opencastproject.util.ConfigurationException("Illegal email address: " + address, ae);
      }
    }
  }

  private SendMode getDefaultSendMode() {
    final SendMode sendMode;
    final Environment environment = EnvironmentUtil.getEnvironment();
    switch (environment) {
      case local:
        sendMode = SendMode.neverSend;
        break;
      case prod:
        sendMode = SendMode.productionMode;
        break;
      default:
        sendMode = SendMode.divertWhenNecessary;
        break;
    }
    return sendMode;
  }

}
