/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.kernel.mail;

import org.apache.commons.lang.RandomStringUtils;
import org.junit.Test;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @author John Crossman
 */
public class NotifierUtilsTest {

  private static final String INSTRUCTOR_EMAIL_ADDRESS = "george@jungle.com";

  @Test
  public void testPrependSubjectPrependInProd() throws MessagingException {
    final Message message = createMessage(300, convert(TeamAddress.grizzlyPeakErrors));
    final String originalSubject = message.getSubject();
    NotifierUtils.setSubjectPerSendMode(message, SendMode.productionMode);
    assertTrue(originalSubject.length() > NotifierUtils.MAX_WIDTH_SUBJECT_LINE);
    // Pad the max width number to account for appended version info
    final int length = message.getSubject().length();
    assertTrue(length < NotifierUtils.MAX_WIDTH_SUBJECT_LINE + NotifierUtils.getApplicationVersionInfo().length() + 1);
    assertTrue(length > NotifierUtils.MAX_WIDTH_SUBJECT_LINE);
    assertTrue(message.getSubject().endsWith(")"));
  }

  @Test
  public void testPrependSubjectProd() throws MessagingException {
    final Message message = createMessage(50, INSTRUCTOR_EMAIL_ADDRESS, TeamAddress.grizzlyPeakErrors.getAddress());
    final String originalSubject = message.getSubject();
    NotifierUtils.setSubjectPerSendMode(message, SendMode.productionMode);
    assertEquals(50, message.getSubject().length());
    assertEquals(originalSubject, message.getSubject());
    assertFalse(message.getSubject().startsWith("["));
  }

  @Test
  public void testIsLimitedSingleTrue() throws AddressException {
    final Address[] addresses = new Address[1];
    addresses[0] = new InternetAddress(TeamAddress.grizzlyPeakDevelopers.getAddress());
    assertTrue(NotifierUtils.isLimitedToGrizzlyPeakTeam(addresses));
  }

  @Test
  public void testIsLimitedToMultipleTrue() throws AddressException {
    final Address[] addresses = new Address[2];
    addresses[0] = new InternetAddress(TeamAddress.grizzlyPeakDevelopers.getAddress());
    addresses[0] = new InternetAddress(TeamAddress.grizzlyPeakErrors.getAddress());
    assertTrue(NotifierUtils.isLimitedToGrizzlyPeakTeam(addresses));
  }

  @Test
  public void testIsLimitedSingleFalse() throws AddressException {
    final Address[] addresses = new Address[1];
    addresses[0] = new InternetAddress(TeamAddress.grizzlyPeakErrors.getAddress());
    assertTrue(NotifierUtils.isLimitedToGrizzlyPeakTeam(addresses));
  }

  @Test
  public void testIsLimitedMultipleFalse() throws AddressException {
    final Address[] addresses = new Address[2];
    addresses[0] = new InternetAddress(TeamAddress.grizzlyPeakDevelopers.getAddress());
    addresses[0] = new InternetAddress(INSTRUCTOR_EMAIL_ADDRESS);
    assertFalse(NotifierUtils.isLimitedToGrizzlyPeakTeam(addresses));
  }

  private Message createMessage(final int subjectLineLength, final String... emailAddresses) throws MessagingException {
    final List<InternetAddress> addressList = new LinkedList<InternetAddress>();
    for (final String emailAddress : emailAddresses) {
      addressList.add(new InternetAddress(emailAddress));
    }
    final MimeMessage message = new MimeMessage(Session.getInstance(new Properties()));
    message.setRecipients(Message.RecipientType.TO, addressList.toArray(new Address[emailAddresses.length]));
    message.setSubject(RandomStringUtils.randomAlphabetic(subjectLineLength));
    return message;
  }

  private String[] convert(final TeamAddress... teamAddresses) {
    final List<String> emailAddresses = new LinkedList<String>();
    for (final TeamAddress teamAddress : teamAddresses) {
      emailAddresses.add(teamAddress.getAddress());
    }
    return emailAddresses.toArray(new String[emailAddresses.size()]);
  }

}
