/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.publication.youtube;

import com.google.api.services.youtube.model.Playlist;
import com.google.api.services.youtube.model.PlaylistItem;
import com.google.api.services.youtube.model.Video;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.opencastproject.google.youtube.GoogleServicesFactory;
import org.opencastproject.google.youtube.HasYouTubePrivacyStatus;
import org.opencastproject.google.youtube.UploadProgressListener;
import org.opencastproject.google.youtube.VideoPublished;
import org.opencastproject.google.youtube.VideoUpload;
import org.opencastproject.google.youtube.YouTubeAPIVersion3ServiceImpl;
import org.opencastproject.google.youtube.YouTubePlaylist;
import org.opencastproject.mediapackage.MediaPackage;
import org.opencastproject.mediapackage.MediaPackageBuilder;
import org.opencastproject.mediapackage.MediaPackageBuilderImpl;
import org.opencastproject.publication.api.PublicationException;
import org.opencastproject.security.api.RecordingAccessRights;
import org.opencastproject.util.env.EProperties;
import org.opencastproject.util.env.EnvironmentUtil;
import org.opencastproject.workflow.handler.YouTubeWorkspace;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import static junit.framework.TestCase.assertNotNull;

/**
 * @author John Crossman
 */
@Ignore
public class YouTubeAPIVersion3ServiceImplTest {

  private final String now = DateFormatUtils.ISO_DATETIME_FORMAT.format(new Date());
  private final String existingYouTubeId = "b-wSOs9N4lY";

  private YouTubeAPIVersion3ServiceImpl service;

  @Before
  public void before() throws IOException {
    if (service == null) {
      final File file = EnvironmentUtil.getFileUnderFelixHome(
              "etc/services/org.opencastproject.publication.youtube.YouTubeV3PublicationServiceImpl.properties");
      final Properties p = new Properties();
      FileInputStream inputStream = new FileInputStream(file);
      p.load(inputStream);
      final EProperties properties = EnvironmentUtil.createEProperties(p, true);
      IOUtils.closeQuietly(inputStream);
      //
      service = new YouTubeAPIVersion3ServiceImpl();
      service.initialize(new GoogleServicesFactory(properties));
    }
  }

  @Test
  public void testExtractVideoInformation() throws Exception {
    final InputStream inputStream = YouTubeAPIVersion3ServiceImpl.class.getResourceAsStream("/media-package-foo.xml");
    final MediaPackageBuilder builder = new MediaPackageBuilderImpl();
    final MediaPackage mediaPackage = builder.loadFromXml(inputStream);
    final YouTubeWorkspace youTubeWorkspace = new YouTubeWorkspace(null);
    final VideoPublished video = youTubeWorkspace.extractVideoInformation(mediaPackage, new String[] {});
    service.updateVideoMetadata(video);
  }

  @Test
  public void testGetVideo() throws IOException {
    Video video = service.getVideoById(existingYouTubeId);
    System.out.println(video.toPrettyString());
  }

  @Test
  public void testCreatePlaylist() throws IOException {
    String title = "Playlist title (" + now + ")";
    HasYouTubePrivacyStatus privacy = RecordingAccessRights.studentsOnlyAccessRights.getYouTubePrivacyStatus();
    String seriesId = "2016D12345";
    final YouTubePlaylist youTubePlaylist = new YouTubePlaylist(null, title, null, privacy, seriesId, null);
    final Playlist playlist = service.createPlaylist(youTubePlaylist);
    assertNotNull(playlist.getId());
  }

  @Test
  public void testGetAllPlaylists() throws IOException {
    final List<Playlist> allPlaylists = service.getAllPlaylists();
    for (final Playlist playlist : allPlaylists) {
      if (playlist.getSnippet().getTitle().startsWith("Physics 7A, 001")) {
        System.out.println(playlist.toPrettyString());
        service.deletePlaylist(playlist.getId());
      }
    }
  }

  @Test
  public void testUpdateVideoMetadata() throws IOException {
    final String[] tags = new String[] { now + "", "UC Berkeley"};
    final HasYouTubePrivacyStatus youTubePrivacyStatus = RecordingAccessRights.studentsOnlyAccessRights.getYouTubePrivacyStatus();
    service.updateVideoMetadata(new VideoPublished(existingYouTubeId, "Young Ones (" + now + ")", null, youTubePrivacyStatus, tags));
  }

  @Test
  public void testGetPlaylistItems() throws IOException {
    String playlistId = "PLkT_XVrWLqJSE0IeqRNxTEIA4gJmPAV9I";
    final List<PlaylistItem> playlistItems = service.getPlaylistItems(playlistId);
    if (playlistItems.isEmpty()) {
      System.out.println("Empty playlist where id = " + playlistId);
    } else {
      System.out.println("Playlist items where id = " + playlistId);
      for (final PlaylistItem item : playlistItems) {
        System.out.println("Playlist item: " + item.getId() + " " + item.getSnippet().getTitle());
      }
    }
  }

  @Test
  public void testAddPlaylistItem() throws IOException, PublicationException, InterruptedException {
    String file = "hdvideo_4_minutes.mp4";
    final RecordingAccessRights rights = RecordingAccessRights.studentsOnlyAccessRights;

    final File videoFile = new File("/Users/john/Projects/bitbucket/uc-berkeley/matterhorn-berkeley-qa/matterhorn-webdriver/src/test/resources/sample-recordings/" + file);
    final String title = file + " (" + now + ")";
    final String description = "Read more: https://en.wikipedia.org/wiki/The_Young_Ones_(TV_series)";
    final UploadProgressListener progressListener = new UploadProgressListener(title, "Playlist title here", videoFile);
    final VideoUpload videoUpload = new VideoUpload(title, description, rights.getYouTubePrivacyStatus(), videoFile, null);
    final Video video = service.addVideoToMyChannel(videoUpload, progressListener);
    service.addPlaylistItem("PLkT_XVrWLqJSE0IeqRNxTEIA4gJmPAV9I", video.getId());
    final int timeoutMinutes = 60;
    final long startUploadMilliseconds = new Date().getTime();
    while (!progressListener.isComplete()) {
      Thread.sleep(30L * 1000L);
      final long howLongWaitingMinutes = (new Date().getTime() - startUploadMilliseconds) / 60000;
      if (howLongWaitingMinutes > timeoutMinutes) {
        throw new PublicationException("Upload to YouTube exceeded " + timeoutMinutes + " minutes for episode " + title);
      }
    }
    System.out.println("Done with upload" + '\n' + video.toPrettyString());
  }

  @Test
  public void testDeleteVideo() throws IOException {
    service.deleteVideo(existingYouTubeId, "PLkT_XVrWLqJSE0IeqRNxTEIA4gJmPAV9I");
  }

}
