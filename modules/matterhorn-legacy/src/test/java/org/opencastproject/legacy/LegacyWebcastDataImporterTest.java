/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.legacy;

import org.json.simple.parser.ParseException;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.opencastproject.notify.Notifier;
import org.opencastproject.participation.impl.persistence.DatabaseCredentials;
import org.opencastproject.participation.impl.persistence.ProvidesCourseCatalogData;
import org.opencastproject.participation.impl.persistence.UCBerkeleyCourseDatabaseImpl;
import org.opencastproject.participation.model.CourseKey;
import org.opencastproject.participation.model.Semester;
import org.opencastproject.util.TestSuiteEnabler;
import org.opencastproject.warehouse.WarehouseCourse;
import org.opencastproject.warehouse.WarehouseDatabase;
import org.osgi.service.cm.ConfigurationException;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import static org.easymock.EasyMock.createNiceMock;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assume.assumeTrue;

/**
 * @author John Crossman
 */
@Ignore
public class LegacyWebcastDataImporterTest {

  private final WarehouseDatabase warehouseDatabase;

  public LegacyWebcastDataImporterTest() {
//    this.warehouseDatabase = WarehouseUtils.getWarehouseDatabase();
    throw new UnsupportedOperationException("WarehouseService needs to be initialized here");
  }

  @BeforeClass
  public static void beforeClass() {
    final TestSuiteEnabler enabler = new TestSuiteEnabler("warehouseDatabaseTests");
    assumeTrue(enabler.getTestSuiteName() + " disabled", enabler.isTestSuiteEnabled());
  }

  @Test
  public void testCoursesLinkedToRecordings() throws IOException, ParseException, ConfigurationException {
    final Notifier notifier = createNiceMock(Notifier.class);
    final DatabaseCredentials credentials = LegacyUtils.getCourseDatabaseCredentials();
    final ProvidesCourseCatalogData courseDatabase = new UCBerkeleyCourseDatabaseImpl(credentials, notifier);
    final LegacyWebcastDAO legacyWebcastDAO = new LegacyWebcastDAOImpl(courseDatabase, new LegacyFileProviderMock());
    final LegacyWebcastDataImporter importer = new LegacyWebcastDataImporter(legacyWebcastDAO);
    importer.run();
    assertPublicHealth200C2();
    assertEnvironSciPolicyManagement117();
  }

  private void assertPublicHealth200C2() throws MalformedURLException {
    final WarehouseCourse w = warehouseDatabase.getCourse(new CourseKey(2012, Semester.Fall, 76097));
    assertEquals(new Integer(572092975), w.getITunesAudioId());
    assertEquals(new URL("http://wbe-itunes.berkeley.edu/media/common/rss/public_health_200c2_101_Fall_2012_Audio__itunes.rss"), w.getAudioFeedRSS());
    assertEquals(new Integer(572093178), w.getITunesVideoId());
    assertEquals(new URL("http://wbe-itunes.berkeley.edu/media/common/rss/public_health_200c2_101_Fall_2012_Video__itunes.rss"), w.getVideoFeedRSS());
    assertEquals("EC-XXv-cvA_iCW-I-luVw6T8J787u-T33k", w.getYouTubePlaylistId());
  }

  private void assertEnvironSciPolicyManagement117() throws MalformedURLException {
    final WarehouseCourse w = warehouseDatabase.getCourse(new CourseKey(2013, Semester.Fall, 29211));
    assertEquals(new Integer(716979484), w.getITunesVideoId());
    assertEquals(new URL("http://wbe-itunes.berkeley.edu/media/common/courses/fall_2013/rss/environ_sci_policy_and_management_117_lab_101_camera.rss"), w.getVideoFeedRSS());
    assertNull(w.getITunesAudioId());
    assertNull(w.getAudioFeedRSS());
    assertEquals("-XXv-cvA_iCKleR38Sy8443ClwVh4pmw", w.getYouTubePlaylistId());
  }

}
