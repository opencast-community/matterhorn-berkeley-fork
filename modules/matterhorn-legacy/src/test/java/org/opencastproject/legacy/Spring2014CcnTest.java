/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.legacy;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.opencastproject.participation.CourseUtils;
import org.opencastproject.util.data.Collections;
import org.opencastproject.warehouse.WarehouseCourse;

import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

/**
 * @author John Crossman
 */
@Ignore
public class Spring2014CcnTest {

  private static Map<Integer, WarehouseCourse> spring2014FromDatabase;
  private static Map<Integer, Integer> expectedRecordingCountMap;

  @Before
  public void before() {
    expectedRecordingCountMap = getExpectedRecordingCountMap();
    spring2014FromDatabase = new HashMap<Integer, WarehouseCourse>();
    throw new UnsupportedOperationException("WarehouseDatabase needs to be initialized here");
//    final List<WarehouseCourse> courses = warehouseDatabase.getCourses(2014, Semester.Spring);
//    for (final WarehouseCourse next : courses) {
//      assertEquals(2014, next.getYear());
//      assertEquals(Semester.Spring, next.getSemester());
//      spring2014FromDatabase.put(next.getCcn(), next);
//    }
  }

  @Test
  public void testSkippedCCNs() throws MalformedURLException {
    final List<Integer> skippedCCNs = Collections.list(52001, 58196, 60170);
    for (final Integer ccn : skippedCCNs) {
      assertNull(spring2014FromDatabase.get(ccn));
    }
  }

  @Test
  public void testExpectedCCNs() throws MalformedURLException {
    final List<Integer> missingCCNs = new LinkedList<Integer>();
    for (final int ccn : expectedRecordingCountMap.keySet()) {
      if (!spring2014FromDatabase.containsKey(ccn)) {
        missingCCNs.add(ccn);
      }
    }
    final int size = missingCCNs.size();
    if (size > 0) {
      fail("Database is missing " + size + " CCNs: " + Collections.mkString(missingCCNs, ", "));
    }
  }

  @Test
  public void testRecordingCounts() throws MalformedURLException {
    final List<String> errorMessageList = new LinkedList<String>();
    for (final int ccn : expectedRecordingCountMap.keySet()) {
      final WarehouseCourse course = spring2014FromDatabase.get(ccn);
      if (course != null) {
        final Integer expectedCount = expectedRecordingCountMap.get(ccn);
        final int actualCount = course.getRecordings().size();
        if (expectedCount != actualCount) {
          errorMessageList.add("CCN " + ccn + " expected " + expectedCount + " recordings but database has " + actualCount);
        }
      }
    }
    if (!errorMessageList.isEmpty()) {
      fail("Errors: " + Collections.mkString(errorMessageList, ";  "));
    }
  }

  @Test
  public void testUnaccountedCCNs() throws MalformedURLException {
    final Map<Integer, WarehouseCourse> unaccounted = new HashMap<Integer, WarehouseCourse>();
    for (final int ccn : spring2014FromDatabase.keySet()) {
      if (!expectedRecordingCountMap.containsKey(ccn)) {
        unaccounted.put(ccn, spring2014FromDatabase.get(ccn));
      }
    }
    if (!unaccounted.isEmpty()) {
      for (final Integer ccn : unaccounted.keySet()) {
        final WarehouseCourse next = unaccounted.get(ccn);
        System.out.println(CourseUtils.getSeriesId(next) + " : " + next.getRecordings().size());
      }
      fail(unaccounted.size() + " CCNs which do not exist in the hard-coded getExpectedRecordingCountMap() list.");
    }
  }

  private static Map<Integer, Integer> getExpectedRecordingCountMap() {
    final Map<Integer, Integer> map = new HashMap<Integer, Integer>();
    map.put(11147, 14);
    map.put(1218, 7);
    map.put(14069, 39);
    map.put(39711, 26);
    map.put(20509, 28);
    map.put(2057, 24);
    map.put(1203, 28);
    map.put(22417, 22);
    map.put(22462, 26);
    map.put(22545, 19);
    map.put(22552, 23);
    map.put(22696, 27);
    map.put(25165, 34);
    map.put(25177, 25);
    map.put(25192, 26);
    map.put(25216, 35);
    map.put(26024, 23);
    map.put(26096, 38);
    map.put(26216, 25);
    map.put(26333, 41);
    map.put(26420, 25);
    map.put(26561, 40);
    map.put(26651, 25);
    map.put(27603, 30);
    map.put(27678, 36);
    map.put(28117, 25);
    map.put(28927, 19);
    map.put(29392, 24);
    map.put(36275, 25);
    map.put(51667, 6);
    map.put(39564, 15);
    map.put(39576, 38);
    map.put(39648, 24);
    map.put(46436, 27);
    map.put(52046, 13);
    map.put(51608, 25);
    map.put(51881, 25);
    map.put(53592, 25);
    map.put(39567, 24);
    map.put(5533, 15);
    map.put(55492, 23);
    map.put(56263, 21);
    map.put(58334, 20);
    map.put(5915, 41);
    map.put(64503, 25);
    map.put(64596, 17);
    map.put(66724, 12);
    map.put(67301, 38);
    map.put(68804, 40);
    map.put(69239, 28);
    map.put(69344, 27);
    map.put(69482, 38);
    map.put(69490, 32);
    map.put(7049, 9);
    map.put(70564, 31);
    map.put(71621, 25);
    map.put(71859, 11);
    map.put(7403, 38);
    map.put(74163, 24);
    map.put(74193, 22);
    map.put(74253, 25);
    map.put(7502, 13);
    map.put(75571, 7);
    map.put(75624, 39);
    map.put(7592, 40);
    map.put(76207, 35);
    map.put(76270, 24);
    map.put(80913, 12);
    map.put(81603, 31);
    map.put(81756, 27);
    map.put(87432, 32);
    map.put(31195, 6);
    map.put(56745, 26);
    map.put(26600, 23);
    map.put(41506, 26);
    map.put(3815, 6);
    map.put(16090, 26);
    map.put(22393, 28);
    map.put(48015, 13);
    map.put(54186, 27);
    return map;
  }
}
