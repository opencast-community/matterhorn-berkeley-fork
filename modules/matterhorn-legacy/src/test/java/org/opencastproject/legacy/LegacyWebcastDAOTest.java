/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.legacy;

import org.json.simple.parser.ParseException;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.opencastproject.participation.model.CanonicalCourse;
import org.opencastproject.participation.model.CourseData;
import org.opencastproject.participation.model.CourseKey;
import org.opencastproject.participation.model.DayOfWeek;
import org.opencastproject.participation.model.HasCourseKey;
import org.opencastproject.participation.model.Instructor;
import org.opencastproject.participation.model.Participation;
import org.opencastproject.participation.model.Room;
import org.opencastproject.participation.model.RoomCapability;
import org.opencastproject.participation.model.Semester;
import org.opencastproject.participation.model.Term;
import org.opencastproject.util.data.Collections;
import org.opencastproject.warehouse.PublishedMedia;
import org.opencastproject.warehouse.WarehouseCourse;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * @author John Crossman
 */
@Ignore
public class LegacyWebcastDAOTest {

  private final static Map<HasCourseKey, PublishedMedia> publishMediaOncePerYear = new HashMap<HasCourseKey, PublishedMedia>();
  private static LegacyWebcastDAO legacyWebcastDAO;

  @BeforeClass
  public static void beforeClass() throws IOException, ParseException {
    // These are static because we only need a single initialization, as opposed to a reset prior to each test method.
    legacyWebcastDAO = setUpMockData();
  }

  @Test
  public void testGetLegacyITunesUMap() throws MalformedURLException {
    final Map<Integer, ITunesCollection> iTunesUMap = legacyWebcastDAO.getLegacyITunesUMap();
    final Set<PublishedMedia> sampleSet = Collections.set(getPublishedMedia2008(), getPublishedMedia2009(),
            getPublishedMedia2010(), getPublishedMedia2011(), getPublishedMedia2012(), getPublishedMedia2013(),
            getPublishedMedia2014());
    assertEquals(7, sampleSet.size());
    for (final PublishedMedia next : sampleSet) {
      final Integer iTunesAudioId = next.getITunesAudioId();
      if (iTunesAudioId != null) {
        final ITunesCollection audio = iTunesUMap.get(iTunesAudioId);
        assertNotNull("Not found: " + next, audio);
        assertEquals(next.getAudioFeedRSS(), audio.getFeed());
      }
      //
      final Integer iTunesVideoId = next.getITunesVideoId();
      if (iTunesVideoId != null) {
        final ITunesCollection video = iTunesUMap.get(iTunesVideoId);
        assertNotNull("Not found: " + next, video);
        assertEquals(next.getVideoFeedRSS(), video.getFeed());
      }
    }
  }

  @Test
  public void testGeographyC110() throws MalformedURLException {
    final Map<Integer, ITunesCollection> iTunesUMap = legacyWebcastDAO.getLegacyITunesUMap();
    final Integer iTunesId = 391536131;
    final ITunesCollection c = iTunesUMap.get(iTunesId);
    assertNotNull(c);
    assertEquals(iTunesId, c.getITunesId());
    assertEquals(new Integer(2010), c.getYear());
    assertEquals(Semester.Fall, c.getSemester());
    assertEquals("Geography C110, 001, Interdisciplinary Studies Field Maj C101, 001", c.getTitle());
    assertNull(c.getContributors());
    assertEquals("Audio", c.getMediaFormat());
    assertEquals("wbe-itunes.berkeley.edu/media/common/rss/geography_c110_Fall_2010_Audio__itunes.rss", c.getFeed().getHost() + c.getFeed().getPath());
  }


  @Test
  public void testLegacyData() throws IOException {
    final Map<CourseKey, WarehouseCourse> legacyData = legacyWebcastDAO.getLegacyWebcast();
    for (final HasCourseKey courseKey : publishMediaOncePerYear.keySet()) {
      final WarehouseCourse w = legacyData.get(courseKey);
      assertNotNull("No match for " + courseKey, w);
      final PublishedMedia p = publishMediaOncePerYear.get(courseKey);
      assertEquals(p.getYouTubePlaylistId(), w.getYouTubePlaylistId());
      assertEquals(p.getITunesAudioId(), w.getITunesAudioId());
      assertEquals(p.getITunesVideoId(), w.getITunesVideoId());
      assertEquals(p.getAudioFeedRSS(), w.getAudioFeedRSS());
      assertEquals(p.getVideoFeedRSS(), w.getVideoFeedRSS());
      assertEquals(p.getScreenFeedRSS(), w.getScreenFeedRSS());
    }
  }

  /**
   * @return One course per year, with data pulled from actual catalog queries
   */
  private static LegacyWebcastDAO setUpMockData() throws IOException, ParseException {
    throw new UnsupportedOperationException("SQL query getCourseDataForLegacyDataMover is no longer supported.");

//    final ProvidesCourseCatalogData courseDatabase = createMock(ProvidesCourseCatalogData.class);
//
//    // 2008: { "semester":"Fall 2008", "videoId":"354818596", "audioId":"354818635", "title":"Computer Science 61B - Fall 2008", "lecturer":"Paul Hilfinger", "descr":"...", "dept":"Computer Science", "youTube":"" } ,
//    final CourseData c2008 = getCourseData(2008, Semester.Fall, 26332, "001", "Data Structures", "61B", "COMPSCI", "Computer Science");
//    expect(courseDatabase.getCourseDataForLegacyDataMover(c2008.getSemester(), c2008.getYear())).andReturn(Collections.set(c2008)).once();
//    publishMediaOncePerYear.put(c2008, getPublishedMedia2008());
//
//    // 2009: { "semester":"Fall 2009", "videoId":"354819201", "audioId":"354819164", "title":"Practice of Art 23AC, 001 - Fall 2009", "lecturer":"Ozge Samanci, Greg Niemeyer", "descr":"...", "dept":"Art", "youTube":"ECF3257F4489B90EA4" } ,
//    final CourseData c2009 = getCourseData(2009, Semester.Fall, 4939, "001", "Foundations of American Cyber-Culture", "23AC", "ART", "Practice of Art");
//    expect(courseDatabase.getCourseDataForLegacyDataMover(c2009.getSemester(), c2009.getYear())).andReturn(Collections.set(c2009)).once();
//    publishMediaOncePerYear.put(c2009, getPublishedMedia2009());
//
//    // 2010: {"semester":"Fall 2010","videoId":"391529859","audioId":"391529712","title":"Biology 1B, 001 - Fall 2010","lecturer":"John P. Huelsenbeck, Alan Shabel, Bruce G. Baldwin\n\n","descr":"...","dept":"Biology","youTube":"ECA0677EF99D42F238"},
//    final CourseData c2010 = getCourseData(2010, Semester.Fall, 7409, "001", "General Biology", "1B", "BIOLOGY", "Biology");
//    expect(courseDatabase.getCourseDataForLegacyDataMover(c2010.getSemester(), c2010.getYear())).andReturn(
//            Collections.set(c2010)).once();
//    publishMediaOncePerYear.put(c2010, getPublishedMedia2010());
//
//    // 2011: {"semester":"Spring 2011","videoId":"438291407","audioId":"438292382","title":"Chemistry 1A, 001 - Spring 2011","lecturer":"Heino Nitsche, Chunmei Li","descr":"...","dept":"Chemistry","youTube":"EC63A76D16A47F4567"},
//    final CourseData c2011 = getCourseData(2011, Semester.Spring, 10953, "001", "General Chemistry", "1A", "CHEM", "Chemistry");
//    expect(courseDatabase.getCourseDataForLegacyDataMover(c2011.getSemester(), c2011.getYear())).andReturn(Collections.set(c2011)).once();
//    // Extra Chemistry classes. We're trying to confuse the LegacyDataMover code.
//    expect(courseDatabase.getCourseDataForLegacyDataMover(Semester.Fall, 2011)).andReturn(Collections
//            .set(getCourseData(2011, Semester.Fall, 10956, "002", "General Chemistry", "1A", "CHEM", "Chemistry"))).once();
//    expect(courseDatabase.getCourseDataForLegacyDataMover(Semester.Fall, 2011)).andReturn(Collections
//            .set(getCourseData(2011, Semester.Fall, 10959, "003", "General Chemistry", "1A", "CHEM", "Chemistry"))).once();
//    publishMediaOncePerYear.put(c2011, getPublishedMedia2011());
//
//    // 2012: {"semester":"Spring 2012","videoId":"496113145","audioId":"496113770","title":"Environmental Economics and Policy C1, 001 - Spring 2012","lecturer":"Gordon Rausser","descr":"...","dept":"EEP","youTube":"ECC3A24A51507AF284"},
//    final CourseData c2012 = getCourseData(2012, Semester.Spring, 1203, "001", "Introduction to Environmental Economics and Policy", "C1", "ENVECON", "Environmental Economics and Policy");
//    expect(courseDatabase.getCourseDataForLegacyDataMover(c2012.getSemester(), c2012.getYear())).andReturn(Collections.set(c2012)).once();
//    publishMediaOncePerYear.put(c2012, getPublishedMedia2012());
//
//    // 2013: {"semester":"Fall 2013","videoId":"","audioId":"701366893","title":"Japanese 7A, 001 - Fall 2013","lecturer":"John R. Wallace","descr":"...","dept":"Japanese","youTube":""},
//    final CourseData c2013 = getCourseData(2013, Semester.Fall, 21039, "001", "Introduction to Pre-Modern Japanese Literature and Culture", "7A", "JAPAN", "Japanese");
//    expect(courseDatabase.getCourseDataForLegacyDataMover(c2013.getSemester(), c2013.getYear())).andReturn(Collections.set(c2013)).once();
//    publishMediaOncePerYear.put(c2013, getPublishedMedia2013());
//
//    // 2014: {"semester":"Spring 2014","videoId":"819827827","audioId":"","title":"Statistics 131A, 001 - Spring 2014","lecturer":"Shobhana Stoyanov","descr":"...","dept":"Statistics","youTube":"-XXv-cvA_iDmBzAfitlZXganWpqMiXkG"}
//    final CourseData c2014 = getCourseData(2014, Semester.Spring, 87432, "001", "Introduction to Probability and Statistics for Life Scientists", "131A", "STAT", "Statistics");
//    expect(courseDatabase.getCourseDataForLegacyDataMover(c2014.getSemester(), c2014.getYear())).andReturn(Collections.set(c2014)).once();
//    publishMediaOncePerYear.put(c2014, getPublishedMedia2014());
//    //
//    expect(courseDatabase.getCourseDataForLegacyDataMover(anyObject(Semester.class), anyInt())).andReturn(new HashSet<CourseData>()).anyTimes();
//    expect(courseDatabase.getHardCodedCoursesForWarehouse()).andReturn(new HashSet<CourseData>()).once();
//    replay(courseDatabase);
//    return new LegacyWebcastDAOImpl(courseDatabase, new LegacyFileProviderImpl());
  }

  private static CourseData getCourseData(final int year, final Semester semester, final int ccn, final String section,
          final String title, final String catalogId, final String deptName, final String deptDescription) {
    final CourseData courseData = new CourseData();
    courseData.setCanonicalCourse(new CanonicalCourse(ccn, null, title));
    courseData.setTerm(new Term(semester, year, null, null));
    courseData.setCatalogId(catalogId);
    courseData.setDeptName(deptName);
    courseData.setDeptDescription(deptDescription);
    courseData.setMeetingDays(Collections.list(DayOfWeek.Monday, DayOfWeek.Wednesday, DayOfWeek.Friday));
    courseData.setParticipationSet(Collections
            .set(new Participation(null, new Instructor(null, "calNetUID", null, "First name", "Last name", null, null), false)));
    courseData.setRoom(new Room(null, "STANLEY", "0106", RoomCapability.screencast));
    courseData.setSection(section);
    return courseData;
  }

  private static PublishedMedia getPublishedMedia2008() throws MalformedURLException {
    return new PublishedMedia(
            null,
            354818635,
            354818596,
            new URL("http://wbe-itunes.berkeley.edu/media/common/rss/computer_science_61b_Fall_2008_Audio__itunes.rss"),
            new URL("http://wbe-itunes.berkeley.edu/media/common/rss/computer_science_61b_Fall_2008_Video__itunes.rss"),
            null);
  }

  private static PublishedMedia getPublishedMedia2009() throws MalformedURLException {
    return new PublishedMedia(
            "ECF3257F4489B90EA4",
            354819164,
            354819201,
            new URL("http://wbe-itunes.berkeley.edu/media/common/rss/practice_of_art_23ac_Fall_2009_Audio__itunes.rss"),
            new URL("http://wbe-itunes.berkeley.edu/media/common/rss/practice_of_art_23ac_Fall_2009_Video__itunes.rss"),
            null);
  }

  private static PublishedMedia getPublishedMedia2010() throws MalformedURLException {
    return new PublishedMedia(
            "ECA0677EF99D42F238",
            391529712,
            391529859,
            new URL("http://wbe-itunes.berkeley.edu/media/common/rss/biology_1b_Fall_2010_Audio__itunes.rss"),
            new URL("http://wbe-itunes.berkeley.edu/media/common/rss/biology_1b_Fall_2010_Video__itunes.rss"),
            null);
  }

  private static PublishedMedia getPublishedMedia2011() throws MalformedURLException {
    return new PublishedMedia(
            "EC63A76D16A47F4567",
            438292382,
            438291407,
            new URL("http://wbe-itunes.berkeley.edu/media/common/rss/chemistry_1a_Spring_2011_Audio__itunes.rss"),
            new URL("http://wbe-itunes.berkeley.edu/media/common/rss/chemistry_1a_Spring_2011_Video__itunes.rss"),
            null);
  }

  private static PublishedMedia getPublishedMedia2012() throws MalformedURLException {
    return new PublishedMedia(
            "ECC3A24A51507AF284",
            496113770,
            496113145,
            new URL("http://wbe-itunes.berkeley.edu/media/common/rss/economics_c3_001_Spring_2012_Audio__itunes.rss"),
            new URL("http://wbe-itunes.berkeley.edu/media/common/rss/economics_c3_001_Spring_2012_Video__itunes.rss"),
            null);
  }

  private static PublishedMedia getPublishedMedia2013() throws MalformedURLException {
    return new PublishedMedia(
            null,
            701366893,
            null,
            new URL("http://wbe-itunes.berkeley.edu/media/common/courses/fall_2013/rss/japanese_7a_001_audio.rss"),
            null,
            null);
  }

  private static PublishedMedia getPublishedMedia2014() throws MalformedURLException {
    return new PublishedMedia(
            "-XXv-cvA_iDmBzAfitlZXganWpqMiXkG",
            null,
            819827827,
            null,
            new URL("http://wbe-itunes.berkeley.edu/media/common/courses/spring_2014/rss/statistics_131a_001_screen.rss"),
            null);
  }

}
