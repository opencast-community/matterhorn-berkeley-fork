/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.legacy;

import com.google.api.services.youtube.model.Video;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.lang.time.DateUtils;
import org.opencastproject.google.youtube.GoogleUtils;
import org.opencastproject.mediapackage.MediaPackageException;
import org.opencastproject.mediapackage.MediaPackageImpl;
import org.opencastproject.mediapackage.Publication;
import org.opencastproject.participation.CourseUtils;
import org.opencastproject.participation.model.Semester;
import org.opencastproject.google.youtube.YouTubeAPIVersion3Service;
import org.opencastproject.google.youtube.YouTubeUtils;
import org.opencastproject.util.DateTimeSupport;
import org.opencastproject.util.UrlSupport;
import org.opencastproject.util.data.Collections;
import org.opencastproject.warehouse.Recording;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

/**
 * @author John Crossman
 */
public abstract class AbstractLegacyFileProvider implements LegacyFileProvider {

  private final Logger logger = LoggerFactory.getLogger(this.getClass());
  private final YouTubeAPIVersion3Service youTubeService;

  protected AbstractLegacyFileProvider() throws IOException {
    youTubeService = YouTubeUtils.getYouTubeAPIVersion3Service();
  }

  protected Map<Integer, ITunesCollection> getITunesCollectionMap(final File file) throws IOException {
    final String[] rows = StringUtils.split(FileUtils.readFileToString(file), '\n');
    // Row zero is header row, so we skip it
    final Map<Integer, ITunesCollection> iTunesUMap = new HashMap<Integer, ITunesCollection>();
    for (int index = 1; index < rows.length; index++) {
      final ITunesCollection next = getITunesCollection(rows[index], '~');
      final Integer iTunesId = next.getITunesId();
      if (iTunesUMap.containsKey(iTunesId)) {
        throw new IllegalStateException("iTunesUMap already contains id: " + iTunesId);
      }
      iTunesUMap.put(iTunesId, next);
    }
    return iTunesUMap;
  }

  /**
   * @param allEpisodesFile null not allowed
   * @return seriesIds mapped to the recordings thereof
   * @throws IOException
   */
  protected Map<String, Set<RecordingMediaPackage>> getSeriesIdMap(final File allEpisodesFile) throws IOException {
    final Map<String, List<MediaPackageImpl>> seriesEpisodeMap = getEpisodesPerSeries(allEpisodesFile);
    return getSeriesIdMap(seriesEpisodeMap);
  }

  /**
   * Protected for sake of unit test.
   * @param seriesEpisodeMap null not allowed
   * @return map of corresponding recordings, minus duplicates and pre-determined exclusions
   */
  protected Map<String, Set<RecordingMediaPackage>> getSeriesIdMap(final Map<String, List<MediaPackageImpl>> seriesEpisodeMap)
          throws IOException {
    final Map<String, Set<RecordingMediaPackage>> map = new HashMap<String, Set<RecordingMediaPackage>>();
    final List<String> seriesIdIgnorelist = LegacyUtils.getSeriesIdIgnorelist();
    final List<RecordingMediaPackage> duplicates = new LinkedList<RecordingMediaPackage>();
    for (final String seriesId : seriesEpisodeMap.keySet()) {
      if (seriesIdIgnorelist.contains(seriesId) || CourseUtils.getCourseKey(seriesId) == null) {
        continue;
      }
      final List<MediaPackageImpl> mediaPackageList = seriesEpisodeMap.get(seriesId);
      for (final MediaPackageImpl next : mediaPackageList) {
        final Date start = fromUTC(next.getStartDateAsString());
        final Date end = next.getDuration() == null || start == null
                ? null
                : DateUtils.addMilliseconds(start, next.getDuration().intValue());
        String youTubeVideoId = getYouTubeVideoId(next.getPublications());
        final String publicationEngagePlayer = getPublicationEngagePlayer(next.getPublications());
        final String seriesIdFromMediaPackage = StringUtils.trimToNull(next.getSeries());
        final boolean validSeriesId = seriesIdFromMediaPackage != null
                && !seriesIdIgnorelist.contains(seriesIdFromMediaPackage)
                && CourseUtils.getCourseKey(seriesIdFromMediaPackage) != null
                && StringUtils.equalsIgnoreCase(seriesIdFromMediaPackage, seriesId);
        boolean addRecording = start != null
                && end != null
                && publicationEngagePlayer != null
                && validSeriesId;
        final Integer durationSeconds;
        if (youTubeVideoId == null) {
          durationSeconds = null;
        } else {
          final Video video = youTubeService.getVideoById(youTubeVideoId);
          addRecording = video != null;
          youTubeVideoId = addRecording ? video.getId() : null;
          durationSeconds = addRecording ? GoogleUtils.getDurationSeconds(video) : null;
        }
        final String episodeId = next.getIdentifier() == null ? null : next.getIdentifier().toString();
        final Recording recording = new Recording(next.getTitle(), null, youTubeVideoId, start, end, durationSeconds, episodeId);
        final RecordingMediaPackage incoming = new RecordingMediaPackage(recording, next);
        if (addRecording) {
          final Set<RecordingMediaPackage> recordings;
          if (map.containsKey(seriesId)) {
            recordings = map.get(seriesId);
          } else {
            recordings = new TreeSet<RecordingMediaPackage>(new RecordingMediaPackageComparator());
            map.put(seriesId, recordings);
          }
          final RecordingMediaPackage previouslyAdded = findMatch(incoming.getRecording(), recordings);
          // The code below can assume that non-null youTubeVideoId points at private or public video at YouTube.
          // In the following choice-making, we favor the recording with YouTube video
          final Recording previous = previouslyAdded == null ? null : previouslyAdded.getRecording();
          final String previousYouTubeVideoId = previouslyAdded == null ? null : previous.getYouTubeVideoId();
          if (previouslyAdded == null) {
            recordings.add(incoming);
          } else if (previouslyAdded == incoming) {
            throw new IllegalStateException("Why are we comparing an instance to itself?! Object: " + incoming);
          } else if (previousYouTubeVideoId == null && youTubeVideoId != null) {
            // New recording is better than the existing
            recordings.remove(previouslyAdded);
            recordings.add(incoming);
          } else {
            final Recording incomingRecording = incoming.getRecording();
            final String incomingTitle = incomingRecording.getTitle();
            if (previousYouTubeVideoId != null && youTubeVideoId != null) {
              // Compare durations. Do nothing if they match.
              final Integer duration = incomingRecording.getTrimmedLengthSeconds();
              if (duration.equals(previous.getTrimmedLengthSeconds())) {
                logger.warn("Choose latter of equivalent recordings: " + incoming + '\n' + previouslyAdded);
              } else {
                incomingRecording.setTitle(incomingTitle + " (YouTube duration: " + duration + ")");
                recordings.add(incoming);
              }
            } else if (!incomingRecording.getRecordingStart().equals(previous.getRecordingStart())) {
              final String startTime = DateFormatUtils.format(incomingRecording.getRecordingStart(), "h:mm a");
              incomingRecording.setTitle(incomingTitle + " (Start time: " + startTime + ")");
              recordings.add(incoming);
            } else if (!incomingTitle.equals(previous.getTitle())) {
              setTitleWhenBetter(incoming.getRecording(), previous.getTitle());
              recordings.add(incoming);
            } else {
              if (incomingRecording.getYouTubeVideoId() != null) {
                throw new IllegalStateException("Recordings with valid YouTube videos should never be discarded: " + incoming);
              }
              final List<String> toleratedDuplicate = Collections.list(
                      "Biology 1B - 2014-01-27",
                      "Mathematics 128A - 2014-01-30: Newton's Method and Its Extensions (no image 26-58 minutes into lecture)",
                      "Mathematics 128A - 2014-03-11: Review",
                      "Computer Science 70 - 2014-01-21",
                      "Computer Science 70 - 2014-02-18",
                      "Computer Science 70 - 2014-03-06",
                      "Computer Science 70 - 2014-01-21",
                      "Computer Science 70 - 2014-04-03",
                      "Astronomy C10 - 2014-01-31: Thermal Radiation; Doppler Effect",
                      "Psychology 131 - 2014-01-27",
                      "Psychology 131 - 2014-03-03");
              if (toleratedDuplicate.contains(incomingTitle)) {
                logger.info("Tolerating duplicate: " + incoming);
              } else {
                duplicates.add(incoming);
              }
            }
          }
        }
      }
    }
    if (!duplicates.isEmpty()) {
      int index = 0;
      for (final RecordingMediaPackage duplicate : duplicates) {
        if (index % 2 == 0) {
          System.out.println("--------");
        }
        System.out.println(duplicate);
      }
      System.out.println("--------");
      throw new IllegalStateException(duplicates.size() / 2 + " duplicates. Not sure how to resolve.");
    }
    return map;
  }

  private void setTitleWhenBetter(final Recording r, final String title) {
    if (title.startsWith(r.getTitle())) {
      // It is possible that existing title is a truncated version of 'title'
      r.setTitle(title);
    }
  }

  private RecordingMediaPackage findMatch(final Recording compareThis, final Set<RecordingMediaPackage> recordings) {
    RecordingMediaPackage match = null;
    for (final RecordingMediaPackage next : recordings) {
      final boolean equalTitle = StringUtils.equals(next.getRecording().getTitle(), compareThis.getTitle());
      if ((equalTitle && next.getRecording().getYouTubeVideoId() != null) || next.getRecording().equals(compareThis)) {
        match = next;
      }
    }
    return match;
  }

  private ITunesCollection getITunesCollection(final String content, final char columnSeparator) {
    final String[] columns = StringUtils.split(content, columnSeparator);
    final String title = get(columns, 3);
    Integer year = LegacyUtils.getYear(get(columns, 0));
    Semester semester = LegacyUtils.getSemester(get(columns, 1));
    if ((semester == null || year == null) && title != null) {
      String semesterMarker = null;
      final String fallMarker = "- Fall ";
      if (title.contains(fallMarker)) {
        semesterMarker = fallMarker;
      } else {
        final String springMarker = "- Spring ";
        if (title.contains(springMarker)) {
          semesterMarker = springMarker;
        }
      }
      if (semester == null && semesterMarker != null) {
        semester = semesterMarker.equals(fallMarker) ? Semester.Fall : Semester.Spring;
      }
      if (year == null) {
        String yearAsString = StringUtils.substringAfter(title, semesterMarker);
        int toInt = NumberUtils.toInt(yearAsString, -1);
        if (toInt == -1) {
          yearAsString = StringUtils.substringBetween(title, semesterMarker, ":");
          toInt = NumberUtils.toInt(yearAsString, -1);
        }
        year = (toInt == -1) ? null : toInt;
      }
    }
    if (year == null || semester == null) {
      throw new IllegalArgumentException("Null year and/or semester: " + content);
    }
    final Integer ccn = LegacyUtils.toWhole(get(columns, 2), null);
    return new ITunesCollection(year, semester, ccn, title, get(columns, 4),
            get(columns, 5), get(columns, 6), UrlSupport.verifyAndConstruct(get(columns, 7), logger),
            LegacyUtils.toWhole(get(columns, 8),
            new Integer(get(columns, 11))));
  }

  private String get(final String[] columns, final int index) {
    return StringUtils.trimToNull(columns[index]);
  }

  private Date fromUTC(final String dateAsString) {
    try {
      return StringUtils.isBlank(dateAsString) ? null : new Date(DateTimeSupport.fromUTC(dateAsString));
    } catch (ParseException e) {
      return null;
    }
  }

  private String getPublicationEngagePlayer(final Publication[] publications) {
    String value = null;
    if (publications != null) {
      for (final Publication p : publications) {
        if (StringUtils.containsIgnoreCase(p.getChannel(), "engage-player") && p.getURI() != null) {
          value = p.getURI().toString();
          break;
        }
      }
    }
    return value;
  }

  private String getYouTubeVideoId(final Publication[] publications) {
    String youTubeVideoId = null;
    if (publications != null) {
      for (final Publication p : publications) {
        if (StringUtils.containsIgnoreCase(p.getChannel(), "youtube") && p.getURI() != null) {
          youTubeVideoId = StringUtils.substringAfter(p.getURI().getQuery(), "v=");
          break;
        }
      }
    }
    return StringUtils.trimToNull(youTubeVideoId);
  }

  /**
   * @param allEpisodesFile every Matterhorn episode will have a line in this file. Each line is valid XML in itself.
   * @return seriesIds mapped to the mediaPackages thereof
   */
  Map<String, List<MediaPackageImpl>> getEpisodesPerSeries(final File allEpisodesFile)
          throws IOException {
    final Map<String, List<MediaPackageImpl>> map = new HashMap<String, List<MediaPackageImpl>>();
    final List<String> lines = FileUtils.readLines(allEpisodesFile);
    for (final String line : lines) {
      if (line == null || line.length() == 0) {
        continue;
      }
      final String seriesId = StringUtils.trimToNull(StringUtils.substringBetween(line, "<series>", "</series>"));
      if (seriesId != null) {
        final List<MediaPackageImpl> episodeXMLs = get(map, seriesId);
        final String title = StringUtils.trimToNull(StringUtils.substringBetween(line, "<title>", "</title>"));
        if (title != null) {
          episodeXMLs.add(getMediaPackage(line));
        }
      }
    }
    return map;
  }

  private MediaPackageImpl getMediaPackage(final String xmlContent) throws IOException {
    try {
      final ByteArrayInputStream xml = new ByteArrayInputStream(xmlContent.getBytes());
      return MediaPackageImpl.valueOf(xml);
    } catch (final MediaPackageException e) {
      throw new IOException(e);
    }
  }

  private List<MediaPackageImpl> get(final Map<String, List<MediaPackageImpl>> map, final String key) {
    final List<MediaPackageImpl> list;
    if (map.containsKey(key)) {
      list = map.get(key);
    } else {
      list = new LinkedList<MediaPackageImpl>();
      map.put(key, list);
    }
    return list;
  }

  private class RecordingMediaPackageComparator implements Comparator<RecordingMediaPackage> {
    @Override
    public int compare(final RecordingMediaPackage r1, final RecordingMediaPackage r2) {
      return new Recording.RecordingComparator().compare(r1.getRecording(), r2.getRecording());
    }
  }
}
