/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.legacy;

import org.opencastproject.mediapackage.MediaPackage;
import org.opencastproject.warehouse.Recording;

/**
 * @author John Crossman
 */
public class RecordingMediaPackage {

  private final Recording recording;
  private final MediaPackage mediaPackage;

  public RecordingMediaPackage(final Recording recording, final MediaPackage mediaPackage) {
    this.recording = recording;
    this.mediaPackage = mediaPackage;
  }

  public Recording getRecording() {
    return recording;
  }

  public MediaPackage getMediaPackage() {
    return mediaPackage;
  }
}
