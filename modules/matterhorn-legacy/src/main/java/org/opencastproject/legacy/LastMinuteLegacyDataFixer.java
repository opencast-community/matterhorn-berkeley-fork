/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.legacy;

import org.opencastproject.participation.CourseUtils;
import org.opencastproject.participation.model.CanonicalCourse;
import org.opencastproject.participation.model.CapturePreferences;
import org.opencastproject.participation.model.DayOfWeek;
import org.opencastproject.participation.model.RecordingAvailability;
import org.opencastproject.participation.model.RecordingType;
import org.opencastproject.participation.model.Semester;
import org.opencastproject.util.UrlSupport;
import org.opencastproject.util.data.Collections;
import org.opencastproject.util.env.Environment;
import org.opencastproject.util.env.EnvironmentUtil;
import org.opencastproject.warehouse.AssociatedLicense;
import org.opencastproject.warehouse.Recording;
import org.opencastproject.warehouse.WarehouseBuilding;
import org.opencastproject.warehouse.WarehouseCourse;
import org.opencastproject.warehouse.WarehouseRoom;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * @author John Crossman
 */
public class LastMinuteLegacyDataFixer {

  private final Logger logger = LoggerFactory.getLogger(this.getClass());
  private final List<WarehouseCourse> fixed;

  public LastMinuteLegacyDataFixer(final Collection<WarehouseCourse> unfixed) {
    fixed = new LinkedList<WarehouseCourse>();
    fixed.addAll(getHardCodedCourses());
    for (final WarehouseCourse next : unfixed) {
      fixAndAdd(next, fixed);
    }
    if (!Environment.prod.equals(EnvironmentUtil.getEnvironment())) {
      fixed.add(getMockMalay());
    }
  }

  public List<WarehouseCourse> getFixed() {
    return fixed;
  }

  /**
   * Fix bad data then add to the list. In some case we might NOT allow a course in the database.
   * @param w never null
   * @param list never null
   */
  private void fixAndAdd(final WarehouseCourse w, final List<WarehouseCourse> list) {
    final String key = CourseUtils.getSeriesId(w);
    final boolean skipThis = LegacyUtils.getSeriesIdIgnorelist().contains(key)
            || (w.getYear() == 2014 && w.getSemester() == Semester.Spring && w.getRecordings().size() < 2);
    if (skipThis) {
      // Skip this course per WCT-4884, WCT-4914, WCT-4885
      return;
    }
    // Apply corrections per WCT-4877
    if ("2008D16072".equals(key)) {
      w.setITunesAudioId(461123874);
      w.setAudioFeedRSS(toURL("http://wbe-itunes.berkeley.edu/media/common/rss/Cognitive_Science_C127__Psychology_C127_Fall_2008_Audio__webcast.rss"));
    } else if ("2008D76145".equals(key)) {
      w.setITunesAudioId(461123850);
      w.setAudioFeedRSS(toURL("http://wbe-itunes.berkeley.edu/media/common/rss/Public_Health_245_Fall_2008_Audio__webcast.rss"));
    } else if ("2009B18215".equals(key)) {
      w.setITunesAudioId(461123238);
      w.setAudioFeedRSS(toURL("http://wbe-itunes.berkeley.edu/media/common/rss/Demography_C175__001__Economics_C175__001_Spring_2009_Audio__webcast.rss"));
    } else if ("2009B22669".equals(key)) {
      w.setITunesAudioId(461123238);
      w.setAudioFeedRSS(toURL("http://wbe-itunes.berkeley.edu/media/common/rss/Demography_C175__001__Economics_C175__001_Spring_2009_Audio__webcast.rss"));
    } else if ("2009B29304".equals(key)) {
      w.setITunesAudioId(461123850);
      w.setAudioFeedRSS(toURL("http://wbe-itunes.berkeley.edu/media/common/rss/Demography_C175__001__Economics_C175__001_Spring_2009_Audio__webcast.rss"));
    } else if ("2009B41009".equals(key)) {
      w.setITunesAudioId(null);
      w.setAudioFeedRSS(toURL("http://wbe-itunes.berkeley.edu/media/common/rss/industrial_engin_and_oper_research_131_Spring_2009_Audio__itunes.rss"));
    } else if ("2009B81852".equals(key)) {
      w.setITunesAudioId(461123891);
      w.setITunesVideoId(461123880);
      w.setAudioFeedRSS(toURL("http://wbe-itunes.berkeley.edu/media/common/rss/Sociology_150A__001_Spring_2009_Audio__webcast.rss"));
      w.setVideoFeedRSS(toURL("http://wbe-itunes.berkeley.edu/media/common/rss/Sociology_150A__001_Spring_2009_Video__webcast.rss"));
    } else if ("2010B29401".equals(key)) {
      w.setITunesAudioId(461123652);
      w.setAudioFeedRSS(toURL("http://wbe-itunes.berkeley.edu/media/common/rss/Environ_Sci__Policy__and_Management_114__001_Spring_2010_Audio__webcast.rss"));
    } else if ("2010B51962".equals(key)) {
      w.setITunesAudioId(461123867);
      w.setITunesVideoId(354822728);
      w.setAudioFeedRSS(toURL("http://wbe-itunes.berkeley.edu/media/common/rss/Letters_and_Science_C70V__001__Physics_C10__001_Spring_2010_Audio__webcast.rss"));
      w.setVideoFeedRSS(toURL("http://wbe-itunes.berkeley.edu/media/common/rss/Letters_and_Science_C70V__001__Physics_C10__001_Spring_2010_Video__webcast.rss"));
    } else if ("2010B52013".equals(key)) {
      w.setITunesAudioId(461123864);
      w.setITunesVideoId(461123965);
      w.setAudioFeedRSS(toURL("http://wbe-itunes.berkeley.edu/media/common/rss/Letters_and_Science_140D__001_Spring_2010_Audio__webcast.rss"));
      w.setVideoFeedRSS(toURL("http://wbe-itunes.berkeley.edu/media/common/rss/Letters_and_Science_140D__001_Spring_2010_Video__webcast.rss"));
    } else if ("2010B69390".equals(key)) {
      w.setITunesAudioId(461123864);
      w.setITunesVideoId(461123867);
      w.setAudioFeedRSS(toURL("http://wbe-itunes.berkeley.edu/media/common/rss/Letters_and_Science_C70V__001__Physics_C10__001_Spring_2010_Audio__webcast.rss"));
      w.setVideoFeedRSS(toURL("http://wbe-itunes.berkeley.edu/media/common/rss/Letters_and_Science_C70V__001__Physics_C10__001_Spring_2010_Video__webcast.rss"));
    } else if ("2010B76235".equals(key)) {
      w.setITunesVideoId(null);
      w.setITunesAudioId(null);
    } else if ("2010D25491".equals(key)) {
      w.setITunesVideoId(null);
      w.setITunesAudioId(null);
      w.setVideoFeedRSS(toURL("http://wbe-itunes.berkeley.edu/media/common/rss/electrical_engineering_141_Fall_2010_Video__itunes.rss"));
      w.setAudioFeedRSS(toURL("http://wbe-itunes.berkeley.edu/media/common/rss/electrical_engineering_141_Fall_2010_Audio__itunes.rss"));
    } else if ("2012B51806".equals(key)) {
      w.setITunesAudioId(498093212);
      w.setAudioFeedRSS(toURL("http://wbe-itunes.berkeley.edu/media/common/rss/espm_c11_Spring_2012_Audio__itunes.rss"));
    } else if ("2014B66724".equals(key)) {
      w.setITunesAudioId(null);
      w.setAudioFeedRSS(toURL("http://wbe-itunes.berkeley.edu/media/common/courses/spring_2014/rss/peace_and_conflict_studies_94_001_audio.rss"));
    } else if ("2010B69078".equals(key)) {
      // See WCT-4879
      w.setDepartmentName("PHYSICS");
      w.setSection("001");
      w.setAudioFeedRSS(
              toURL("http://wbe-itunes.berkeley.edu/media/common/rss/physics_8a_001_Spring_2010_Audio__itunes.rss"));
      w.setITunesAudioId(354822572);
    } else if ("2010B69171".equals(key)) {
      // See WCT-4879
      w.setDepartmentName("PHYSICS");
      w.setSection("002");
      w.setAudioFeedRSS(toURL("http://wbe-itunes.berkeley.edu/media/common/rss/physics_8a_002_Spring_2010_Audio__itunes.rss"));
      w.setITunesAudioId(354822624);
    } else if ("2011B69078".equals(key)) {
      // See WCT-4879
      w.setSection("001");
      w.setAudioFeedRSS(toURL("http://wbe-itunes.berkeley.edu/media/common/rss/physics_8a_Spring_2011_Audio__itunes.rss"));
      w.setITunesAudioId(438307792);
    } else if ("2014B26420".equals(key)) {
      final Set<Recording> filteredSet = new HashSet<Recording>();
      for (final Recording next : w.getRecordings()) {
        if (next.getYouTubeVideoId() != null) {
          filteredSet.add(next);
        }
      }
      w.setRecordings(filteredSet);
    } else if (new Integer(852691737).equals(w.getITunesAudioId())) {
      w.setITunesAudioId(null);
      w.setAudioFeedRSS(null);
      w.setITunesVideoId(852691737);
      w.setScreenFeedRSS(toURL("http://wbe-itunes.berkeley.edu/media/common/courses/spring_2014/rss/architecture_180ac_001_screen.rss"));
    } else if ("2014B7049".equals(key)) {
      w.setYouTubePlaylistId("PL-XXv-cvA_iCwnFP6YVN3eUvxaozN0_Og");
    }
    list.add(w);
  }

  private List<WarehouseCourse> getHardCodedCourses() {
    final WarehouseRoom pimentel1 = new WarehouseRoom(WarehouseBuilding.Pimentel, "1");
    final String[] joelFajans = { "Joel Fajans" };
    final String physics8ADescription = "Introduction to forces, kinetics, equilibria, fluids, waves, and heat. This course presents concepts and methodologies for understanding physical phenomena, and is particularly useful preparation for upper division study in biology and architecture.";
    final AssociatedLicense licenseCC = AssociatedLicense.creative_commons;
    return Collections.list(
            create(2010, Semester.Spring, 69078, "8A", "PHYSICS", pimentel1, "Physics 8A",
                    "Introductory Physics", physics8ADescription, joelFajans, licenseCC, "002",
                    toURL("http://wbe-itunes.berkeley.edu/media/common/rss/physics_8a_002_Spring_2010_Audio__itunes.rss"), 354822624,
                    null, null, null, DayOfWeek.Tuesday, DayOfWeek.Thursday),
            create(2010, Semester.Spring, 69171, "8A", "PHYSICS", pimentel1, "Physics 8A",
                    "Introductory Physics", physics8ADescription, joelFajans, licenseCC, "001",
                    toURL("http://wbe-itunes.berkeley.edu/media/common/rss/physics_8a_001_Spring_2010_Audio__itunes.rss"), 354822572,
                    null, null, null, DayOfWeek.Monday, DayOfWeek.Wednesday, DayOfWeek.Friday)
    );
  }

  private static WarehouseCourse create(final int year, final Semester semester, final int ccn,
          final String catalogId, final String deptName, final WarehouseRoom room, final String name, final String title,
          final String description, final String[] instructors, final AssociatedLicense license,
          final String section, final URL audioRSS, final Integer iTunesAudioId, final URL videoRSS,
          final Integer iTunesVideoId, final String youTubePlaylistId, final DayOfWeek... days) {
    final WarehouseCourse w = new WarehouseCourse(new CanonicalCourse(ccn, name, title), year, semester, catalogId, deptName, section, Collections.list(days), room,
            description, instructors, youTubePlaylistId, new CapturePreferences(), license);
    w.setAudioFeedRSS(audioRSS);
    w.setITunesAudioId(iTunesAudioId);
    w.setITunesVideoId(iTunesVideoId);
    LegacyUtils.setVideoFeedRSSCautiously(w, videoRSS);
    return w;
  }

  /**
   * @return mock data allows us to test department name containing forward slash.
   */
  private WarehouseCourse getMockMalay() {
    final CapturePreferences preferences = new CapturePreferences(RecordingType.videoOnly,
            RecordingAvailability.publicCreativeCommons, 0);
    // The YouTube playlist is real but does not match other aspects of this mock data
    final WarehouseCourse w = new WarehouseCourse(new CanonicalCourse(85006, "Name", "Title"), 2014,
            Semester.Fall,
            "100A", "MALAY/I", "001", Collections.list(DayOfWeek.Monday, DayOfWeek.Wednesday, DayOfWeek.Friday),
            new WarehouseRoom(WarehouseBuilding.Dwinelle, "117"), "Description", new String[] { "John Doe" },
            "PL-XXv-cvA_iDy-rACIy-kh40XJJDp3Y0X", preferences, AssociatedLicense.creative_commons);
    // The following is arbitrary and might not be valid
    w.setITunesVideoId(819827828);
    w.setVideoFeedRSS(toURL(
            "http://wbe-itunes.berkeley.edu/media/common/courses/spring_2014/rss/statistics_131a_001_screen.rss"));
    return w;
  }

  private URL toURL(String url) {
    return UrlSupport.toURL(url, logger);
  }

}
