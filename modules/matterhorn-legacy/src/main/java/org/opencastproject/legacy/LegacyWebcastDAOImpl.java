/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.legacy;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.opencastproject.participation.CourseUtils;
import org.opencastproject.participation.impl.persistence.ProvidesCourseCatalogData;
import org.opencastproject.participation.model.CapturePreferences;
import org.opencastproject.participation.model.CourseData;
import org.opencastproject.participation.model.CourseKey;
import org.opencastproject.participation.model.RecordingAvailability;
import org.opencastproject.participation.model.RecordingType;
import org.opencastproject.participation.model.Room;
import org.opencastproject.participation.model.Semester;
import org.opencastproject.participation.model.Term;
import org.opencastproject.warehouse.AssociatedLicense;
import org.opencastproject.warehouse.WarehouseBuilding;
import org.opencastproject.warehouse.WarehouseCourse;
import org.opencastproject.warehouse.WarehouseRoom;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.Set;

/**
 * @author John Crossman
 */
public class LegacyWebcastDAOImpl implements LegacyWebcastDAO {

  private final Logger logger = LoggerFactory.getLogger(this.getClass());

  private final ProvidesCourseCatalogData courseDatabase;
  private final Map<CourseKey, WarehouseCourse> legacyWebcast;
  private final Map<Integer, ITunesCollection> iTunesUMap;

  public LegacyWebcastDAOImpl(final ProvidesCourseCatalogData courseDatabase, final LegacyFileProvider legacyFileProvider) throws IOException, ParseException {
    this.courseDatabase = courseDatabase;
    final File json = legacyFileProvider.getWebcastBerkeleyLegacyJSON();
    //
    final JSONParser parser = new JSONParser();
    final Object parse = parser.parse(FileUtils.readFileToString(json));
    final JSONArray legacyJSON;
    if (parse instanceof JSONArray) {
      legacyJSON = (JSONArray) parse;
    } else {
      legacyJSON = new JSONArray();
      legacyJSON.add(parse);
    }
    legacyWebcast = parseLegacyWebcastJSON(legacyJSON);
    LegacyUtils.putSpring2014Recordings(legacyWebcast, legacyFileProvider.getSpring2014RecordingsFromMediaPackage(),
            courseDatabase);
    iTunesUMap = legacyFileProvider.getITunesCollectionMap();
    for (final WarehouseCourse w : legacyWebcast.values()) {
      final ITunesCollection audio = iTunesUMap.get(w.getITunesAudioId());
      if (audio != null) {
        w.setAudioFeedRSS(audio.getFeed());
      }
      final ITunesCollection video = iTunesUMap.get(w.getITunesVideoId());
      if (video != null) {
        LegacyUtils.setVideoFeedRSSCautiously(w, video.getFeed());
      }
    }
  }

  @Override
  public Map<CourseKey, WarehouseCourse> getLegacyWebcast() {
    return legacyWebcast;
  }

  @Override
  public Map<Integer, ITunesCollection> getLegacyITunesUMap() {
    return iTunesUMap;
  }

  private Map<CourseKey, WarehouseCourse> parseLegacyWebcastJSON(final JSONArray jsonArray) {
    throw new UnsupportedOperationException("SQL query getCourseDataForLegacyDataMover is no longer supported.");

//    final Map<String, CourseData> catalogDataMap = new HashMap<String, CourseData>();
//    final List<Term> termsQueried = new LinkedList<Term>();
//    for (final Object obj : jsonArray) {
//      final JSONObject json = (JSONObject) obj;
//      final Term term = getTerm(get(json, "semester"));
//      if (term != null) {
//        final Integer year = term.getTermYear();
//        if (!termsQueried.contains(term)) {
//          final Set<CourseData> courseDataSet;
//          final Semester semester = term.getSemester();
//          logger.info(semester.name() + ' ' + year + ": Merging legacy JSON with data from materialized view");
//          courseDataSet = courseDatabase.getCourseDataForLegacyDataMover(semester, year);
//          put(catalogDataMap, courseDataSet);
//          termsQueried.add(term);
//        }
//      }
//    }
//    put(catalogDataMap, courseDatabase.getHardCodedCoursesForWarehouse());
//    final Map<CourseKey, WarehouseCourse> map = new HashMap<CourseKey, WarehouseCourse>();
//    for (final Object obj : jsonArray) {
//      // Iterate through {@link #iTunesUPodcastsJsFile}
//      final JSONObject json = (JSONObject) obj;
//      final String title = getTitle(json);
//      // Example of key: bioengineering200001spring2013
//      final String key = normalize(title);
//      // Match JSON entry with a result from Materialized View query
//      final CourseData c = catalogDataMap.get(key);
//      if (c == null) {
//        logger.error("Entry in JSON file (key = " + key + ") has no apparent match in results from catalog db (ie, materialized view)");
//      } else {
//        final CourseKey courseKey = getCourseKey(c);
//        if (map.containsKey(courseKey)) {
//          logger.error("JSON file contains two separate entries that map to " + courseKey);
//        } else {
//          addWarehouseCourse(map, c, json);
//        }
//      }
//    }
//    System.out.println(map.size() + " out of " + jsonArray.size() + " CCN matches");
//    return map;
  }


  private void put(final Map<String, CourseData> catalogDataMap, final Set<CourseData> courseDataSet) {
    for (final CourseData next : courseDataSet) {
      // Example key1: bioengineering200spring2013
      final String key1 = normalize(next.getDeptDescription() + next.getCatalogId() + next.getSemester() + next.getYear());
      catalogDataMap.put(key1, next);
      // Example of key2 (includes section number): bioengineering200001spring2013
      final String key2 = normalize(next.getDeptDescription() + next.getCatalogId() + next.getSection() + next.getSemester()
              + next.getYear());
      catalogDataMap.put(key2, next);
    }
  }

  private CourseKey getCourseKey(final CourseData c) {
    final Term term = c.getTerm();
    return new CourseKey(term.getTermYear(), term.getSemester(), c.getCanonicalCourse().getCcn());
  }

  private void addWarehouseCourse(final Map<CourseKey, WarehouseCourse> map, final CourseData courseData,
          final JSONObject json) {
    final WarehouseCourse w = getWarehouseCourse(courseData, json);
    addWarehouseCourse(map, w);
  }
  private void addWarehouseCourse(final Map<CourseKey, WarehouseCourse> map, final WarehouseCourse w) {
    final CourseKey courseKey = new CourseKey(w.getYear(), w.getSemester(), w.getCcn());
    if (map.containsKey(courseKey)) {
      final WarehouseCourse course = map.get(courseKey);
      combine(course, w);
    }
    map.put(courseKey, w);
  }

  private void combine(final WarehouseCourse c1, final WarehouseCourse c2) {
    if (!c1.equals(c2)) {
      throw new IllegalStateException("CourseKeys are not equal: " + c1 + " != " + c2);
    }
    final String description = c2.getDescription();
    if (StringUtils.length(description) > StringUtils.length(c1.getDescription())) {
      c1.setDescription(description);
    }
    if (c1.getAudioFeedRSS() == null) {
      c1.setAudioFeedRSS(c2.getAudioFeedRSS());
    }
    if (c1.getVideoFeedRSS() == null) {
      c1.setVideoFeedRSS(c2.getVideoFeedRSS());
    }
    if (c1.getScreenFeedRSS() == null) {
      c1.setScreenFeedRSS(c2.getScreenFeedRSS());
    }
    if (c1.getITunesAudioId() == null) {
      c1.setITunesAudioId(c2.getITunesAudioId());
    }
    if (c1.getITunesVideoId() == null) {
      c1.setITunesVideoId(c2.getITunesVideoId());
    }
    if (c1.getYouTubePlaylistId() == null) {
      c1.setYouTubePlaylistId(c2.getYouTubePlaylistId());
    }
  }

  private String normalize(final String str) {
    final String normalized = StringUtils.trimToEmpty(str).replaceAll("[^a-zA-Z0-9]", StringUtils.EMPTY).toLowerCase();
    return normalized.replace("amp", StringUtils.EMPTY).replace("lab", StringUtils.EMPTY);
  }


  private WarehouseCourse getWarehouseCourse(final CourseData c, final JSONObject json) {
    final String youTubePlaylistId = get(json, "youTube");
    final String descr = get(json, "descr");
    final String[] lecturers = getLecturers(json, c);
    final Integer audioId = LegacyUtils.toWhole(get(json, "audioId"), null);
    final Integer videoId = LegacyUtils.toWhole(get(json, "videoId"), null);
    final RecordingAvailability availability = (youTubePlaylistId == null) ? RecordingAvailability.studentsOnly : RecordingAvailability.publicCreativeCommons;
    final RecordingType recordingType = (videoId == null) ? RecordingType.audioOnly : RecordingType.screencast;
    final CapturePreferences preferences = new CapturePreferences(recordingType, availability, 0);
    final WarehouseRoom room = getWarehouseRoom(c.getRoom());
    final WarehouseCourse w = new WarehouseCourse(c.getCanonicalCourse(), c.getTerm().getTermYear(),
            c.getTerm().getSemester(), c.getCatalogId(), c.getDeptName(), c.getSection(), c.getMeetingDays(), room,
            descr, lecturers, youTubePlaylistId, preferences, AssociatedLicense.creative_commons);
    w.setITunesAudioId(audioId);
    w.setITunesVideoId(videoId);
    return w;
  }

  private static WarehouseRoom getWarehouseRoom(final Room room) {
    final String normalized = StringUtils.trimToEmpty(room.getBuilding()).replaceAll("[^a-zA-Z0-9]", StringUtils.EMPTY).toLowerCase();
    WarehouseBuilding building = null;
    for (final WarehouseBuilding next : WarehouseBuilding.values()) {
      if (next.name().toLowerCase().equals(normalized)) {
        building = next;
        break;
      }
    }
    return (building == null) ? null : new WarehouseRoom(building, room.getRoomNumber());
  }

  private String[] getLecturers(final JSONObject json, final CourseData courseData) {
    final String lecturer = get(json, "lecturer");
    return StringUtils.isBlank(lecturer)
            ? CourseUtils.getLecturers(courseData)
            : StringUtils.split(lecturer, ',');
  }

  private static Term getTerm(final String value) {
    final Semester semester = LegacyUtils.getSemester(StringUtils.substringBefore(value, " "));
    final Integer year = LegacyUtils.getYear(StringUtils.substringAfter(value, " "));
    return (semester == null || year == null) ? null : new Term(semester, year);
  }

  private String getTitle(final JSONObject json) {
    final String title = get(json, "title");
    return StringUtils.replace(title, "Fal ", "Fall ");
  }

  private String get(final JSONObject json, final Object key) {
    final Object obj = json == null ? null : json.get(key);
    return (obj == null) ? StringUtils.EMPTY : StringUtils.trimToNull(obj.toString());
  }

}
