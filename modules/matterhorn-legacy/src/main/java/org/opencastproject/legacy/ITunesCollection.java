/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.legacy;

import org.opencastproject.participation.model.Semester;

import java.net.URL;

/**
 * @author John Crossman
 */
public class ITunesCollection {

  private final Integer year;
  private final Semester semester;
  private final Integer ccn;
  private final String title;
  private final String contributors;
  private final String category;
  private final String mediaFormat;
  private final URL feed;
  private final Integer iTunesId;

  public ITunesCollection(final Integer year, final Semester semester, final Integer ccn, final String title,
          final String contributors, final String category, final String mediaFormat, final URL feed,
          final Integer iTunesId) {
    this.year = year;
    this.semester = semester;
    this.ccn = ccn;
    this.title = title;
    this.contributors = contributors;
    this.category = category;
    this.mediaFormat = mediaFormat;
    this.feed = feed;
    this.iTunesId = iTunesId;
  }

  public Integer getYear() {
    return year;
  }

  public Semester getSemester() {
    return semester;
  }

  public Integer getCcn() {
    return ccn;
  }

  public String getTitle() {
    return title;
  }

  public String getContributors() {
    return contributors;
  }

  public String getCategory() {
    return category;
  }

  public String getMediaFormat() {
    return mediaFormat;
  }

  public URL getFeed() {
    return feed;
  }

  public Integer getITunesId() {
    return iTunesId;
  }
}
