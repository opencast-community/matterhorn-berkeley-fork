/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.legacy;

import com.google.api.services.youtube.model.Playlist;
import com.google.api.services.youtube.model.Video;
import org.opencastproject.google.youtube.GoogleUtils;
import org.opencastproject.participation.model.CourseKey;
import org.opencastproject.google.youtube.YouTubeAPIVersion3Service;
import org.opencastproject.google.youtube.YouTubeUtils;
import org.opencastproject.warehouse.Recording;
import org.opencastproject.warehouse.WarehouseCourse;
import org.opencastproject.warehouse.WarehouseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Map;

/**
 * @author John Crossman
 */
public final class LegacyWebcastDataImporter implements Runnable {

  private final Logger logger = LoggerFactory.getLogger(this.getClass());
  private final WarehouseService warehouseService;
  private final LegacyWebcastDAO legacyWebcastDAO;
  private final YouTubeAPIVersion3Service youTubeService;

  public LegacyWebcastDataImporter(final LegacyWebcastDAO legacyWebcastDAO) throws IOException {
    this.legacyWebcastDAO = legacyWebcastDAO;
    this.youTubeService = YouTubeUtils.getYouTubeAPIVersion3Service();
    this.warehouseService = null; //WarehouseUtils.getWarehouseService();
    throw new UnsupportedOperationException("WarehouseService needs to be initialized here");
  }

  @Override
  public void run() {
    final Map<CourseKey, WarehouseCourse> map = legacyWebcastDAO.getLegacyWebcast();
    final Map<Integer, ITunesCollection> iTunesUMap = legacyWebcastDAO.getLegacyITunesUMap();
    for (final CourseKey courseKey : map.keySet()) {
      final WarehouseCourse course = map.get(courseKey);
      course.setAudioFeedRSS(getFeedURL(iTunesUMap, course.getITunesAudioId()));
      final URL feedURL = getFeedURL(iTunesUMap, course.getITunesVideoId());
      LegacyUtils.setVideoFeedRSSCautiously(course, feedURL);
    }
    final LastMinuteLegacyDataFixer fixer = new LastMinuteLegacyDataFixer(map.values());
    final List<WarehouseCourse> fixedCourses = fixer.getFixed();
    for (final WarehouseCourse w : fixedCourses) {
      verifyYouTubeVideoIds(w);
      warehouseService.upsertCourse(w);
      String name = w.getCanonicalCourse().getName();
      name = (name == null) ? w.getCanonicalCourse().getTitle() : name;
      logger.warn("Saved: " + w.getYear() + w.getSemester().getTermCode() + w.getCcn() + " - " + name);
    }
  }

  /**
   * We do not check YouTube video status. If the video has private status then that could change - put it in the
   * database. But if the videoId returns null then set the property to null on the course or recording instance.
   * @param w null not allowed.
   */
  private void verifyYouTubeVideoIds(final WarehouseCourse w) {
      try {
        final String playlistId = w.getYouTubePlaylistId();
        if (playlistId != null) {
          final Playlist playlist = youTubeService.getPlaylistById(playlistId);
          w.setYouTubePlaylistId(playlist == null ? null : playlist.getId());
        }
        for (final Recording next : w.getRecordings()) {
          final String videoId = next.getYouTubeVideoId();
          if (videoId != null) {
            final Video video = youTubeService.getVideoById(videoId);
            next.setYouTubeVideoId(video == null ? null : video.getId());
            if (next.getTrimmedLengthSeconds() == null && video != null) {
              next.setTrimmedLengthSeconds(GoogleUtils.getDurationSeconds(video));
            }
          }
        }
      } catch (final IOException e) {
        throw new RuntimeException("Failed to query YouTube with playlist or video id in " + w, e);
      }
  }

  private URL getFeedURL(final Map<Integer, ITunesCollection> iTunesUMap, final Integer key) {
    return iTunesUMap.containsKey(key) ? iTunesUMap.get(key).getFeed() : null;
  }

//  public static void main(final String[] args) throws IOException, ConfigurationException, ParseException {
//    final WarehouseService warehouseService = LegacyUtils.getWarehouseService();
//    final ProvidesCourseCatalogData courseDatabase = new UCBerkeleyCourseDatabaseImpl(LegacyUtils.getCourseDatabaseCredentials());
//    final LegacyWebcastDAOImpl legacyWebcastDAO = new LegacyWebcastDAOImpl(courseDatabase, new LegacyFileProviderImpl());
//    final YouTubeServiceDelegate youTubeServiceDelegate = new YouTubeServiceDelegate();
//    new LegacyWebcastDataImporter(warehouseService, legacyWebcastDAO, youTubeServiceDelegate).run();
//  }

}
