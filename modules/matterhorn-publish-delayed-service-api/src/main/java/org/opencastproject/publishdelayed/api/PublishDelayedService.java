/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */

package org.opencastproject.publishdelayed.api;

import org.opencastproject.workflow.api.WorkflowInstance;

import java.util.Date;
import java.util.List;

/**
 * Publish Delayed service.
 *
 */
public interface PublishDelayedService {
  
  /**
   * Returns a {@code List} of {@link WorkflowInstance} that are publish-delayed.
   *  
   * @return workflows
   */
  List<WorkflowInstance> listPublishDelayedWorkflows();
  
  /**
   * Counts the workflows that are publish-delayed.
   *  
   * @return count
   */
  int countPublishDelayedWorkflows();
  
  /**
   * Resumes processing of those workflows whose publish date is before 
   * a given date.
   *  
   * @param workFlows
   * @param date  if null, the current date is assumed
   * @return count of the workflows that were resumed
   */
  int resumePublishDelayedWorkflows(List<WorkflowInstance> workFlows, Date date);

}
