/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.impl.persistence;

import org.apache.commons.lang.ClassUtils;
import org.opencastproject.participation.CourseUtils;
import org.opencastproject.participation.impl.Column;
import org.opencastproject.participation.model.Term;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Array;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

/**
 * @author John Crossman
 */
public abstract class AbstractResultSetRowConverter<T> implements ResultSetRowConverter<T> {

  private final Logger logger = LoggerFactory.getLogger(this.getClass());

  protected final Term term;
  final boolean edoDatabase;

  protected AbstractResultSetRowConverter() {
    this.term = Term.getTermFromProperties();
    String dbType = CourseUtils.getCourseManagementServiceProperties().getProperty("db.courseCatalog.type");
    this.edoDatabase = dbType.equals("edo_db");
  }

  /**
   * @param resultSet never null
   * @return instance populated with relevant column data of resultSet
   * @throws SQLException
   */
  public abstract T convert(ResultSet resultSet) throws SQLException;

  /**
   * @param r one row of results from database query
   * @param column column we are looking for
   * @param <T> data type of column
   * @return value, possibly null
   * @throws SQLException
   */
  protected <T> T get(final ResultSet r, final Column<T> column) throws SQLException {
    final String columnName = column.name();
    try {
      final T result;
      if (isAssignable(column, String.class)) {
        result = (T) r.getString(columnName);
      } else if (isAssignable(column, Integer.class)) {
        result = (T) new Integer(r.getInt(columnName));
      } else if (isAssignable(column, Date.class)) {
        final Timestamp timestamp = r.getTimestamp(columnName);
        result = timestamp == null ? null : (T) new Date(timestamp.getTime());
      } else if (isAssignable(column, Array.class)) {
        result = (T) r.getArray(columnName);
      } else if (isAssignable(column, Object.class)) {
        result = (T) r.getArray(columnName);
      } else {
        throw new UnsupportedOperationException("Unexpected column type: " + column.getType());
      }
      return result;
    } catch (final SQLException e) {
      logger.error("FAILURE on column '" + columnName + "' where type = " + column.getType().getSimpleName(), e);
      throw e;
    }
  }

  private boolean isAssignable(final Column column, final Class clazz) {
    return ClassUtils.isAssignable(column.getType(), clazz);
  }

}
