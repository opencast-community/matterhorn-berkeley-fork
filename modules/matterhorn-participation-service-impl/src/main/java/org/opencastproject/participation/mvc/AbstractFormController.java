/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.mvc;

import org.opencastproject.notify.Notifier;
import org.springframework.core.Conventions;
import org.springframework.http.HttpMethod;
import org.springframework.validation.BeanPropertyBindingResult;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author John Crossman
 */
public abstract class AbstractFormController<T> extends HttpServlet {

  protected Notifier notifier;
  protected final String formView;
  protected final String finishView;

  protected AbstractFormController(final String formView, final String finishView) {
    this.formView = formView;
    this.finishView = finishView;
  }

  protected abstract void doProcess(HttpServletRequest request, HttpServletResponse response, HttpMethod httpMethod) throws IOException;

  protected abstract T bind(HttpServletRequest request);

  private void handle(final HttpServletRequest request, final HttpServletResponse response, final HttpMethod method) throws IOException {
    try {
      doProcess(request, response, method);
    } catch (final Exception e) {
      notifier.notifyEngineeringTeam("Error when handling " + request.getRequestURI(), e);
      final ModelAndView mav = new ModelAndView(finishView);
      final T model = bind(request);
      final BeanPropertyBindingResult errors = new BeanPropertyBindingResult(model, Conventions.getVariableName(model));
      errors.reject(Key.unexpectedSystemError.name());
      mav.put(Key.errors, errors);
      mav.render(response);
    }
  }

  @Override
  protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
    handle(request, response, HttpMethod.GET);
  }

  @Override
  protected void doPost(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
    handle(request, response, HttpMethod.POST);
  }

  public void setNotifier(final Notifier notifier) {
    this.notifier = notifier;
  }

}
