/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.impl.persistence;

import org.opencastproject.participation.model.Instructor;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author John Crossman
 */
class ResultConverterForInstructor extends AbstractResultSetRowConverter<Instructor> {

  @Override
  public Instructor convert(final ResultSet r) throws SQLException {
    final Instructor instructor = new Instructor();
    instructor.setCalNetUID(get(r, ColStr.ldap_uid));
    instructor.setFirstName(get(r, ColStr.first_name));
    instructor.setLastName(get(r, ColStr.last_name));
    instructor.setEmail(get(r, ColStr.email_address));
    instructor.setDepartment(get(r, ColStr.dept_description));
    return instructor;
  }
}
