/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.lang.time.FastDateFormat;

import org.opencastproject.participation.impl.persistence.*;
import org.opencastproject.participation.model.CourseData;
import org.opencastproject.participation.model.CourseKey;
import org.opencastproject.participation.model.DayOfWeek;
import org.opencastproject.participation.model.HasCourseKey;
import org.opencastproject.participation.model.Instructor;
import org.opencastproject.participation.model.Participation;
import org.opencastproject.participation.model.Room;
import org.opencastproject.participation.model.Semester;
import org.opencastproject.participation.model.Term;
import org.opencastproject.salesforce.SalesforceUtils;
import org.opencastproject.scheduler.api.SchedulerException;
import org.opencastproject.security.api.User;
import org.opencastproject.security.util.SecurityUtil;
import org.opencastproject.util.env.EnvironmentUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author John King
 */
public final class CourseUtils {

  /**
   * Private.
   */
  private CourseUtils() {
  }

  /**
   * @see org.opencastproject.participation.model.CourseData#getParticipationSet()
   * @param courseDataSet never null
   * @return zero or more instructors extracted from list of courses
   */
  public static Set<Instructor> extractInstructorSet(final Set<CourseData> courseDataSet) {
    final Set<Instructor> instructorSet = new HashSet<Instructor>();
    for (final CourseData courseData : courseDataSet) {
      instructorSet.addAll(extractInstructorSet(courseData));
    }
    return instructorSet;
  }

  /**
   * @see org.opencastproject.participation.model.CourseData#getParticipationSet()
   * @param courseData never null
   * @return zero or more instructors extracted from list of courses
   */
  private static Set<Instructor> extractInstructorSet(final CourseData courseData) {
    final Set<Instructor> instructorSet = new HashSet<Instructor>();
    if (courseData.getParticipationSet() != null) {
      for (final Participation participation : courseData.getParticipationSet()) {
        final Instructor instructor = participation.getInstructor();
        if (instructor == null) {
          throw new IllegalStateException("Null instructor on participation of courseData: " + courseData);
        }
        instructorSet.add(instructor);
      }
    }
    return instructorSet;
  }

  /**
   * @param room Null allowed
   * @return true if room and {@link org.opencastproject.participation.model.Room#getCapability()} are not null
   */
  public static boolean isRoomCapable(final Room room) {
    return room != null && room.getCapability() != null;
  }

  /**
   * @see org.opencastproject.salesforce.HasSalesforceId#getSalesforceID()
   * @param courseDataSet Courses with presumably null salesforceID
   * @param allRooms list pulled from Salesforce. All entries must have non-null salesforceID
   * @param instructorList list pulled from Salesforce. All entries must have non-null salesforceID
   * @param term must have non-null salesforceID
   */
  public static void setSalesforceIDs(final Set<CourseData> courseDataSet, final List<Room> allRooms,
                                      final List<Instructor> instructorList, final Term term) {
    final StringBuilder b = new StringBuilder();
    for (final CourseData course : courseDataSet) {
      course.getTerm().setSalesforceID(term.getSalesforceID());
      final int indexOfMatch = allRooms.indexOf(course.getRoom());
      if (indexOfMatch > -1) {
        final Room roomFromSalesforce = allRooms.get(indexOfMatch);
        course.getRoom().setSalesforceID(roomFromSalesforce.getSalesforceID());
      }
      final Set<Participation> participationSet = course.getParticipationSet();
      for (final Participation p : participationSet) {
        final Instructor incoming = p.getInstructor();
        final int indexOf = instructorList.indexOf(incoming);
        final Instructor fromSalesforce = instructorList.get(indexOf);
        incoming.setSalesforceID(fromSalesforce.getSalesforceID());
      }
      if (course.getTerm().getSalesforceID() == null) {
        appendCourseRelatedErrorMessage(b, course, " term");
      }
      if (course.getRoom().getSalesforceID() == null) {
        appendCourseRelatedErrorMessage(b, course, " room " + course.getRoom());
      }
      for (final Participation p : course.getParticipationSet()) {
        if (p.getInstructor().getSalesforceID() == null) {
          appendCourseRelatedErrorMessage(b, course, " instructor " + p.getInstructor());
        }
      }
    }
    if (b.length() > 0) {
      throw new IllegalStateException(StringUtils.abbreviate(b.toString(), 200));
    }
  }

  /**
   * @param courseDataSet Null not allowed
   * @param knownRooms Null not allowed
   * @return courses with room objects that have a match within list of known rooms
   */
  public static Set<CourseData> removeCoursesWithUnrecognizedRooms(final Set<CourseData> courseDataSet, final List<Room> knownRooms) {
    final Set<CourseData> validCourseDataSet = new HashSet<CourseData>();
    for (final CourseData courseData : courseDataSet) {
      if (knownRooms.contains(courseData.getRoom())) {
        validCourseDataSet.add(courseData);
      }
    }
    return validCourseDataSet;
  }

  /**
   * @param room
   *          Null not allowed
   * @return all lowercase, spaces replaced by dashes.
   */
  public static String getCaptureAgentName(final Room room) {
    final String buildingName = StringUtils.replaceChars(room.getBuilding(), ' ', '-');
    String roomNumber = StringUtils.trimToNull(room.getRoomNumber());
    final String agentName = roomNumber == null ? buildingName : buildingName + '-' + roomNumber;
    return agentName == null ? null : agentName.toLowerCase();
  }

  private static void appendCourseRelatedErrorMessage(final StringBuilder b, final CourseData course, final String messageSuffix) {
    b.append(course.getCanonicalCourse().getCcn()).append(" missing salesforceID on ").append(messageSuffix);
  }

  public static boolean thisUserCanScheduleRecordings(final Set<Participation> participationSet, final User user) {
    return SecurityUtil.isAdminUser(user) || isLastUndecidedUser(participationSet, user);
  }

  public static boolean isLastUndecidedUser(final Set<Participation> participationSet, final User user) {
    boolean otherUnapproved = false;
    for (final Participation p : participationSet) {
      final boolean sameUser = p.getInstructor().getCalNetUID().equals(user.getUserName());
      if (!sameUser && !p.isApproved()) {
        otherUnapproved = true;
        break;
      }
    }
    return !otherUnapproved;
  }

  /**
   * @param set never null
   * @param user logged in
   * @return Logged in user, if present in the set, comes first in resulting list. Other users are ordered by last name.
   */
  public static List<Participation> sortParticipants(final Set<Participation> set, final User user) {
    final LinkedList<Participation> list = new LinkedList<Participation>(set);
    Collections.sort(list, new ParticipationComparator(user));
    return list;
  }

  /**
   * Returns the number of hours (Daylight Savings Time-adjusted) between a
   * timezone and GMT for a given date.
   *
   * @param date usually the current date
   * @param tz usually the same as the local timezone
   * @return standard time plus offset
   */
  static int getHoursOffsetFromGMT(Date date, TimeZone tz) {
    final int millisecondsPerHour = 60 * 60 * 1000;
    final int localStandardTime = tz.getRawOffset() / millisecondsPerHour;
    if (tz.inDaylightTime(date)) {
      final int addDSTSavings = tz.getDSTSavings() / millisecondsPerHour;
      return localStandardTime + addDSTSavings;
    } else {
      return localStandardTime;
    }
  }

  public static String getRecurrenceRule(final Date date, final TimeZone tz, final List<DayOfWeek> days, final int startTimeMilitaryPST) {
    final int hoursPacificUS = startTimeMilitaryPST / 100;
    final int hoursGMT = (hoursPacificUS - getHoursOffsetFromGMT(date, tz)) % 24;
    final boolean rollover = hoursPacificUS - getHoursOffsetFromGMT(date, tz) >= 24;
    final int minutes = (startTimeMilitaryPST % 100);
    final StringBuilder byDay = new StringBuilder();
    for (int i = 0; i < days.size(); i++) {
      if (i > 0) {
        byDay.append(',');
      }
      // If the course start time is after 1600 PST (or 1700 PDT) then we set the recurrence days to + 1 day
      // so that GMT hours can be used when scheduling
      if (rollover) {
        byDay.append(days.get(i).getNextDayOfWeek().name().substring(0, 2).toUpperCase());
      } else {
        byDay.append(days.get(i).name().substring(0, 2).toUpperCase());
      }
    }
    return "FREQ=WEEKLY;BYDAY=" + byDay.toString() + ";BYHOUR=" + hoursGMT + ";BYMINUTE=" + minutes;
  }

  public static String getTemporalPropertyTemplate(final Term term, int startTimeMilitaryPST, int endTimeMilitaryPST) throws SchedulerException {
    return "start=" + offsetAndFormatISO(term.getStartDate(), startTimeMilitaryPST)
        + "; end=" + offsetAndFormatISO(term.getEndDate(), endTimeMilitaryPST) + "; scheme=W3C-DTF;";
  }

  /**
   * @param seriesId expected format is 2014B5915 which contains year, semester code and CCN.
   * @return parsed input is populated into strongly typed bean.
   */
  public static CourseKey getCourseKey(final String seriesId) {
    final String trimmed = StringUtils.trimToNull(seriesId);
    final CourseKey courseKey;
    if (trimmed != null && trimmed.length() >= 6) {
      final int year = NumberUtils.toInt(trimmed.substring(0, 4), 0);
      final Semester semester = getSemesterByCode(trimmed.charAt(4));
      final int ccn = NumberUtils.toInt(trimmed.substring(5), 0);
      final boolean validData = year > 0 && semester != null && ccn > 0;
      courseKey = validData ? new CourseKey(year, semester, ccn) : null;
    } else {
      courseKey = null;
    }
    return courseKey;
  }

  public static Date dateFromSalesforceFormat(final String dateFromSalesforce) throws SchedulerException {
    Date date;
    try {
      date = new SimpleDateFormat(SalesforceUtils.SALESFORCE_DATE_FORMAT).parse(dateFromSalesforce);
    } catch (ParseException e) {
      throw new SchedulerException(e);
    }
    return date;
  }

  public static String[] getLecturers(final CourseData courseData) {
    final Set<Instructor> set = CourseUtils.extractInstructorSet(courseData);
    final List<String> list = new LinkedList<String>();
    for (final Instructor next : set) {
      final String name = StringUtils.trimToEmpty(next.getFirstName()) + ' '
              + StringUtils.trimToEmpty(next.getLastName());
      list.add(name);
    }
    return list.toArray(new String[list.size()]);
  }

  public static String getTermId(final int termYear, final char termCode) {
    final String year = Integer.toString(termYear);
    return "" + year.charAt(0) + year.charAt(2) + year.charAt(3) + legacyToEdoCode(termCode);
  }

  private static char legacyToEdoCode(final char termCode) {
    switch (termCode) {
      case 'B':
        return '2';
      case 'C':
        return '5';
      case 'D':
        return '8';
      default:
        throw new UnsupportedOperationException("Unrecognized term code: " + termCode);
    }
  }

  private static Semester getSemesterByCode(final char semesterCode) {
    for (final Semester semester : Semester.values()) {
      if (semester.getTermCode() == semesterCode) {
        return semester;
      }
    }
    return null;
  }

  private static String offsetAndFormatISO(final String dateFromSalesforce, final int militaryTimePST) throws SchedulerException {
    try {
      final String hoursMinutes = (militaryTimePST > 999 ? StringUtils.EMPTY : "0") + militaryTimePST;
      final Date datePST = new SimpleDateFormat(SalesforceUtils.SALESFORCE_DATE_FORMAT + " HHmm").parse(dateFromSalesforce + ' ' + hoursMinutes);
      final TimeZone tz = TimeZone.getTimeZone("US/Pacific");
      final Date datePlusOffset = DateUtils.addHours(datePST, -getHoursOffsetFromGMT(datePST, tz));
      return FastDateFormat.getInstance("yyyy-MM-dd'T'HH:mm:ss'Z'").format(datePlusOffset);
    } catch (ParseException e) {
      throw new SchedulerException("Failed to parse and apply timezone offset to " + dateFromSalesforce, e);
    }
  }

  public static String getCourseOfferingId(final HasCourseKey courseKey) {
    return StringUtils.EMPTY + courseKey.getYear() + courseKey.getSemester().getTermCode() + courseKey.getCcn();
  }

  public static String getSeriesId(final HasCourseKey courseKey) {
    return StringUtils.EMPTY + courseKey.getYear() + courseKey.getSemester().getTermCode() + courseKey.getCcn();
  }

  public static DatabaseCredentials getCatalogCredentials(final Properties properties) {
    return getDatabaseCredentials("db.courseCatalog.", properties, WebcastDatabase.course_catalog);
  }

  public static DatabaseCredentials getDatabaseCredentials(final String prefix, final Properties properties, final WebcastDatabase webcastDatabase) {
    final String hostname = properties.getProperty(prefix + "hostname");
    final int port = NumberUtils.toInt(properties.getProperty(prefix + "port"), -1);
    final String databaseName = properties.getProperty(prefix + "databaseName");
    final String username = properties.getProperty(prefix + "username");
    final String password = properties.getProperty(prefix + "password");
    return new DatabaseCredentials(hostname, port, databaseName, username, password, webcastDatabase);
  }

  public static int hashCode(final HasCourseKey courseKey) {
    int result = courseKey.getYear();
    result = 31 * result + (courseKey.getSemester() != null ? courseKey.getSemester().hashCode() : 0);
    result = 31 * result + courseKey.getCcn();
    return result;
  }

  public static boolean equals(final HasCourseKey courseKey, final Object o) {
    final boolean equals;
    if (o instanceof HasCourseKey) {
      final HasCourseKey that = (HasCourseKey) o;
      equals = courseKey.getYear() == that.getYear() && courseKey.getSemester() == that.getSemester() && courseKey.getCcn() == that.getCcn();
    } else {
      equals = false;
    }
    return equals;
  }

  public static DatabaseCredentials getCourseDatabaseCredentials() throws IOException {
    return CourseUtils.getCatalogCredentials(getCourseManagementServiceProperties());
  }

  public static Properties getCourseManagementServiceProperties() {
    FileInputStream inputStream = null;
    try {
      final File file = EnvironmentUtil.getFileUnderFelixHome("etc/services/org.opencastproject.participation.impl.CourseManagementServiceImpl.properties");
      final Properties p2 = new Properties();
      inputStream = new FileInputStream(file);
      p2.load(inputStream);
      return EnvironmentUtil.createEProperties(p2, true);
    } catch (IOException e) {
      throw new RuntimeException(e);
    } finally {
      IOUtils.closeQuietly(inputStream);
    }
  }

  private static final class ParticipationComparator implements Comparator<Participation> {

    private final String loggedInUser;

    ParticipationComparator(final User loggedInUser) {
      this.loggedInUser = loggedInUser.getUserName();
    }

    @Override
    public int compare(final Participation p1, final Participation p2) {
      final int result;
      if (loggedInUser.equals(p1.getInstructor().getCalNetUID())) {
        result = -1;
      } else if (loggedInUser.equals(p2.getInstructor().getCalNetUID())) {
        result = 1;
      } else {
        result = p1.getInstructor().getLastName().compareTo(p2.getInstructor().getLastName());
      }
      return result;
    }
  }

}
