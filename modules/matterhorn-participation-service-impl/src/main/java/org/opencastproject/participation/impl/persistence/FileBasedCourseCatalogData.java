/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.impl.persistence;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import org.apache.commons.lang.StringUtils;
import org.opencastproject.participation.model.Booking;
import org.opencastproject.participation.model.CourseData;
import org.opencastproject.participation.model.CourseKey;
import org.opencastproject.participation.model.Instructor;
import org.opencastproject.participation.model.Participation;
import org.opencastproject.participation.model.Term;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * @author John Crossman
 */
public class FileBasedCourseCatalogData implements ProvidesCourseCatalogData {

  private final Logger logger = LoggerFactory.getLogger(this.getClass());

  private final String dataDirectory;

  private final Cache<File, Long> fileLastModifiedMap;

  /**
   * @param dataDirectory base directory expected to have 'courses' and 'instructors' sub-directories.
   */
  public FileBasedCourseCatalogData(final String dataDirectory) {
    logger.info("dataDirectory = " + dataDirectory);
    this.dataDirectory = dataDirectory;
    fileLastModifiedMap = CacheBuilder.newBuilder()
        .concurrencyLevel(4)
        .initialCapacity(3)
        .expireAfterAccess(1, TimeUnit.DAYS).build();
  }

  @Override
  public Set<CourseData> getCourseData() {
    try {
      final Set<CourseData> dataSet = new HashSet<CourseData>();
      final List<Properties> courseDataFiles = getCourseDataFiles();
      if (courseDataFiles.size() > 0) {
        final ResultSetRowConverter<CourseData> converter = new ResultConverterForCourse(true);
        for (final Properties properties : courseDataFiles) {
          final CourseData courseData = converter.convert(new ResultSetProperties(properties));
          final Term term = courseData.getTerm();
          if (term != null) {
            final String csv = properties.getProperty("instructor_calnet_id_list");
            final List<String> calNetUIDList = split(csv);
            final Set<Participation> participationSet = new HashSet<Participation>();
            for (final String calNetUID : calNetUIDList) {
              participationSet.add(loadInstructorData(calNetUID));
            }
            courseData.setParticipationSet(participationSet);
            dataSet.add(courseData);
            logger.info("Course added to resultSet: " + courseData);
          }
        }
      }
      // Reconcile cross-listed courses before returning
      return Booking.reconcileCrossListedCourses(dataSet);
    } catch (final Exception e) {
      throw new CourseDatabaseException("Error loading certain XML property files in the directory " + dataDirectory, e);
    }
  }

  @Override
  public CourseData getCourse(final CourseKey courseKey) {
    final Set<CourseData> set = getCourseData();
    for (final CourseData next : set) {
      if (next.getCcn() == courseKey.getCcn()) {
        return next;
      }
    }
    return null;
  }

  private List<String> split(final String csv) {
    final List<String> list = new LinkedList<String>();
    final String[] split = StringUtils.split(csv, ',');
    for (final String next : split) {
      final String value = StringUtils.trimToNull(next);
      if (value != null) {
        list.add(value);
      }
    }
    return list;
  }

  private Participation loadInstructorData(final String calNetUID) throws IOException, SQLException {
    if (calNetUID == null) {
      throw new IllegalArgumentException("calNetUID is NULL");
    }
    final ResultSetRowConverter<Instructor> converter = new ResultConverterForInstructor();
    final File file = new File(dataDirectory, "instructors/" + calNetUID + ".xml");
    final Properties properties = loadProperties(file);
    final Instructor instructor = converter.convert(new ResultSetProperties(properties));
    instructor.setCalNetUID(calNetUID);
    logger.info("Instructor data properties loaded from XML file: " + file.getAbsolutePath());
    return new Participation(null, instructor, false);
  }

  private List<Properties> getCourseDataFiles() throws IOException {
    final List<Properties> list = new LinkedList<Properties>();
    final File directory = new File(dataDirectory, "courses");
    final File[] xmlFiles = directory.listFiles(new CanReadFileFilterXML());
    if (xmlFiles != null) {
      for (final File xmlFile : xmlFiles) {
        logger.info("Course data properties loaded from XML file: " + xmlFile.getAbsolutePath());
        final Long previousLastModified = fileLastModifiedMap.getIfPresent(xmlFile);
        final long lastModified = xmlFile.lastModified();
        final boolean loadProperties = (previousLastModified == null) || previousLastModified < lastModified;
        if (loadProperties) {
          list.add(loadProperties(xmlFile));
        } else {
          logger.info("Ignoring " + xmlFile.getAbsolutePath() + " because file has not been modified since " + lastModified);
        }
        fileLastModifiedMap.put(xmlFile, lastModified);
      }
    }
    return list;
  }

  private Properties loadProperties(final File file) throws IOException {
    final Properties p = new Properties();
    p.loadFromXML(new FileInputStream(file));
    return p;
  }

  private final class CanReadFileFilterXML implements FilenameFilter {
    @Override
    public boolean accept(final File file, final String name) {
      return StringUtils.endsWithIgnoreCase(name, "xml") && file.canRead();
    }
  }

}
