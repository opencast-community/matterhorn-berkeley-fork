/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.model;

import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public final class CanonicalCourse {

  private final int ccn;
  private final String name;
  private final String title;

  /**
   * @param ccn Course control number
   * @param name for example, "Economics C3, 001"
   * @param title for example, "Introduction to Environmental Economics and Policy"
   */
  public CanonicalCourse(final int ccn, final String name, final String title) {
    this.ccn = ccn;
    this.name = name;
    this.title = title;
  }

  /**
   * @return course control number
   */
  public int getCcn() {
    return ccn;
  }

  /**
   * @return for example, "Economics C3, 001"
   */
  public String getName() {
    return name;
  }

  /**
   * @return for example, "Introduction to Environmental Economics and Policy"
   */
  public String getTitle() {
    return title;
  }

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this);
  }

  @Override
  public boolean equals(final Object o) {
    return (o instanceof CanonicalCourse) && ccn == ((CanonicalCourse) o).getCcn();
  }

  @Override
  public int hashCode() {
    return HashCodeBuilder.reflectionHashCode(this);
  }
}
