/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.impl.persistence;

import org.opencastproject.participation.model.Booking;
import org.opencastproject.participation.model.CourseData;
import org.opencastproject.participation.model.CourseKey;
import org.opencastproject.participation.model.Semester;

import java.util.Set;

public interface CourseCatalogService {

  /**
   * Return all courses scheduled in specified semester and year.
   * @param semester never null
   * @param year year of current semester or greater
   * @return all course offerings listed in current UC Berkeley catalog.
   * @throws CourseDatabaseException if there is a problem communicating with the underlying data store
   */
  Set<CourseData> getCourseData(Semester semester, int year);

  /**
   * Return specific course
   * @param courseKey never null
   * @return all course offerings listed in current UC Berkeley catalog.
   */
  CourseData getCourse(CourseKey courseKey);

  /**
   * Returns one {@code Booking} containing the reference course and cross-listed courses.
   * 
   * @param referenceCourseId  series or course offering identifier in yearSemesterCCNs format, e.g. {@literal 2014D12345}
   * @return  null if the series is not cross-listed
   * @throws CourseDatabaseException if there is a problem communicating with the underlying data store
   */
  Booking getCrossListedCourseSet(String referenceCourseId);

}
