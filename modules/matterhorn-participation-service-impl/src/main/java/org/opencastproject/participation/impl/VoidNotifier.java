/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.impl;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.opencastproject.kernel.mail.NotifierUtils;
import org.opencastproject.notify.Notifier;
import org.opencastproject.notify.SendType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author John Crossman
 */
public class VoidNotifier implements Notifier {

  private Logger logger = LoggerFactory.getLogger(CourseManagementServiceImpl.class);
  private boolean engineeringNotified;

  public VoidNotifier() {
  }

  public VoidNotifier(final Logger logger) {
    this.logger = logger;
  }

  @Override
  public SendType notifyEngineeringTeam(final String subject, final String message, final Object... debugInfo) {
    logger.info(subject);
    logger.info(message + '\n' + NotifierUtils.getDebugInfo(debugInfo));
    engineeringNotified = true;
    return SendType.messageLoggedNotSent;
  }

  @Override
  public SendType notifyEngineeringTeam(final Throwable exception, final Object... debugInfo) {
    logger.info(ExceptionUtils.getStackTrace(exception));
    logger.info(NotifierUtils.getDebugInfo(debugInfo));
    engineeringNotified = true;
    return SendType.messageLoggedNotSent;
  }

  @Override
  public SendType notifyEngineeringTeam(final String subject, final Throwable exception, final Object... debugInfo) {
    logger.info(subject);
    logger.info(ExceptionUtils.getStackTrace(exception));
    logger.info(NotifierUtils.getDebugInfo(debugInfo));
    engineeringNotified = true;
    return SendType.messageLoggedNotSent;
  }

  public boolean isEngineeringNotified() {
    return engineeringNotified;
  }
}
