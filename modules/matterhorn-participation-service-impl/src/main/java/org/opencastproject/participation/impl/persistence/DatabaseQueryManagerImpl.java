/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.impl.persistence;

import com.google.common.util.concurrent.RateLimiter;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.opencastproject.participation.impl.Column;
import org.opencastproject.util.env.Environment;
import org.opencastproject.util.env.EnvironmentUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author John Crossman
 */
public final class DatabaseQueryManagerImpl implements DatabaseQueryManager {

  private static final Map<WebcastDatabase, DatabaseQueryManager> singletonMap = new HashMap<WebcastDatabase, DatabaseQueryManager>();

  private final Logger logger;
  private final ProducesSQL sqlProducer;
  private ComboPooledDataSource pooledDataSource;
  // Throttle SQL queries such that we get a max of 1 every per 2 seconds. See WCT-5050
  private final RateLimiter rateLimiter;

  public static DatabaseQueryManager getInstance(final String jdbcDriver, final DatabaseCredentials credentials,
                                                 final ProducesSQL sqlProducer, final RateLimiter rateLimiter) {
    return getInstance(jdbcDriver, credentials, sqlProducer, rateLimiter, LoggerFactory.getLogger(DatabaseQueryManagerImpl.class));
  }

  static DatabaseQueryManager getInstance(final String jdbcDriver, final DatabaseCredentials credentials,
          final ProducesSQL sqlProducer, final RateLimiter rateLimiter, final Logger logger) {
    if (Environment.ci.equals(EnvironmentUtil.getEnvironment())) {
      throw new UnsupportedOperationException("Bamboo (build server) is not allowed to run database queries against Oracle views.");
    }
    final WebcastDatabase webcastDatabase = credentials.getWebcastDatabase();
    if (!singletonMap.containsKey(webcastDatabase)) {
      final DatabaseQueryManager queryManager = new DatabaseQueryManagerImpl(jdbcDriver, credentials, sqlProducer, rateLimiter, logger);
      singletonMap.put(webcastDatabase, queryManager);
    }
    return singletonMap.get(webcastDatabase);
  }

  private DatabaseQueryManagerImpl(final String jdbcDriver, final DatabaseCredentials credentials,
          final ProducesSQL sqlProducer, final RateLimiter rateLimiter, final Logger logger) {
    this.sqlProducer = sqlProducer;
    this.rateLimiter = rateLimiter;
    this.logger = logger;
    final Date beforeAttempt = new Date();
    try {
      pooledDataSource = new ComboPooledDataSource();
      pooledDataSource.setDriverClass(jdbcDriver);
      pooledDataSource.setJdbcUrl(credentials.getURL());
      pooledDataSource.setUser(credentials.getUsername());
      pooledDataSource.setPassword(credentials.getPassword());
      pooledDataSource.setMinPoolSize(2);
      pooledDataSource.setInitialPoolSize(0);
      pooledDataSource.setAcquireIncrement(2);
      pooledDataSource.setMaxPoolSize(20);
      pooledDataSource.setMaxIdleTime(600);
      pooledDataSource.setMaxIdleTimeExcessConnections(300);
    } catch (final Exception e) {
      final long seconds = (new Date().getTime() - beforeAttempt.getTime()) / 1000;
      final String message = seconds + " seconds after invoking DriverManager.getConnection(): " + e.getMessage();
      throw new CourseDatabaseException(message, e);
    }
  }

  public <T> Set<T> getSet(final ResultSetRowConverter<T> rowConverter, final QueryType queryType,
          final Map<Column, Object> tokenMap) throws CourseDatabaseException {
    rateLimiter.acquire();
    Statement statement = null;
    final String sql = sqlProducer.produceSQL(queryType, tokenMap);
    if (logger.isDebugEnabled()) {
      logger.debug("Query: " + queryType + "\n" + sql);
    }
    Connection connection = null;
    try {
      connection = pooledDataSource.getConnection();
      statement = connection.createStatement();
      final ResultSet resultSet = statement.executeQuery(sql);
      final Set<T> set = new HashSet<T>();
      while (resultSet.next()) {
        set.add(rowConverter.convert(resultSet));
      }
      return set;
    } catch (final SQLException e) {
      throw new CourseDatabaseException("Failed to connect and/or run query:" + '\n' + sql, e);
    } finally {
      closeWithTolerance(statement);
      closeWithTolerance(connection);
    }
  }

  public void upsert(final QueryType queryType, final Map<Column, Object> map) {
    rateLimiter.acquire();
    Statement statement = null;
    final String sql = sqlProducer.produceSQL(queryType, map);
    if (logger.isDebugEnabled()) {
      logger.debug(sql);
    }
    Connection connection = null;
    try {
      connection = pooledDataSource.getConnection();
      statement = connection.createStatement();
      statement.executeUpdate(sql);
    } catch (final SQLException e) {
      throw new CourseDatabaseException("Failed to connect and/or run query:" + '\n' + sql, e);
    } finally {
      closeWithTolerance(statement);
      closeWithTolerance(connection);
    }
  }

  private void closeWithTolerance(final Object obj) {
    try {
      if (obj instanceof ResultSet) {
        ((ResultSet) obj).close();
      } else if (obj instanceof Statement) {
        ((Statement) obj).close();
      } else if (obj instanceof Connection) {
        ((Connection) obj).close();
      }
    } catch (final SQLException ignore) {
      // We swallow this exception so the connection.close() is still invoked.
      ignore.printStackTrace();
    }
  }

}
