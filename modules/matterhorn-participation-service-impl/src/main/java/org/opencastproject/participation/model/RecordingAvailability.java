/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.model;

import org.opencastproject.salesforce.RepresentsSalesforceField;
import org.opencastproject.security.api.RecordingAccessRights;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.xml.bind.annotation.XmlEnum;

/**
 * @author Fernando Alvarez
 */
@XmlEnum
public enum RecordingAvailability implements RepresentsSalesforceField {

  // TODO Publication channel "PublicationChannel.Matterhorn" (which means publish to mmm which is engage) can be removed any time after version GP 1.4.30 when engage is dropped
  // Leaving it in doesn't actually harm anything since all it does is set "org.opencastproject.workflow.config.mmm = true" which becomes meaningless once we remove the publish-engage operation from the workflow
  // TODO "PublicationChannel.ITunes" is meaningless unless is is used in a workflow conditional statement (e.g. like the youtube conditional prior to 1.4.30)
  publicCreativeCommons("Public, licensed as Creative Commons 3.0-BY-NC-ND", License.creativeCommons, RecordingAccessRights.publicAccessRights, PublicationChannel.Matterhorn, PublicationChannel.YouTube),
  publicNoRedistribute("Public, with no license to redistribute", License.allRightsReserved, RecordingAccessRights.publicAccessRights, PublicationChannel.Matterhorn, PublicationChannel.YouTube),
  studentsOnly("Students-only, with no license to redistribute", License.allRightsReserved, RecordingAccessRights.studentsOnlyAccessRights, PublicationChannel.Matterhorn, PublicationChannel.YouTube);

  private final String recordingAvailability;
  private final License license;
  private final RecordingAccessRights recordingAccessRights;
  private final Set<PublicationChannel> distribution;

  public License getLicense() {
    return license;
  }

  public RecordingAccessRights getRecordingAccessRights() {
    return recordingAccessRights;
  }

  public Set<PublicationChannel> getDistribution() {
    return distribution;
  }

  @Override
  public String getSalesforceValue() {
    return recordingAvailability;
  }

  RecordingAvailability(final String recordingAvailability, final License license,
          final RecordingAccessRights recordingAccessRights, final PublicationChannel... publications) {
    this.recordingAvailability = recordingAvailability;
    this.license = license;
    this.recordingAccessRights = recordingAccessRights;
    this.distribution = new HashSet<PublicationChannel>(Arrays.asList(publications));
  }
}
