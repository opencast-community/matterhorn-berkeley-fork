/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.model;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.opencastproject.metadata.dublincore.DublinCoreValue;
import org.opencastproject.participation.CourseUtils;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * @author John Crossman
 */
public class CourseData extends CatalogMetadata implements HasCourseKey {

  private static final int START_TIME_OFFSET = 7;
  private static final int END_TIME_OFFSET = 3;

  private CanonicalCourse canonicalCourse;
  private Term term;
  private String section;
  private String deptDescription;
  private String displayName;

  private List<DayOfWeek> meetingDays;
  private String startTime;
  private String endTime;

  private String courseOfferingId;
  private Room room;
  private Character printCode;
  private Integer studentCount;
  private Set<Participation> participationSet;

  @Override public int getYear() {
    return term.getYear();
  }

  @Override public Semester getSemester() {
    return term.getSemester();
  }

  @Override public int getCcn() {
    return canonicalCourse.getCcn();
  }

  public Set<Participation> getParticipationSet() {
    return participationSet == null ? new HashSet<Participation>() : participationSet;
  }

  public void setParticipationSet(final Set<Participation> participationSet) {
    this.participationSet = participationSet;
  }

  @SuppressWarnings("all")
  public String getCourseOfferingId() {
    if (StringUtils.isEmpty(courseOfferingId)) {
      final StringBuilder builtCourseOfferingId = new StringBuilder();
      builtCourseOfferingId.append(getYear());
      builtCourseOfferingId.append(term.getSemester().getTermCode());
      builtCourseOfferingId.append(getCcn());
      return builtCourseOfferingId.toString();
    } else {
      return courseOfferingId;
    }
  }

  public void setCourseOfferingId(final String courseOfferingId) {
    this.courseOfferingId = courseOfferingId;
  }

  public CanonicalCourse getCanonicalCourse() {
    return canonicalCourse;
  }

  public void setCanonicalCourse(final CanonicalCourse canonicalCourse) {
    this.canonicalCourse = canonicalCourse;
  }

  public Term getTerm() {
    return term;
  }

  public void setTerm(final Term term) {
    this.term = term;
  }

  public String getSection() {
    return section;
  }

  public void setSection(final String section) {
    this.section = section;
  }

  @SuppressWarnings("unused")
  public String getDeptDescription() {
    return deptDescription;
  }

  public void setDeptDescription(final String deptDescription) {
    this.deptDescription = deptDescription;
  }

  public String getDisplayName() {
    return displayName;
  }

  public void setDisplayName(String displayName) {
    this.displayName = displayName;
  }

  public List<DayOfWeek> getMeetingDays() {
    return meetingDays;
  }

  public void setMeetingDays(final List<DayOfWeek> meetingDays) {
    this.meetingDays = meetingDays;
  }

  public String getOffsetStartTime() {
    int militaryTime = Integer.parseInt(getStartTime()) + START_TIME_OFFSET;
    return (militaryTime > 999 ? StringUtils.EMPTY : "0") + militaryTime;
  }

  public String getOffsetEndTime() {
    int militaryTime = Integer.parseInt(getEndTime()) + END_TIME_OFFSET;
    return (militaryTime > 999 ? StringUtils.EMPTY : "0") + militaryTime;
  }

  public String getStartTime() {
    return startTime;
  }

  public void setStartTime(final String startTime) {
    this.startTime = startTime;
  }

  public String getEndTime() {
    return endTime;
  }

  public void setEndTime(final String endTime) {
    this.endTime = endTime;
  }

  public Room getRoom() {
    return room;
  }

  public void setRoom(Room room) {
    this.room = room;
  }

  public Integer getStudentCount() {
    return studentCount;
  }

  public void setStudentCount(final Integer studentCount) {
    this.studentCount = studentCount;
  }

  public Character getPrintCode() {
    return printCode;
  }

  @SuppressWarnings("unused")
  public void setPrintCode(final Character printCode) {
    this.printCode = printCode;
  }

  public String getRecordingTitle() {
    final String[] tokens = getCanonicalCourse().getName().split(",");
    return tokens[0];
  }

  public String getSeriesTitle() {
    final String term = getTerm().getSemester() + " " + getTerm().getYear();
    return getCanonicalCourse().getName() + " - " + term;
  }

  public List<DublinCoreValue> getInstructors() {
    final List<DublinCoreValue> instructorsList = new LinkedList<DublinCoreValue>();
    for (final Participation p : getParticipationSet()) {
      final Instructor instructor = p.getInstructor();
      final String name = WordUtils.capitalizeFully(instructor.getFirstName()) + ' ' + WordUtils.capitalizeFully(instructor.getLastName());
      instructorsList.add(new DublinCoreValue(name));
    }
    return instructorsList;
  }

  public String getSeriesDescription() {
    final StringBuilder instructors = new StringBuilder();
    for (final DublinCoreValue instructor : getInstructors()) {
      if (instructors.length() > 0) {
        instructors.append(", ");
      }
      instructors.append(instructor.getValue());
    }
    final String courseTitle = StringUtils.isBlank(getCanonicalCourse().getTitle()) ? getCanonicalCourse().getName() : getCanonicalCourse().getTitle();
    return courseTitle.concat(" - ").concat(instructors.toString());
  }

  @Override
  public String toString() {
    return new ToStringBuilder(ToStringStyle.SHORT_PREFIX_STYLE)
        .append("canonicalCourse", canonicalCourse)
        .append("courseOfferingId", courseOfferingId)
        .append("section", section)
        .append("meetingDays", meetingDays)
        .append("startTime", startTime)
        .append("endTime", endTime)
        .append("term", term)
        .append("room", room)
        .append("studentCount", studentCount)
        .toString();
  }

  @Override
  public final boolean equals(final Object o) {
    return (o instanceof HasCourseKey) && CourseUtils.equals(this, o);
  }

  @Override
  public final int hashCode() {
    return CourseUtils.hashCode(this);
  }
}
