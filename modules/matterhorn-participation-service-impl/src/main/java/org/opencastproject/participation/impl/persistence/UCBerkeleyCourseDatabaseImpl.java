/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.impl.persistence;

import com.google.common.util.concurrent.RateLimiter;
import org.apache.commons.lang.CharUtils;
import org.apache.commons.lang.StringUtils;
import org.opencastproject.notify.Notifier;
import org.opencastproject.participation.CourseUtils;
import org.opencastproject.participation.impl.Column;
import org.opencastproject.participation.model.*;
import org.opencastproject.util.env.EnvironmentUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * @author John Crossman
 */
public class UCBerkeleyCourseDatabaseImpl implements ProvidesCourseCatalogData {

  private enum CourseDatabaseQuery implements QueryType {
    getCourseByCourseKey,
    getCourseInstructorByCourseKey,
    getCourseOfferingsOfSemester,
    getCrossListingSubjectArea,
    getInstructorsBySemesterYear,
    getPersonByLdapUID
  }

  private enum Token implements Column<Object> {
    ccn,
    courseControlNumber,
    displayName,
    ldapUID,
    termCd,
    termId,
    termYr;

    public Class<Object> getType() {
      return Object.class;
    }
  }

  private static final Map<String, String> displayNameTranslations;
  private static final Map<String, CatalogMetadata> translationsCache = new HashMap<String, CatalogMetadata>();

  static {
    displayNameTranslations = new HashMap<String, String>();
    displayNameTranslations.put("ARESEC", "A,RESEC");
    displayNameTranslations.put("BIOENG", "BIO ENG");
    displayNameTranslations.put("CIVENG", "CIV ENG");
    displayNameTranslations.put("COGSCI", "COG SCI");
    displayNameTranslations.put("CYPLAN", "CY PLAN");
    displayNameTranslations.put("EALANG", "EA LANG");
    displayNameTranslations.put("ELENG", "EL ENG");
    displayNameTranslations.put("LDARCH", "LD ARCH");
    displayNameTranslations.put("LS", "L & S");
    displayNameTranslations.put("MATSCI", "MAT SCI");
    displayNameTranslations.put("MECENG", "MEC ENG");
    displayNameTranslations.put("PBHLTH", "PB HLTH");
    displayNameTranslations.put("PUBPOL", "PUB POL");
    displayNameTranslations.put("SASIAN", "S ASIAN");
    displayNameTranslations.put("SSEASN", "S,SEASN");
  }

  private final Logger logger = LoggerFactory.getLogger(this.getClass());

  private final DatabaseQueryManager queryManager;

  private final Notifier notifier;

  private final Term term = Term.getTermFromProperties();

  private Map<Integer, Set<Instructor>> instructorMap;

  /**
   * @see oracle.jdbc.OracleDriver
   * @param credentials for database connection
   * @param notifier never null
   * @throws IOException when required resource files not found
   */
  public UCBerkeleyCourseDatabaseImpl(final DatabaseCredentials credentials, final Notifier notifier) throws IOException {
    this(credentials, notifier, LoggerFactory.getLogger(UCBerkeleyCourseDatabaseImpl.class));
  }

    /**
     * @see oracle.jdbc.OracleDriver
     * @param credentials for database connection
     * @throws IOException when required resource files not found
     */
  public UCBerkeleyCourseDatabaseImpl(final DatabaseCredentials credentials, final Notifier notifier, final Logger logger) throws IOException {
    logger.debug("Instantiating UCBerkeleyCourseDatabaseImpl with credentials: " + credentials);

    final String dbType = CourseUtils.getCourseManagementServiceProperties().getProperty("db.courseCatalog.type");
    final File directory = EnvironmentUtil.getFileUnderFelixHome("etc/sql/" + dbType);
    final ProducesSQL producesSQL = new ProduceSQLFreemarker(directory, "sql");

    final String jdbcDriver = "oracle.jdbc.driver.OracleDriver";
    final RateLimiter rateLimiter = RateLimiter.create(2.0);
    queryManager = DatabaseQueryManagerImpl.getInstance(jdbcDriver, credentials, producesSQL, rateLimiter, logger);
    this.notifier = notifier;
  }

  @Override
  public Set<CourseData> getCourseData() {
    final Set<CourseData> courseDataListSet = getCourseData(CourseDatabaseQuery.getCourseOfferingsOfSemester, true);
    addInstructors(courseDataListSet);
    return courseDataListSet;
  }

  private void addInstructors(final Set<CourseData> courseDataListSet) {
    final Map<Integer, Set<Instructor>> instructorMap = getInstructorCcnMap();
    for (final CourseData courseData : courseDataListSet) {
      final int ccn = courseData.getCcn();
      if (instructorMap.containsKey(ccn)) {
        final Set<Instructor> instructorSet = instructorMap.get(ccn);
        courseData.setParticipationSet(toParticipationSet(instructorSet));
      }
    }
  }

  private Map<Integer, Set<Instructor>> getInstructorCcnMap() {
    if (this.instructorMap == null) {
      final Map<Column, Object> tokenMap = getTermAsTokenMap();
      final ResultSetRowConverter<Person> rowConverter = new ResultConverterForCourseInstructor(term);
      final Set<Person> instructorSet = queryManager.getSet(rowConverter, CourseDatabaseQuery.getInstructorsBySemesterYear, tokenMap);
      instructorMap = new HashMap<Integer, Set<Instructor>>();
      for (final Person next : instructorSet) {
        final CourseKey courseKey = next.getCourseKey();
        final int ccn = courseKey.getCcn();
        if (!instructorMap.containsKey(ccn)) {
          instructorMap.put(ccn, new HashSet<Instructor>());
        }
        final Instructor instructor = new Instructor(null, next.getCalNetUID(), next.getRole(), next.getFirstName(),
                next.getLastName(), next.getEmail(), next.getDepartment());
        instructorMap.get(ccn).add(instructor);
      }
    }
    return instructorMap;
  }

  public CourseData getCourse(final CourseKey courseKey) {
    final Map<Column, Object> args = getTermAsTokenMap();
    args.put(Token.ccn, Integer.toString(courseKey.getCcn()));
    final Set<CourseData> set = getCourseData(args, CourseDatabaseQuery.getCourseByCourseKey, false);
    final CourseData courseData;
    if (set.isEmpty()) {
      courseData = null;
    } else {
      courseData = set.iterator().next();
      // We don't pass the same Set instance because the iterator was already incremented.
      final ResultSetRowConverter<Instructor> rowConverter = new ResultConverterForInstructor();
      final Set<Instructor> instructorSet = queryManager.getSet(rowConverter, CourseDatabaseQuery.getCourseInstructorByCourseKey, args);
      courseData.setParticipationSet(toParticipationSet(instructorSet));
    }
    return courseData;
  }

  List<Booking> getCrossListedCourses(final Set<CourseData> courseData) {
    return Booking.crossListedBookings(courseData);
  }

  private Set<CourseData> getCourseData(final QueryType queryType, final boolean includeStudentCount) {
    return getCourseData(getTermAsTokenMap(), queryType, includeStudentCount);
  }

  private Set<CourseData> getCourseData(final Map<Column, Object> courseQueryArgs, final QueryType queryType,
          final boolean includeStudentCount) {
    final Map<String, CourseData> courseDataMap = new HashMap<String, CourseData>();
    final ResultSetRowConverter<CourseData> converter = new ResultConverterForCourse(includeStudentCount);
    final Set<CourseData> courseDataSet = queryManager.getSet(converter, queryType, courseQueryArgs);
    for (final CourseData c : courseDataSet) {
      final boolean validRoom = c.getRoom() != null && StringUtils.isNotBlank(c.getRoom().getBuilding());
      final boolean validStartEndTimes = c.getStartTime() != null && c.getEndTime() != null;
      final boolean validMeetingDays = c.getMeetingDays() != null && c.getMeetingDays().size() > 0;
      if (validRoom && validStartEndTimes && validMeetingDays) {
        String displayName = StringUtils.trimToNull(c.getDisplayName());
        if (c.getDeptName() == null && c.getCatalogId() == null && displayName != null) {
          final String estimatedCatalogId = displayName.contains(" ") ? StringUtils.substringAfterLast(displayName, " ") : displayName;
          if (estimatedCatalogId.startsWith("C")) {
            findMissingCourseData(c, displayName);
          }
        }
        final String key = CourseUtils.getSeriesId(c);
        final CourseData duplicate = courseDataMap.get(key);
        courseDataMap.put(key, merge(c, duplicate));
      }
    }
    return new HashSet<CourseData>(courseDataMap.values());
  }

  private void findMissingCourseData(final CourseData c, final String displayName) {
    String translation = translateToCrossListingDisplayName(displayName);
    CatalogMetadata metadata = translationsCache.get(translation);
    if (metadata == null) {
      final Map<Column, Object> args = new HashMap<Column, Object>();
      args.put(Token.displayName, translation);
      final Set<CatalogMetadata> set = queryManager.getSet(new ResultConverterForCatalogMetadata(), CourseDatabaseQuery.getCrossListingSubjectArea, args);
      if (set.isEmpty()) {
        logger.warn("No cross-listing found where displayName = " + displayName);
      } else {
        metadata = set.iterator().next();
        translationsCache.put(translation, metadata);
      }
    }
    if (metadata != null) {
      c.setDeptName(metadata.getDeptName());
      c.setCatalogId(metadata.getCatalogId());
    }
  }

  static String translateToCrossListingDisplayName(final String s) {
    final String result;
    String token = StringUtils.trimToNull(s);
    if (token == null) {
      result = null;
    } else {
      for (String key : displayNameTranslations.keySet()) {
        String prefix = key + " ";
        if (token.startsWith(prefix)) {
          token = StringUtils.replaceOnce(token, prefix, displayNameTranslations.get(key) + " ");
        }
      }
      result = StringUtils.join(token.split("[^\\w]+"), '%');
    }
    return result;
  }

  private CourseData merge(final CourseData c1, final CourseData c2) {
    final CourseData result;
    final Character codeA = 'A';
    if (c1 == null) {
      result = c2;
    } else if (c2 == null) {
      result = c1;
    } else if (codeA.equals(c1.getPrintCode())) {
      result = c1;
    } else if (codeA.equals(c2.getPrintCode())) {
      result = c2;
    } else {
      final int size1 = c1.getMeetingDays().size();
      final int size2 = c2.getMeetingDays().size();
      result = size1 >= size2 ? c1 : c2;
      notifier.notifyEngineeringTeam("Two courses with same CCN (CourseDataMover)",
              "The courses compared are in debug section below. CourseDataMover chose the course with most meeting days.", c1, c2);
    }
    return result;
  }

  private <I extends Instructor> Set<Participation> toParticipationSet(final Set<I> instructorSet) {
    final Set<Participation> participationSet = new HashSet<Participation>();
    for (final I instructor : instructorSet) {
      final Participation wrapper = new Participation();
      wrapper.setInstructor(instructor);
      participationSet.add(wrapper);
    }
    return participationSet;
  }

  private Map<Column,Object> getTermAsTokenMap() {
    final Map<Column, Object> map = new HashMap<Column, Object>();
    final Term term = Term.getTermFromProperties();
    final char termCode = term.getSemester().getTermCode();
    final Integer termYear = term.getYear();
    map.put(Token.termCd, CharUtils.toString(termCode));
    map.put(Token.termYr, Integer.toString(termYear));
    map.put(Token.termId, CourseUtils.getTermId(termYear, termCode));
    return map;
  }

  private class ResultConverterForCourseInstructor extends AbstractResultSetRowConverter<Person> {

    private final Term term;

    private ResultConverterForCourseInstructor(final Term term) {
      this.term = term;
    }

    @Override
    public Person convert(final ResultSet r) throws SQLException {
      final Integer ccn = get(r, ColInt.course_cntl_num);
      return new Person(new CourseKey(term.getYear(), term.getSemester(), ccn),
              get(r, ColStr.ldap_uid), null, get(r, ColStr.first_name), get(r, ColStr.last_name),
              get(r, ColStr.email_address), get(r, ColStr.dept_description));
    }
  }

}
