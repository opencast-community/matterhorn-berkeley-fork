/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.model;

import org.opencastproject.salesforce.RepresentsSalesforceField;

import javax.xml.bind.annotation.XmlEnum;

/**
 * @author John Crossman
 */
@XmlEnum
public enum License implements RepresentsSalesforceField {

  creativeCommons("Creative Commons 3.0: Attribution-NonCommercial-NoDerivs"),
  allRightsReserved("Copyright @2020 UC Regents; all rights reserved");

  private final String salesforceValue;

  private License(final String salesforceValue) {
    this.salesforceValue = salesforceValue;
  }

  public String getSalesforceValue() {
    return salesforceValue;
  }

}
