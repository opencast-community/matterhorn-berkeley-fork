/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.mvc;

import org.opencastproject.capture.admin.api.CaptureAgentStateService;
import org.opencastproject.metadata.dublincore.DublinCore;
import org.opencastproject.metadata.dublincore.DublinCoreCatalog;
import org.opencastproject.notify.Notifier;
import org.opencastproject.participation.CourseUtils;
import org.opencastproject.participation.api.CourseManagementService;
import org.opencastproject.participation.impl.CourseCaptureScheduler;
import org.opencastproject.participation.model.CapturePreferences;
import org.opencastproject.participation.model.CourseOffering;
import org.opencastproject.participation.model.Participation;
import org.opencastproject.participation.model.RecordingAvailability;
import org.opencastproject.participation.model.RecordingType;
import org.opencastproject.participation.model.SignUp;
import org.opencastproject.participation.model.YesOrNo;
import org.opencastproject.scheduler.api.SchedulerException;
import org.opencastproject.scheduler.api.SchedulerService;
import org.opencastproject.security.api.AccessControlEntry;
import org.opencastproject.security.api.AccessControlList;
import org.opencastproject.security.api.Organization;
import org.opencastproject.security.api.SecurityService;
import org.opencastproject.security.api.User;
import org.opencastproject.security.util.SecurityUtil;
import org.opencastproject.series.api.SeriesService;
import org.opencastproject.util.NotFoundException;
import org.opencastproject.util.NumberUtils;
import org.opencastproject.util.data.Tuple;
import org.opencastproject.util.date.DateUtil;
import org.opencastproject.workflow.api.WorkflowService;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Conventions;
import org.springframework.http.HttpMethod;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.ServletRequestDataBinder;

import java.io.IOException;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author John Crossman
 */
public class SignUpFormController extends AbstractFormController<SignUp> {

  private final Logger logger = LoggerFactory.getLogger(this.getClass());
  private CourseManagementService courseManagementService;
  private SchedulerService schedulerService;
  private SeriesService seriesService;
  private CaptureAgentStateService captureAgentStateService;
  private SecurityService securityService;
  private final SignUpFormValidator validator = new SignUpFormValidator();

  public SignUpFormController() {
    super("signUp", "signUpFinish");
  }

  protected void doProcess(final HttpServletRequest request, final HttpServletResponse response, final HttpMethod httpMethod) throws IOException {
    final User user = securityService.getUser();
    final String courseOfferingId = getCourseOfferingId(request);
    final SignUp signUp = bind(request);
    final Errors errors = new BeanPropertyBindingResult(signUp, Conventions.getVariableName(signUp));
    CourseOffering courseOffering = getCourseOffering(courseOfferingId, user, errors);
    validator.validateBeforeProcess(courseOffering, user, errors);
    final ModelAndView mav;
    if (errors.hasErrors()) {
      mav = new ModelAndView(finishView);
      mav.message(Key.footer, Key.contactUs);

      // We do this to prevent an unauthorized user seeing details.
      try {
        if (!validator.isAuthorized(user, courseOffering.getParticipationSet())) {
          courseOffering.getCapturePreferences().setRecordingType(null);
        }
      } catch(Throwable e) {
        // Otherwise we get "An unexpected error occurred."
        // instead of the "The system does not recognize the submitted course identifier." which is what we really want.
        // Sure its a hack, but no time now to fix this right, probably with a new just errors form.
      }

    } else {
      switch (httpMethod) {
        case GET:
          setDefaultsAsNeeded(courseOffering);
          mav = new ModelAndView(formView);
          showForm(signUp, courseOffering, user, mav);
          break;
        case POST:
          final Set<Participation> participationSet = courseOffering.getParticipationSet();
          final boolean scheduled = courseOffering.isScheduled();
          validator.validatePost(signUp, user, participationSet, scheduled, errors);
          if (errors.hasErrors()) {
            mav = new ModelAndView(formView);
            showForm(signUp, courseOffering, user, mav);
            mav.put(signUp);
          } else {
            try {
              final RecordingAvailability recordingAvailability = scheduled
                  ? courseOffering.getCapturePreferences().getRecordingAvailability()
                  : signUp.getRecordingAvailability();
              final CapturePreferences cp =
                  new CapturePreferences(signUp.getRecordingType(), recordingAvailability, signUp.getPublishDelayDays());
              mav = new ModelAndView(finishView);
              approveCapturePreferences(courseOfferingId, user, cp, scheduled);
              //
              final boolean readyToSchedule = !scheduled
                  && (SecurityUtil.isAdminUser(user) || CourseUtils.isLastUndecidedUser(participationSet, user));
              if (readyToSchedule) {
                final CourseCaptureScheduler scheduler =
                        new CourseCaptureScheduler(schedulerService, seriesService, captureAgentStateService);
                final String captureAgentName = courseManagementService.getCaptureAgentName(courseOffering.getCourseOfferingId());

                // Schedule Matterhorn asynchronously to improve the user experience; response time otherwise runs
                // approximately one second per recording
                Organization org = securityService.getOrganization();
                scheduleCourses(org, securityService, notifier, scheduler, courseOffering, user, signUp, captureAgentName, recordingAvailability);

                // Record in Salesforce even when Matterhorn scheduling fails
                courseManagementService.setRecordingsScheduledTrue(courseOfferingId);
                if (!scheduler.isKnownAgent(captureAgentName)) {
                  reportUnknownCaptureAgent(courseOfferingId, captureAgentName);
                }
                mav.message(Key.pageH1, Key.signUpH1);
                logger.info("Recordings for course {} have been scheduled", courseOfferingId);
              } else {
                approveCapturePreferences(courseOfferingId, user, cp, scheduled);
              }
              // Get latest from Salesforce
              courseOffering = courseManagementService.getCourseOffering(courseOfferingId);
              mav.message(Key.footer, Key.signUpFinishFooter);
            } catch (final NotFoundException e) {
              throw new IOException(e);
            }
          }
          break;
        default:
          throw new UnsupportedOperationException("HTTP method not supported: " + httpMethod);
      }
    }
    if (errors.hasErrors()) {
      mav.put(Key.errors, errors);
    }
    if (courseOffering != null) {
      mav.put(courseOffering);
      final Set<Participation> participationSet = courseOffering.getParticipationSet();
      if (participationSet != null && !participationSet.isEmpty()) {
        mav.put(CourseUtils.sortParticipants(participationSet, user));
      }
      mav.put(Key.startTime, DateUtil.getTimeOfDay(courseOffering.getStartTime()));
      mav.put(Key.endTime, DateUtil.getTimeOfDay(courseOffering.getEndTime()));
      mav.put(Key.offsetStartTime, DateUtil.getTimeOfDay(courseOffering.getOffsetStartTime()));
      mav.put(Key.offsetEndTime, DateUtil.getTimeOfDay(courseOffering.getOffsetEndTime()));
      mav.put(Key.publishByDayOptions, NumberUtils.range(1, 10));
    }
    mav.put(user);
    mav.render(response);
  }

  private void scheduleCourses(final Organization org, final SecurityService securityService, final Notifier notifier, final CourseCaptureScheduler scheduler, final CourseOffering courseOffering,
          final User user, final SignUp signUp, final String captureAgentName, final RecordingAvailability recordingAvailability) {
    new Thread(new Runnable() {
      public void run() {
        securityService.setOrganization(org);
        securityService.setUser(user);
        final String anonymousRole = org.getAnonymousRole();
        try {
          // TODO Changed ACL permission to "write" in order to temporarily patch the issue reported in
          // JIRA WCT-5043 "Sign up: instructors can't schedule recordings"
          // final AccessControlList acl = new AccessControlList(new AccessControlEntry(anonymousRole,
          // WorkflowService.READ_PERMISSION, true));
          final AccessControlList acl = new AccessControlList(new AccessControlEntry(anonymousRole, WorkflowService.WRITE_PERMISSION, true));
          final DublinCoreCatalog series = scheduler.createSeries(courseOffering, recordingAvailability, acl);
          final String seriesId = series.getFirst(DublinCore.PROPERTY_IDENTIFIER);
          scheduler.scheduleMatterhornRecordings(seriesId, courseOffering, signUp, captureAgentName);
        } catch (final SchedulerException e) {
          final Tuple<String, String> email = formatEmail(courseOffering, user);
          final String subject = email.getA();
          final String message = email.getB();
          notifier.notifyEngineeringTeam(subject, message, courseOffering);
          logger.error(subject, e);
          notifier.notifyEngineeringTeam(subject, e, courseOffering, user);
        }
      }

      private Tuple<String, String> formatEmail(CourseOffering courseOffering, User user) {
        final String subject = "Scheduling failed. Please manually schedule.";

        final StringBuilder messageLineOne = new StringBuilder(user.getUserName());
        messageLineOne.append(" approved ").append(courseOffering.getSeriesTitle()).append(" but scheduling failed.");

        final StringBuilder messageLineTwo = new StringBuilder("\n");
        messageLineTwo.append("Salesforce project page: https://na1.salesforce.com/").append(
                courseOffering.getSalesforceID());

        final StringBuilder messageLineThree = new StringBuilder("\n");
        messageLineThree
                .append("This scheduling should be done via the Sign Up form, using the Sign Up form link in Salesforce (aka Admin override). Do not schedule via Matterhorn (Series ID will not be correct).");

        final StringBuilder messageLineFour = new StringBuilder("\n");
        messageLineFour
                .append("In order to sign up via the Sign Up form, you will need to un-check the Recordings Scheduled checkbox for the Project in Salesforce.")
                .append(" If the Series was already created in Matterhorn, you will also need to delete the series using the Series Rest endpoint.");

        final String message = messageLineOne.toString().concat(messageLineTwo.toString())
                .concat(messageLineThree.toString()).concat(messageLineFour.toString());

        return Tuple.tuple(subject, message);
      }
    }).start();
  }


  protected SignUp bind(final HttpServletRequest request) {
    final SignUp signUp = new SignUp();
    final ServletRequestDataBinder binder = new ServletRequestDataBinder(signUp);
    binder.bind(request);
    return signUp;
  }

  private void approveCapturePreferences(final String courseOfferingId, final User user, final CapturePreferences cp, final boolean mustOnlyAgreeToTerms) throws NotFoundException {
    if (mustOnlyAgreeToTerms && !SecurityUtil.isAdminUser(user)) {
      courseManagementService.approveCapturePreferences(courseOfferingId, user.getUserName());
    } else {
      courseManagementService.updateCapturePreferences(courseOfferingId, user, cp);
    }
  }

  private void reportUnknownCaptureAgent(final String courseOfferingId, final String captureAgentName) {
    final String subject = courseOfferingId + " scheduled for unknown Capture Agent";
    final String message = "At this time, no Matterhorn Capture Agent is associated with the room ("
        .concat(captureAgentName)
        .concat(") of course ")
        .concat(courseOfferingId)
        .concat(". If you suspect a problem then contact the ETS Operations team.");
    notifier.notifyEngineeringTeam(subject, message, captureAgentName);
  }

  private CourseOffering getCourseOffering(final String courseOfferingId, final User user, final Errors errors) {
    final CourseOffering courseOffering = (courseOfferingId == null) ? null : courseManagementService.getCourseOffering(courseOfferingId);
    if (user == null) {
      errors.reject(Key.pleaseLogIn.name());
    } else if (courseOffering != null) {
      if (!validator.isAuthorized(user, courseOffering.getParticipationSet())) {
        errors.reject(Key.youAreNotCourseInstructor.name());
      }
    }
    return courseOffering;
  }


  private String getCourseOfferingId(final HttpServletRequest request) {
    final String id = StringUtils.trimToNull(request.getParameter("id"));
    return (id == null) ? StringUtils.trimToNull(request.getParameter("courseOfferingId")) : id;
  }

  private void showForm(final SignUp signUp, final CourseOffering c, final User user, final ModelAndView mav) throws IOException {
    signUp.setCourseOfferingId(c.getCourseOfferingId());
    final CapturePreferences cp = c.getCapturePreferences();
    if (cp != null && cp.getRecordingType() != null) {
      // Only set these properties if recordingType is set in Salesforce
      signUp.setRecordingType(cp.getRecordingType());
      signUp.setRecordingAvailability(cp.getRecordingAvailability());
      final boolean publishDelay = cp.getDelayPublishByDays() > 0;
      signUp.setPublishDelay(publishDelay ? YesOrNo.yes : YesOrNo.no);
      signUp.setPublishDelayDays(publishDelay ? cp.getDelayPublishByDays() : null);
    }
    mav.put(signUp);
    mav.put(RecordingType.values());
    mav.put(RecordingAvailability.values());
    mav.message(Key.pageH1, Key.signUpH1);
    mav.put(Key.thisUserCanScheduleRecordings, CourseUtils.thisUserCanScheduleRecordings(c.getParticipationSet(), user));
  }

  private void setDefaultsAsNeeded(final CourseOffering c) {
    c.setCourseOfferingId(c.getCourseOfferingId());
    final CapturePreferences cp = c.getCapturePreferences();
    c.setCapturePreferences(cp == null ? new CapturePreferences() : cp);
    final RecordingAvailability r = c.getCapturePreferences().getRecordingAvailability();
    c.getCapturePreferences().setRecordingAvailability(r == null ? RecordingAvailability.publicCreativeCommons : r);
    c.setParticipationSet(c.getParticipationSet());
  }

  public void setCourseManagementService(final CourseManagementService courseManagementService) {
    this.courseManagementService = courseManagementService;
  }

  public void setSecurityService(final SecurityService securityService) {
    this.securityService = securityService;
  }

  public void setSchedulerService(final SchedulerService schedulerService) {
    this.schedulerService = schedulerService;
  }

  public void setCaptureAgentStateService(final CaptureAgentStateService captureAgentStateService) {
    this.captureAgentStateService = captureAgentStateService;
  }

  public void setSeriesService(final SeriesService seriesService) {
    this.seriesService = seriesService;
  }
}
