/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.mvc;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.opencastproject.util.env.EnvironmentUtil;
import org.springframework.core.Conventions;
import org.springframework.validation.ObjectError;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * @author John Crossman
 */
public class ModelAndView {

  private final Configuration configuration = new Configuration();
  private final Map<String, Object> map;
  private final String viewName;

  public ModelAndView(final String viewName) throws IOException {
    map = new HashMap<String, Object>();
    this.viewName = viewName;
    //
    final File templateLoading = EnvironmentUtil.getFileUnderFelixHome("shared-resources/freemarker");
    if (!templateLoading.exists()) {
      throw new IOException("Directory for template-loading does not exist: " + templateLoading);
    }
    configuration.setDirectoryForTemplateLoading(templateLoading);
    final File xmlFile = EnvironmentUtil.getFileUnderFelixHome("shared-resources/messages-signUp.xml");
    if (xmlFile.exists()) {
      FileInputStream inputStream = null;
      try {
        final Properties p = new Properties();
        inputStream = FileUtils.openInputStream(xmlFile);
        p.loadFromXML(inputStream);
        final Enumeration<?> enumeration = p.propertyNames();
        while (enumeration.hasMoreElements()) {
          final Object key = enumeration.nextElement();
          map.put((String) key, p.get(key));
        }
      } finally {
        IOUtils.closeQuietly(inputStream);
      }
    }
  }

  public void render(final HttpServletResponse response) throws IOException {
    try {
      // TODO: What is Key.cssAndJsURLPrefix?
      // ANSWER: The following code would allow you to set up a local js and css server. The benefit is that
      // js and css files can modified and tested without re-deploying Matterhorn. See Freemarker templates to see how
      // Key.cssAndJsURLPrefix is used.
//      final String user = StringUtils.trimToNull(System.getenv().get("USER"));
//      final boolean isLocalEnv = EnvironmentUtil.getEnvironment().equals(Environment.local) && (user != null);
//      put(Key.cssAndJsURLPrefix, isLocalEnv ? "http://localhost/~john/matterhorn" : StringUtils.EMPTY);
      put(Key.cssAndJsURLPrefix, StringUtils.EMPTY);
      //
      final Template template = configuration.getTemplate(viewName + ".ftl");
      final StringWriter result = new StringWriter();
      final Map<String, Object> clone = new HashMap<String, Object>(map);
      clone.put(Key.messageSource.name(), this);
      template.process(clone, result);
      response.getOutputStream().write(result.toString().getBytes());
    } catch (final TemplateException e) {
      throw new IOException(e);
    }
  }

  public void message(final Key key, final Key propertyKey, final Object... args) {
    map.put(key.name(), getProperty(propertyKey.name(), args));
  }

  /**
   * DO NOT REMOVE. Referenced by template.
   * @param error null not allowed.
   * @return property value associated with error code
   */
  @SuppressWarnings("unused")
  public String getErrorMessage(final ObjectError error) {
    return getProperty(error.getCode(), error.getArguments());
  }

  public String getProperty(final String key, final Object... args) {
    final Object obj = map.get(key);
    final String property = (obj == null) ? null : StringUtils.trimToNull(obj.toString());
    String value = property == null ? key : property;
    int counter = 0;
    if (args != null) {
      for (final Object arg : args) {
        value = value.replace("{" + counter++ + "}", arg.toString());
      }
    }
    return value;
  }

  public ModelAndView put(final Object obj) {
    if (obj != null) {
      try {
        put(Conventions.getVariableName(obj), obj);
      } catch (final IllegalArgumentException e) {
        System.err.println(ExceptionUtils.getStackTrace(e));
      }
    }
    return this;
  }

  public ModelAndView put(final Key key, final Object obj) {
    return put(key.name(), obj);
  }

  private ModelAndView put(final String key, final Object obj) {
    if (obj != null) {
      map.put(key, obj);
    }
    return this;
  }

  public Map<String, Object> getMap() {
    return map;
  }

}
