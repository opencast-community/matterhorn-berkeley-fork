/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.impl.persistence;

import org.opencastproject.participation.model.CourseData;
import org.opencastproject.participation.model.CourseKey;

import java.util.Set;

/**
 * @author John Crossman
 */
public interface ProvidesCourseCatalogData {

  /**
   * Return all courses scheduled in specified rooms and term.
   * @return all course offerings listed in current UC Berkeley catalog.
   * @throws CourseDatabaseException if there is a problem communicating with the underlying data store
   */
  Set<CourseData> getCourseData();

  /**
   * @param courseKey never null
   * @return null when not found
   */
  CourseData getCourse(CourseKey courseKey);

}
