/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.model;

import org.apache.commons.lang.CharUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.opencastproject.salesforce.HasSalesforceId;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 * @author John Crossman
 */
public class Room implements HasSalesforceId {

  private String salesforceID;
  private String building;
  private String roomNumber;
  private RoomCapability capability;

  public Room() {
  }

  public Room(final String salesforceID, final String building, final String roomNumber, final RoomCapability capability) {
    this.salesforceID = salesforceID;
    this.building = StringUtils.trimToNull(building);
    this.roomNumber = trimAndFixRoomNumber(roomNumber);
    this.capability = capability;
  }

  public String getSalesforceID() {
    return salesforceID;
  }

  public void setSalesforceID(final String salesforceID) {
    this.salesforceID = salesforceID;
  }

  public String getBuilding() {
    return building;
  }

  public void setBuilding(final String building) {
    this.building = StringUtils.trimToNull(building);
  }

  public String getRoomNumber() {
    return roomNumber;
  }

  public void setRoomNumber(final String roomNumber) {
    this.roomNumber = trimAndFixRoomNumber(roomNumber);
  }

  public RoomCapability getCapability() {
    return capability;
  }

  public void setCapability(final RoomCapability capability) {
    this.capability = capability;
  }

  @Override
  public String toString() {
    return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
        .append("building", building)
        .append("roomNumber", roomNumber)
        .append("capability", capability).toString();
  }
  @Override
  public boolean equals(final Object o) {
    final boolean equals;
    if (o instanceof Room) {
      final Room that = (Room) o;
      // Ignore 'capability' and 'salesforceID' properties
      equals = StringUtils.equalsIgnoreCase(this.roomNumber, that.roomNumber)
          && (this.building != null)
          && (that.building != null)
          && normalizeBuilding(this.building).equals(normalizeBuilding(that.building));
    } else {
      equals = false;
    }
    return equals;
  }

  private String normalizeBuilding(final String building) {
    return (building == null) ? null : StringUtils.replaceChars(building.toLowerCase(), ' ', '-').trim();
  }

  @Override
  public int hashCode() {
    final String roomNumberLowercase = StringUtils.lowerCase(roomNumber);
    final String buildingLowercase = normalizeBuilding(building);
    return new HashCodeBuilder().append(roomNumberLowercase).append(buildingLowercase).toHashCode();
  }

  private String trimAndFixRoomNumber(final String value) {
    final String trimmedValue = StringUtils.trimToNull(value);
    final boolean isNumeric = NumberUtils.isNumber(trimmedValue) && !CharUtils
            .isAsciiAlpha(trimmedValue.charAt(trimmedValue.length() - 1));
    final int numericValue = isNumeric ? (int) Float.parseFloat(trimmedValue) : -1;
    return (numericValue == -1) ? trimmedValue : Integer.toString(numericValue);
  }

}
