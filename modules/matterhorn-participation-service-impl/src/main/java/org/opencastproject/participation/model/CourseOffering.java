/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.model;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.map.AnnotationIntrospector;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.xc.JaxbAnnotationIntrospector;
import org.opencastproject.salesforce.HasSalesforceId;

import java.io.IOException;
import java.io.StringWriter;

public final class CourseOffering extends CourseData implements HasSalesforceId {

  private String salesforceID;
  private boolean scheduled;
  private SalesforceProjectLifecycle stageOfLifecycle;
  private StageOfApproval stageOfApproval;
  private String adminScheduleOverride;
  private CapturePreferences capturePreferences;

  public String getSalesforceID() {
    return salesforceID;
  }

  public void setSalesforceID(final String salesforceID) {
    this.salesforceID = salesforceID;
  }

  public void setScheduled(final boolean scheduled) {
    this.scheduled = scheduled;
  }
  
  public boolean isScheduled() {
	  return scheduled;
  }

  public void setStageOfApproval(final StageOfApproval stageOfApproval) {
    this.stageOfApproval = stageOfApproval;
  }

  public SalesforceProjectLifecycle getStageOfLifecycle() {
    return stageOfLifecycle;
  }

  public void setStageOfLifecycle(final SalesforceProjectLifecycle stageOfLifecycle) {
    this.stageOfLifecycle = stageOfLifecycle;
  }

  public StageOfApproval getStageOfApproval() {
    return stageOfApproval;
  }

  public CapturePreferences getCapturePreferences() {
    return capturePreferences;
  }

  public void setCapturePreferences(final CapturePreferences capturePreferences) {
    this.capturePreferences = capturePreferences;
  }

  public String getAdminScheduleOverride() {
    return adminScheduleOverride;
  }

  public void setAdminScheduleOverride(String adminScheduleOverride) {
    this.adminScheduleOverride = adminScheduleOverride;
  }

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this);
  }

  public String toJson() throws IOException {
    final ObjectMapper mapper = new ObjectMapper();
    final AnnotationIntrospector introspector = new JaxbAnnotationIntrospector();
    mapper.getSerializationConfig().withAnnotationIntrospector(introspector);
    final StringWriter sw = new StringWriter();
    mapper.writeValue(sw, this);
    return sw.toString();
  }

}
