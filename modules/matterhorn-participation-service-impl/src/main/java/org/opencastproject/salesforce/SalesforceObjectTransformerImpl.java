/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.salesforce;

import org.apache.commons.lang.math.NumberUtils;
import org.opencastproject.participation.model.CanonicalCourse;
import org.opencastproject.participation.model.CapturePreferences;
import org.opencastproject.participation.model.CourseData;
import org.opencastproject.participation.model.CourseOffering;
import org.opencastproject.participation.model.DayOfWeek;
import org.opencastproject.participation.model.RecordingAvailability;
import org.opencastproject.participation.model.Instructor;
import org.opencastproject.participation.model.Participation;
import org.opencastproject.participation.model.RecordingType;
import org.opencastproject.participation.model.Room;
import org.opencastproject.participation.model.RoomCapability;
import org.opencastproject.participation.model.SalesforceProjectLifecycle;
import org.opencastproject.participation.model.Semester;
import org.opencastproject.participation.model.StageOfApproval;
import org.opencastproject.participation.model.Term;
import org.opencastproject.salesforce.entity.Contact;
import org.opencastproject.salesforce.entity.CourseField;
import org.opencastproject.salesforce.entity.Global;
import org.opencastproject.salesforce.entity.Location;
import org.opencastproject.salesforce.entity.RecordType;
import org.opencastproject.salesforce.entity.SemesterTerm;
import org.opencastproject.util.data.Collections;

import com.sforce.soap.partner.sobject.SObject;
import com.sforce.ws.bind.XmlObject;

import org.apache.commons.lang.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeSet;

/**
 * @author John Crossman
 */
public class SalesforceObjectTransformerImpl implements SalesforceObjectTransformer {

  @Override
  public CourseOffering getCourseOffering(final SObject sObject) {
    final Map<String, String> properties = getProperties(sObject);
    properties.putAll(getPropertiesFromChildNodes(sObject, Location.Room__r));
    properties.putAll(getPropertiesFromChildNodes(sObject, CourseField.Parent_Project__r));
    for (final SalesforceFieldInstructor field : SalesforceFieldInstructor.values()) {
      properties.putAll(getPropertiesFromChildNodes(sObject, create(field.name() + "__r")));
    }
    return getCourseOffering(properties);
  }

  @Override
  public Instructor getInstructor(final SObject sObject) {
    return getInstructor(getProperties(sObject));
  }

  @Override
  public Room getRoom(final SObject sObject) {
    return getRoom(getProperties(sObject));
  }

  @Override
  public Term getTerm(final SObject record) {
    final Map<String, String> properties = getProperties(record);
    final String semesterName = get(properties, SemesterTerm.Semester_Term__c);
    final int year = NumberUtils.toInt(get(properties, SemesterTerm.Semester_Year__c), 0);
    //
    return Term.construct((semesterName == null) ? null : Semester.valueOf(semesterName),
        year,
        get(properties, SemesterTerm.Semester_Start_Date__c),
        get(properties, SemesterTerm.Semester_End_Date__c),
        get(properties, Global.Id));
  }

  @Override
  public boolean isRecordingsScheduled(final SObject record) {
    final Map<String, String> properties = getProperties(record);
    return "true".equalsIgnoreCase(get(properties, CourseField.Recordings_Scheduled__c));
  }

  @Override
  public Map<CourseData, SObject> extractAssociatedCourseOfferings(final SObject[] sObjects, final Semester semester, final Integer termYear) {
    final Map<CourseData, SObject> map = new HashMap<CourseData, SObject>(sObjects.length);
    for (final SObject sObject : sObjects) {
      final CourseOffering courseOffering = getCourseOffering(sObject);
      final Term term = Term.construct(semester, termYear, null, null, null);
      courseOffering.setTerm(term);
      map.put(courseOffering, sObject);
    }
    return map;
  }

  @Override
  public Set<Participation> getParticipationSet(final SObject sObject) {
    return getParticipationSet(getProperties(sObject));
  }

  @Override
  public SObject getSObjectForCourseCreation(final CourseData courseData) {
    final SObject sObject = new SObject();
    sObject.setType(RecordType.opportunity.getSalesforceValue());
    SalesforceUtils.setField(sObject, Global.RecordTypeId, "01230000001GYjTAAW");
    SalesforceUtils.setField(sObject, CourseField.Parent_Project__c, courseData.getTerm().getSalesforceID());
    SalesforceUtils.setField(sObject, CourseField.CCN__c, Integer.toString(courseData.getCanonicalCourse().getCcn()));
    SalesforceUtils.setField(sObject, CourseField.StageName, SalesforceProjectLifecycle.prospecting.getSalesforceValue());
    //
    setFieldsForCourseUpdate(sObject, courseData);
    return sObject;
  }

  @Override
  public void setFieldsForCourseUpdate(final SObject sObject, final CourseData courseData) {
    SalesforceUtils.setField(sObject, Global.Name, courseData.getCanonicalCourse().getName());
    SalesforceUtils.setField(sObject, CourseField.Course_Title__c, courseData.getCanonicalCourse().getTitle());

    SalesforceUtils.setField(sObject, CourseField.Section__c, courseData.getSection());
    SalesforceUtils.setField(sObject, CourseField.Room__c, courseData.getRoom().getSalesforceID());
    //
    final SimpleDateFormat dateFormat = SalesforceUtils.getDateFormatConvention();
    final String semesterEndDate = courseData.getTerm().getEndDate();
    try {
      SalesforceUtils.setField(sObject, CourseField.CloseDate, dateFormat.parse(semesterEndDate));
    } catch (final ParseException e) {
      throw new SalesforceOperationException("Failure when parsing semesterEndDate: " + semesterEndDate, e);
    }
    SalesforceUtils.setField(sObject, CourseField.Students_Enrolled__c, courseData.getStudentCount());
    SalesforceUtils.setField(sObject, CourseField.Schedule_Days__c, Collections.mkString(courseData.getMeetingDays(), ";"));
    SalesforceUtils.setField(sObject, CourseField.Start_Time__c, SalesforceUtils.toSalesforceTimeFormat(courseData.getStartTime()));
    SalesforceUtils.setField(sObject, CourseField.End_Time__c, SalesforceUtils.toSalesforceTimeFormat(courseData.getEndTime()));
    // We MUST iterate through all six fields even if the course only has a single instructor.
    final SortedMap<SalesforceFieldInstructor, Participation> participationMap =
        SalesforceUtils.getValidatedSalesforceIndexMap(courseData.getParticipationSet());
    for (final SalesforceFieldInstructor field : SalesforceFieldInstructor.values()) {
      if (participationMap.containsKey(field)) {
        final Participation participation = participationMap.get(field);
        final Instructor instructor = participation.getInstructor();
        if (instructor.getSalesforceID() == null) {
          throw new IllegalStateException("Instructor " + instructor + " does not exist in Salesforce and cannot be attached to course " + courseData);
        }
        SalesforceUtils.setField(sObject, getInstructorC(field), instructor.getSalesforceID());
        final HasSalesforceFieldName approvalFieldName = SalesforceUtils.getInstructorApprovalFieldName(field);
        SalesforceUtils.setField(sObject, approvalFieldName, participation.isApproved());
      } else {
        SalesforceUtils.setField(sObject, getInstructorC(field), " ");
        final HasSalesforceFieldName approvalField = create(SalesforceUtils.getInstructorApprovalFieldName(field).name());
        SalesforceUtils.setField(sObject, approvalField, Boolean.FALSE);
      }
    }
  }

  private HasSalesforceFieldName getInstructorC(final SalesforceFieldInstructor field) {
    return new HasSalesforceFieldName() {
      @Override
      public String name() {
        return field.name() + "__c";
      }
    };
  }

  private CourseOffering getCourseOffering(final Map<String, String> properties) {
    final CourseOffering courseOffering = new CourseOffering();
    //
    final Integer ccn = getInteger(get(properties, CourseField.CCN__c));
    final String name = get(properties, Global.Name);
    final String title = get(properties, CourseField.Course_Title__c);
    final CanonicalCourse canonicalCourse = new CanonicalCourse(ccn, name, title);
    courseOffering.setCanonicalCourse(canonicalCourse);
    final String studentsEnrolledRaw = get(properties, CourseField.Students_Enrolled__c);
    final String studentsEnrolled = StringUtils.trimToNull(StringUtils.substringBefore(studentsEnrolledRaw, "."));
    courseOffering.setStudentCount(StringUtils.isNumeric(studentsEnrolled) ? getInteger(studentsEnrolled) : null);
    //
    courseOffering.setSalesforceID(get(properties, Global.Id));
    courseOffering.setCourseOfferingId(get(properties, CourseField.CourseOfferingID__c));
    final String startTime = SalesforceUtils.toMilitaryTime(get(properties, CourseField.Start_Time__c));
    courseOffering.setStartTime(startTime);
    final String endTime = SalesforceUtils.toMilitaryTime(get(properties, CourseField.End_Time__c));
    courseOffering.setEndTime(endTime);
    //
    final String semesterName = get(properties, CourseField.Parent_Project__r, SemesterTerm.Semester_Term__c);
    final int year = NumberUtils.toInt(get(properties, CourseField.Parent_Project__r, SemesterTerm.Semester_Year__c), 0);
    final Term term = Term.construct(
        (semesterName == null) ? null : Semester.valueOf(semesterName),
        (year == 0) ? null : year,
        get(properties, CourseField.Parent_Project__r, SemesterTerm.Semester_Start_Date__c),
        get(properties, CourseField.Parent_Project__r, SemesterTerm.Semester_End_Date__c),
        null);
    courseOffering.setTerm(term);
    //
    courseOffering.setSection(get(properties, CourseField.Section__c));
    final List<DayOfWeek> days = split(get(properties, CourseField.Schedule_Days__c), ';', DayOfWeek.values());
    courseOffering.setMeetingDays(days);

    final Boolean scheduled = "true".equalsIgnoreCase(get(properties, CourseField.Recordings_Scheduled__c));
    courseOffering.setScheduled(scheduled);
    courseOffering.setStageOfApproval(findMatch(get(properties, CourseField.Approval_Status__c), StageOfApproval.values()));
    courseOffering.setStageOfLifecycle(findMatch(get(properties, CourseField.StageName), SalesforceProjectLifecycle.values()));
    courseOffering.setCapturePreferences(getCapturePreferences(properties));
    courseOffering.setAdminScheduleOverride(get(properties, CourseField.admin_schedule_override__c));
    courseOffering.setRoom(getRoomFromNestedProperties(properties));
    //
    courseOffering.setParticipationSet(getParticipationSet(properties));
    return courseOffering;
  }

  private HasSalesforceFieldName create(final String name) {
    return new HasSalesforceFieldName() {
      @Override
      public String name() {
        return name;
      }
    };
  }

  private Set<Participation> getParticipationSet(final Map<String, String> properties) {
    final Set<Participation> participationSet = new TreeSet<Participation>(new Comparator<Participation>() {
      @Override
      public int compare(final Participation p1, Participation p2) {
        return p1.getSalesforceField().getIdentifier() - p2.getSalesforceField().getIdentifier();
      }
    });
    for (final SalesforceFieldInstructor field : SalesforceFieldInstructor.values()) {
      final Participation participation = new Participation();
      final Instructor instructor = new Instructor();
      instructor.setSalesforceID(properties.get(field.name() + "__c"));
      if (instructor.getSalesforceID() != null) {
        instructor.setCalNetUID(properties.get(field.name() + "__r.Calnet_UID__c"));
        participation.setApproved("true".equalsIgnoreCase(properties.get(field.name() + "_Approval__c")));
        instructor.setFirstName(properties.get(field.name() + "__r.FirstName"));
        instructor.setLastName(properties.get(field.name() + "__r.LastName"));
        instructor.setEmail(properties.get(field.name() + "__r.Email"));
        participation.setInstructor(instructor);
        participation.setSalesforceField(field);
        participationSet.add(participation);
      }
    }
    return participationSet;
  }

  private CapturePreferences getCapturePreferences(final Map<String, String> properties) {
    final RecordingType recordingType = findMatch(properties, CourseField.Recording_Type__c, RecordingType.values());
    final RecordingAvailability recordingAvailability = findMatch(properties, CourseField.Recording_Availability__c, RecordingAvailability.values());
    final String delayPublishBy = get(properties, CourseField.Publish_Delay_Days__c);
    final Integer delayPublishByDays = (StringUtils.isBlank(delayPublishBy)) ? 0 : getInteger(delayPublishBy);
    return new CapturePreferences(recordingType, recordingAvailability, delayPublishByDays);
  }

  private <T extends RepresentsSalesforceField> T findMatch(final Map<String, String> properties, final HasSalesforceFieldName fieldName, final T[] values) {
    return SalesforceUtils.findSalesforceMatch(properties, fieldName, values);
  }

  private static <T extends RepresentsSalesforceField> T findMatch(final String next, final T[] entries) {
    return SalesforceUtils.findSalesforceMatch(next, entries);
  }

  private Room getRoomFromNestedProperties(final Map<String, String> properties) {
    final Room room = new Room();
    room.setSalesforceID(get(properties, Global.Id));
    room.setBuilding(get(properties, Location.Room__r, Location.Building__c));
    room.setRoomNumber(get(properties, Location.Room__r, Location.Room_Number_Text__c));
    room.setSalesforceID(get(properties, Location.Room__r, Global.Id));
    final RoomCapability capability = findMatch(get(properties, Location.Room__r, CourseField.Recording_Capabilities__c), RoomCapability.values());
    room.setCapability(capability);
    return room;
  }

  private Room getRoom(final Map<String, String> properties) {
    final Room room = new Room();
    room.setSalesforceID(get(properties, Global.Id));
    room.setBuilding(get(properties, Location.Building__c));
    room.setRoomNumber(get(properties, Location.Room_Number_Text__c));
    room.setSalesforceID(get(properties, Global.Id));
    final RoomCapability capability = findMatch(get(properties, CourseField.Recording_Capabilities__c), RoomCapability.values());
    room.setCapability(capability);
    return room;
  }

  private Instructor getInstructor(final Map<String, String> properties) {
    final Instructor instructor = new Instructor();
    instructor.setSalesforceID(get(properties, Global.Id));
    instructor.setFirstName(get(properties, Contact.FirstName));
    instructor.setLastName(get(properties, Contact.LastName));
    instructor.setEmail(get(properties, Contact.Email));
    instructor.setCalNetUID(get(properties, Contact.Calnet_UID__c));
    instructor.setRole(get(properties, Contact.Role__c));
    instructor.setDepartment(get(properties, Contact.Department));
    return instructor;
  }

  private Map<String, String> getPropertiesFromChildNodes(final SObject sObject, final HasSalesforceFieldName... childTypes) {
    final Map<String, String> properties = new HashMap<String, String>();
    for (final HasSalesforceFieldName childType : childTypes) {
      final Iterator<XmlObject> children = sObject.getChildren();
      if (children != null) {
        while (children.hasNext()) {
          final XmlObject child = children.next();
          final String localPart = SalesforceUtils.getLocalPart(child);
          if (childType.name().equals(localPart)) {
            final Map<String, String> childProperties = getProperties(child);
            for (final String childKey : childProperties.keySet()) {
              properties.put(localPart + '.' + childKey, childProperties.get(childKey));
            }
          }
        }
      }
    }
    return properties;
  }

  /**
   * @param xmlObject never null
   * @return see {@link SalesforceUtils#getProperties(com.sforce.ws.bind.XmlObject)}
   */
  private Map<String, String> getProperties(final XmlObject xmlObject) {
    return SalesforceUtils.getProperties(xmlObject);
  }

  /**
   * @param listAsString never null
   * @param delimiter what separates entries
   * @param values one or more enum entries.
   * @param <T> type
   * @return result of split
   */
  private <T extends RepresentsSalesforceField> List<T> split(final String listAsString, final char delimiter, final T[] values) {
    if (listAsString == null) {
      return null;
    }
    final List<T> list = new LinkedList<T>();
    final String[] split = StringUtils.split(listAsString, delimiter);
    for (final String next : split) {
      final T match = SalesforceUtils.findSalesforceMatch(next, values);
      if (match != null) {
        list.add(match);
      }
    }
    return list;
  }

  private Integer getInteger(final String value) {
    Integer result;
    if (NumberUtils.isNumber(value)) {
      result = new Integer(value);
    } else {
      final Float aFloat = org.apache.commons.lang.math.NumberUtils.createFloat(value);
      result = (aFloat == null) ? null : aFloat.intValue();
    }
    return result;
  }

  private String get(final Map<String, String> properties, final HasSalesforceFieldName parentField, final HasSalesforceFieldName field) {
    return properties.get(parentField.name() + '.' + field.name());
  }

  private static String get(final Map<String, String> properties, final HasSalesforceFieldName field) {
    return SalesforceUtils.get(properties, field);
  }

}
