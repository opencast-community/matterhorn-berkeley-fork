/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.salesforce;

import org.opencastproject.notify.Notifier;

import com.sforce.soap.partner.Connector;
import com.sforce.soap.partner.GetServerTimestampResult;
import com.sforce.soap.partner.PartnerConnection;
import com.sforce.soap.partner.fault.ApiFault;
import com.sforce.soap.partner.fault.ExceptionCode;
import com.sforce.ws.ConnectionException;
import com.sforce.ws.ConnectorConfig;

import org.apache.commons.lang.time.DateFormatUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;

/**
 * @author John Crossman
 */
class SalesforceConnectionFactory {

  private final Logger logger;
  private final Notifier notifier;
  private final Properties connectorProperties;
  private PartnerConnection connection;

  SalesforceConnectionFactory(Properties connectorProperties, Notifier notifier, Logger logger) {
    this.connectorProperties = connectorProperties;
    this.notifier = notifier;
    this.logger = logger;
  }

  SalesforceConnectionFactory(final Properties connectorProperties, final Notifier notifier) {
    this(connectorProperties, notifier, LoggerFactory.getLogger(SalesforceConnectorServiceImpl.class));
  }

  /**
   * Package-local for security reasons.
   * @return connection after login
   */
  PartnerConnection getConnection() {
    try {
      connection = (connection == null) ? newConnection() : connection;
      try {
        // Verify valid Session ID before handing over the connection to client code.
        connection.getServerTimestamp();
      } catch (final ApiFault e) {
        final ExceptionCode exceptionCode = e.getExceptionCode();
        if (exceptionCode == null) {
          throw e;
        } else {
          switch (exceptionCode) {
            case INVALID_SESSION_ID:
              connection = newConnection();
              final GetServerTimestampResult serverTimestamp = connection.getServerTimestamp();
              logger.info("Renewed Salesforce connection at "
                  + DateFormatUtils.ISO_DATETIME_FORMAT.format(serverTimestamp.getTimestamp()));
              break;
            default:
              throw e;
          }
        }
      }
      return connection;
    } catch (final ConnectionException e) {
      final String message = "Unable to create Salesforce connection: " + e.getMessage();
      notifier.notifyEngineeringTeam(message, e);
      throw new SalesforceConnectionException(message, e);
    }
  }

  private PartnerConnection newConnection() throws ConnectionException {
    final ConnectorConfig connectorConfig = SalesforceUtils.getConnectorConfig(connectorProperties);
    return Connector.newConnection(connectorConfig);
  }

}
