/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.salesforce.entity;

import org.opencastproject.salesforce.HasSalesforceFieldName;

/**
 * Salesforce fields, both custom and standard.
 * @author John Crossman
 */
public enum CourseField implements HasSalesforceFieldName {

  Section__c, CCN__c, CourseOfferingID__c, Course_Title__c, Schedule_Days__c, End_Time__c, Students_Enrolled__c,
  Start_Time__c, Recording_Type__c, Recording_Availability__c, Publish_Delay_Days__c, Recording_Capabilities__c,
  Recordings_Scheduled__c, StageName, Approval_Status__c, Parent_Project__r, CloseDate, Room__c, Parent_Project__c,
  admin_schedule_override__c;

}
