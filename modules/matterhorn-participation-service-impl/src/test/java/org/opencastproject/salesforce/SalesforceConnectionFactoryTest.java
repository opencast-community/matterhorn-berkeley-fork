/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.salesforce;

import com.sforce.soap.partner.PartnerConnection;
import com.sforce.ws.ConnectionException;
import org.junit.Ignore;
import org.junit.Test;
import org.opencastproject.notify.Notifier;
import org.opencastproject.participation.UnitTestUtils;

import java.io.IOException;
import java.util.Properties;

import static junit.framework.Assert.assertNotNull;
import static org.easymock.EasyMock.createNiceMock;

/**
 * @author John Crossman
 */
@Ignore
public class SalesforceConnectionFactoryTest {

  @Test
  public void testExpiresConnection() throws ConnectionException, IOException {
    final Notifier notifier = createNiceMock(Notifier.class);
    final Properties connectionProperties = UnitTestUtils.getSalesforceConnectionProperties();
    SalesforceConnectionFactory factory = new SalesforceConnectionFactory(connectionProperties, notifier);
    PartnerConnection connection = factory.getConnection();
    assertNotNull(connection.getServerTimestamp());
    connection.logout();
    connection = factory.getConnection();
    assertNotNull(connection.getServerTimestamp());
  }

}
