/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.salesforce;

import static org.junit.Assert.assertTrue;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.opencastproject.participation.model.DayOfWeek;
import org.opencastproject.participation.model.RecordingAvailability;
import org.opencastproject.participation.model.Instructor;
import org.opencastproject.participation.model.License;
import org.opencastproject.participation.model.Participation;
import org.opencastproject.participation.model.RecordingType;
import org.opencastproject.participation.model.RoomCapability;
import org.opencastproject.participation.model.SalesforceProjectLifecycle;
import org.opencastproject.participation.model.Semester;
import org.opencastproject.participation.model.StageOfApproval;

import org.junit.Test;

import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.SortedMap;

/**
 * @author John Crossman
 */
public class SalesforceUtilsTest {

  @Test
  public void testFindSalesforceMatch() {
    assertNull(SalesforceUtils.findSalesforceMatch(null, License.values()));
    assertNull(SalesforceUtils.findSalesforceMatch(null, DayOfWeek.values()));
    assertNull(SalesforceUtils.findSalesforceMatch(null, RecordingAvailability.values()));
    assertNull(SalesforceUtils.findSalesforceMatch(null, RecordingType.values()));
    assertNull(SalesforceUtils.findSalesforceMatch(null, RoomCapability.values()));
    assertNull(SalesforceUtils.findSalesforceMatch(null, SalesforceProjectLifecycle.values()));
    assertNull(SalesforceUtils.findSalesforceMatch(null, Semester.values()));
    assertNull(SalesforceUtils.findSalesforceMatch(null, StageOfApproval.values()));
  }

  @Test
  public void testMilitaryToSalesforceBefore1AM() {
    assertEquals("00:30a", SalesforceUtils.toSalesforceTimeFormat("0030"));
  }

  @Test
  public void testMilitaryToSalesforceAM() {
    assertEquals("09:30a", SalesforceUtils.toSalesforceTimeFormat("0930"));
  }

  @Test
  public void testMilitaryToSalesforcePM() {
    assertEquals("02:00p", SalesforceUtils.toSalesforceTimeFormat("1400"));
    assertEquals("11:00p", SalesforceUtils.toSalesforceTimeFormat("2300"));
    assertEquals("12:30p", SalesforceUtils.toSalesforceTimeFormat("1230"));
  }

  @Test
  public void testSalesforceToMilitaryBefore1AM() {
    assertEquals("0045", SalesforceUtils.toMilitaryTime("00:45a"));
  }

  @Test
  public void testSalesforceToMilitaryAM() {
    assertEquals("0930", SalesforceUtils.toMilitaryTime("9:30a"));
    assertEquals("0930", SalesforceUtils.toMilitaryTime("09:30a"));
  }

  @Test
  public void testSalesforceToMilitaryPM() {
    assertEquals("1400", SalesforceUtils.toMilitaryTime("2:00p"));
    assertEquals("1200", SalesforceUtils.toMilitaryTime("12:00p"));
    assertEquals("1250", SalesforceUtils.toMilitaryTime("12:50p"));
    assertEquals("2300", SalesforceUtils.toMilitaryTime("11:00p"));
  }

  @Test
  public void testGetSafeEmailAddress() {
    final Instructor instructor = new Instructor();
    final String email = "joe@instructor.com";
    instructor.setEmail(email);
    final String safeEmailAddress = SalesforceUtils.getSafeEmailAddress(instructor);
    assertFalse(email.equalsIgnoreCase(safeEmailAddress));
  }
  
  @Test
  public void conformsToSalesforceEmailValidationRequirementWhenPassedNullEmailValue() {
    final String expectedSafeEmailAddress = SalesforceUtils.DEFAULT_EMAIL + "-DISABLE";
    final Instructor instructor = new Instructor();
    instructor.setEmail(null);
    final String safeEmailAddress = SalesforceUtils.getSafeEmailAddress(instructor);
    assertTrue(expectedSafeEmailAddress.equalsIgnoreCase(safeEmailAddress));
  }

  @Test(expected = IllegalStateException.class)
  public void testGetAvailableWhenNothingAvailable() {
    final Set<Participation> set = createParticipationSet(1, 2, 3, 4, 5, 6);
    final SalesforceFieldInstructor[] array = SalesforceUtils.getAvailableInstructorFields(set);
    assertEquals(0, array.length);
  }

  @Test(expected = IllegalStateException.class)
  public void testGetAvailableWhenSixthPositionTaken() {
    final Set<Participation> set = createParticipationSet(6);
    final SalesforceFieldInstructor[] array = SalesforceUtils.getAvailableInstructorFields(set);
    assertEquals(0, array.length);
  }

  @Test
  public void testGetAvailableWhenFiveAvailable() {
    final Set<Participation> set = createParticipationSet(1);
    final SalesforceFieldInstructor[] array = SalesforceUtils.getAvailableInstructorFields(set);
    assertEquals(5, array.length);
    assertEquals(2, array[0].getIdentifier());
    assertEquals(3, array[1].getIdentifier());
    assertEquals(4, array[2].getIdentifier());
    assertEquals(5, array[3].getIdentifier());
    assertEquals(6, array[4].getIdentifier());
  }

  @Test
  public void testGetAvailableWhenThreeAvailable() {
    final Set<Participation> set = createParticipationSet(1, 3);
    final SalesforceFieldInstructor[] array = SalesforceUtils.getAvailableInstructorFields(set);
    assertEquals(3, array.length);
    assertEquals(4, array[0].getIdentifier());
    assertEquals(5, array[1].getIdentifier());
    assertEquals(6, array[2].getIdentifier());
  }

  @Test
  public void testGetAvailableWhenOneAvailable() {
    final Set<Participation> set = createParticipationSet(2, 4, 5);
    final SalesforceFieldInstructor[] array = SalesforceUtils.getAvailableInstructorFields(set);
    assertEquals(1, array.length);
    assertEquals(6, array[0].getIdentifier());
  }

  @Test
  public void testGetAvailableWhenAllAvailable() {
    final Set<Participation> set = createParticipationSet();
    final SalesforceFieldInstructor[] array = SalesforceUtils.getAvailableInstructorFields(set);
    assertEquals(6, array.length);
    for (final SalesforceFieldInstructor field : array) {
      assertEquals(field, array[field.getIdentifier() - 1]);
    }
  }

  @Test
  public void testGetValidatedSalesforceSortedMap() {
    final Set<Participation> set = createParticipationSet(1, 2, 4, 5, 6);
    final SortedMap<SalesforceFieldInstructor, Participation> map = SalesforceUtils.getValidatedSalesforceIndexMap(set);
    assertEquals(5, map.size());
    final Iterator<Participation> iterator = map.values().iterator();
    for (final SalesforceFieldInstructor value : SalesforceFieldInstructor.values()) {
      if (!SalesforceFieldInstructor.Instructor_3.equals(value)) {
        final Participation next = iterator.next();
        assertEquals(value, next.getSalesforceField());
      }
    }
  }

  @Test
  public void testGetValidatedSalesforce() {
    final Set<Participation> set = createParticipationSet(3, 2, 6, 4, 1, 5);
    final SortedMap<SalesforceFieldInstructor, Participation> map = SalesforceUtils.getValidatedSalesforceIndexMap(set);
    assertEquals(6, map.size());
    // Index in Salesforce starts with "1"
    int index = 1;
    for (final Participation participation : map.values()) {
      assertNotNull(participation);
      assertEquals(index++, participation.getSalesforceField().getIdentifier());
    }
  }

  private Set<Participation> createParticipationSet(final Integer... identifiers) {
    final Set<Participation> set = new LinkedHashSet<Participation>();
    final long time = new Date().getTime();
    for (final Integer identifier : identifiers) {
      final SalesforceFieldInstructor field = getSalesforceFieldInstructorByIdentifier(identifier);
      final String calNetUID = (field == null) ? time % 2000 + "" : time % 10000 + field.getIdentifier() + "";
      set.add(new Participation(field, new Instructor(calNetUID), false));
    }
    return set;
  }

  private SalesforceFieldInstructor getSalesforceFieldInstructorByIdentifier(final Integer identifier) {
    if (identifier != null) {
      for (final SalesforceFieldInstructor field : SalesforceFieldInstructor.values()) {
        if (identifier.equals(field.getIdentifier())) {
          return field;
        }
      }
    }
    return null;
  }

}
