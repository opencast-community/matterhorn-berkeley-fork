/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.model;

import org.junit.Test;
import org.opencastproject.participation.CourseUtils;

import java.util.LinkedList;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;

/**
 * @author John Crossman
 */
public class CrossListedCourseComparatorTest {

  @Test
  public void testFourCrossListings() {
    final SortedSet<CourseData> set = new TreeSet<CourseData>(new CrossListedCourseComparator());
    set.add(getCourseData(77286, "Energy and Society", "PUB POL", "Public Policy", "C284", "001"));
    set.add(getCourseData(27240, "Energy and Society", "ENE,RES", "Energy and Resources Group", "C200", "001"));
    final CourseData expectedFirst = getCourseData(77130, "Energy and Society", "PUB POL", "Public Policy", "C184", "001");
    set.add(expectedFirst);
    set.add(getCourseData(27174, "Energy and Society", "ENE,RES", "Energy and Resources Group", "C100", "001"));
    //
    verify(set, 4, expectedFirst);
  }

  @Test
  public void testUnofficialCrossListing() {
    final SortedSet<CourseData> set = new TreeSet<CourseData>(new CrossListedCourseComparator());
    set.add(getCourseData(77130, "Legal Studies", "LEGALST", "Directed Group Study", "198", "004"));
    final CourseData expectedFirst = getCourseData(51517, "Legal Studies", "LEGALST", "Directed Group Study", "98", "004");
    set.add(expectedFirst);
    //
    verify(set, 2, expectedFirst);
  }

  @Test
  public void testNullSectionNumber() {
    final SortedSet<CourseData> set = new TreeSet<CourseData>(new CrossListedCourseComparator());
    set.add(getCourseData(77130, "Legal Studies", "LEGALST", "Directed Group Study", "198", "004"));
    final CourseData expectedFirst = getCourseData(51517, "Legal Studies", "LEGALST", "Directed Group Study", "234", null);
    set.add(expectedFirst);
    //
    verify(set, 2, expectedFirst);
  }

  private void verify(final SortedSet<CourseData> set, final int expectedSize, final CourseData expectedFirst) {
    assertEquals(expectedSize, set.size());
    assertSame(expectedFirst, set.first());
    //
    final List<String> keys = new LinkedList<String>();
    for (final CourseData next : set) {
      final String seriesId = CourseUtils.getSeriesId(next);
      assertFalse(keys.contains(seriesId));
      keys.add(seriesId);
    }
  }

  private CourseData getCourseData(final int ccn, final String title, final String deptName, final String deptDescription, final String catalogId, final String sectionNum) {
    final String name = (sectionNum == null)
            ? deptDescription + ' ' + catalogId
            : deptDescription + ' ' + catalogId + ", " + sectionNum;
    final CourseData c = new CourseData();
    c.setTerm(Term.construct(Semester.Fall, 2014, null, null, null));
    c.setCanonicalCourse(new CanonicalCourse(ccn, name, title));
    c.setDeptName(deptName);
    c.setDeptDescription(deptDescription);
    c.setCatalogId(catalogId);
    c.setSection(sectionNum);
    return c;
  }

}
