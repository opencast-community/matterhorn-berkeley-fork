/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.impl;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.opencastproject.participation.UnitTestUtils;
import org.opencastproject.participation.impl.persistence.FileBasedCourseCatalogData;
import org.opencastproject.participation.model.CourseOffering;
import org.opencastproject.participation.model.DayOfWeek;
import org.opencastproject.participation.model.Instructor;
import org.opencastproject.participation.model.Room;
import org.opencastproject.salesforce.SalesforceConnectorServiceImpl;
import org.opencastproject.util.NotFoundException;
import org.osgi.service.cm.ConfigurationException;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * @author John Crossman
 */
@Ignore
public class CourseDataMoverFileBasedTest {

  private CourseManagementServiceImpl courseManagementService;

  @Before
  public void before() throws IOException, ConfigurationException {
    final VoidNotifier notifier = new VoidNotifier();
    courseManagementService = new CourseManagementServiceImpl();
    final SalesforceConnectorServiceImpl connectorService = new SalesforceConnectorServiceImpl();
    connectorService.setNotifier(notifier);
    connectorService.updated(UnitTestUtils.getSalesforceConnectionProperties());
    courseManagementService.setSalesforceConnectorService(connectorService);
    //
    final CourseDataMover dataMover = new CourseDataMover();
    dataMover.setCourseManagementService(courseManagementService);
    final Resource resource = new ClassPathResource("/courseCatalogDataTest/courses/modern-physics-1.xml");
    final File coursesDirectory = new File(resource.getFile().getParent());
    dataMover.setCourseDatabase(new FileBasedCourseCatalogData(coursesDirectory.getParent()));
    dataMover.setNotifier(notifier);
    dataMover.setThrowExceptionInRunMethod(true);
    //
    dataMover.run();
  }

  @Ignore
  @Test
  public void testEmail() throws NotFoundException {
    final HashSet<Instructor> set = new HashSet<Instructor>();
    final Instructor i = new Instructor();
    i.setEmail("duijnstee@berkeley.edu-DISABLE");
    i.setCalNetUID("891046");
    i.setFirstName("Ivo");
    i.setLastName("Duijnstee");
    i.setDepartment("Integrative Biology");
    set.add(i);
    final List<Instructor> instructorList = courseManagementService.createOrUpdateInstructors(set);
    assertTrue(!instructorList.isEmpty());
    final Instructor instructor = instructorList.get(0);
    assertNotNull(instructor.getSalesforceID());
  }

  @Test
  public void testModernPhysics2() {
    final Room room = new Room(null, "Dwinelle", "155", null);
    assertCourseOffing("2013D83746", "Modern Physics 2", room, DayOfWeek.Tuesday, DayOfWeek.Thursday);
  }

  private void assertCourseOffing(final String courseOfferingID, final String title, final Room room, final DayOfWeek... days) {
    final CourseOffering courseOffering = courseManagementService.getCourseOffering(courseOfferingID);
    assertEquals(room, courseOffering.getRoom());
    assertEquals(title, courseOffering.getCanonicalCourse().getTitle());
    assertEquals("1000", courseOffering.getStartTime());
    assertEquals("1430", courseOffering.getEndTime());
    final List<DayOfWeek> meetingDays = courseOffering.getMeetingDays();
    assertEquals(days.length, meetingDays.size());
    for (final DayOfWeek day : days) {
      assertTrue(meetingDays.contains(day));
    }
  }

}
