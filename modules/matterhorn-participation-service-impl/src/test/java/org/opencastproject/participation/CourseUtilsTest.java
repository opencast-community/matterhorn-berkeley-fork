/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation;

import net.fortuna.ical4j.model.DateList;
import net.fortuna.ical4j.model.DateTime;
import net.fortuna.ical4j.model.Recur;
import net.fortuna.ical4j.model.WeekDay;
import net.fortuna.ical4j.model.parameter.Value;
import net.fortuna.ical4j.model.property.RRule;

import org.junit.Test;

import org.opencastproject.metadata.dublincore.DCMIPeriod;
import org.opencastproject.metadata.dublincore.EncodingSchemeUtils;
import org.opencastproject.participation.model.CanonicalCourse;
import org.opencastproject.participation.model.CourseData;
import org.opencastproject.participation.model.CourseKey;
import org.opencastproject.participation.model.DayOfWeek;
import org.opencastproject.participation.model.Instructor;
import org.opencastproject.participation.model.Participation;
import org.opencastproject.participation.model.Room;
import org.opencastproject.participation.model.RoomCapability;
import org.opencastproject.participation.model.Semester;
import org.opencastproject.participation.model.Term;
import org.opencastproject.salesforce.SalesforceFieldInstructor;
import org.opencastproject.scheduler.api.SchedulerException;
import org.opencastproject.security.api.User;
import org.opencastproject.util.data.Collections;

import java.text.ParseException;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNull;
import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * @author John Crossman
 */
public class CourseUtilsTest {
  
  @Test
  public void testGetDistinctInstructors() {
    final Participation p = new Participation();
    final Instructor instructor = new Instructor();
    instructor.setCalNetUID(new Object().hashCode() + "");
    p.setInstructor(instructor);
    final CourseData c1 = createCourseData(p);
    //
    final Participation[] pArray = new Participation[6];
    for (int index = 0; index < 3; index++) {
      final Participation p1 = new Participation();
      final Instructor i1 = new Instructor();
      i1.setCalNetUID(index + "");
      p1.setInstructor(i1);
      pArray[index] = p1;
      // This next instructor will be a dupe of others created in this loop so they should have no effect on
      // count of distinct instructors
      final Participation p2 = new Participation();
      final Instructor i2 = new Instructor();
      i2.setCalNetUID((index + 1) % 2 + "");
      p2.setInstructor(i2);
      pArray[index + 3] = p2;
    }
    final CourseData c2 = createCourseData(pArray);
    //
    final Set<CourseData> courseDataList = Collections.createSet(c1, c2);
    final Set<Instructor> set = CourseUtils.extractInstructorSet(courseDataList);
    assertEquals(4, set.size());
  }

  @Test
  public void testIsRoomCapable() {
    assertFalse(CourseUtils.isRoomCapable(null));
    assertFalse(CourseUtils.isRoomCapable(new Room()));
    final Room room = new Room();
    room.setCapability(RoomCapability.screencastAndVideo);
    assertTrue(CourseUtils.isRoomCapable(room));
  }

  @Test
  public void testGetCaptureAgentName() {
    final Room room = new Room();
    assertNull(CourseUtils.getCaptureAgentName(new Room()));
    assertEquals("hearst-field-annex", CourseUtils.getCaptureAgentName(new Room(null, "Hearst Field Annex", null, null)));
    assertEquals("hearst-field-annex-a1", CourseUtils.getCaptureAgentName(new Room(null, "Hearst Field Annex", "A1", null)));
  }

  @Test
  public void testRemoveCoursesWithUnrecognizedRooms() {
    final int arbitraryCCN = this.hashCode();
    final List<Room> allRooms = new LinkedList<Room>();
    final Room evans160 = new Room("salesforceID-1", "Evans", "160", RoomCapability.screencast);
    allRooms.add(evans160);
    final Set<CourseData> courseDataSet = new HashSet<CourseData>();
    final Term term = Term.construct(Semester.Fall, 2014, null, null, null);
    courseDataSet.add(createCourseData(term, createCanonicalCourse(arbitraryCCN + 1), evans160));
    final Room unknownRoom = new Room("salesforceID-1", "Evans", "160", null);
    courseDataSet.add(createCourseData(term, createCanonicalCourse(arbitraryCCN + 1), unknownRoom));
    //
    final Set<CourseData> result = CourseUtils.removeCoursesWithUnrecognizedRooms(courseDataSet, allRooms);
    assertEquals(1, result.size());
    final CourseData actual = result.iterator().next();
    assertEquals(evans160, actual.getRoom());
  }

  @Test
  public void testSetSalesforceIDs() {
    final Set<CourseData> courseDataSet = new HashSet<CourseData>();
    final List<Room> allRooms = new LinkedList<Room>();
    final List<Instructor> instructorList = new LinkedList<Instructor>();
    final Term term = Term.construct(Semester.Fall, 2014, null, null, null);
    term.setSalesforceID("term-123");
    final int arbitraryCCN = this.hashCode();
    for (int index = 0; index < 3; index++) {
      final Room room = new Room("salesforceID-" + index, "building-" + index, index + 10 + "", RoomCapability.screencast);
      allRooms.add(room);
      final CourseData courseData = new CourseData();
      courseData.setCanonicalCourse(createCanonicalCourse(arbitraryCCN + index));
      courseData.setTerm(Term.construct(term.getSemester(), term.getYear(), null, null, null));
      courseData.setRoom(clone(room));
      // Arbitrary number of instructors
      final int instructorCount = (index % 2 == 0) ? 1 : 2;
      final long arbitraryID = new Date().getTime();
      for (int instructorIndex = 0; instructorIndex < instructorCount; instructorIndex++) {
        final long nextID = arbitraryID + instructorIndex;
        final Instructor instructor = new Instructor();
        instructor.setCalNetUID("calNetUID-" + nextID);
        instructor.setSalesforceID("salesforce-" + nextID);
        instructorList.add(instructor);
        final Instructor clone = new Instructor();
        clone.setCalNetUID(instructor.getCalNetUID());
        final Participation participation = new Participation(SalesforceFieldInstructor.Instructor_1, clone, false);
        courseData.setParticipationSet(Collections.set(participation));
      }
      courseDataSet.add(courseData);
    }
    CourseUtils.setSalesforceIDs(courseDataSet, allRooms, instructorList, term);
    for (final CourseData courseData : courseDataSet) {
      final String errorMessagePrefix = courseData.getCanonicalCourse().getCcn() + " missing salesforceID on ";
      assertNotNull(errorMessagePrefix + " term", courseData.getTerm().getSalesforceID());
      assertNotNull(errorMessagePrefix + " room " + courseData.getRoom(), courseData.getRoom().getSalesforceID());
      for (final Participation p : courseData.getParticipationSet()) {
        assertNotNull(errorMessagePrefix + " instructor " + p.getInstructor(), p.getInstructor().getSalesforceID());
      }
    }
  }
  
  @Test
  public void testGetHoursOffsetFromGMTForPST() throws SchedulerException {
    final int HOURS_OFFSET_FROM_GMT_FOR_PST = -8;
    final TimeZone tz = TimeZone.getTimeZone("America/Los_Angeles");
    final Date courseStartDatePST = CourseUtils.dateFromSalesforceFormat("2014-12-05");
    assertThat(CourseUtils.getHoursOffsetFromGMT(courseStartDatePST, tz)).isEqualTo(HOURS_OFFSET_FROM_GMT_FOR_PST);
  }
  
  @Test
  public void testGetHoursOffsetFromGMTForPDT() throws SchedulerException {
    final int HOURS_OFFSET_FROM_GMT_FOR_PDT = -7;
    final TimeZone tz = TimeZone.getTimeZone("America/Los_Angeles");
    final Date courseStartDatePDT = CourseUtils.dateFromSalesforceFormat("2014-04-25");
    assertThat(CourseUtils.getHoursOffsetFromGMT(courseStartDatePDT, tz)).isEqualTo(HOURS_OFFSET_FROM_GMT_FOR_PDT);
  }

  @Test(expected = SchedulerException.class)
  public void testTemporalPropertyError() throws SchedulerException {
    final String invalidStartDate = "XXXX-99-99";
    final Term term = Term.construct(Semester.Fall, 2014, invalidStartDate, "2014-12-05", null);
    CourseUtils.getTemporalPropertyTemplate(term, 1100, 1130);
  }

  @Test
  public void testTemporalPropertyStartMidnightGMT() throws SchedulerException {
    final Term term = Term.construct(Semester.Fall, 2014, "2014-08-22", "2014-12-05", null);
    final Date semesterStartDate = CourseUtils.dateFromSalesforceFormat(term.getStartDate());
    final TimeZone tz = TimeZone.getTimeZone("US/Pacific");
    final int offset = CourseUtils.getHoursOffsetFromGMT(semesterStartDate, tz);
    assertEquals("start=2014-08-23T01:00:00Z; end=2014-12-06T03:30:00Z; scheme=W3C-DTF;", CourseUtils.getTemporalPropertyTemplate(term, 1800, 1930));
  }

  @Test
  public void testTemporalPropertyEarlyDay() throws SchedulerException {
    final Term term = Term.construct(Semester.Fall, 2014, "2014-08-22", "2014-12-05", null);
    final Date semesterStartDate = CourseUtils.dateFromSalesforceFormat(term.getStartDate());
    final TimeZone tz = TimeZone.getTimeZone("US/Pacific");
    final int offset = CourseUtils.getHoursOffsetFromGMT(semesterStartDate, tz);
    final String expected = "start=2014-08-22T" + (9 - offset) + ":00:00Z; end=2014-12-05T" + (10 - offset) + ":30:00Z; scheme=W3C-DTF;";
    assertEquals("start=2014-08-22T16:00:00Z; end=2014-12-05T18:30:00Z; scheme=W3C-DTF;", CourseUtils.getTemporalPropertyTemplate(term, 900, 1030));
  }
  
  //This test inspired by WCT-4725 - Sign up form: Salesforce semester project start and end dates not respected when scheduling recordings
  @Test
  public void testSemesterProjectStartAndEndDatesPDT() throws SchedulerException {
    final Term term = Term.construct(Semester.Spring, 2014, "2014-01-21", "2014-05-09", null);
    final Date semesterStartDate = CourseUtils.dateFromSalesforceFormat(term.getStartDate());
    final TimeZone tz = TimeZone.getDefault();
    final int offset = CourseUtils.getHoursOffsetFromGMT(semesterStartDate, tz);
    final String expected = "start=2014-01-21T16:00:00Z; end=2014-05-09T16:30:00Z; scheme=W3C-DTF;";
    assertEquals(expected, CourseUtils.getTemporalPropertyTemplate(term, 800, 930));
  }
  
  @SuppressWarnings("deprecation")
  @Test
  public void testSemesterProjectStartAndEndDatesFall2014() throws SchedulerException {
    final Term term = Term.construct(Semester.Fall, 2014, "2014-08-28", "2014-12-12", null);
    final Date semesterStartDate = CourseUtils.dateFromSalesforceFormat(term.getStartDate());
    final TimeZone tz = TimeZone.getDefault();
    final int offset = CourseUtils.getHoursOffsetFromGMT(semesterStartDate, tz);
    final String expected = "start=2014-08-28T16:30:00Z; end=2014-12-12T19:00:00Z; scheme=W3C-DTF;";
    assertEquals(expected, CourseUtils.getTemporalPropertyTemplate(term, 930, 1100));
  }

  @Test
  public void testDecodeTemporal() throws SchedulerException, ParseException {
    final Term term = Term.construct(Semester.Fall, 2014, "2014-01-30", "2014-03-09", null);
    final int startTimeMilitaryPST = 1030;
    final int endTimeMilitaryPST = 1300;
    final String temporal = CourseUtils.getTemporalPropertyTemplate(term, startTimeMilitaryPST, endTimeMilitaryPST);
    assertEquals("start=2014-01-30T18:30:00Z; end=2014-03-09T20:00:00Z; scheme=W3C-DTF;", temporal);
    //
    // Logic as copied from SchedulerServiceImpl
    // Then edited when getRecurrenceRule signature changed
    // TODO seems we're really only testing duration, is there a better way?
    //
    final DCMIPeriod dcmiPeriod = EncodingSchemeUtils.decodeMandatoryPeriod(temporal);
    final Date start = dcmiPeriod.getStart();
    final Date end = dcmiPeriod.getEnd();
    Long duration = 0L;
    final TimeZone tz = TimeZone.getTimeZone("UTC");
    final List<DayOfWeek> days = Collections.list(DayOfWeek.Monday, DayOfWeek.Wednesday, DayOfWeek.Friday);
    final String recurrenceRule = CourseUtils.getRecurrenceRule(start, tz, days, startTimeMilitaryPST);
    DateTime seed = new DateTime(true);
    DateTime period = new DateTime(true);
    if (tz.inDaylightTime(start) && !tz.inDaylightTime(end)) {
      seed.setTime(start.getTime() + 3600000);
      period.setTime(end.getTime());
      duration = (end.getTime() - (start.getTime() + 3600000)) % (24 * 60 * 60 * 1000);
    } else if (!tz.inDaylightTime(start) && tz.inDaylightTime(end)) {
      seed.setTime(start.getTime());
      period.setTime(end.getTime() + 3600000);
      duration = ((end.getTime() + 3600000) - start.getTime()) % (24 * 60 * 60 * 1000);
    } else {
      seed.setTime(start.getTime());
      period.setTime(end.getTime());
      duration = (end.getTime() - start.getTime()) % (24 * 60 * 60 * 1000);
    }
    assertEquals(new Long(5400000), duration);
    //
    final Recur recur = new RRule(recurrenceRule).getRecur();
    final DateList dates = recur.getDates(seed, period, Value.DATE_TIME);
    for (final Object next : dates) {
      final DateTime dateTime = (DateTime) next;
      new Date(dateTime.getTime());
    }
  }

  @Test
  public void testSortParticipants() {
    final Participation other0 = create("0");
    final Participation other1 = create("1");
    final Participation other2 = create("2");
    final Participation current = create("3");
    final User currentUser = new User(current.getInstructor().getCalNetUID(), "organization", new String[0]);
    //
    final List<Participation> list1 = CourseUtils.sortParticipants(Collections.set(other2, other0, other1), currentUser);
    assertSameCalNetUID(other0, list1.get(0));
    assertSameCalNetUID(other1, list1.get(1));
    assertSameCalNetUID(other2, list1.get(2));
    //
    final List<Participation> list2 = CourseUtils.sortParticipants(Collections.set(other2, other1, current), currentUser);
    assertSameCalNetUID(current, list2.get(0));
    assertSameCalNetUID(other1, list2.get(1));
    assertSameCalNetUID(other2, list2.get(2));
    //
    final List<Participation> list3 = CourseUtils.sortParticipants(Collections.set(other2, other1, current, other0), currentUser);
    assertSameCalNetUID(current, list3.get(0));
    assertSameCalNetUID(other0, list3.get(1));
    assertSameCalNetUID(other1, list3.get(2));
    assertSameCalNetUID(other2, list3.get(3));
  }
  
  @Test
  public void testDayOfWeek() {
    assertThat(DayOfWeek.Sunday.getNextDayOfWeek()).isEqualTo(DayOfWeek.Monday);
    assertThat(DayOfWeek.Thursday.getNextDayOfWeek()).isEqualTo(DayOfWeek.Friday);
  }
  
  @Test
  public void testCreateRecurrenceRuleForCourseStartingAfterMidnightGMTDuringPST() throws SchedulerException, ParseException {
    final TimeZone tz = TimeZone.getTimeZone("America/Los_Angeles");
    final Date courseStartDatePST = CourseUtils.dateFromSalesforceFormat("2014-12-05");
    final int courseStartTimePST = 1600;
    final int coursesStartSevenMinutesLate = 7;
    final int offsetGMTDuringPST = CourseUtils.getHoursOffsetFromGMT(courseStartDatePST, tz);
    final int recordingStartTimeGMTForCourseStartingAfterMidnightGMTDuringPST = (courseStartTimePST/100 - offsetGMTDuringPST) % 24;
    final List<DayOfWeek> meetingDays = Collections.list(DayOfWeek.Monday, DayOfWeek.Wednesday, DayOfWeek.Friday);
        
    final String recurrenceRule = CourseUtils.getRecurrenceRule(courseStartDatePST, tz, meetingDays, courseStartTimePST + coursesStartSevenMinutesLate);
    
    final Recur recur = new RRule(recurrenceRule).getRecur();
    assertThat(recur.getHourList()).hasSize(1).contains(recordingStartTimeGMTForCourseStartingAfterMidnightGMTDuringPST);
    assertThat(recur.getMinuteList()).hasSize(1).contains(coursesStartSevenMinutesLate);
    assertThat(recur.getDayList()).containsOnly(new WeekDay("TU"), new WeekDay("TH"), new WeekDay("SA"));
  }
  
  @Test
  public void testCreateRecurrenceRuleForCourseStartingBeforeMidnightGMTDuringPDT() throws SchedulerException, ParseException {
    final TimeZone tz = TimeZone.getTimeZone("America/Los_Angeles");
    final Date courseStartDatePDT = CourseUtils.dateFromSalesforceFormat("2014-04-25");
    final int courseStartTimePDT = 1600;
    final int coursesStartSevenMinutesLate = 7;
    final int offsetGMTDuringPDT = CourseUtils.getHoursOffsetFromGMT(courseStartDatePDT, tz);
    final int recordingStartTimeGMTForCourseStartingBeforeMidnightGMTDuringPDT = (courseStartTimePDT/100 - offsetGMTDuringPDT) % 24;
    final List<DayOfWeek> meetingDays = Collections.list(DayOfWeek.Monday, DayOfWeek.Wednesday, DayOfWeek.Friday);
        
    final String recurrenceRule = CourseUtils.getRecurrenceRule(courseStartDatePDT, tz, meetingDays, courseStartTimePDT + coursesStartSevenMinutesLate);
    
    final Recur recur = new RRule(recurrenceRule).getRecur();
    assertThat(recur.getHourList()).hasSize(1).contains(recordingStartTimeGMTForCourseStartingBeforeMidnightGMTDuringPDT);
    assertThat(recur.getMinuteList()).hasSize(1).contains(coursesStartSevenMinutesLate);
    assertThat(recur.getDayList()).containsOnly(new WeekDay("MO"), new WeekDay("WE"), new WeekDay("FR"));
  }
  
  @Test
  public void testCreateRecurrenceRuleForCourseStartingAfterMidnightGMTDuringPDT() throws SchedulerException, ParseException {
    final TimeZone tz = TimeZone.getTimeZone("America/Los_Angeles");
    final Date courseStartDatePDT = CourseUtils.dateFromSalesforceFormat("2014-04-25");
    final int courseStartTimePDT = 1700;
    final int coursesStartSevenMinutesLate = 7;
    final int offsetGMTDuringPDT = CourseUtils.getHoursOffsetFromGMT(courseStartDatePDT, tz);
    
    final int recordingStartTimeGMTForCourseStartingAfterMidnightGMTDuringPDT = (courseStartTimePDT/100 - offsetGMTDuringPDT) % 24;
    final List<DayOfWeek> meetingDays = Collections.list(DayOfWeek.Monday, DayOfWeek.Wednesday, DayOfWeek.Friday);
    
    final String recurrenceRule = CourseUtils.getRecurrenceRule(courseStartDatePDT, tz, meetingDays, courseStartTimePDT + coursesStartSevenMinutesLate);
    
    final Recur recur = new RRule(recurrenceRule).getRecur();
    assertThat(recur.getHourList()).hasSize(1).contains(recordingStartTimeGMTForCourseStartingAfterMidnightGMTDuringPDT);
    assertThat(recur.getMinuteList()).hasSize(1).contains(coursesStartSevenMinutesLate);
    assertThat(recur.getDayList()).containsOnly(new WeekDay("TU"), new WeekDay("TH"), new WeekDay("SA"));
  }

  @Test
  public void testGetInvalidCourseKey() {
    assertNull(CourseUtils.getCourseKey(null));
    assertNull(CourseUtils.getCourseKey("  "));
    assertNull(CourseUtils.getCourseKey("420a9ce6-cfb4-43fc-91b9-6bd7f7f3eeab"));
    assertNull(CourseUtils.getCourseKey("2015B"));
    assertNull(CourseUtils.getCourseKey("201B123"));
    assertNull(CourseUtils.getCourseKey("2014E5915"));
  }

  @Test
  public void testGetSeriesId() {
    final String seriesId = "2014D5915";
    final CourseKey key = CourseUtils.getCourseKey(seriesId);
    assertEquals(seriesId, CourseUtils.getSeriesId(key));
  }

  @Test
  public void testNotEqualsHasCourseKey() {
    assertFalse(CourseUtils.equals(new CourseKey(2014, Semester.Fall, 123), new CourseKey(2015, Semester.Fall, 123)));
    assertFalse(CourseUtils.equals(new CourseKey(2014, Semester.Fall, 123), new CourseKey(2014, Semester.Spring, 123)));
    assertFalse(CourseUtils.equals(new CourseKey(2014, Semester.Fall, 123), new CourseKey(2014, Semester.Fall, 234)));
    //
    final CourseData courseData = new CourseData();
    courseData.setCanonicalCourse(new CanonicalCourse(234, "name", "title"));
    courseData.setTerm(Term.construct(Semester.Fall, 2014, null, null, null));
    assertFalse(CourseUtils.equals(new CourseKey(2014, Semester.Fall, 123), courseData));
  }

  @Test
  public void testEqualsHasCourseKey() {
    assertTrue(CourseUtils.equals(new CourseKey(2014, Semester.Fall, 123), new CourseKey(2014, Semester.Fall, 123)));
    final CourseData courseData = new CourseData();
    courseData.setCanonicalCourse(new CanonicalCourse(123, "name", "title"));
    courseData.setTerm(Term.construct(Semester.Fall, 2014, null, null, null));
    assertTrue(CourseUtils.equals(new CourseKey(2014, Semester.Fall, 123), courseData));
  }

  @Test
  public void testGetCourseKey() {
    final CourseKey c1 = CourseUtils.getCourseKey("1999B1");
    assertEquals(1999, c1.getYear());
    assertEquals(Semester.Spring, c1.getSemester());
    assertEquals(1, c1.getCcn());
    //
    final CourseKey c2 = CourseUtils.getCourseKey("2525C99999");
    assertEquals(2525, c2.getYear());
    assertEquals(Semester.Summer, c2.getSemester());
    assertEquals(99999, c2.getCcn());
    //
    final CourseKey c3 = CourseUtils.getCourseKey("2014D5915");
    assertEquals(2014, c3.getYear());
    assertEquals(Semester.Fall, c3.getSemester());
    assertEquals(5915, c3.getCcn());
  }

  @Test
  public void testGetTermId() {
    assertEquals("2168", CourseUtils.getTermId(2016, 'D'));
    assertEquals("2162", CourseUtils.getTermId(2016, 'B'));
  }

  private void assertSameCalNetUID(final Participation p1, final Participation p2) {
    assertEquals(p1.getInstructor().getCalNetUID(), p2.getInstructor().getCalNetUID());
  }

  private Participation create(final String lastName) {
    final Instructor instructor = new Instructor(lastName);
    instructor.setLastName(lastName);
    return new Participation(SalesforceFieldInstructor.Instructor_1, instructor, false);
  }
  
  private CanonicalCourse createCanonicalCourse(final int ccn) {
    return new CanonicalCourse(ccn, null, null);
  }

  private Room clone(final Room room) {
    return new Room(null, room.getBuilding(), room.getRoomNumber(), room.getCapability());
  }

  private CourseData createCourseData(final Participation... participation) {
    final double random = Math.random();
    final int ccn = new Double(random * 1000000000).intValue();
    final CanonicalCourse cc = new CanonicalCourse(ccn, "Name " + random, "Title " + random);
    return createCourseData(Term.construct(Semester.Spring, 2014, null, null, null), cc, new Room(), participation);
  }

  private CourseData createCourseData(final Term term, final CanonicalCourse canonicalCourse, Room room, Participation... participation) {
    final CourseData c = new CourseData();
    c.setCanonicalCourse(canonicalCourse);
    c.setTerm(term);
    c.setRoom(room);
    c.setParticipationSet(Collections.createSet(participation));
    return c;
  }
}
