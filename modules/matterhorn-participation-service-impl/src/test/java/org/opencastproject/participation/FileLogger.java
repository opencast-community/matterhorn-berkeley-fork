/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation;

import org.apache.commons.lang.ClassUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.opencastproject.util.env.EnvironmentUtil;
import org.ops4j.pax.logging.PaxContext;
import org.ops4j.pax.logging.PaxLogger;
import org.ops4j.pax.logging.slf4j.Slf4jLogger;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Date;

/**
 * @author John Crossman
 */
public class FileLogger extends Slf4jLogger {

  public FileLogger(final int logLevel, final Class clazz) {
    super(ClassUtils.getShortCanonicalName(clazz), new TimestampFilePaxLogger(logLevel, clazz));
  }

  private static class TimestampFilePaxLogger implements PaxLogger {

    private final String name;
    private final Writer logger;
    private final int logLevel;

    TimestampFilePaxLogger(final int logLevel, final Class clazz) {
      name = ClassUtils.getShortClassName(clazz);
      this.logLevel = logLevel;
      try {
        final File dir = new File(EnvironmentUtil.getMatterhornHome(), "logs");
        boolean ready = dir.exists() || dir.mkdir();
        if (ready) {
          String fileName = name + '_' + DateFormatUtils.format(new Date(), "yyyy-MM-dd_HHmmss") + ".log";
          logger = new BufferedWriter(new FileWriter(new File(dir, fileName)));
        } else {
          throw new IOException("Failed to create directory: " + dir.getAbsolutePath());
        }
      } catch (IOException e) {
        throw new RuntimeException(e);
      }
    }

    @Override
    public boolean isTraceEnabled() {
      return true;
    }

    @Override
    public boolean isDebugEnabled() {
      return true;
    }

    @Override
    public boolean isWarnEnabled() {
      return true;
    }

    @Override
    public boolean isInfoEnabled() {
      return true;
    }

    @Override
    public boolean isErrorEnabled() {
      return true;
    }

    @Override
    public boolean isFatalEnabled() {
      return true;
    }

    @Override
    public void trace(final String message, final Throwable throwable) {
      write(message + toS(throwable));
    }

    @Override
    public void debug(final String message, final Throwable throwable) {
      write(message + toS(throwable));
    }

    @Override
    public void inform(final String message, final Throwable throwable) {
      write(message + toS(throwable));
    }

    @Override
    public void warn(final String message, final Throwable throwable) {
      write(message + toS(throwable));
    }

    @Override
    public void error(final String message, final Throwable throwable) {
      write(message + toS(throwable));
    }

    @Override
    public void fatal(final String message, final Throwable throwable) {
      write(message + toS(throwable));
    }

    @Override
    public void trace(final String message, final Throwable throwable, final String s2) {
      write(message + toS(throwable) + s2);
    }

    @Override
    public void debug(final String message, final Throwable throwable, final String s2) {
      write(message + toS(throwable) + s2);
    }

    @Override
    public void inform(final String message, final Throwable throwable, final String s2) {
      write(message + toS(throwable) + s2);
    }

    @Override
    public void warn(final String message, final Throwable throwable, final String s2) {
      write(message + toS(throwable) + s2);
    }

    @Override
    public void error(final String message, final Throwable throwable, final String s2) {
      write(message + toS(throwable) + s2);
    }

    @Override
    public void fatal(final String message, final Throwable throwable, final String s2) {
      write(message + toS(throwable) + s2);
    }

    @Override
    public int getLogLevel() {
      return logLevel;
    }

    @Override
    public String getName() {
      return this.name;
    }

    @Override
    public PaxContext getPaxContext() {
      return new PaxContext();
    }

    private String toS(Throwable throwable) {
      return (throwable == null) ? "" : "\n" + ExceptionUtils.getStackTrace(throwable) + "\n";
    }

    private void write(String output) {
      try {
        logger.write(output + "\n");
        logger.flush();
      } catch (final IOException e) {
        e.printStackTrace();
      }
    }

  }
}
