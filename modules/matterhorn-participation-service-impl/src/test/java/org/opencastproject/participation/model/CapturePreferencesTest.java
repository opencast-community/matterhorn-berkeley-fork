/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */

package org.opencastproject.participation.model;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class CapturePreferencesTest {

  private static final RecordingType RECORDING_TYPE = RecordingType.videoAndScreencast;
  private static final RecordingAvailability RECORDING_AVAILABILITY = RecordingAvailability.publicCreativeCommons;
  private static final int DELAY_PUBLISH_BY_DAYS = 1;

  private CapturePreferences aCapturePreferences = new CapturePreferences();

  @Before
  public void setUp() throws Exception {
    aCapturePreferences.setRecordingType(RECORDING_TYPE);
    aCapturePreferences.setRecordingAvailability(RECORDING_AVAILABILITY);
    aCapturePreferences.setDelayPublishByDays(DELAY_PUBLISH_BY_DAYS);
  }

  @Test
  public void testCapturePreferencesStringStringString() {
    CapturePreferences capturePreferences = new CapturePreferences(RECORDING_TYPE, RECORDING_AVAILABILITY, DELAY_PUBLISH_BY_DAYS);
    assertThat(aCapturePreferences.getRecordingType(), equalTo(capturePreferences.getRecordingType()));
    assertThat(aCapturePreferences.getRecordingAvailability(), equalTo(capturePreferences.getRecordingAvailability()));
    assertThat(aCapturePreferences.getDelayPublishByDays(), equalTo(capturePreferences.getDelayPublishByDays()));
  }

}