/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.mvc;

import static org.easymock.EasyMock.anyObject;
import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.reset;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.opencastproject.security.api.DefaultOrganization.DEFAULT_ORGANIZATION_ADMIN;

import org.opencastproject.participation.api.CourseManagementService;
import org.opencastproject.participation.model.CanonicalCourse;
import org.opencastproject.participation.model.CapturePreferences;
import org.opencastproject.participation.model.CourseOffering;
import org.opencastproject.participation.model.DayOfWeek;
import org.opencastproject.participation.model.Instructor;
import org.opencastproject.participation.model.Participation;
import org.opencastproject.participation.model.RecordingAvailability;
import org.opencastproject.participation.model.RecordingType;
import org.opencastproject.participation.model.Room;
import org.opencastproject.participation.model.RoomCapability;
import org.opencastproject.participation.model.Semester;
import org.opencastproject.participation.model.StageOfApproval;
import org.opencastproject.participation.model.Term;
import org.opencastproject.salesforce.SalesforceFieldInstructor;
import org.opencastproject.security.api.DefaultOrganization;
import org.opencastproject.security.api.Organization;
import org.opencastproject.security.api.SecurityService;
import org.opencastproject.security.api.User;
import org.opencastproject.util.data.Collections;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

import java.io.IOException;

import javax.servlet.ServletException;

/**
 * @author John Crossman
 */
public class SignUpFormControllerTest {

  private SignUpFormController controller;
  private CourseOffering course;
  private SecurityService securityService;
  private final Organization authorizedOrg = new DefaultOrganization();
  private final User authorizedUser = new User("calNetUID1", authorizedOrg.getId(), new String[]{"ROLE_EMPLOYEE-TYPE-STAFF"});
  private final User adminUID = new User("adminUID", authorizedOrg.getId(), new String[]{DEFAULT_ORGANIZATION_ADMIN});

  private MockHttpServletRequest request;
  private MockHttpServletResponse response;

  @Before
  public void before() throws IOException {
    course = new CourseOffering();
    course.setSalesforceID("006V0000004OXPQIA4");
    course.setCanonicalCourse(new CanonicalCourse(1234, "Name", "Title"));
    final Term term = Term.construct(Semester.Fall, 2014, "2014-08-22", "2014-12-05", null);
    course.setTerm(term);
    course.setRoom(new Room("salesforceID", "building", "roomNumber", RoomCapability.screencastAndVideo));
    course.setMeetingDays(Collections.list(DayOfWeek.Monday, DayOfWeek.Wednesday, DayOfWeek.Friday));
    course.setStartTime("1830");
    course.setEndTime("1900");
    final String calNetUID1 = "calNetUID1";
    final Instructor i1 = new Instructor(calNetUID1);
    i1.setFirstName("Lindsey");
    i1.setLastName("Buckingham");
    final Participation p1 = new Participation(SalesforceFieldInstructor.Instructor_1, i1, false);
    final Instructor i2 = new Instructor("calNetUID2");
    i2.setFirstName("Stevie");
    i2.setLastName("Nicks");
    course.setCapturePreferences(new CapturePreferences(RecordingType.audioOnly, RecordingAvailability.studentsOnly, 1));
    final Participation p2 = new Participation(SalesforceFieldInstructor.Instructor_2, i2, true);
    course.setParticipationSet(Collections.set(p1, p2));
    course.setStageOfApproval(StageOfApproval.partiallyApproved);
    course.setCourseOfferingId(StringUtils.EMPTY + term.getYear() + term.getSemester().getTermCode() + course.getCanonicalCourse().getCcn());
    //
    controller = new SignUpFormController();
    final CourseManagementService courseManagementService = createMock(CourseManagementService.class);
    expect(courseManagementService.getCourseOffering(course.getCourseOfferingId())).andReturn(course).anyTimes();
    expect(courseManagementService.getCourseOffering(anyObject(String.class))).andReturn(null).anyTimes();

    replay(courseManagementService);
    //
    securityService = createMock(SecurityService.class);
    resetAndReplay(securityService, authorizedUser.getUserName(), authorizedUser.getRoles());

    //
    controller.setCourseManagementService(courseManagementService);
    controller.setSecurityService(securityService);
    //
    request = new MockHttpServletRequest();
    response = new MockHttpServletResponse();
    response.setOutputStreamAccessAllowed(true);
  }

  @Test
  public void unauthorizedUserCannotAccessUnScheduledSignUpForm() throws ServletException, IOException {
    resetAndReplay(securityService, "unauthorizedUser", "ROLE_EMPLOYEE-TYPE-STAFF");
    request.setParameter("id", course.getCourseOfferingId());
    controller.doGet(request, response);
    final String content = new String(response.getContentAsByteArray());
    assertFalse(content.contains(course.getCanonicalCourse().getTitle()));
    assertTrue(content.contains("you are not authorized"));
    assertTrue(content.contains("contact"));
    assertFalse(content.contains("Recording Type"));
  }

  @Test
  public void unauthorizedUserCannotSeeConfirmation() throws ServletException, IOException {
    resetAndReplay(securityService, "unauthorizedUser", "ROLE_EMPLOYEE-TYPE-STAFF");
    course.setScheduled(true);
    request.setParameter("id", course.getCourseOfferingId());
    controller.doGet(request, response);
    final String content = new String(response.getContentAsByteArray());
    assertTrue(content.contains("you are not authorized"));
    assertTrue(content.contains("contact"));
    assertFalse(content.contains("(signed up)"));
    assertFalse(content.contains("Recording Type"));
  }

  @Test
  public void unauthorizedUserDoesNotSeeOffsetTimesWhenCourseIsApprovedAndScheduled() throws ServletException, IOException {
    resetAndReplay(securityService, "unauthorizedUser", "ROLE_EMPLOYEE-TYPE-STAFF");
    request.setParameter("id", course.getCourseOfferingId());
    course.setScheduled(true);
    course.setStageOfApproval(StageOfApproval.approved);
    final Instructor i1 = new Instructor("calNetUID1");
    i1.setFirstName("Lindsey");
    i1.setLastName("Buckingham");
    final Participation p1 = new Participation(SalesforceFieldInstructor.Instructor_1, i1, true);
    final Instructor i2 = new Instructor("calNetUID2");
    i2.setFirstName("Stevie");
    i2.setLastName("Nicks");
    final Participation p2 = new Participation(SalesforceFieldInstructor.Instructor_2, i2, true);
    course.setParticipationSet(Collections.set(p1, p2));
    controller.doGet(request, response);
    final String content = new String(response.getContentAsByteArray());
    assertFalse(content.contains("Recording Time:"));
    assertFalse(content.contains("6:37pm-7:03pm"));
    assertFalse(content.contains("The following settings have been selected for capture"));
  }

  @Test
  public void adminUserCanSeePartialApprovals() throws ServletException, IOException {
    resetAndReplay(securityService, adminUID.getUserName(), adminUID.getRoles());
    request.setParameter("id", course.getCourseOfferingId());
    controller.doGet(request, response);
    final String content = new String(response.getContentAsByteArray());
    assertTrue(content.contains(course.getCanonicalCourse().getName()));
    assertTrue(content.contains("Monday"));
    assertTrue(content.contains("6:30pm-7:00pm"));
    final int indexOfLindsey = content.indexOf("Lindsey Buckingham");
    assertTrue(indexOfLindsey > -1);
    assertTrue(indexOfLindsey < content.indexOf("(not yet signed up)"));
    assertTrue(content.contains("Your co-instructor(s) signed up with the following settings"));
    assertTrue(content.contains("Audio only"));
    assertTrue(content.contains("days after capture"));
  }


 @Test
 public void adminUserSeesOffsetTimesWhenCourseIsApprovedAndScheduled() throws ServletException, IOException {
   resetAndReplay(securityService, adminUID.getUserName(), adminUID.getRoles());
   request.setParameter("id", course.getCourseOfferingId());

   course.setScheduled(true);
   course.setStageOfApproval(StageOfApproval.approved);

   final Instructor i1 = new Instructor("calNetUID1");
   i1.setFirstName("Lindsey");
   i1.setLastName("Buckingham");
   final Participation p1 = new Participation(SalesforceFieldInstructor.Instructor_1, i1, true);
   final Instructor i2 = new Instructor("calNetUID2");
   i2.setFirstName("Stevie");
   i2.setLastName("Nicks");
   final Participation p2 = new Participation(SalesforceFieldInstructor.Instructor_2, i2, true);
   course.setParticipationSet(Collections.set(p1, p2));
   controller.doGet(request, response);
   final String content = new String(response.getContentAsByteArray());
   assertTrue(content.contains("Recording Time:"));
   assertTrue(content.contains("6:37pm-7:03pm"));
   assertTrue(content.contains("The following settings have been selected for capture"));
 }

  @Test
  public void adminSchedulesOverride() throws ServletException, IOException {
    // Minor issue, getting same message as if it were an instructor, i.e. "... but we need you to agree  ..."
    course.setAdminScheduleOverride("adminUID");
    course.setScheduled(true);
    request.setParameter("id", course.getCourseOfferingId());
    controller.doGet(request, response);
    final String content = new String(response.getContentAsByteArray());
    assertTrue(content.contains("Recording Time:"));
    assertTrue(content.contains("6:37pm-7:03pm"));
    assertTrue(content.contains("Recordings have been scheduled with the following preferences"));
  }

  @Test
  public void adminUserCanSeeDetailsIfCourseAlreadyScheduledButPartiallyApproved() throws ServletException, IOException {
    resetAndReplay(securityService, adminUID.getUserName(), adminUID.getRoles());
    request.setParameter("id", course.getCourseOfferingId());
    course.setStageOfApproval(StageOfApproval.partiallyApproved);
    course.setScheduled(true);
    controller.doGet(request, response);
    final String content = new String(response.getContentAsByteArray());
    assertTrue(content.contains("The following settings have been selected for capture"));
    assertTrue(content.contains("Recording Time:"));
    assertTrue(content.contains("6:37pm-7:03pm"));
  }

  @Test
  public void lastInstructorSeesCoInstructorSelections() throws ServletException, IOException {
    resetAndReplay(securityService, "calNetUID1", "ROLE_EMPLOYEE-TYPE-STAFF");
    request.setParameter("id", course.getCourseOfferingId());
    controller.doGet(request, response);
    final String content = new String(response.getContentAsByteArray());
    assertFalse(content.contains("6:37pm-7:03pm"));
    assertTrue(content.contains("6:30pm-7:00pm"));
    assertTrue(content.contains("Your co-instructor(s) signed up with the following settings"));
  }

  @Test
  public void lastInstructorSeesOffsetTimesWhenApprovingScheduledCourse() throws ServletException, IOException {
    resetAndReplay(securityService, "calNetUID1", "ROLE_EMPLOYEE-TYPE-STAFF");
    request.setParameter("id", course.getCourseOfferingId());
    course.setScheduled(true);
    controller.doGet(request, response);
    final String content = new String(response.getContentAsByteArray());
    assertTrue(content.contains("Recording Time:"));
    assertTrue(content.contains("6:37pm-7:03pm"));
    assertTrue(content.contains("but we need you to agree to the terms described below"));
  }

  @Test
  public void lastInstructorSeesOffsetTimesWhenCourseIsPartiallyApprovedAndScheduledByAdminOverride() throws ServletException, IOException {
    resetAndReplay(securityService, "calNetUID1", "ROLE_EMPLOYEE-TYPE-STAFF");
    request.setParameter("id", course.getCourseOfferingId());
    course.setAdminScheduleOverride("adminUID");
    course.setScheduled(true);
    course.setStageOfApproval(StageOfApproval.partiallyApproved);
    controller.doGet(request, response);
    final String content = new String(response.getContentAsByteArray());
    assertTrue(content.contains("Recording Time:"));
    assertTrue(content.contains("6:37pm-7:03pm"));
    assertTrue(content.contains("but we need you to agree to the terms described below"));
  }

  @Test
  public void instructorShouldNotSeeSubmittingWillScheduleRecordingsTextWhenCourseIsPartiallyApproved() throws ServletException, IOException {
    resetAndReplay(securityService, "calNetUID1", "ROLE_EMPLOYEE-TYPE-STAFF");
    request.setParameter("id", course.getCourseOfferingId());
    course.setStageOfApproval(StageOfApproval.partiallyApproved);
    controller.doGet(request, response);
    final String content = new String(response.getContentAsByteArray());
    assertFalse(content.contains("Submitting will schedule recordings!"));
  }

  @Test
  public void instructorSeesNoonTimesAs12AndNot0() throws ServletException, IOException {
    resetAndReplay(securityService, "calNetUID1", "ROLE_EMPLOYEE-TYPE-STAFF");
    request.setParameter("id", course.getCourseOfferingId());
    course.setStartTime("1200");
    course.setEndTime("1230");
    controller.doGet(request, response);
    final String content = new String(response.getContentAsByteArray());
    assertFalse(content.contains("0:00pm-0:30pm"));
    assertTrue(content.contains("12:00pm-12:30pm"));
  }

  @Test
  public void instructorSeesOffsetTimesWhenCourseIsApprovedAndScheduled() throws ServletException, IOException {
    resetAndReplay(securityService, "calNetUID1", "ROLE_EMPLOYEE-TYPE-STAFF");
    request.setParameter("id", course.getCourseOfferingId());
    course.setScheduled(true);
    course.setStageOfApproval(StageOfApproval.approved);
    final Instructor i1 = new Instructor("calNetUID1");
    i1.setFirstName("Lindsey");
    i1.setLastName("Buckingham");
    final Participation p1 = new Participation(SalesforceFieldInstructor.Instructor_1, i1, true);
    final Instructor i2 = new Instructor("calNetUID2");
    i2.setFirstName("Stevie");
    i2.setLastName("Nicks");
    final Participation p2 = new Participation(SalesforceFieldInstructor.Instructor_2, i2, true);
    course.setParticipationSet(Collections.set(p1, p2));
    controller.doGet(request, response);
    final String content = new String(response.getContentAsByteArray());
    assertTrue(content.contains("Recording Time:"));
    assertTrue(content.contains("6:37pm-7:03pm"));
    assertTrue(content.contains("You have already signed up with the following settings"));
  }

  @Test
  public void testMissingParameter() throws ServletException, IOException {
    controller.doGet(request, response);
    final String content = new String(response.getContentAsByteArray());
    assertTrue(content.contains("Sign Up"));
    assertTrue(content.contains("Sorry, There is a Problem"));
    assertTrue(content.contains("ets_logo.png"));
  }

  @Test
  public void testNoSuchCourse() throws Exception {
    request.setParameter("id", "noSuchCourse");
    controller.doGet(request, response);
    final String content = new String(response.getContentAsByteArray());
    assertTrue(content.contains("The system does not recognize the submitted course"));
  }

  @Test
  public void testHeaderInfo() throws ServletException, IOException {
    request.setParameter("id", course.getCourseOfferingId());
    controller.doGet(request, response);
    final String content = new String(response.getContentAsByteArray());
    assertTrue(content.contains(course.getCanonicalCourse().getName()));
    assertTrue(content.contains(course.getRoom().getBuilding()));
    assertTrue(content.contains(course.getRoom().getRoomNumber()));
    assertTrue(content.contains("Monday"));
    assertTrue(content.contains("6:30pm-7:00pm"));
    final int indexOfLindsey = content.indexOf("Lindsey Buckingham");
    assertTrue(indexOfLindsey > -1);
    assertTrue(indexOfLindsey < content.indexOf("(not yet signed up)"));
    assertTrue(content.contains("Your co-instructor(s) signed up with the following settings"));
    assertTrue(content.contains("Audio only"));
    assertTrue(content.contains("days after capture"));
  }

  @Test
  public void testNoPublishDelay() throws ServletException, IOException {
    course.setCapturePreferences(
            new CapturePreferences(RecordingType.videoAndScreencast, RecordingAvailability.publicNoRedistribute, 0));
    request.setParameter("id", course.getCourseOfferingId());
    controller.doGet(request, response);
    final String content = new String(response.getContentAsByteArray());
    assertTrue(content.contains("Video, Computer Screen, and Audio"));
    assertTrue(content.contains("recordingAvailability"));
    assertTrue(content.contains("As soon after capture as possible"));
  }

  private void resetAndReplay(final SecurityService securityService, final String calNetUID, final String... roles) {
    final User user = new User(calNetUID, authorizedOrg.getId(), roles);
    reset(securityService);
    expect(securityService.getUser()).andReturn(user).once();
    expect(securityService.getOrganization()).andReturn(authorizedOrg).once();
    replay(securityService);
  }

}
