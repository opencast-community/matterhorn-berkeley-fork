/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.model;


import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.fest.assertions.Assertions.assertThat;

/**
 * Suite of tests covering the management of cross-listed courses using the {@code Booking} abstraction.
 */
public class CrossListedCoursesTest {

  //courseAlpha and courseBeta are cross-listed courses
  // courseGamma is a control to ensure that non-cross-listed courses
  // are not affected
  @Test
  public void testNotEqual() {
    assertThat(courseAlpha).isNotEqualTo(courseBeta).isNotEqualTo(courseGamma);
  }
  
  // Three courses, two of which are cross-listed go in, two courses - the 
  // not cross-listed and one of the cross-listed come out
  @Test
  public void justOneCrossListedCourseKept() {
    final Set<CourseData> courseDataListSet = new HashSet<CourseData>();
    courseDataListSet.add(courseAlpha);
    courseDataListSet.add(courseBeta);
    courseDataListSet.add(courseGamma);
    assertThat(Booking.reconcileCrossListedCourses(courseDataListSet)).hasSize(2);
  }
  
  // The total number of instructors, including the 
  // instructor from courseGamma, must be conserved.
  @Test
  public void allInstructorsKept() {
    final Set<CourseData> courseDataListSet = new HashSet<CourseData>();
    int instructorCount = 0;
    // Adds one instructor
    courseDataListSet.add(courseAlpha);
    // Adds two instructors
    courseDataListSet.add(courseBeta);
    // Adds one instructor
    courseDataListSet.add(courseGamma);
    for (final CourseData courseData : Booking.reconcileCrossListedCourses(courseDataListSet)) {
      instructorCount = instructorCount + courseData.getParticipationSet().size();
    }
    assertThat(instructorCount).isEqualTo(4);
  }
  
  //Between them the cross-listed courses (courseAlpha and courseBeta) have
  // three instructors:
  // --Course -- -- Instructor -- -- CalNetID --
  // courseAlpha  instructor1       111111
  // courseBeta   instructor2       222222
  // courseBeta   instructor4       444444
  // all must be present in the reconciled course (courseBeta), but courseGamma's 
  // Instructor3 must not
 @Test
 public void bothCrossListedInstructorsKept() {
   Integer reconciledCourse = courseBeta.getCanonicalCourse().getCcn();
   final Map<Integer,Set<String>> instructorsPerCourse = new HashMap<Integer,Set<String>>();
   courseDataListSet.add(courseAlpha);
   courseDataListSet.add(courseBeta);
   courseDataListSet.add(courseGamma);
   for (final CourseData courseDatum : Booking.reconcileCrossListedCourses(courseDataListSet)) {
     Set<String> instructors = new HashSet<String>();
     for (Participation participation : courseDatum.getParticipationSet()) {
       instructors.add(participation.getInstructor().getCalNetUID());
     }
     instructorsPerCourse.put(courseDatum.getCanonicalCourse().getCcn(), instructors);
   }
   // reconciledCourse has three instructors, not including Instructor3
   assertThat(instructorsPerCourse.get(reconciledCourse)).containsOnly("111111", "222222", "444444");
 }
 
 @Test
 public void crossListedCoursesAreBookingsThatAreCrossListed() {
   final Set<CourseData> courseDataListSet = new HashSet<CourseData>();
   courseDataListSet.add(courseAlpha);
   courseDataListSet.add(courseBeta);
   courseDataListSet.add(courseGamma);
   
   final List<Booking> bookings = Booking.crossListedBookings(courseDataListSet);
   for (Booking booking : bookings) {
     assertThat(booking.isCrossListed()).isTrue();
   }
 }
 
// @Test
// public void thisBookingIsOneReferenceCoursePlusOneNonReferenceCourse() {
//   final Set<CourseData> courseDataListSet = new HashSet<CourseData>();
//   courseDataListSet.add(courseAlpha);
//   courseDataListSet.add(courseBeta);
//   courseDataListSet.add(courseGamma);
//
//   final List<Booking> bookings = Booking.crossListedBookings(courseDataListSet);
//
//   assertThat(bookings.size()).isEqualTo(1);
//   for (Booking booking : bookings) {
//     assertThat(booking.getNonReferenceCourses().size()).isEqualTo(1);
//   }
// }
  
  @Before
  public void setUp() {
    courseAlpha = getSharedCourseOfferingValues();
    courseAlpha.setSalesforceID("1234567");
    courseAlpha.setCourseOfferingId("2013B54321");
    courseAlpha.setCanonicalCourse(new CanonicalCourse(54321, "ETS 101", "SF Giants Baseball"));
    courseAlpha.setDeptName("SF_GIANTS");
    Participation participation1 = new Participation();
    participation1.setApproved(true);
    Instructor instructor1 = new Instructor();
    instructor1.setCalNetUID("111111");
    instructor1.setDepartment("ETS");
    instructor1.setSalesforceID("98876554");
    instructor1.setEmail("posey@bekeley.edu");
    instructor1.setFirstName("Buster");
    instructor1.setLastName("Posey");
    instructor1.setRole("Faculty");
    participation1.setInstructor(instructor1);
    final Set<Participation> participationList = new HashSet<Participation>();
    participationList.add(participation1);
    courseAlpha.setParticipationSet(participationList);
    courseDataListSet.add(courseAlpha);
    
    courseBeta = getSharedCourseOfferingValues();
    courseBeta.setSalesforceID("7654321");
    courseBeta.setCourseOfferingId("2013B12345");
    courseBeta.setCanonicalCourse(new CanonicalCourse(12345, "ETS 001", "Introductory Physics"));
    courseBeta.setDeptName("PHYSICS");
    Participation participation2 = new Participation();
    participation2.setApproved(false);
    Instructor instructor2 = new Instructor();
    instructor2.setCalNetUID("222222");
    instructor2.setDepartment("ETS");
    instructor2.setSalesforceID("1234567");
    instructor2.setEmail("lincecum@bekeley.edu");
    instructor2.setFirstName("Tim");
    instructor2.setLastName("Lincecum");
    instructor2.setRole("Faculty");
    participation2.setInstructor(instructor2);
    Participation participation4 = new Participation();
    participation4.setApproved(false);
    Instructor instructor4 = new Instructor();
    instructor4.setCalNetUID("444444");
    instructor4.setDepartment("ETS");
    instructor4.setSalesforceID("1254567");
    instructor4.setEmail("bochy@bekeley.edu");
    instructor4.setFirstName("Bruce");
    instructor4.setLastName("Bochy");
    instructor4.setRole("Faculty");
    participation4.setInstructor(instructor4);
    final Set<Participation> participationList2 = new HashSet<Participation>();
    participationList2.add(participation2);
    participationList2.add(participation4);
    courseBeta.setParticipationSet(participationList2);
    courseDataListSet.add(courseBeta);
    
    courseGamma = getSharedCourseOfferingValues();
    courseGamma.setSalesforceID("7654320");
    courseGamma.setCourseOfferingId("2013B12340");
    courseGamma.setCanonicalCourse(new CanonicalCourse(12340, "ETS 8A", "Introductory Physics"));
    courseGamma.setStartTime("1000");
    courseGamma.setDeptName("PHYSICS");
    Participation participation3 = new Participation();
    participation3.setApproved(false);
    Instructor instructor3 = new Instructor();
    instructor3.setCalNetUID("333333");
    instructor3.setDepartment("ETS");
    instructor3.setSalesforceID("1234560");
    instructor3.setEmail("zito@bekeley.edu");
    instructor3.setFirstName("Barry");
    instructor3.setLastName("Zito");
    instructor3.setRole("Faculty");
    participation3.setInstructor(instructor3);
    final Set<Participation> participationList3 = new HashSet<Participation>();
    participationList3.add(participation3);
    courseGamma.setParticipationSet(participationList3);
    courseDataListSet.add(courseGamma);
  }

  private CourseOffering getSharedCourseOfferingValues() {
    final CourseOffering courseOffering = new CourseOffering();
    final Term term = Term.construct(Semester.Spring, 2013, "2014-01-01", "2014-05-30", null);
    courseOffering.setTerm(term);
    courseOffering.setSection("001");

    List<DayOfWeek> meetingDays = new LinkedList<DayOfWeek>();
    meetingDays.add(DayOfWeek.Tuesday);
    meetingDays.add(DayOfWeek.Thursday);
    courseOffering.setMeetingDays(meetingDays);

    courseOffering.setStartTime("0900");
    courseOffering.setEndTime("1000");

    Room room = new Room();
    room.setBuilding("Pimentel");
    room.setCapability(RoomCapability.screencastAndVideo);
    room.setSalesforceID("123456");
    room.setRoomNumber("1");
    courseOffering.setRoom(room);

    courseOffering.setScheduled(false);
    courseOffering.setStageOfApproval(StageOfApproval.approved);
    courseOffering.setStageOfLifecycle(SalesforceProjectLifecycle.prospecting);

    CapturePreferences capPrefs = new CapturePreferences();
    capPrefs.setRecordingType(RecordingType.videoAndScreencast);
    capPrefs.setRecordingAvailability(RecordingAvailability.publicCreativeCommons);
    capPrefs.setDelayPublishByDays(0);
    courseOffering.setCapturePreferences(capPrefs);

    return courseOffering;
  }
  
  private final Set<CourseData> courseDataListSet = new HashSet<CourseData>();
  private CourseOffering courseAlpha;
  private CourseOffering courseBeta;
  private CourseOffering courseGamma;
}
