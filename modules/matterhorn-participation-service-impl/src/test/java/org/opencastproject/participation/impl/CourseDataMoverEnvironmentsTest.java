/**
" *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */

package org.opencastproject.participation.impl;

import static org.mockito.Mockito.doReturn;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.Ignore;
import org.opencastproject.notify.Notifier;
import org.opencastproject.util.env.Environment;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.osgi.service.cm.ConfigurationException;

import java.io.IOException;
import java.util.Properties;

/**
 * <code>CourseManagementServiceImpl</code> class's <code>updatedConfiguration</code> 
 * method is the driver for determining which course data source to use when creating
 * courses for a term.
 * 
 * This set of tests covers:
 * <ul>
 * <li>Election of data source driven by properties and environment</li>
 * <li>Properties file and contents validation</li>
 * 
 * @author Fernando Alvarez
 *
 */
@Ignore
@RunWith(JUnitParamsRunner.class)
public class CourseDataMoverEnvironmentsTest {

  private CourseManagementServiceImpl sut;
  private Properties properties;

  @Test
  public void sourceCoursesFromTestFile() throws ConfigurationException, IOException {
    doNothing().when(sut).startFileWatchingCourseDataMover();
    properties.put("dataMover.fake", true);
    
    sut.updatedConfiguration(properties);
    
    // Test programmed to call this method
    verify(sut, times(1)).startFileWatchingCourseDataMover();
    
    // Test programmed to not call this method
    verify(sut, never()).scheduleCourseDataMover(properties);
  }
  
  //Running CourseManagementSericeImpl in an Environment categorized as 
  // VIEW_BASED_ENVIRONMENTS in the service's properties file requires that
  // the Course Data Mover source courses from a database data source
  @Test
  public void sourceCoursesFromDatabase() throws ConfigurationException, IOException {
    doNothing().when(sut).scheduleCourseDataMover(properties);
    properties.put("dataMover.fake", false);
    sut.updatedConfiguration(properties);
    
    // Test programmed to call this method
    verify(sut, times(1)).scheduleCourseDataMover(properties);
    
    // Test programmed to not call this method
    verify(sut, never()).startFileWatchingCourseDataMover();
  }
  
  @Test(expected = ConfigurationException.class)
  @Parameters(method = "getInvalidKeyValuePairs")
  public void invalidProperties(String key, String value) throws ConfigurationException {
    Properties props = new Properties();
    props.put(key, value);
    sut.updatedConfiguration(props);
  }
  
  @Test(expected = ConfigurationException.class)
  public void nullPropertiesFileThrowsException() throws ConfigurationException {
    sut.updatedConfiguration(null);
  }
  
  @Test(expected = ConfigurationException.class)
  public void emptyPropertiesFileThrowsException() throws ConfigurationException {
    Properties props = new Properties();
    sut.updatedConfiguration(props);
  }
  
  @Parameters(method = "getOPPKeyValuePairs")
  public void otherPeoplesPropertiesDoNotThrowException(String key, String value) throws ConfigurationException {
    Properties props = new Properties();
    props.put(key, value);
    sut.updatedConfiguration(props);
  }
  
  @Before
  public void setUp() throws Exception {
    sut = spy(new CourseManagementServiceImpl());
    sut.setNotifier(mock(Notifier.class));
    
    // We need a valid Environment but since it is an enum we can't assume 
    // we know what that could be over time, so we grab the first in ordinal 
    // order, and use it throughout the test
    Environment environment = Environment.values()[0];
    
    // Mocked method supports using the same environment throughout test
    doReturn(environment).when(sut).getEnvironment();
    
    // Some tests take a seeded properties file containing properties that are
    // not germane to the code being tested
    properties = new Properties();
    properties.put("dataMover.cron.intervalMinutes", 0);
  }

  @After
  public void tearDown() throws Exception {
  }
}
