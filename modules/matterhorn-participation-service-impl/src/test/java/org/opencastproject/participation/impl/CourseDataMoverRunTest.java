/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.impl;

import org.apache.commons.lang.StringUtils;
import org.opencastproject.participation.FileLogger;
import org.opencastproject.participation.CourseUtils;
import org.opencastproject.participation.UnitTestUtils;
import org.opencastproject.participation.api.CourseManagementService;
import org.opencastproject.participation.impl.persistence.ProvidesCourseCatalogData;
import org.opencastproject.participation.impl.persistence.UCBerkeleyCourseDatabaseImpl;
import org.opencastproject.salesforce.SalesforceConnectorServiceImpl;

import org.junit.Test;
import org.ops4j.pax.logging.PaxLogger;
import org.osgi.service.cm.ConfigurationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Properties;

/**
 * @author John Crossman
 */
public class CourseDataMoverRunTest {

  @Test
  public void testRun() throws Exception {
    final String fakeMode = StringUtils.trimToNull(System.getenv("COURSE_DATA_MOVER_FAKE_MODE"));
    if (fakeMode == null) {
      LoggerFactory.getLogger(this.getClass()).debug("Skipping test because it was NOT invoked by our custom CDM shell script");
    } else {
      boolean fake = Boolean.getBoolean(fakeMode);
      Logger logger = new FileLogger(PaxLogger.LEVEL_DEBUG, CourseDataMover.class);
      CourseDataMover dataMover = new CourseDataMover(logger);
      dataMover.setCourseManagementService(getCourseManagementService(fake, logger));
      dataMover.setCourseDatabase(getCourseDatabase(logger));
      dataMover.setNotifier(new VoidNotifier(logger));
      dataMover.setThrowExceptionInRunMethod(true);
      // Run!
      dataMover.run();
    }
  }

  private ProvidesCourseCatalogData getCourseDatabase(Logger logger) throws IOException {
    return new UCBerkeleyCourseDatabaseImpl(UnitTestUtils.getTestCatalogCredentials(), new VoidNotifier(logger), logger);
  }

  private CourseManagementService getCourseManagementService(boolean fakeMode, Logger logger) throws ConfigurationException, IOException {
    SalesforceConnectorServiceImpl salesforce = new SalesforceConnectorServiceImpl(logger);
    salesforce.setNotifier(new VoidNotifier(logger));
    salesforce.updated(UnitTestUtils.getSalesforceConnectionProperties());

    CourseManagementServiceImpl service = new CourseManagementServiceImpl(logger);
    Properties properties = CourseUtils.getCourseManagementServiceProperties();
    properties.setProperty(CourseManagementServiceImpl.DATA_MOVER_FAKE_KEY, Boolean.toString(fakeMode));
    service.updatedConfiguration(properties);
    service.setSalesforceConnectorService(salesforce);
    return service;
  }

}
