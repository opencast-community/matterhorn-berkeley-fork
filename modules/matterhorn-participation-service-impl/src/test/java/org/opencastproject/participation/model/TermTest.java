/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

/**
 * @author John Crossman
 */
public class TermTest {

  @Test
  public void testGetTermYearAndCode() {
    final Term termId2168 = Term.construct("2168", null, null);
    assertEquals(Semester.Fall, termId2168.getSemester());
    assertEquals(2016, termId2168.getYear().intValue());

    final Term termId2162 = Term.construct("2162", null, null);
    assertEquals(Semester.Spring, termId2162.getSemester());
    assertEquals(2016, termId2162.getYear().intValue());
  }

  @Test
  public void testNotEqualsOnSemester() {
    final Term term1 = Term.construct(Semester.Fall, 2013, "start", "end", "1");
    final Term term2 = Term.construct(Semester.Spring, 2013, "start", "end", "1");
    assertFalse(term1.equals(term2));
    //
    final Set<Term> set = new HashSet<Term>();
    set.add(term1);
    set.add(term2);
    assertEquals(2, set.size());
  }

  @Test
  public void testNotEqualsOnYear() {
    final Term term1 = Term.construct(Semester.Spring, 2013, "start", "end", "1");
    final Term term2 = Term.construct(Semester.Spring, 2014, "start", "end", "1");
    assertFalse(term1.equals(term2));
    //
    final Set<Term> set = new HashSet<Term>();
    set.add(term1);
    set.add(term2);
    assertEquals(2, set.size());
  }

  @Test
  public void testEquals() {
    final Term term1 = Term.construct(Semester.Spring, 2014, "start1", "end1", "1");
    final Term term2 = Term.construct(Semester.Spring, 2014, "start2", "end2", "2");
    assertEquals(term1, term2);
    //
    final Set<Term> set = new HashSet<Term>();
    set.add(term1);
    set.add(term2);
    assertEquals(1, set.size());
  }
}
