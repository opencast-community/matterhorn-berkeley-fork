/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.impl.persistence;

import org.apache.commons.lang.StringUtils;
import org.junit.Ignore;
import org.junit.Test;
import org.opencastproject.participation.CourseUtils;
import org.opencastproject.participation.UnitTestUtils;
import org.opencastproject.participation.impl.VoidNotifier;
import org.opencastproject.participation.model.Booking;
import org.opencastproject.participation.model.CanonicalCourse;
import org.opencastproject.participation.model.CourseData;
import org.opencastproject.participation.model.CourseKey;
import org.opencastproject.participation.model.DayOfWeek;
import org.opencastproject.participation.model.Instructor;
import org.opencastproject.participation.model.Participation;
import org.opencastproject.participation.model.Room;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.opencastproject.participation.impl.persistence.UCBerkeleyCourseDatabaseImpl.translateToCrossListingDisplayName;

/**
 * @author John Crossman
 */
public class UCBerkeleyCourseDatabaseImplTest {


  @Test
  public void testPrepareForSqlLike() {
    assertNull(translateToCrossListingDisplayName(null));
    assertNull(translateToCrossListingDisplayName("  "));
    String expectedLS = "L%S%C123";
    assertEquals(expectedLS, translateToCrossListingDisplayName(" L & S C123 "));
    assertEquals(expectedLS, translateToCrossListingDisplayName("LS    C123"));
    assertEquals("S%ASIAN%123", translateToCrossListingDisplayName("SASIAN 123"));
    assertEquals("S%SEASN%123", translateToCrossListingDisplayName("SSEASN 123"));
  }

  @Test
  @Ignore
  public void testGetCourse() throws IOException {
    UCBerkeleyCourseDatabaseImpl courseDatabase = new UCBerkeleyCourseDatabaseImpl(UnitTestUtils.getTestCatalogCredentials(), new VoidNotifier());
    final CourseKey courseKey = CourseUtils.getCourseKey("2016D13572");
    final CourseData course = courseDatabase.getCourse(courseKey);
    //
    assertNotNull(course);
    assertEquals("BIOLOGY", course.getDeptName());
    final Set<Participation> set = course.getParticipationSet();
    assertEquals(4, set.size());
    for (final Participation p : set) {
      System.out.println(p.getInstructor());
    }
  }

  @Test
  @Ignore
  public void testGetCourseOfferings() throws IOException {
    UCBerkeleyCourseDatabaseImpl courseDatabase = new UCBerkeleyCourseDatabaseImpl(UnitTestUtils.getTestCatalogCredentials(), new VoidNotifier());
    final Set<CourseData> courseDataSet = courseDatabase.getCourseData();
    assertTrue(courseDataSet.size() > 0);
    //
    final Set<Integer> ccnNumbersFound = new HashSet<Integer>();
    for (final CourseData next : courseDataSet) {
      final CanonicalCourse canonicalCourse = next.getCanonicalCourse();
      assertNotNull(canonicalCourse);
      final Integer ccn = canonicalCourse.getCcn();
      assertNotNull(ccn);
      assertTrue("list.add() returned false which means duplicate ccn found.", ccnNumbersFound.add(ccn));
      assertNotNull(canonicalCourse.getTitle());
      //
      assertNotNull(next.getStartTime());
      assertNotNull(next.getEndTime());
      assertNotNull(next.getTerm().getSemester());
      assertNotNull(next.getTerm().getYear());
      final Room room = next.getRoom();
      assertNotNull(room);
      assertNotNull(StringUtils.trimToNull(room.getBuilding()));
      //
      final List<DayOfWeek> meetingDays = next.getMeetingDays();
      assertNotNull(meetingDays);
      assertFalse(meetingDays.isEmpty());
      assertNotNull(next.getSection());
      assertNotNull(next.getStudentCount());
      final Set<Participation> set = next.getParticipationSet();
      if (set != null && !set.isEmpty()) {
        for (final Participation p : set) {
          final Instructor instructor = p.getInstructor();
          assertNotNull(instructor.getCalNetUID());
          if (instructor.getEmail() == null) {
            System.out.println("Null email for " + instructor);
          }
          assertNotNull(instructor.getLastName());
        }
      }
    }
    assertEquals(ccnNumbersFound.size(), courseDataSet.size());
    assertTrue(ccnNumbersFound.contains(42424));

    final List<Booking> crossListedBookings = courseDatabase.getCrossListedCourses(courseDataSet);
    for (final Booking booking : crossListedBookings) {
      final Set<CourseData> crossListedCourses = booking.getCrossListedCourses();
      for (final CourseData courseData : crossListedCourses) {
        final String seriesId = CourseUtils.getSeriesId(courseData);
        if ("2014D42424".equals(seriesId) || "2014D29212".equals(seriesId)) {
          System.out.println(seriesId + " has " + crossListedCourses.size() + " cross-listings: ");
          for (final CourseData next : crossListedCourses) {
            System.out.print(CourseUtils.getSeriesId(next) + " " + next.getDeptName() + ' ' + next.getCatalogId() + ", ");
          }
          System.out.println();
          break;
        }
      }
    }

  }

}
