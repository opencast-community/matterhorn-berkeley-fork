/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.model;

import org.junit.Test;
import org.opencastproject.util.data.Collections;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

/**
 * @author John Crossman
 */
public class CourseKeyTest {

  @Test
  public void testEqualsCourseData() {
    final HasCourseKey c1 = new CourseKey(2012, Semester.Fall, 1203);
    final HasCourseKey c2 = getCourseData(2012, Semester.Fall, 1203, "001", "Introduction to Environmental Economics and Policy", "C1", "ENVECON", "Environmental Economics and Policy");
    assertEquals(c1, c2);
  }

  @Test
  public void testNotEqualsYear() {
    assertFalse(new CourseKey(2012, Semester.Fall, 1203).equals(new CourseKey(2013, Semester.Fall, 1203)));
  }

  @Test
  public void testNotEqualsSemester() {
    assertFalse(new CourseKey(2012, Semester.Spring, 1203).equals(new CourseKey(2012, Semester.Fall, 1203)));
  }

  @Test
  public void testNotEqualsCCN() {
    assertFalse(new CourseKey(2012, Semester.Fall, 1203).equals(new CourseKey(2012, Semester.Fall, 9999)));
  }

  private static CourseData getCourseData(final int year, final Semester semester, final int ccn, final String section,
          final String title, final String catalogId, final String deptName, final String deptDescription) {
    final CourseData courseData = new CourseData();
    courseData.setCanonicalCourse(new CanonicalCourse(ccn, null, title));
    courseData.setTerm(Term.construct(semester, year, null, null, null));
    courseData.setCatalogId(catalogId);
    courseData.setDeptName(deptName);
    courseData.setDeptDescription(deptDescription);
    courseData.setMeetingDays(Collections.list(DayOfWeek.Monday, DayOfWeek.Wednesday, DayOfWeek.Friday));
    courseData.setParticipationSet(Collections
            .set(new Participation(null, new Instructor(null, "calNetUID", null, "First name", "Last name", null, null), false)));
    courseData.setRoom(new Room(null, "STANLEY", "0106", RoomCapability.screencast));
    courseData.setSection(section);
    return courseData;
  }

}
