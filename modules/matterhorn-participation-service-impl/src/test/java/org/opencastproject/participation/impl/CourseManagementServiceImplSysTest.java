/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.impl;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.opencastproject.participation.UnitTestUtils;
import org.opencastproject.participation.model.CanonicalCourse;
import org.opencastproject.participation.model.CapturePreferences;
import org.opencastproject.participation.model.CourseData;
import org.opencastproject.participation.model.CourseOffering;
import org.opencastproject.participation.model.DayOfWeek;
import org.opencastproject.participation.model.Instructor;
import org.opencastproject.participation.model.Participation;
import org.opencastproject.participation.model.RecordingAvailability;
import org.opencastproject.participation.model.RecordingType;
import org.opencastproject.participation.model.Room;
import org.opencastproject.participation.model.RoomCapability;
import org.opencastproject.participation.model.Semester;
import org.opencastproject.participation.model.Term;
import org.opencastproject.salesforce.SalesforceUtils;
import org.opencastproject.security.api.DefaultOrganization;
import org.opencastproject.security.api.User;
import org.opencastproject.util.EqualsUtil;
import org.opencastproject.util.NotFoundException;
import org.opencastproject.util.data.Collections;
import org.osgi.service.cm.ConfigurationException;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNull;
import static junit.framework.Assert.fail;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

/**
 * @author John Crossman
 */
@Ignore
public class CourseManagementServiceImplSysTest {

  private static final String COURSE_SF_GIANTS_BASEBALL_101 = "2013D54321";
  private static final String INSTRUCTOR_BUSTER_POSEY = "212386";
  private static final String INSTRUCTOR_TIM_LINCECUM = "322588";

  private static final String COURSE_CHEMICAL_STRUCTURE_AND_REACTIVITY = "2013D11315";

  private static final String ADMIN_USER= "271592";

  private CourseManagementServiceImpl courseManagementService;

  @Before
  public void before() throws ConfigurationException, NotFoundException, IOException {
    courseManagementService = new CourseManagementServiceImpl();
    courseManagementService.updatedConfiguration(UnitTestUtils.getSalesforceConnectionProperties());
  }

  @Test
  public void testCreateOrUpdateCourses() throws NotFoundException {
    final Instructor i = new Instructor();
    i.setCalNetUID("973108");
    i.setFirstName("Lindsay");
    i.setLastName("Sperling");
    i.setEmail("ljsperling@berkeley.edu");
    //
    final Participation p = new Participation();
    p.setInstructor(i);
    //
    final CanonicalCourse cc = new CanonicalCourse(11121, "Chemistry 1C", "General Chemistry Laboratory");
    //
    final Term t = Term.construct(Semester.Spring, 2013, "2013-01-22", "2013-05-10", null);
    //
    final Room r = new Room();
    r.setBuilding("Pimentel");
    r.setRoomNumber("1");
    //
    final CourseData c = new CourseData();
    c.setParticipationSet(Collections.set(p));
    c.setCanonicalCourse(cc);
    c.setTerm(t);
    c.setStudentCount(274);
    c.setRoom(r);
    c.setSection("002");
    c.setMeetingDays(Collections.list(DayOfWeek.Wednesday));
    c.setStartTime("1800");
    c.setEndTime("1900");
    //
    courseManagementService.createOrUpdateCourses(Collections.set(c));
  }

  @Test
  public void testGetCourseOfferingNull() throws NotFoundException {
    assertNull(courseManagementService.getCourseOffering("XXX"));
  }

  @Test
  public void testGetCourseOffering() throws NotFoundException {
    final CourseOffering offering = courseManagementService.getCourseOffering(COURSE_SF_GIANTS_BASEBALL_101);
    assertNotNull(offering.getSalesforceID());
    assertEquals(COURSE_SF_GIANTS_BASEBALL_101, offering.getCourseOfferingId());
    final CanonicalCourse canonicalCourse = offering.getCanonicalCourse();
    final Integer ccn = canonicalCourse.getCcn();
    assertNotNull(ccn);
    assertEquals(54321, ccn.intValue());
    assertEquals("SF Giants Baseball 101", canonicalCourse.getTitle());
    // Course Offering
    assertEquals("001", offering.getSection());
    final Term term = offering.getTerm();
    final Integer termYear = term.getYear();
    assertNotNull(termYear);
    assertEquals(2013, termYear.intValue());
    assertEquals(Semester.Fall, term.getSemester());
    assertEquals("1000", offering.getStartTime());
    assertEquals("1100", offering.getEndTime());
    final String startDate = term.getStartDate();
    final String endDate = term.getEndDate();
    final SimpleDateFormat expectedFormat = SalesforceUtils.getDateFormatConvention();
    try {
      expectedFormat.parse(startDate);
      expectedFormat.parse(endDate);
    } catch (final ParseException e) {
      e.printStackTrace();
      fail(expectedFormat.toPattern() + " expected but semester [start|end] date format is wrong: " + startDate + "; " + endDate);
    }
    //
    final List<DayOfWeek> expected = toList(DayOfWeek.Tuesday, DayOfWeek.Thursday);
    assertTrue(EqualsUtil.eqListUnsorted(expected, offering.getMeetingDays()));
    // Room
    final Room room = offering.getRoom();
    assertEquals("1", room.getRoomNumber());
    assertEquals("Pimentel", room.getBuilding());
    assertEquals(RoomCapability.screencastAndVideo, room.getCapability());
    // Participation
    final CapturePreferences capturePreferences = offering.getCapturePreferences();
    assertNotNull(capturePreferences.getRecordingAvailability());
    assertNotNull(capturePreferences.getDelayPublishByDays());
    // Instructor
    final List<Participation> participationList = new LinkedList<Participation>(offering.getParticipationSet());
    assertNotNull(offering.getStageOfApproval());
    {
      final Participation participation = participationList.get(0);
      final Instructor instructor = participation.getInstructor();
      assertEquals("Buster", instructor.getFirstName());
      assertEquals("Posey", instructor.getLastName());
      assertEquals(INSTRUCTOR_BUSTER_POSEY, instructor.getCalNetUID());
      assertNull(participation.getSalesforceField());
      assertTrue(participation.isApproved());
    }
    {
      final Participation participation = participationList.get(1);
      final Instructor instructor = participation.getInstructor();
      assertEquals("Tim", instructor.getFirstName());
      assertEquals("Lincecum", instructor.getLastName());
      assertEquals(INSTRUCTOR_TIM_LINCECUM, instructor.getCalNetUID());
      assertEquals(1, participation.getSalesforceField().getIdentifier());
    }
  }

  @Test
  public void testUpdateParticipation() throws NotFoundException {
    final CourseOffering offering1 = courseManagementService.getCourseOffering(COURSE_CHEMICAL_STRUCTURE_AND_REACTIVITY);
    final CapturePreferences newPreferences = getNotEqual(offering1.getCapturePreferences());
    courseManagementService.updateCapturePreferences(offering1.getCourseOfferingId(), new User(), newPreferences);
    final CourseOffering offering2 = courseManagementService.getCourseOffering(COURSE_CHEMICAL_STRUCTURE_AND_REACTIVITY);
    // assertEquals(updateResult, courseManagementService.allInstructorsApprove(offering2.getParticipationSet()));
    assertEquals(newPreferences, offering2.getCapturePreferences());
  }

  @Test
  public void testVerifyInstructors() throws NotFoundException {
    final CourseOffering offering = courseManagementService.getCourseOffering(COURSE_CHEMICAL_STRUCTURE_AND_REACTIVITY);
    final Set<Participation> participationSet = offering.getParticipationSet();
    assertEquals(2, participationSet.size());
    for (final Participation p : participationSet) {
      final Instructor instructor = p.getInstructor();
      assertNotNull(instructor);
      assertNotNull(instructor.getCalNetUID());
//      assertNotNull(instructor.getEmail());
    }
  }

  @Test
  public void testUpdateParticipationByAdmin() throws NotFoundException {
    final CourseOffering offering1 = courseManagementService.getCourseOffering(COURSE_CHEMICAL_STRUCTURE_AND_REACTIVITY);
    String adminScheduleOverride1 = offering1.getAdminScheduleOverride();
    final CapturePreferences newPreferences = getNotEqual(offering1.getCapturePreferences());
    final User adminUser = new User(ADMIN_USER, DefaultOrganization.DEFAULT_ORGANIZATION_NAME, new String[] {DefaultOrganization.DEFAULT_ORGANIZATION_ADMIN});
    courseManagementService.updateCapturePreferences(offering1.getCourseOfferingId(), adminUser, newPreferences);
    final CourseOffering offering2 = courseManagementService.getCourseOffering(COURSE_CHEMICAL_STRUCTURE_AND_REACTIVITY);
    assertEquals(newPreferences, offering2.getCapturePreferences());
    assertEquals(ADMIN_USER, offering2.getAdminScheduleOverride());
    // reset the admin override for repeated testing
    courseManagementService.updateCapturePreferences(offering1.getCourseOfferingId(), adminUser, newPreferences);
    final CourseOffering offering3 = courseManagementService.getCourseOffering(COURSE_CHEMICAL_STRUCTURE_AND_REACTIVITY);
    assertEquals(adminScheduleOverride1, offering3.getAdminScheduleOverride());
  }

  @Ignore
  @Test
  public void testSetRecordingsScheduledTrue() throws NotFoundException {
    final String offeringId = COURSE_SF_GIANTS_BASEBALL_101;
    courseManagementService.setRecordingsScheduledTrue(offeringId);
    final CourseOffering offering = courseManagementService.getCourseOffering(offeringId);
    assertTrue(offering.isScheduled());
  }

  @Test
  public void testGetAllTermsFromSalesforceMap() throws NotFoundException {
    final Set<Term> fromSalesforceSet = courseManagementService.getAllTerms();
    final Map<Integer, List<Semester>> distinctEntries = new HashMap<Integer, List<Semester>>();
    final List<String> salesforceIdList = new LinkedList<String>();
    for (final Term fromSalesforce : fromSalesforceSet) {
      final String salesforceID = fromSalesforce.getSalesforceID();
      assertNotNull(salesforceID);
      assertFalse(salesforceIdList.contains(salesforceID));
      salesforceIdList.add(salesforceID);
      //
      final Integer termYear = fromSalesforce.getYear();
      final List<Semester> listFromMap = distinctEntries.get(termYear);
      if (listFromMap == null) {
        distinctEntries.put(termYear, new LinkedList<Semester>());
      } else {
        final Semester semester = fromSalesforce.getSemester();
        assertFalse(listFromMap.contains(semester));
        listFromMap.add(semester);
      }
    }
  }

  @Test
  public void testGetAllCaptureEnabledRooms() throws NotFoundException {
	  final Set<Room> roomSet = courseManagementService.getAllRooms();
    assertTrue(roomSet.size() > 0);
    boolean foundRoom = false;
    for (final Room room : roomSet) {
      assertNotNull(room.getSalesforceID());
      assertNotNull(room.getBuilding());
      assertNotNull(room.getCapability());
      assertNotNull(room.getRoomNumber());
      if ("Pimentel".equals(room.getBuilding()) && "1".equals(room.getRoomNumber())) {
        foundRoom = true;
        assertSame(RoomCapability.screencastAndVideo, room.getCapability());
      }
    }
    assertTrue(foundRoom);
  }

//  @Ignore
//  @Test
//  public void testGetCourseCaptureApprovalStatus() throws NotFoundException {
//    assertEquals(StageOfApproval.partiallyApproved, courseManagementService.getCourseCaptureApprovalStatus(COURSE_SF_GIANTS_BASEBALL_101));
//    assertEquals(StageOfApproval.approved, courseManagementService.getCourseCaptureApprovalStatus(COURSE_BIOLOGY_1A));
//  }

//  @Test
//  public void testGetCourseOfferingsBySemesterYear() throws NotFoundException {
//    final Set<CourseOffering> list = courseManagementService.getCourseOfferingsBySemesterYear(Semester.Fall, 2013);
//    assertTrue(list.size() >= 2);
//  }

//  private Instructor createInstructor() {
//    final Instructor instructor = new Instructor();
//    instructor.setCalNetUID(new Date().getTime() * Math.random() + "");
//    return instructor;
//  }

  private CapturePreferences getNotEqual(final CapturePreferences c) {
    final CapturePreferences notEqual = new CapturePreferences();
    for (final RecordingType next : RecordingType.values()) {
      if (!next.equals(c.getRecordingType())) {
        notEqual.setRecordingType(next);
        }
    }

    for (final RecordingAvailability next : RecordingAvailability.values()) {
      if (!next.equals(c.getRecordingAvailability())) {
        notEqual.setRecordingAvailability(next);
      }
    }
    return notEqual;
  }

  private <T> List<T> toList(T... arry) {
    if (arry == null) {
      return null;
    }
    final List<T> list = new LinkedList<T>();
    java.util.Collections.addAll(list, arry);
    return list;
  }

}
