/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.impl.persistence;

import static junit.framework.Assert.assertEquals;
import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.replay;
import static org.junit.Assert.assertNull;

import org.opencastproject.participation.model.CanonicalCourse;
import org.opencastproject.participation.model.CourseData;
import org.opencastproject.participation.model.DayOfWeek;
import org.opencastproject.participation.model.Room;
import org.opencastproject.participation.model.Semester;
import org.opencastproject.util.date.TimeOfDay;

import org.junit.Assert;
import org.junit.Test;

import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;

/**
 * @author John Crossman
 */
public class ResultConverterForCourseTest extends AbstractResultConverterTest {

  private final String displayName = "BIOLOGY 1A";
  private final String instructionFormat = "LEC";
  private final String sectionNum = "001";

  @Test
  public void testRoomNumberParsing() throws Exception {
    final ResultSet r = createMockResultSet("Genetics & Plant Bio", "209", " M W F");
    final CourseData c = new ResultConverterForCourse(true).convert(r);
    assertEquals("GPB", c.getRoom().getBuilding());
    assertEquals("209", c.getRoom().getRoomNumber());
  }

  @Test
  public void testConvert() throws Exception {
    final ResultSet r = createMockResultSet("Hertz", "222", "MOWEFR");
    //
    final CourseData c = new ResultConverterForCourse(true).convert(r);
    final CanonicalCourse cc = c.getCanonicalCourse();
    assertEquals(123, cc.getCcn());
    assertEquals(displayName + ", " + instructionFormat + ' ' + sectionNum, cc.getName());
    assertEquals("0830", c.getStartTime());
    assertEquals("1100", c.getEndTime());
    assertEquals(Semester.Fall, c.getTerm().getSemester());
    assertEquals(2016, c.getTerm().getYear().intValue());
    final Room room = c.getRoom();
    assertEquals("Hertz", room.getBuilding());
    assertEquals("222", c.getRoom().getRoomNumber());
    final List<DayOfWeek> dayOfWeekList = new LinkedList<DayOfWeek>();
    dayOfWeekList.add(DayOfWeek.Monday);
    dayOfWeekList.add(DayOfWeek.Wednesday);
    dayOfWeekList.add(DayOfWeek.Friday);
    assertEquals(dayOfWeekList, c.getMeetingDays());
    assertEquals(sectionNum, c.getSection());
    assertEquals(25, c.getStudentCount().intValue());
  }

  @Test
  public void testConvertWheelerAuditorium() throws Exception {
    final ResultSet r = createMockResultSet(" Wheeler  ", "150", " M W F");
    final CourseData c = new ResultConverterForCourse(true).convert(r);
    assertEquals("Wheeler", c.getRoom().getBuilding());
    assertEquals("150", c.getRoom().getRoomNumber());
  }

  @Test
  public void testUnscheduledCourse() throws Exception {
    final ResultSet r = createMockResultSet(null, null, "");
    //
    final CourseData c = new ResultConverterForCourse(true).convert(r);
    assertNull(c.getRoom());
    assertNull(c.getMeetingDays());
  }

  @Test
  public void testConvertEdoDbPauleyBallroom() throws Exception {
    final CourseData c = new ResultConverterForCourse(true).convert(createMockResultSet("Pauley Ballroom", " ", ""));
    assertEquals("Pauley Ballroom", c.getRoom().getBuilding());
    assertNull(c.getRoom().getRoomNumber());
  }

  @Test
  public void testConvertEdoDbWheelerAud() throws Exception {
    final CourseData c = new ResultConverterForCourse(true).convert(createMockResultSet(" Wheeler ", " 150 ", ""));
    assertEquals("Wheeler", c.getRoom().getBuilding());
    assertEquals("150", c.getRoom().getRoomNumber());
  }

  @Test
  public void testGetTimeOfDay() {
    {
      final TimeOfDay timeOfDay = ResultConverterForCourse.getTimeOfDay("0830", "A");
      Assert.assertEquals(8, timeOfDay.getHour());
      Assert.assertEquals(30, timeOfDay.getMinutes());
      Assert.assertEquals(TimeOfDay.DayPeriod.AM, timeOfDay.getDayPeriod());
    }
    {
      final TimeOfDay timeOfDay = ResultConverterForCourse.getTimeOfDay("1100", "A");
      Assert.assertEquals(11, timeOfDay.getHour());
      Assert.assertEquals(0, timeOfDay.getMinutes());
      Assert.assertEquals(TimeOfDay.DayPeriod.AM, timeOfDay.getDayPeriod());
    }
    {
      final TimeOfDay timeOfDay = ResultConverterForCourse.getTimeOfDay("1100", "P");
      Assert.assertEquals(11, timeOfDay.getHour());
      Assert.assertEquals(0, timeOfDay.getMinutes());
      Assert.assertEquals(TimeOfDay.DayPeriod.PM, timeOfDay.getDayPeriod());
    }
    {
      final TimeOfDay timeOfDay = ResultConverterForCourse.getTimeOfDay("0500", "P");
      Assert.assertEquals(5, timeOfDay.getHour());
      Assert.assertEquals(0, timeOfDay.getMinutes());
      Assert.assertEquals(TimeOfDay.DayPeriod.PM, timeOfDay.getDayPeriod());
    }
  }

  private ResultSet createMockResultSet(final String buildingName, final String roomNumber, final String meetingDays) throws Exception {
    final ResultSet r = createMock(ResultSet.class);
    expectReturn(r, ColInt.course_cntl_num, 123);
    expectReturn(r, ColStr.dept_name);
    expectReturn(r, ColStr.dept_description);
    expectReturn(r, ColStr.course_title);
    expectReturn(r, ColStr.catalog_id);
    expectReturn(r, ColStr.display_name, displayName);
    expectReturn(r, ColStr.location, (buildingName == null) ? null : buildingName + (roomNumber == null ? "" : ' ' + roomNumber));
    expectReturn(r, ColStr.meeting_start_time, "0830");
    expectReturn(r, ColStr.meeting_start_time_ampm_flag, "A");
    expectReturn(r, ColStr.meeting_end_time, "1100");
    expectReturn(r, ColStr.meeting_end_time_ampm_flag, "A");
    expectReturn(r, ColStr.meeting_days, meetingDays);
    expectReturn(r, ColStr.instruction_format, instructionFormat);
    expectReturn(r, ColStr.section_num, sectionNum);
    expectReturn(r, ColInt.student_count, 25);
    replay(r);
    return r;
  }
}
